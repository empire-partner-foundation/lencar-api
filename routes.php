<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * Start Routing
 */

 //filedownloads
$f3->route('GET /downloads/@filename',
function($f3,$args) {
    // send() method returns FALSE if file doesn't exist
    if (!Web::instance()->send($args['filename']))
        // Generate an HTTP 404
    $f3->error(404);
}
);

$access->deny('/admin*');
$access->allow('/admin*', 1); // allow "admin" to access /secured.htm

$user = 1;//$f3->get('SESSION.USER.role');

$access->authorize($user, function ($route, $subject) {
    global $f3;
    $f3->set('SESSION.count', 4);
    $f3->set('SESSION.message', array('msg' => "You are not authorized to access $route", 'alert' => 'warning'));
    $f3->reroute("/");
});

/*
*   Auth Controller
*/
/*******************************************************************************************************/
$f3->route('GET /login', 'AuthController->loginPage');
$f3->route('POST /login', 'AuthController->loginPost');
$f3->route('POST /property_login', 'AuthController->property_login');
$f3->route('GET|POST /logout', 'AuthController->logout');

$f3->route('GET|POST /verify/@email/@verificationCode', 'AuthController->verifyPage');
$f3->route('GET|POST /verify/@email', 'AuthController->verifyPost');

$f3->route('GET /register', 'AuthController->registerPage');
$f3->route('POST /register', 'AuthController->registerPost');

$f3->route('GET|POST /agents', 'AuthController->agents');
$f3->route('POST /resendotp', 'AuthController->resendOTPPost');
$f3->route('GET|POST /activate/@code', 'AuthController->activate');

$f3->route('POST /reset/password', 'AuthController->passwordUpdatePost');
$f3->route('GET /password/reset', 'AuthController->passwordResetPage');
$f3->route('POST /password/reset', 'AuthController->passwordResetPost');
$f3->route('GET /password/reset/@email/@verificationCode/@resetStatus', 'AuthController->passwordResetChange');
$f3->route('GET|POST /forgotPassword', 'AuthController->forgot');

/*******************************************************************************************************/






/*
*   Tenant Controller
*/
/*******************************************************************************************************/
$f3->route('GET|POST /tenant/dashboard', 'TenantController->dashboard');
$f3->route('GET|POST /tenant/onboarding', 'TenantController->onboarding');

#Tenant Profile
$f3->route('GET|POST /tenant/profile', 'TenantController->profile');

#Tenant Application
$f3->route('GET|POST /tenant/application', 'TenantController->viewApplications');

#Tenant Training Forms (Additional Info)
$f3->route('GET|POST /tenant/additional-info', 'TenantController->viewRequests');
$f3->route('POST /additional-info', 'TenantController->formPost');
$f3->route('POST /dashboard/additional-info', 'TenantController->trainingInfo');

#Tenant Application Documents
$f3->route('GET|POST /tenant/documents', 'TenantController->viewDocuments');

#Tenant Application Applicants Credit Check Consent
$f3->route('GET|POST /consent/@id', 'TenantController->spouse_consent');
/*******************************************************************************************************/






/*
*   Communication Controller
*/
/*******************************************************************************************************/
$f3->route('GET|POST /tenant/messages', 'CommunicationController->tenant_communication_module');
$f3->route('GET|POST /tenant/messages/@toUserID', 'CommunicationController->tenant_communication_module');
$f3->route('GET|POST /admin/messages', 'CommunicationController->admin_communication_module');
$f3->route('GET|POST /admin/messages/@toUserID', 'CommunicationController->admin_communication_module');
/*******************************************************************************************************/









/*
*   Ajax Controller
*/
/*******************************************************************************************************/
$f3->route('GET|POST /messages/@toUserID', 'AjaxController->get_user_messages');
$f3->route('GET|POST /messages/unread', 'AjaxController->get_unread_messages');
$f3->route('GET|POST /read/messages', 'AjaxController->update_unread_messages');
$f3->route('GET|POST /create/message', 'AjaxController->create_message');

$f3->route('GET|POST /rental/property/fees', 'AjaxController->get_property_admin_fees');
$f3->route('GET|POST /agent/properties', 'AjaxController->get_agent_properties');
$f3->route('GET|POST /property/agents', 'AjaxController->get_property_agents');

$f3->route('GET|POST /tenant/applications/unit/@applicationID/@userID/@action', 'AjaxController->updateApplication');
$f3->route('GET|POST /tenant/form/fields', 'AjaxController->form_fields');
$f3->route('GET|POST /tenant/field/options', 'AjaxController->field_options');

$f3->route('GET|POST /formsave/@formtype', 'AjaxController->formsave');
$f3->route('GET|POST /admin/form/state', 'AjaxController->field_state');
$f3->route('GET|POST /form/create/', 'AjaxController->create_form');
$f3->route('GET|POST /form/update/', 'AjaxController->update_form');
$f3->route('GET|POST /adminformsave/@formtype', 'AjaxController->adminFormSave');

$f3->route('GET|POST /verifyForm/@formtype', 'AjaxController->verifyForm');
$f3->route('GET|POST /getApplicationVerification/@applicationID', 'AjaxController->getApplicationVerification');
$f3->route('GET|POST /approveApplication/@applicationID', 'AjaxController->approveApplication');


$f3->route('GET|POST /additional/property/charges/@propertyReferenceID', 'AjaxController->getpropertyReferenceCharges');
$f3->route('GET|POST /add/charge', 'AjaxController->addCharge');
$f3->route('GET|POST /admin/application/additional/charges', 'RentalPropertyController->additionalCharges');

$f3->route('GET|POST /@action', 'AjaxController->@action');

$f3->route('GET|POST /getProofOfPayment/@applicationID', 'AjaxController->getProofOfPayment');
$f3->route('GET|POST /getProofOfPayment/@applicationID/rental', 'AjaxController->getRentalProofOfPayment');

$f3->route('GET|POST /getComplexInfo/@complexID', 'AjaxController->getComplexInfo');
$f3->route('GET|POST /complex/available/units/@complexID/@applicationID', 'AjaxController->availableUnits');
$f3->route('GET|POST /complex/available/units/affordable/@complexID/@applicationID', 'AjaxController->affordableUnits');
$f3->route('GET|POST /complex/trench/units/@complexID/@applicationID/@trenchID', 'AjaxController->trenchUnits');
/*******************************************************************************************************/






/*
 * Admin
 */
/*******************************************************************************************************/
$f3->route('GET|POST /admin', 'AdminController->dashboard');
$f3->route('GET|POST /admin/xdslogs', 'AdminController->viewXdsLogs');
$f3->route('GET|POST /admin/backuplogs', 'AdminController->viewBackupLogs');
$f3->route('GET|POST /admin/agent/create', 'AdminController->createAgent');
$f3->route('GET|POST /admin/agent/delete/@agentID', 'AdminController->deleteAgent');
$f3->route('GET|POST /admin/agent/update/@id', 'AdminController->updateAgent');
$f3->route('GET|POST /admin/users', 'AdminController->viewUsers');
$f3->route('GET|POST /admin/application/notification/emails', 'AdminController->view_application_email_notifications_users_list');
$f3->route('GET|POST /admin/application/notification/email/create', 'AdminController->create_application_notification_email');
$f3->route('GET|POST /admin/application/notification/email/edit', 'AdminController->edit_application_notification_email');
$f3->route('GET|POST /admin/notification/emails/delete/@id', 'AdminController->delete_application_notification_email');

$f3->route('GET|POST /admin/users/create', 'AdminController->createUser');
$f3->route('GET|POST /admin/users/update/@id', 'AdminController->updateUser');
$f3->route('GET|POST /admin/users/delete/@id', 'AdminController->deleteUser');
$f3->route('GET|POST /admin/finance/paymentsettings', 'AdminController->paymentSettings');
$f3->route('GET|POST /admin/tenants', 'AdminController->viewTenants');
$f3->route('GET|POST /admin/applicants', 'AdminController->viewApplicants');
$f3->route('GET|POST /admin/applicant/update/@userID/@userState', 'AdminController->updateAppliantProfile');

$f3->route('GET|POST /admin/documents', 'AdminController->viewDocuments');
$f3->route('GET|POST /admin/documents/create', 'AdminController->createDocuments');
$f3->route('GET|POST /admin/documents/update/@id', 'AdminController->updateDocuments');
$f3->route('GET|POST /admin/documents/delete/@id', 'AdminController->deleteDocuments');

$f3->route('GET|POST /admin/forms', 'AdminController->viewForms');
$f3->route('GET|POST /admin/form/@id/results', 'AdminController->viewResults');
$f3->route('GET|POST /admin/form/update/@id', 'AdminController->updateForm');
$f3->route('GET|POST /admin/form/delete/@id', 'AdminController->deleteForm');
/*******************************************************************************************************/

/*
* Affordability Bucket Controller
*/
/*******************************************************************************************************/
$f3->route('GET|POST /admin/trench', 'AffordabilityBucketController->viewTrench');
$f3->route('GET|POST /admin/trench/create', 'AffordabilityBucketController->createTrench');
$f3->route('GET|POST /admin/trench/update/@id', 'AffordabilityBucketController->updateTrench');
$f3->route('GET|POST /admin/trench/delete/@id', 'AffordabilityBucketController->deleteTrench');
/*******************************************************************************************************/





/*
* Rental Property Controller
*/
/*******************************************************************************************************/
$f3->route('GET|POST /admin/units', 'RentalPropertyController->viewUnits');
$f3->route('GET|POST /admin/units/create', 'RentalPropertyController->createUnit');
$f3->route('GET|POST /admin/units/update/@id', 'RentalPropertyController->updateUnit');
$f3->route('GET|POST /admin/units/delete/@id', 'RentalPropertyController->deleteUnit');
$f3->route('GET|POST /admin/units/upload/@fileName', 'RentalPropertyController->uploadDocuments');
$f3->route('GET|POST /admin/units/charges/@propertyReference', 'RentalPropertyController->viewUnitCharges');
$f3->route('GET|POST /admin/units/charges/@propertyReference/delete/@id', 'RentalPropertyController->deleteUnitCharges');
$f3->route('GET|POST /admin/property/upload', 'RentalPropertyController->uploadPropertyDocuments');
$f3->route('GET|POST /admin/application/additional/charge/update', 'RentalPropertyController->editUnitCharges');
/*******************************************************************************************************/






/*
* Applications Controller
*/
/*******************************************************************************************************/
#Fixed Charges
$f3->route('GET|POST /admin/charges', 'ApplicationsController->viewFixedCharges');
$f3->route('GET|POST /admin/charges/delete/@id', 'ApplicationsController->deleteFixedCharges');
$f3->route('GET|POST /admin/charge/update', 'ApplicationsController->editFixedCharges');
$f3->route('GET|POST /admin/create/charges', 'ApplicationsController->additionalFixedCharges');

#Applications
$f3->route('GET|POST /tenant/application/create', 'ApplicationsController->createApplication');

$f3->route('GET|POST /admin/applications', 'ApplicationsController->viewApplications');
$f3->route('GET|POST /admin/application/status/@status/@id', 'ApplicationsController->updateApplicationStatus');
$f3->route('GET|POST /admin/application/delete/@id', 'ApplicationsController->deleteApplication');
$f3->route('GET|POST /admin/application/view/@id', 'ApplicationsController->viewApplication');
$f3->route('GET|POST /admin/application/edit/@id', 'ApplicationsController->editApplication');
$f3->route('GET|POST /admin/application/approve', 'ApplicationsController->approveApplication');
$f3->route('GET|POST /admin/application/verifyProofOfPayment', 'ApplicationsController->verifyProofOfPayment');
$f3->route('GET|POST /admin/application/invite', 'ApplicationsController->inviteTenant');
$f3->route('GET|POST /admin/application/assign', 'ApplicationsController->assignUnit');
$f3->route('GET|POST /admin/application/upgrade/@applicationID/@userID', 'ApplicationsController->upgradeApplication');
$f3->route('GET|POST /admin/application/verifyProofOfPayment/deposit', 'ApplicationsController->verifyDepositProofOfPayment');
$f3->route('GET|POST /admin/share/application', 'ApplicationsController->share_application');
$f3->route('GET|POST /admin/application/verification/@id', 'ApplicationsController->re_run_application_verification');
$f3->route('GET|POST /generate/creditreport/@id/@idnumber/@userID/@apiID/@adminID', 'ApplicationsController->generateCreditReport');
$f3->route('GET|POST /banking/details/@id/@userID/@apiID/@adminID', 'ApplicationsController->verifyBankingDetails');
/*******************************************************************************************************/






/*
* Property Owner Controller
*/
/*******************************************************************************************************/
$f3->route('GET|POST /application/@applicationID/@action', 'PropertyOwnerController->property_owner_response');
$f3->route('GET|POST /admin/property_owner', 'PropertyOwnerController->property_owner');
/*******************************************************************************************************/






$f3->route('GET|POST /admin/checklist', 'TenantPortalController->viewChecklist');






$f3->route('GET|POST /admin/finance', 'TenantPortalController->viewFinance');
$f3->route('GET|POST /admin/invoices', 'TenantPortalController->viewInvoices');
$f3->route('GET|POST /view/invoice/@id', 'TenantPortalController->viewInvoice');
$f3->route('GET|POST /admin/invoice/delete/@id', 'TenantPortalController->deleteInvoice');






$f3->route('GET|POST /admin/groups', 'GroupsController->index');
$f3->route('GET|POST /admin/groups/@action', 'GroupsController->@action');
$f3->route('GET|POST /admin/groups/@action/@id', 'GroupsController->@action');









////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


$f3->route('GET|POST /listeningendpoint/documentsocr', 'UsersController->docs_validation');
$f3->route('GET|POST /verifyIDDocument/@id', 'UsersController->verifyIDDocument');




$f3->route('GET|POST /admin/tenant/@id/lease', 'TenantPortalController->viewLeaseDocuments');



$f3->route('GET|POST /user/invoices', 'UsersController->invoices');

/*
 * Reports
 */
$f3->route('GET|POST /reports', 'ReportsController->index');
$f3->route('GET|POST /reports/monthly/@section/@year/@month', 'ReportsController->monthly_report');
$f3->route('GET|POST /reports/yearly/@section/@year', 'ReportsController->yearly_report');

//message set to empty
if ($f3->get('SESSION.count') == 0) {
    $f3->set('SESSION.count', 0);
    $f3->set('SESSION.message', array('msg' => '', 'alert' => ''));
}

if ($f3->get('SESSION.count') > 0) {
    $curr_sess = 0;
    $curr_sess = (integer) $f3->get('SESSION.count');
    $curr_sess = $curr_sess - 1;

    $f3->set('SESSION.count', $curr_sess);
}
$f3->set('SESSION.count', 0);

$f3->set('SESSION.LAST_ACTIVITY', time());
