<?php

/**
 * @author Nimrod
 * @organization Smart-View Technology
 */
require_once 'PasswordGenerator.php';

class Auth
{

    public static function createSession($f3, $user)
    {
        try {
            global $db;
            // new Session();

            $f3->set('SESSION.USER.id', $user['id']);
            $f3->set('SESSION.USER.name', $user['firstName']);
            $f3->set('SESSION.USER.surname', $user['lastName']);
            $f3->set('SESSION.USER.email', $user['email']);
            $f3->set('SESSION.USER.role', $user['pmLevel']);
            $f3->set('SESSION.LAST_ACTIVITY', time());

            return true;
        } catch (Exception $exception) {
            throw new Exception($exception->getMessage());
        }
    }

    public static function user($f3)
    {

        if (!$f3->get('SESSION.USER.id')) {
            return false;
        }

        return [
            'usr_id' => $f3->get('SESSION.USER.id'),
            'usr_firstname' => $f3->get('SESSION.USER.name'),
            'usr_surname' => $f3->get('SESSION.USER.surname'),
            'usr_email' => $f3->get('SESSION.USER.email'),
            'usr_role' => $f3->get('SESSION.USER.role'),
        ];
    }

    public static function destroySession($f3)
    {
        $f3->clear('SESSION');
    }

}
