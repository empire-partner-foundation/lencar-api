<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class CommunicationController extends AppController
{
    /*
    * Tenant Communication
    */
    public function tenant_communication_module($f3)
    {
        try {
            date_default_timezone_set('Africa/Johannesburg'); //or change to whatever timezone you want
            global $db;
            $this->authCheck($f3);
            $curlResponse = [];

            $administrators = [];
            $curlResponse['administrators'] = json_encode($administrators);

            $sql = "SELECT pa.*, us.id AS userID 
            FROM propertyagents pa 
            LEFT JOIN users us ON us.agentID = pa.id AND us.isDeleted = :isDeleted
            WHERE pa.isDeleted = :isDeleted AND us.agentID IS NOT NULL 
            GROUP BY pa.id";
            $vars = array(
                ':isDeleted' => 'n'
            );
            $property_agents = $db->exec($sql, $vars);
            $curlResponse['property_agents'] = json_encode($property_agents);

            echo json_encode($curlResponse); 
        } catch (Exception $exception) {
            return $exception->getMessage();
        }
    }

    /*
    * Admin Communication
    */
    public function admin_communication_module($f3)
    {
        try {
            date_default_timezone_set('Africa/Johannesburg'); //or change to whatever timezone you want
            global $db;
            $this->authCheck($f3);
            $curlResponse = [];

            $query = "SELECT * FROM users WHERE pmLevel= :pmLevel AND isDeleted= :isDeleted AND id <> :userID";
            $vars = array(
                ':pmLevel' => 1,
                ':isDeleted' => "n",
                ':userID' => $f3->get("POST.userID")
            );
            $administrators = $db->exec($query, $vars);
            $curlResponse['administrators'] = json_encode($administrators);
    
            $query = "SELECT distinct ap.userID, ap.dateCreated, ap.userID, ap.applicationStatus, ap.propertyOwnerApproval, ap.applicationFee, rt.title as rentTrench, ap.propertyReference, ad.applicants, ad.firstName, ad.lastName, ad.housing, vd.viewingDate, (SELECT COUNT(*) FROM applicantdata dd WHERE ad.userID = dd.userID AND dd.dataType = 'keySlip') AS keySlipUpload, (SELECT COUNT(*) FROM applicantdata dd WHERE ad.userID = dd.userID AND dd.dataType = 'leaseAgreement') AS leaseAgreementUpload, rp.id AS propertyReferenceID, rp.agentFullName, rp.rentDeposit, rp.lease, rp.ownerFullName, rp.ownerEmail, rp.ownerContactNumber, ad.partnerConsent
            FROM applications ap 
            LEFT JOIN applicantdetails ad ON ap.userID = ad.userID
            LEFT JOIN rentalproperties rp ON ad.id = rp.applicantID AND ap.id = rp.applicationID
            LEFT JOIN rentTrench rt ON rt.id = ad.rentTrench
            LEFT JOIN visits vd ON ap.id = vd.applicationID AND ap.userID = vd.userID AND rp.propertyReference = vd.propertyReference
            WHERE ap.isDeleted = :del GROUP BY ap.userID ORDER BY ap.dateCreated DESC";
            $vars = array(
                ':del' => "n",
            );
            $applicants = $db->exec($query, $vars);
            $curlResponse['applicants'] = json_encode($applicants);

            echo json_encode($curlResponse);
        } catch (Exception $exception) {
            return $exception->getMessage();
        }
    }
}