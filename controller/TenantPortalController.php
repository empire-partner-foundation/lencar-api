<?php

class TenantPortalController extends AppController
{

    public function viewLeaseDocuments($f3, $params)
    {
        global $db;
        $query = "SELECT * FROM applicantdata WHERE userID = :userID 
        AND dataLabel 
        IN('leaseAgreement','leaseAgreement', 'leaseAgreement1', 'leaseAgreement2', 'leaseAgreement3', 'leaseAgreement4', 'leaseAgreement5','leaseAgreement6', 'leaseAgreement7', 'leaseAgreement8', 'leaseAgreement9',
            'leaseAgreement10', 'leaseAgreement11', 'leaseAgreement12', 'leaseAgreement13', 'leaseAgreement14', 'leaseAgreement15', 'leaseAgreement16', 'leaseAgreement17', 'leaseAgreement18', 'leaseAgreement19'
        ) ORDER BY id ASC";
        $vars = array(
            ':userID' => $params['id'],
        );
        $documents = $db->exec($query, $vars);

        $newDocsArr = [];
        foreach ($documents as $document) {
            $title = preg_replace("([A-Z])", " $0", $document['dataLabel']);

            $newDocsArr[] = ["id" => $document['id'], "dataType" => $document['dataType'],
                "dataLabel" => $document['dataLabel'], "data" => $document['data'], "title" => $title, 'dateUpdated' => $document['dateUpdated']];
        }
        $curlResponse['documents'] = json_encode($newDocsArr);

        echo json_encode($curlResponse);
    }

    

    public function deleteInvoice($f3, $params)
    {
        global $db;
        $this->authCheck($f3);

        $attributes = ["isDeleted" => 'y'];
        $definition = $this->getDefinition($attributes);
        try {
            $idArray = ['id = ?', $params['id']];

            $user = $this->getRepository('invoices')->updateRecord($idArray, $definition);
            $f3->set("SESSION.count",1);
            $f3->set('SESSION.message', array('msg' => 'Invoice deleted successfully!', 'alert' => 'success'));

            $f3->reroute("/admin/invoices");
        } catch (Exception $exception) {
            echo json_encode($exception->getMessage());
        }

    }

    public function viewInvoice($f3, $params)
    {
        global $db;
        //$this->authCheck($f3);

        $query = "SELECT * FROM invoices WHERE id = :id ORDER BY id DESC";
        $vars = array(
            ':id' => $params['id'],
        );
        $myinvoices = $db->exec($query, $vars);

        $curlResponse['myinvoices'] = json_encode($myinvoices);

        $query = "SELECT ad.email, ad.firstName AS tenant_name, ad.lastName AS tenant_surname, ad.address AS tenant_address, ad.phone AS tenant_phone, 
        ad.totalIncome, uc.title, uc.dateCreated AS issue_date, inv.recipient AS company_name, inv.description AS invoice_description, 
        inv.paid AS amount_total, inv.id AS invoice_number, ap.unitID, ap.complexID , ap.id AS applicationID, ad.id AS applicantID, ap.propertyReference
        FROM applicantdetails ad 
        INNER JOIN userChecklist uc ON ad.userID = uc.userID AND uc.title = :title AND uc.isVerified = :isVerified 
        INNER JOIN invoices inv ON ad.userID = inv.userID AND inv.id = :invoiceID 
        INNER JOIN applications ap ON ad.userID = ap.userID 
        WHERE ad.userID = :userID 
        ORDER BY ad.id DESC";
        $vars = array(
            ':userID' => $myinvoices[0]['userID'],
            ':title' => 'rentDeposit',
            ':isVerified' => 'y',
            ':invoiceID' => $params['id']
        );
        $invoice = $db->exec($query, $vars);

        $curlResponse['invoice'] = json_encode($invoice);

        $query = "SELECT * FROM rentalproperties WHERE applicationID = :applicationID AND applicantID = :applicantID AND propertyReference = :propertyReference";
        $vars = array(
            ':applicationID' => $invoice[0]['applicationID'],
            ':applicantID' => $invoice[0]['applicantID'],
            ':propertyReference' => $invoice[0]['propertyReference'],
        );
        $rentalproperty = $db->exec($query, $vars);
        $curlResponse['rentalproperty'] = json_encode($rentalproperty);

        $query = "SELECT * FROM paymentsettings";
        $paymentsettings = $db->exec($query);
        $curlResponse['paymentsettings'] = json_encode($paymentsettings);

        $query = "SELECT * FROM rentTrench WHERE id = :trenchID";
        $vars = array(
            ':trenchID' => $allocatedUnit[0]['trenchID'],
        );
        $trench = $db->exec($query, $vars);
        $curlResponse['trench'] = json_encode($trench);

        $query = "SELECT * FROM charges WHERE propertyReference = :propertyReference AND isDeleted = :isDeleted";
        $vars = array(
            ':propertyReference' => $invoice[0]['propertyReference'],
            ':isDeleted' => 'n',
        );
        $additionChargeCosts = $db->exec($query, $vars);
        $curlResponse['additionChargeCosts'] = json_encode($additionChargeCosts);

        $query = "SELECT * FROM fixedCharges WHERE isDeleted = :isDeleted";
        $vars = array(
            ':isDeleted' => 'n',
        );
        $fixedCharges = $db->exec($query, $vars);
        $curlResponse['fixedCharges'] = json_encode($fixedCharges);

        $query = "SELECT * FROM complex WHERE id = :complexID";
        $vars = array(
            ':complexID' => $invoice[0]['complexID']
        );
        $complex = $db->exec($query, $vars);
        $curlResponse['complex'] = json_encode($complex);

        echo json_encode($curlResponse);
    }

    public function viewInvoices($f3)
    {
        global $db;
        $this->authCheck($f3);
        $curlResponse = [];

        $query = "SELECT * FROM invoices WHERE isDeleted = :isDeleted ORDER BY id DESC";
        $vars = array(
            ':isDeleted' => 'n'
        );
        $invoices = $db->exec($query, $vars);
        $curlResponse['invoices'] = json_encode($invoices);

        $query = "SELECT (SELECT COUNT(*) FROM invoices WHERE isDeleted = :isDeleted) AS totalInvoices, (SELECT COUNT(*) FROM invoices WHERE status = :paid AND isDeleted = :isDeleted) AS paidInvoices, (SELECT COUNT(*) FROM invoices WHERE status = :pending AND isDeleted = :isDeleted) AS pendingInvoices, (SELECT SUM(paid) FROM invoices WHERE status = :paid AND isDeleted = :isDeleted) AS totalIncome;";
        $vars = array(
            ':isDeleted' => 'n',
            ':paid' => 'paid',
            ':pending' => 'pending',
        );
        $totals = $db->exec($query, $vars);
        $curlResponse['totals'] = json_encode($totals);

        echo json_encode($curlResponse);
    }

    /*
     * Units Category/Complex
     */
    public function viewComplex($f3)
    {
        global $db;
        $curlResponse = [];
        $query = "SELECT * FROM complex WHERE deleted = :deleted";
        $vars = array(
            ':deleted' => "n",
        );
        $complexes = $db->exec($query, $vars);
        $f3->set('data', $complexes);

        $curlResponse['complex'] = json_encode($complexes);
        echo json_encode($curlResponse);
    }

    public function createComplex($f3)
    {
        $this->authCheck($f3);
        global $db;
        try {
            $f3->set('POST.dateCreated', date("Y-m-d"));
            $f3->set('POST.dateUpdated', date("Y-m-d"));
            $attributes = $f3->get('POST');

            if(isset($attributes['region']) && isset($attributes['complexName'])){
                $curlResponse['complex'] = "details";
                $definition = $this->getDefinition($attributes);
                $this->getRepository('complex')->createRecord($definition);
            } else {
                $mainID = $attributes['complexID'];
                $resultUrl = (array) json_decode($f3->get('POST.files'));
                $docsArr = (array) json_decode($f3->get('POST.docsArr'));

                $output = array();
                $count = 0;
                foreach ($resultUrl as $fileUrl => $bool) {
                    $update = false;
                    $formFieldName = $docsArr[$count];

                    $query = "INSERT INTO metaData (mainID, dataType, data, dataLabel, dateCreated, dateUpdated)
                            VALUES (:mainID, :dataType, :data, :dataLabel, NOW(), NOW())";
                    $vars = array(
                        ':mainID' => $mainID,
                        ':dataType' => "document",
                        ':data' => $fileUrl,
                        ':dataLabel' => $formFieldName,
                    );
                
                    $docsData = $db->exec($query, $vars);
                    $count++;
                }
            }
        } catch (Exception $exception) {
            echo json_encode($exception->getMessage());
        }
    }

    public function deleteComplex($f3, $params)
    {
        $this->authCheck($f3);
        $attributes = ["deleted" => 'y', "dateUpdated" => date("Y-m-d")];
        $definition = $this->getDefinition($attributes);
        try {
            $idArray = ['id = ?', $params['id']];
            $user = $this->getRepository('complex')->updateRecord($idArray, $definition);
            $f3->set('SESSION.message', array('msg' => 'Complex deleted successfully!', 'alert' => 'success'));
        } catch (Exception $exception) {
            echo json_encode($exception->getMessage());
        }

    }

    public function updateComplex($f3, $params)
    {
        $this->authCheck($f3);
        global $db;
        try {
            if ($f3->get('POST')) {
                $f3->set('POST.dateUpdated', date("Y-m-d"));
                $attributes = $f3->get('POST');
                $definition = $this->getDefinition($attributes);

                $idArray = ['id = ?', $params['id']];

                $user = $this->getRepository('complex')->updateRecord($idArray, $definition);

                //complex Documents
                if ($f3->get('POST.files')) {

                    $resultUrl = (array) json_decode($f3->get('POST.files'));
                    $docsArr = (array) json_decode($f3->get('POST.docsArr'));

                    $count = 0;
                    if (sizeof($resultUrl) > 0) {
                        foreach ($resultUrl as $fileUrl => $bool) {
                            $update = false;
                            $formFieldName = $docsArr[$count];

                            $query = "SELECT * FROM metaData WHERE mainID = :mainID";
                            $vars = array(
                                ':mainID' => $params['id'],
                            );
                            $complexDocuments = $db->exec($query, $vars);

                            foreach ($complexDocuments as $row) {
                                if ($row['dataLabel'] == $formFieldName) {
                                    $update = true;
                                }
                            }

                            if ($update) {
                                $query = "UPDATE metaData SET data=:data, dateUpdated = NOW() WHERE mainID = :mainID AND dataLabel=:dataLabel";
                                $vars = array(
                                    ':data' => $fileUrl,
                                    ':mainID' => $params['id'],
                                    ':dataLabel' => $formFieldName,
                                );
                                $docsData = $db->exec($query, $vars);
                            } else {
                                $query = "INSERT INTO metaData (mainID, datatype, data, dataLabel, dateCreated, dateUpdated)
                                        VALUES (:mainID, :datatype, :data, :dataLabel, NOW(), NOW())";
                                $vars = array(
                                    ':mainID' => $params['id'],
                                    ':datatype' => "document",
                                    ':data' => $fileUrl,
                                    ':dataLabel' => $formFieldName,
                                );
                                $docsData = $db->exec($query, $vars);
                            }
                            $count++;
                        }
                    }
                }
            }

            $query = "SELECT * FROM metaData WHERE mainID = :mainID";
            $vars = array(
                ':mainID' => $params['id'],
            );
            $Documents = $db->exec($query, $vars);
            $curlResponse['Documents'] = json_encode($Documents);


            $f3->set('complexDocuments', $Documents);

            //$complex = $this->getRepository('complex')->getByAttribute('id', $params['id']);
            $query = "SELECT * FROM complex WHERE id = :id";
            $vars = array(
                ':id' => $params['id'],
            );
            $complex = $db->exec($query, $vars);
            $f3->set('POST', $complex[0]);
            $curlResponse['complex'] = json_encode($complex);
            echo json_encode($curlResponse);

        } catch (Exception $exception) {
            echo json_encode($exception->getMessage());
        }
    }

}
