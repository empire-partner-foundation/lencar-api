<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class ApplicationsController extends AppController
{

    public function viewFixedCharges($f3, $params)
    {
        global $db;
        $curlResponse = [];
    
        $query = "SELECT * FROM fixedCharges WHERE isDeleted = :isDeleted";
        $vars = array(
            ':isDeleted' => 'n',
        );
        $charges = $db->exec($query, $vars);
        $curlResponse['charges'] = json_encode($charges);
        echo json_encode($curlResponse);
    }

    public function deleteFixedCharges($f3, $params)
    {
        global $db;
        $chargeID  = $params['id'];
        $query = "UPDATE fixedCharges SET isDeleted = :isDeleted, dateUpdated = NOW() WHERE id = :id;";
        $vars = array(
            ':isDeleted' => 'y',
            ':id' => (int) $chargeID,
        );
        $deletecharge = $db->exec($query, $vars);
    }

    public function editFixedCharges($f3)
    {
        global $db;
        $query = "UPDATE fixedCharges SET chargeName = :chargeName, chargeDescription = :chargeDescription, cost = :cost, dateUpdated = NOW() WHERE id = :id;";
        $vars = array(
            ':chargeName' => $f3->get('POST.chargeName'),
            ':chargeDescription' => $f3->get('POST.chargeDescription'),
            ':cost' => (float) $f3->get('POST.cost'),
            ':id' => (int) $f3->get('POST.chargeID'),
        );
        $editCharge = $db->exec($query, $vars);
    }

    public function additionalFixedCharges($f3)
    {
        global $db;
        $curlResponse = [];
        $query = "INSERT INTO fixedCharges (chargeName, chargeDescription, cost, dateCreated) 
                                VALUES (:chargeName, :chargeDescription, :cost, NOW())";
        $vars = array(
            ':chargeName' => $f3->get('POST.chargeName'),
            ':chargeDescription' =>  $f3->get('POST.chargeDescription'),
            ':cost' =>  (float) $f3->get('POST.cost'),
        );
        $charge = $db->exec($query, $vars);
    }

    public function viewApplications($f3)
    {
        global $db;
        $this->authCheck($f3);
        $curlResponse = [];

        if ($f3->get('POST.uploadSignedDocs')) {
            $folder = "user_" . $f3->get("POST.userID");
            $query = "SELECT dt.*, ad.applicationID AS current_applicationID  FROM applicantdetails ad 
            LEFT JOIN applicantdata dt ON ad.userID = dt.userID AND (ad.applicationID = dt.applicationID OR dt.applicationID IS NULL)
            WHERE ad.userID = :userID;";
            $vars = array(
                ':userID' => $f3->get("POST.userID"),
            );
            $userdata = $db->exec($query, $vars);
            $count = 0;
            $resultUrl = (array) json_decode($f3->get('POST.files'));
            $CheckList = (array) json_decode($f3->get('POST.docsArr'));
            foreach ($resultUrl as $fileUrl => $bool) {
                $update = false;
                $formFieldName = $CheckList[$count];

                foreach ($userdata as $row) {
                    if ($row['dataType'] == $formFieldName) {
                        $update = true;
                    }
                    $current_applicationID = $row['current_applicationID'];
                }
                if ($update) {
                    $query = "UPDATE applicantdata SET data=:data, dateUpdated = :dateUpdated
                    WHERE userID = :userID AND dataType = :dataType, AND applicationID = :applicationID";
                    $vars = array(
                        ':data' => $fileUrl,
                        ':userID' => $f3->get("POST.userID"),
                        ':dataType' => $formFieldName,
                        ':dateUpdated' => date("Y-m-d H:i:s"),
                        ':applicationID' => $current_applicationID
                    );
                    $applicantdata = $db->exec($query, $vars);
                } else {
                    $query = "INSERT INTO applicantdata (userID, data, dataType, dataLabel, applicationID, dateCreated, dateUpdated)
                                VALUES (:userID, :data, :dataType, :dataLabel, :applicationID, :dateCreated, :dateUpdated)";
                    $vars = array(
                        ':userID' => $f3->get("POST.userID"),
                        ':data' => $fileUrl,
                        ':dataType' => $formFieldName,
                        ':dataLabel' => $formFieldName,
                        ':applicationID' => $current_applicationID,
                        ':dateCreated' => date("Y-m-d H:i:s"),
                        ':dateUpdated' => date("Y-m-d H:i:s")
                    );
                    $applicantdata = $db->exec($query, $vars);
                }
                $count++;
            }

        }

        $sql = "SELECT * FROM propertyagents WHERE isDeleted = 'n';";
        $results = $db->exec($sql);
        $curlResponse['agents'] = json_encode($results);

        $query = "SELECT userType, agentID FROM users WHERE id = :userID AND isDeleted=:isDeleted";
        $vars = array(
            ':userID' => $f3->get("POST.mainUserID"),
            ':isDeleted' => 'n',
        );
        $user_profile = $db->exec($query, $vars);

        $filter_applications_by_agent = '';
        if($user_profile[0]['userType']=='3' && !empty($user_profile[0]['agentID'])){
            $filter_applications_by_agent = 'AND agentID = '.$user_profile[0]['agentID'];
        }

        $query = "SELECT COUNT(*) as totalUnits FROM rentalproperties WHERE deleted=:deleted $filter_applications_by_agent";
        $vars = array(
            ':deleted' => 'n',
        );
        $totalUnits = $db->exec($query, $vars);
        $curlResponse['totalUnits'] = $totalUnits[0]['totalUnits'];

        $query = "SELECT COUNT(*) as occupiedUnits FROM rentalproperties WHERE deleted=:deleted AND occupied=:occupied $filter_applications_by_agent";
        $vars = array(
            ':deleted' => 'n',
            ':occupied' => 'y',
        );
        $occupiedUnits = $db->exec($query, $vars);
        $curlResponse['occupiedUnits'] = $occupiedUnits[0]['occupiedUnits'];
        $curlResponse['availableUnits'] = $totalUnits[0]['totalUnits'] - $occupiedUnits[0]['occupiedUnits'];

        $query = "SELECT COUNT(*) as totalApplications  FROM applications WHERE isDeleted = :isDeleted $filter_applications_by_agent";
        $vars = array(
            ':isDeleted' => 'n',
        );
        $applicantdata = $db->exec($query, $vars);

        $f3->set('totalApplications', $applicantdata[0]['totalApplications']);
        $curlResponse['totalApplications'] = $applicantdata[0]['totalApplications'];

        $query = "SELECT COUNT(*) as approvedApplications  FROM applications WHERE isDeleted = :isDeleted AND applicationStatus = :status $filter_applications_by_agent";
        $vars = array(
            ':status' => "a",
            ':isDeleted' => 'n',
        );
        $applicantdata = $db->exec($query, $vars);
        $curlResponse['approvedApplications'] = $applicantdata[0]['approvedApplications'];

        $query = "SELECT COUNT(*) as declinedApplications  FROM applications WHERE isDeleted = :isDeleted AND applicationStatus = :status $filter_applications_by_agent";
        $vars = array(
            ':status' => "d",
            ':isDeleted' => 'n',
        );
        $applicantdata = $db->exec($query, $vars);
        $curlResponse['declinedApplications'] = $applicantdata[0]['declinedApplications'];

        $query = "SELECT COUNT(*) as pendingApplications  FROM applications WHERE isDeleted = :isDeleted AND applicationStatus = :status $filter_applications_by_agent";
        $vars = array(
            ':status' => "p",
            ':isDeleted' => 'n',
        );
        $applicantdata = $db->exec($query, $vars);
        $curlResponse['pendingApplications'] = $applicantdata[0]['pendingApplications'];

        $query = "SELECT ap.agentID, ap.id, ap.dateCreated, ap.userID, ap.applicationStatus, ap.propertyOwnerApproval, ap.applicationFee, rt.title as rentTrench, ap.propertyReference, ad.applicants, ad.firstName, ad.lastName, ad.housing, vd.viewingDate, 
        (SELECT COUNT(*) FROM applicantdata dd WHERE ad.userID = dd.userID AND dd.dataType = 'keySlip' AND dd.applicationID = ap.id) AS keySlipUpload, 
        (SELECT COUNT(*) FROM applicantdata dd WHERE ad.userID = dd.userID AND dd.dataType = 'leaseAgreement' AND dd.applicationID = ap.id) AS leaseAgreementUpload, 
        (SELECT dd.data FROM applicantdata dd WHERE ad.userID = dd.userID AND dd.dataType = 'leaseAgreement' AND dd.applicationID = ap.id LIMIT 1) AS leaseAgreementDocument, 
        (SELECT dd.data FROM applicantdata dd WHERE ad.userID = dd.userID AND dd.dataType = 'keySlip' AND dd.applicationID = ap.id LIMIT 1 ) AS keySlipDocument, 
        rp.id AS propertyReferenceID, CONCAT(pa.firstName, ' ', pa.lastName) AS agentFullName, rp.rentDeposit, rp.lease, rp.ownerFullName, rp.ownerEmail, rp.propertyDescription, rp.ownerContactNumber, ad.partnerConsent
        FROM applications ap 
        LEFT JOIN applicantdetails ad ON ap.userID = ad.userID
        LEFT JOIN rentalproperties rp ON ad.id = rp.applicantID AND ap.id = rp.applicationID
        LEFT JOIN propertyagents pa ON pa.id = rp.agentID
        LEFT JOIN rentTrench rt ON rt.id = ad.rentTrench
        LEFT JOIN visits vd ON ap.id = vd.applicationID AND ap.userID = vd.userID AND rp.propertyReference = vd.propertyReference
        WHERE ap.isDeleted = :del ORDER BY ap.dateCreated DESC";
        $vars = array(
            ':del' => "n",
        );
        $applicantdata = $db->exec($query, $vars);
        $curlResponse['applicantdata'] = json_encode($applicantdata);

        $query = "SELECT * FROM complex WHERE deleted = :deleted";
        $vars = array(
            ':deleted' => "n",
        );
        $complexes = $db->exec($query, $vars);
        $curlResponse['complexes'] = json_encode($complexes);
        echo json_encode($curlResponse);
    }

    public function createApplication($f3, $params)
    {
        try {
            global $db;
            $this->authCheck($f3);
            $query = "INSERT INTO applications (userID, complexID, agentID, dateCreated, dateUpdated) VALUES(:userID, :agentID, :complexID, NOW(), NOW()) ";
            $vars = array(
                ':userID' => $f3->get("POST.userID"),
                ':complexID' => $f3->get('POST.complexID'),
                ':agentID' => $f3->get('POST.agentID')
            );
            $userdata = $db->exec($query, $vars);
            $applicationID = $db->lastInsertId();

            $sql = "SELECT * FROM propertyagents where id = :agentID AND isDeleted = :isDeleted";
            $vars = array(
                ':agentID' => $f3->get('POST.agentID'),
                ':isDeleted' => 'n',
            );
            $property_agent = $db->exec($sql, $vars);

            $sql = "UPDATE `rentalproperties` SET applicationID = :applicationID, dateUpdated = :dateUpdated WHERE propertyReference = :propertyReference AND agentID = :agentID;";
            $vars1 = array(
                ':applicationID' => $applicationID,
                ':dateUpdated' => date("Y-m-d H:i:s"),
                ':agentID' => $f3->get('POST.agentID'),
                ':propertyReference' => $f3->get('POST.propertyReference'),
                
            );
            $update = $db->exec($sql, $vars1);

            $query = "SELECT *  from applicantdetails WHERE userID = :id";
            $vars = array(
                ':id' => $f3->get("POST.userID"),
            );
            $results = $db->exec($query, $vars);
            $params["IDNumber"] = $results[0]["idPassportNo"];
            $idVerificationResponse = $this->validateID($f3, $params);
            $idVerificationResponse = strtolower($idVerificationResponse);
            $verifiedData = [];
            $firstName = strtolower($results[0]["firstName"]);
            $lastName = strtolower($results[0]["lastName"]);
            $idPassportNo = strtolower($results[0]["idPassportNo"]);

            if (strpos($idVerificationResponse, $firstName) > -1) {
                array_push($verifiedData, "firstName");
            }

            if (strpos($idVerificationResponse, $lastName) > -1) {
                array_push($verifiedData, "lastName");
            }

            if (strpos($idVerificationResponse, $idPassportNo) > -1) {
                array_push($verifiedData, "idPassportNo");
                array_push($verifiedData, "dob");
            }

            $arrCheckList = [];
            $arrCheckList["personalInfo"] = ["firstName", "lastName", "idPassportNo", "dob", "address", "maritalStatus"];
            $arrCheckList["contactInfo"] = ["email", "phone", "altPhone"];
            $arrCheckList["employerInfo"] = ["occupation", "employer", "employerContact", "totalIncome", "totalExpenses"];
            $arrCheckList["emergencyInfo"] = ["nextOfKin", "nextOfKinContact"];
            $arrCheckList["documentInfo"] = ["idDocument", "payslip", "bankStatement"];
            $update = false;
            foreach ($verifiedData as $column) {
                $query = "SELECT * FROM userChecklist WHERE userID = :userID AND title = :title";
                $vars = array(
                    ':userID' => $f3->get("POST.userID"),
                    ':title' => $column,
                );
                $userdata = $db->exec($query, $vars);

                if (sizeof($userdata) > 0) {
                    $update = true;
                }
                if ($update) {
                    $query = "UPDATE userChecklist SET approvedBy=:adminID, isVerified=:isVerified WHERE id=:id";
                    $vars = array(
                        ':adminID' => $userID,
                        ':isVerified' => "y",
                        ':id' => $userdata[0]['id'],
                    );
                    $userChecklist = $db->exec($query, $vars);
                } else {
                    $query = "INSERT INTO userChecklist (userID, approvedBy, title, isVerified, dateCreated, dateUpdated)
                        VALUES (:userID, :adminID, :title, :isVerified, NOW(), NOW())";
                    $vars = array(
                        ':userID' => $f3->get("POST.userID"),
                        ':adminID' => $userID,
                        ':title' => $column,
                        ':isVerified' => "y",
                    );
                    $userChecklist = $db->exec($query, $vars);
                }
            }
            echo 'Application created successfully!';
        } catch (Exception $exception) {
            echo json_encode($exception->getMessage());
        }

    }

    public function updateApplicationStatus($f3, $params)
    {
        $this->authCheck($f3);
        $status = $params['status'];
        $attributes = ["applicationStatus" => $status, "dateUpdated" => date("Y-m-d")];
        $definition = $this->getDefinition($attributes);
        try {
            $idArray = ['id = ?', $params['id']];

            $user = $this->getRepository('applications')->updateRecord($idArray, $definition);
            $f3->set('SESSION.message', array('msg' => 'Application updated successfully!', 'alert' => 'success'));

            $curlResponse = array('msg' => 'Application updated successfully!', 'alert' => 'success');
            echo json_encode($curlResponse);

            //$f3->reroute("/admin/applications");
        } catch (Exception $exception) {
            echo json_encode($exception->getMessage());
        }

    }

    public function deleteApplication($f3, $params)
    {
        $this->authCheck($f3);
        $attributes = ["isDeleted" => 'y', "dateUpdate" => date("Y-m-d")];
        $definition = $this->getDefinition($attributes);
        try {
            $idArray = ['id = ?', $params['id']];
            $user = $this->getRepository('applications')->updateRecord($idArray, $definition);
            $curlResponse = array('msg' => 'Application deleted successfully!', 'alert' => 'success');
            echo json_encode($curlResponse);
        } catch (Exception $exception) {
            echo json_encode($exception->getMessage());
        }

    }

    public function editApplication($f3, $params)
    {
        global $db;
        $this->authCheck($f3);
        $curlResponse = [];

        $query = "SELECT dt.*, ad.id AS applicantID FROM applicantdetails ad LEFT JOIN applicantdata dt ON ad.userID = dt.userID WHERE ad.userID = :userID";
        $vars = array(
            ':userID' => $params['id'],
        );
        $userdata = $db->exec($query, $vars);

        $curlResponse['userdata'] = json_encode($userdata);

        if ($f3->get('POST.submission')) {
            $folder = "user_" . $params['id'];
            $attributes = $f3->get('POST');
            $docsArr = [];
            $resultUrl = (array) json_decode($f3->get('POST.files'));
            $docsArr = (array) json_decode($f3->get('POST.docsArr'));
            $count = 0;

            foreach ($resultUrl as $fileUrl => $bool) {
                $update = false;
                $formFieldName = $docsArr[$count];

                foreach ($userdata as $row) {
                    if ($row['dataLabel'] == $formFieldName) {
                        $update = true;
                    }
                }
                if ($update) {
                    $query = "UPDATE applicantdata SET data=:data WHERE userID = :userID AND dataLabel=:dataLabel";
                    $vars = array(
                        ':data' => $fileUrl,
                        ':userID' =>  $params['id'],
                        ':dataLabel' => $formFieldName,
                    );
                    $applicantdata = $db->exec($query, $vars);
                } else {
                    $query = "INSERT INTO applicantdata (userID, data, dataType, dataLabel, dateCreated, dateUpdated)
                                VALUES (:userID, :data, :dataType, :dataLabel, NOW(), NOW())";
                    $vars = array(
                        ':userID' =>  $params['id'],
                        ':data' => $fileUrl,
                        ':dataType' => "document",
                        ':dataLabel' => $formFieldName,
                    );
                    $applicantdata = $db->exec($query, $vars);
                }
                $count++;
            }
            $totalIncome = 0;
            
            if ($f3->get('POST.totalIncome') || $f3->get('POST.applicant2TotalIncome')) {

                $income = 0;
                if ((int) $f3->get('POST.applicants') > 0) {
                    $query = "SELECT * FROM `applicants` where applicantID = :applicantID";
                    $vars = array(
                        ':applicantID' => (int) $user_profiles[0]['id'],
                    );
                    $applicants = $db->exec($query, $vars);
                    
                    $applicant = 2;
                    for ($count=0; $count < count($applicants); $count++) { 
                        $occupation = 'applicant'.$applicant.'Occupation';
                        $employer = 'applicant'.$applicant.'Employer';
                        $employerContact = 'applicant'.$applicant.'EmployerContact';
                        $employmentDuration = 'applicant'.$applicant.'EmploymentDuration';
                        $grossIncome = 'applicant'.$applicant.'GrossIncome';
                        $totalIncome = 'applicant'.$applicant.'TotalIncome';
                        $additionalIncome = 'applicant'.$applicant.'AdditionalIncome';
                        $totalExpense = 'applicant'.$applicant.'TotalExpense';
                        $employmentType = 'applicant'.$applicant.'EmploymentType';
                        $otherEmploymentType = 'applicant'.$applicant.'OtherEmploymentType';
                        
                        $query = "UPDATE applicants SET occupation = :occupation, employer = :employer, employerContact = :employerContact, employmentDuration = :employmentDuration, 
                        grossIncome = :grossIncome, totalIncome = :totalIncome, additionalIncome = :additionalIncome, totalExpense = :totalExpense, employmentType =:employmentType, 
                        otherEmploymentType =:otherEmploymentType, dateUpdated = NOW() 
                        WHERE id =:id AND applicantID = :applicantID";
                        $vars = array(
                            ':occupation' => $f3->get("POST.$occupation"),
                            ':employer' => $f3->get("POST.$employer"),
                            ':employerContact' => $f3->get("POST.$employerContact"),
                            ':employmentDuration' => $f3->get("POST.$employmentDuration"),
                            ':grossIncome' => $f3->get("POST.$grossIncome"),
                            ':totalIncome' => $f3->get("POST.$totalIncome"),
                            ':additionalIncome' => $f3->get("POST.$additionalIncome"),
                            ':totalExpense' => $f3->get("POST.$totalExpense"),
                            ':employmentType' => $f3->get("POST.$employmentType"),
                            ':otherEmploymentType' => $f3->get("POST.$otherEmploymentType"),
                            ':id' => (int) $applicants[$count]['id'],
                            ':applicantID' => (int) $user_profiles[0]['id'],
                        );
                        $update_applicant = $db->exec($query, $vars);
                        $applicant ++;
                    }
                    
                    $query = "SELECT * FROM `applicants` where applicantID = :applicantID";
                    $vars = array(
                        ':applicantID' => (int) $user_profiles[0]['id'],
                    );
                    $applicants = $db->exec($query, $vars);

                    foreach ($applicants as $applicant) {
                        $income = $income + (float) $applicant['totalIncome'];
                    }
                    $income = round($income, 2);
                }
                $attributes['totalIncome'] = $f3->get('POST.totalIncome');
                $maxIncome = (float) $attributes['totalIncome'] + $income;

                $attributes['rentTrench'] = 1;
                $query = "SELECT * FROM `rentTrench` where minIncome <= :minIncome AND maxIncome >= :maxIncome AND deleted= :isDeleted";
                $vars = array(
                    ':minIncome' => $maxIncome,
                    ':maxIncome' => $maxIncome,
                    ':isDeleted' => "n",
                );
                $rentTrench = $db->exec($query, $vars);
                $curlResponse['rentTrench'] = json_encode($rentTrench);
                $attributes['rentTrench'] = $rentTrench[0]['id'];

                if($maxIncome > 15000) {
                    $attributes['housing'] = 'affordable';
                } else if ($maxIncome < 15000){
                    $attributes['housing'] = 'social';
                }
            }
  
            
            //partners
            if ((int) $f3->get('POST.applicants') > 1 && $f3->get('POST.applicant2FullName')) {
                //clear created applicants
                $query = "SELECT * FROM `applicants` where applicantID = :applicantID ORDER BY id ASC;";
                $vars = array(
                    ':applicantID' => (int) $userdata[0]['applicantID'],
                );
                $applicants = $db->exec($query, $vars);

                $pos = 0;
                if(count($applicants) > 0){
                    for ($applicant=1; $applicant < (int) $f3->get('POST.applicants');) { 
                        //update applicants
                        $fullName = 'applicant'.$applicant.'FullName';
                        $email = 'applicant'.$applicant.'Email';
                        $idNumber = 'applicant'.$applicant.'IdPassportNo';
                        $disability = 'applicant'.$applicant.'Disability';
    
                        $query = "UPDATE applicants SET applicantID = :applicantID, fullName = :fullName, email = :email, idNumber = :idNumber, disability = :disability, dateUpdated = NOW() WHERE id = :id;";
                        $vars = array(
                            ':applicantID' => (int) $userdata[0]['applicantID'],
                            ':fullName' => $f3->get("POST.$fullName"),
                            ':email' => $f3->get("POST.$email"),
                            ':idNumber' => $f3->get("POST.$idNumber"),
                            ':disability' => $f3->get("POST.$disability"),
                            ':id' => $applicants[$pos]['id']
                        );
                        $update_applicant = $db->exec($query, $vars);
                        $applicant++;
                        if($applicants[$pos]['email']!=$f3->get("POST.$email")){
                            //send email
                            $spouse_data['firstName'] = $userdata[0]['firstName']. ' '. $userdata[0]['lastName'];
                            $spouse_data['email'] =$f3->get("POST.$email");
                            $spouse_data['name'] = $f3->get("POST.$fullName");
                            $spouse_data['id'] = $userdata[0]['applicantID'];
                            $subject = 'Spouse/Partner Consent Request';
                
                            $this->dispatchEmail($spouse_data, $subject, "consent");
                        }
                        $pos++;
                    }
                } else {

                    for ($applicant=1; $applicant < (int) $f3->get('POST.applicants');) { 
                        //clear created applicants
                        $applicant++;
                        $fullName = 'applicant'.$applicant.'FullName';
                        $email = 'applicant'.$applicant.'Email';
                        $idNumber = 'applicant'.$applicant.'IdPassportNo';
                        $disability = 'applicant'.$applicant.'Disability';
    
                        $query = "INSERT INTO applicants (applicantID, fullName, email, idNumber, disability, dateCreated, dateUpdated) VALUES(:applicantID, :fullName, :email, :idNumber, :disability, :dateCreated, :dateUpdated)";
                        $vars = array(
                            ':applicantID' => (int) $userdata[0]['applicantID'],
                            ':fullName' => $f3->get("POST.$fullName"),
                            ':email' => $f3->get("POST.$email"),
                            ':idNumber' => $f3->get("POST.$idNumber"),
                            ':disability' => $f3->get("POST.$disability"),
                            ':dateCreated' => date("Y-m-d H:i:s"),
                            ':dateUpdated' => date("Y-m-d H:i:s")
                        );
                        $save_applicant = $db->exec($query, $vars);

                        //send email
                        $spouse_data['firstName'] = $userdata[0]['firstName']. ' '. $userdata[0]['lastName'];
                        $spouse_data['email'] =$f3->get("POST.$email");
                        $spouse_data['name'] = $f3->get("POST.$fullName");
                        $spouse_data['id'] = $userdata[0]['applicantID'];
                        $subject = 'Spouse/Partner Consent Request';
            
                        $this->dispatchEmail($spouse_data, $subject, "consent");
                    }
                }
            }

            //occupants
            if ((int) $f3->get('POST.occupants') > 0 && $f3->get('POST.occupant1FullName')) {
                //clear created occupants
                $query = "SELECT * FROM `occupants` where applicantID = :applicantID ORDER BY id ASC;";
                $vars = array(
                    ':applicantID' => (int) $userdata[0]['applicantID'],
                );
                $occupants = $db->exec($query, $vars);

                if(count($occupants) > 0){
                    for ($occupant=1; $occupant <= (int) $f3->get('POST.occupants'); $occupant++) { 
                        //update occupants
                        $fullName = 'occupant'.$occupant.'FullName';
                        $dob = 'occupant'.$occupant.'DateOfBirth';

                        $query = "UPDATE occupants SET applicantID = :applicantID, fullName = :fullName, dob = :dob, dateUpdated = NOW() WHERE id = :id;";
                        $vars = array(
                            ':applicantID' => (int) $userdata[0]['applicantID'],
                            ':fullName' => $f3->get("POST.$fullName"),
                            ':dob' => $f3->get("POST.$dob"),
                            ':id' => $occupants[$occupant]['id']
                        );
                        $update_occupant = $db->exec($query, $vars);
                    }
                } else {
                    for ($occupant=1; $occupant <= (int) $f3->get('POST.occupants'); $occupant++) { 
                        //clear created occupants
                        $fullName = 'occupant'.$occupant.'FullName';
                        $dob = 'occupant'.$occupant.'DateOfBirth';
    
                        $query = "INSERT INTO occupants (applicantID, fullName, dob, dateCreated, dateUpdated) VALUES(:applicantID, :fullName, :dob, :dateCreated, :dateUpdated)";
                        $vars = array(
                            ':applicantID' => (int) $userdata[0]['applicantID'],
                            ':fullName' => $f3->get("POST.$fullName"),
                            ':dob' => $f3->get("POST.$dob"),
                            ':dateCreated' => date("Y-m-d H:i:s"),
                            ':dateUpdated' => date("Y-m-d H:i:s")
                        );
                        $save_occupant = $db->exec($query, $vars);
                    }
                }
            }

            //children
            if ((int) $f3->get('POST.children') > 0 && $f3->get('POST.child1FullName')) {
                //clear created applicants
                $query = "SELECT * FROM `children` where applicantID = :applicantID ORDER BY id ASC;";
                $vars = array(
                    ':applicantID' => (int) $userdata[0]['applicantID'],
                );
                $children = $db->exec($query, $vars);

                if(count($children) > 0){
                    for ($child=0; $child < (int) $f3->get('POST.children'); $child++) { 
                        //update children
                        $fullName = 'child'.($child+1).'FullName';
                        $age = 'child'.($child+1).'Age';
                        $disability = 'child'.($child+1).'Disability';
    
                        $query = "UPDATE children SET applicantID = :applicantID, fullName = :fullName, age = :age, disability = :disability, dateUpdated = NOW() WHERE id = :id;";
                        $vars = array(
                            ':applicantID' => (int) $userdata[0]['applicantID'],
                            ':fullName' => $f3->get("POST.$fullName"),
                            ':age' => $f3->get("POST.$age"),
                            ':disability' => $f3->get("POST.$disability"),
                            ':id' => $children[$child]['id'],
                        );
                        $update_child = $db->exec($query, $vars);
                    }
                } else {
                    for ($child=0; $child < (int) $f3->get('POST.children'); $child++) { 
                        //clear created children
                        $fullName = 'child'.($child+1).'FullName';
                        $age = 'child'.($child+1).'Age';
                        $disability = 'child'.($child+1).'Disability';
    
                        $query = "INSERT INTO children (applicantID, fullName, age, disability, dateCreated, dateUpdated) VALUES(:applicantID, :fullName, :age, :disability, :dateCreated, :dateUpdated)";
                        $vars = array(
                            ':applicantID' => (int) $userdata[0]['applicantID'],
                            ':fullName' => $f3->get("POST.$fullName"),
                            ':age' => $f3->get("POST.$age"),
                            ':disability' => $f3->get("POST.$disability"),
                            ':dateCreated' => date("Y-m-d H:i:s"),
                            ':dateUpdated' => date("Y-m-d H:i:s")
                        );
                        $save_child = $db->exec($query, $vars);
                    }
                }
            }

            //vehicles
            if ((int) $f3->get('POST.vehicles') > 0 && $f3->get('POST.vehicle1Type')) {
                //clear created applicants
                $query = "SELECT * FROM `vehicles` where applicantID = :applicantID ORDER BY id ASC;";
                $vars = array(
                    ':applicantID' => (int) $userdata[0]['applicantID'],
                );
                $vehicles = $db->exec($query, $vars);

                if(count($vehicles) > 0){
                    for ($vehicle=0; $vehicle < (int) $f3->get('POST.vehicles'); $vehicle++) { 
                        //update vehicles
                        $type = 'vehicle'.($vehicle+1).'Type';
                        $color = 'vehicle'.($vehicle+1).'Color';
                        $registrationNumber = 'vehicle'.($vehicle+1).'RegistrationNumber';
    
                        $query = "UPDATE vehicles SET applicantID = :applicantID, type = :type, color = :color, registrationNumber = :registrationNumber, dateUpdated = NOW() WHERE id = :id;";
                        $vars = array(
                            ':applicantID' => (int) $userdata[0]['applicantID'],
                            ':type' => $f3->get("POST.$type"),
                            ':color' => $f3->get("POST.$color"),
                            ':registrationNumber' => $f3->get("POST.$registrationNumber"),
                            ':id' => $vehicles[$vehicle]['id']
                        );
                        $update_vehicle = $db->exec($query, $vars);
                    }
                } else {
                    for ($vehicle=0; $vehicle < (int) $f3->get('POST.vehicles'); $vehicle++) { 
                        //clear created vehicles
                        $type = 'vehicle'.($vehicle+1).'Type';
                        $color = 'vehicle'.($vehicle+1).'Color';
                        $registrationNumber = 'vehicle'.($vehicle+1).'RegistrationNumber';
    
                        $query = "INSERT INTO vehicles (applicantID, type, color, registrationNumber, dateCreated, dateUpdated) VALUES(:applicantID, :type, :color, :registrationNumber, :dateCreated, :dateUpdated)";
                        $vars = array(
                            ':applicantID' => (int) $userdata[0]['applicantID'],
                            ':type' => $f3->get("POST.$type"),
                            ':color' => $f3->get("POST.$color"),
                            ':registrationNumber' => $f3->get("POST.$registrationNumber"),
                            ':dateCreated' => date("Y-m-d H:i:s"),
                            ':dateUpdated' => date("Y-m-d H:i:s")
                        );
                        $save_vehicle = $db->exec($query, $vars);
                    }
                }
            }

            //pets
            if ((int) $f3->get('POST.pets') > 0 && $f3->get('POST.pet1Size')) {
                //clear created applicants
                $query = "SELECT * FROM `pets` where applicantID = :applicantID ORDER BY id ASC;";
                $vars = array(
                    ':applicantID' => (int) $userdata[0]['applicantID'],
                );
                $pets = $db->exec($query, $vars);

                if(count($pets) > 0){
                    for ($pet=0; $pet < (int) $f3->get('POST.pets'); $pet++) { 
                        //update pets
                        $type = 'pet'.($pet+1).'Type';
                        $breed = 'pet'.($pet+1).'Breed';
                        $size = 'pet'.($pet+1).'Size';
    
                        $query = "UPDATE pets SET applicantID = :applicantID, type = :type, breed = :breed, size = :size, dateUpdated = NOW() WHERE id = :id;";
                        $vars = array(
                            ':applicantID' => (int) $userdata[0]['applicantID'],
                            ':type' => $f3->get("POST.$type"),
                            ':breed' => $f3->get("POST.$breed"),
                            ':size' => $f3->get("POST.$size"),
                            ':id' => $pets[$pet]['id']
                        );
                        $update_pet = $db->exec($query, $vars);
                    }
                } else {
                    for ($pet=0; $pet < (int) $f3->get('POST.pets'); $pet++) { 
                        //clear created pets
                        $type = 'pet'.($pet+1).'Type';
                        $breed = 'pet'.($pet+1).'Breed';
                        $size = 'pet'.($pet+1).'Size';
    
                        $query = "INSERT INTO pets (applicantID, type, breed, size, dateCreated, dateUpdated) VALUES(:applicantID, :type, :breed, :size, :dateCreated, :dateUpdated)";
                        $vars = array(
                            ':applicantID' => (int) $userdata[0]['applicantID'],
                            ':type' => $f3->get("POST.$type"),
                            ':breed' => $f3->get("POST.$breed"),
                            ':size' => $f3->get("POST.$size"),
                            ':dateCreated' => date("Y-m-d H:i:s"),
                            ':dateUpdated' => date("Y-m-d H:i:s")
                        );
                        $save_pet = $db->exec($query, $vars);
                    }
                }
            }

            //emergency contacts
            if ((int) $f3->get('POST.emergencyContacts') > 0 && $f3->get('POST.emergencyContact1FullName')) {
                //clear created occupants
                $query = "DELETE FROM `emergencyContacts` where applicantID = :applicantID;";
                $vars = array(
                    ':applicantID' => (int) $userdata[0]['applicantID'],
                );
                $clear = $db->exec($query, $vars);

                $query = "SELECT * FROM `emergencyContacts` where applicantID = :applicantID ORDER BY id ASC;";
                $vars = array(
                    ':applicantID' => (int) $userdata[0]['applicantID'],
                );
                $emergencyContacts = $db->exec($query, $vars);

                if(count($emergencyContacts) > 0){
                    for ($emergencyContact=1; $emergencyContact <= (int) $f3->get('POST.emergencyContacts');) { 
                        //update emergencyContacts
                       
                        $fullName = 'emergencyContact'.$emergencyContact.'FullName';
                        $phoneNumber = 'emergencyContact'.$emergencyContact.'PhoneNumber';

                        $query = "UPDATE emergencyContacts SET applicantID = :applicantID, fullName = :fullName, phoneNumber = :phoneNumber, dateUpdated = :dateUpdated WHERE id = :id;";
                        $vars = array(
                            ':applicantID' => (int) $userdata[0]['applicantID'],
                            ':fullName' => $f3->get("POST.$fullName"),
                            ':phoneNumber' => $f3->get("POST.$phoneNumber"),
                            ':dateUpdated' => date("Y-m-d H:i:s"),
                            ':id' => $emergencyContacts[$emergencyContact-1]['id']
                        );
                        $update_emergencyContact = $db->exec($query, $vars);
                        $emergencyContact++;
                    }
                } else {
                    for ($emergencyContact=1; $emergencyContact <= (int) $f3->get('POST.emergencyContacts');) { 
                        //clear created emergencyContacts
                        
                        $fullName = 'emergencyContact'.$emergencyContact.'FullName';
                        $phoneNumber = 'emergencyContact'.$emergencyContact.'PhoneNumber';
    
                        $query = "INSERT INTO emergencyContacts (applicantID, fullName, phoneNumber, dateCreated, dateUpdated) VALUES(:applicantID, :fullName, :phoneNumber, :dateCreated, :dateUpdated)";
                        $vars = array(
                            ':applicantID' => (int) $userdata[0]['applicantID'],
                            ':fullName' => $f3->get("POST.$fullName"),
                            ':phoneNumber' => $f3->get("POST.$phoneNumber"),
                            ':dateCreated' => date("Y-m-d H:i:s"),
                            ':dateUpdated' => date("Y-m-d H:i:s")
                        );
                        $save_emergencyContact = $db->exec($query, $vars);
                        $emergencyContact++;
                    }
                }
            }

            if (count($userdata) > 0) {
                $update = true;
            } else {
                $attributes['userID'] = $params['id'];
                $update = false;
            }
            
            $definition = $this->getDefinition($attributes);
            $idArray = ['userID =?', $params['id']];

            if ($f3->get('POST.firstName')) {
                $userArray = ['id =?', $params['id']];

                $this->getRepository("users")->updateRecord($userArray, $definition);
            }

            if ($update) {
                $info = $this->getRepository("applicantdetails")->updateRecord($idArray, $definition);
            } else {
                $info = $this->getRepository("applicantdetails")->createRecord($definition);
            }
        }

        $query = "SELECT dt.*  FROM applicantdetails ad LEFT JOIN applicantdata dt ON ad.userID = dt.userID WHERE ad.userID = :userID";
        $vars = array(
            ':userID' => $params['id'],
        );
        $userdata = $db->exec($query, $vars);
        $curlResponse['userdata'] = json_encode($userdata);

        $newDocsArr = [];

        foreach($userdata as $row){
            $title = preg_replace("([A-Z])", " $0", $row['dataLabel']);

            $ext = substr($row['data'], -5);
            $extArr = explode(".", $ext);

            $imgArr = ['jpg', 'jpeg', 'png'];
            $docArr = ['pdf', 'docx', 'doc'];

            if (in_array(strtolower($extArr[1]), $imgArr)) {
                $docType = "img";
            }elseif (in_array(strtolower($extArr[1]), $docArr)) {
                $docType = "doc";
            }
            else {
                $docType = "null";
            }
            $newDocsArr[] = ["docType" => $docType, "id"=>$row['id'],"dataType"=>$row['dataType'],
                            "dataLabel"=>$row['dataLabel'],"data"=>$row['data'],"title"=>$title];
        }
        $curlResponse['documentDetails'] = json_encode($newDocsArr);
        $curlResponse['profilePercentage'] = round($profilePerc);

        $query = "SELECT *  from applicantdetails WHERE userID = :id";
        $vars = array(
            ':id' => $params['id'],
        );
        $results = $db->exec($query, $vars);
        $curlResponse['applicantdetails'] = json_encode($results);

        $query = "SELECT *  from applicants WHERE applicantID = :applicantID ORDER BY id ASC;";
        $vars = array(
            ':applicantID' => $results[0]['id'],
        );
        $applicants = $db->exec($query, $vars);
        $curlResponse['applicants'] = json_encode($applicants);

        $query = "SELECT *  from occupants WHERE applicantID = :applicantID ORDER BY id ASC;";
        $vars = array(
            ':applicantID' => $results[0]['id'],
        );
        $occupants = $db->exec($query, $vars);
        $curlResponse['occupants'] = json_encode($occupants);
   
        $query = "SELECT *  from children WHERE applicantID = :applicantID ORDER BY id ASC;";
        $vars = array(
            ':applicantID' => $results[0]['id'],
        );
        $children = $db->exec($query, $vars);
        $curlResponse['children'] = json_encode($children);

        $query = "SELECT *  from vehicles WHERE applicantID = :applicantID ORDER BY id ASC;";
        $vars = array(
            ':applicantID' => $results[0]['id'],
        );
        $vehicles = $db->exec($query, $vars);
        $curlResponse['vehicles'] = json_encode($vehicles);

        $query = "SELECT *  from pets WHERE applicantID = :applicantID ORDER BY id ASC;";
        $vars = array(
            ':applicantID' => $results[0]['id'],
        );
        $pets = $db->exec($query, $vars);
        $curlResponse['pets'] = json_encode($pets);

        $query = "SELECT * from emergencyContacts WHERE applicantID = :applicantID ORDER BY id ASC;";
        $vars = array(
            ':applicantID' => $results[0]['id'],
        );
        $emergency_contacts = $db->exec($query, $vars);
        $curlResponse['emergency_contacts'] = json_encode($emergency_contacts);

        //GET USER APPLICATIONS
        $query = "SELECT * FROM applications WHERE userID = :userID ORDER BY id DESC;";
        $vars = array(
            ':userID' => $params['id'],
        );
        $application = $db->exec($query, $vars);

        //GET USER APPLICATION REQUESTS
        $query = "SELECT * FROM requests WHERE applicationID = :applicationID;";
        $vars = array(
            ':applicationID' => $application[0]['id']
        ); 
        $requests = $db->exec($query, $vars);
        $curlResponse['requests'] = json_encode($requests);


        $arrCheckList = [];

        $profilePerc = 0;

        $arrCheckList["personalInfo"] = ["firstName", "lastName", "idPassportNo", "dob", "address", "maritalStatus","bankName","branchCode","accountNumber","accountType"];
        $arrCheckList["contactInfo"] = ["email", "phone", "altPhone"];
        $arrCheckList["employerInfo"] = ["occupation", "employer", "employerContact", "totalIncome", "totalExpenses"];
        //$arrCheckList["emergencyInfo"] = ["nextOfKin", "nextOfKinContact"];
        $arrCheckList["emergencyInfo"] = [];
        for ($i=0; $i < count($emergency_contacts); $i++) { 
            array_push($arrCheckList["emergencyInfo"], "emergencyContact".($i+1)."FullName");
            array_push($arrCheckList["emergencyInfo"], "emergencyContact".($i+1)."PhoneNumber");
        }
        $arrCheckList["documentInfo"] = ["idDocument", "payslip", "bankStatement","proofOfPayment"];

        foreach ($arrCheckList as $key => $list) {
            foreach ($list as $column) {
                if ($key == "personalInfo" && $results[0][$column] != "") {
                    $profilePerc = $profilePerc + 2;
                    $personalPerc = $personalPerc + 2;
                }
                if ($key == "contactInfo" && $results[0][$column] != "") {
                    $profilePerc = $profilePerc + 6.66;
                    $contactPerc = $contactPerc + 6.66;
                }
                if ($key == "employerInfo" && $results[0][$column] != "") {
                    $profilePerc = $profilePerc + 4;
                    $employerPerc = $employerPerc + 4;
                }
                // if ($key == "emergencyInfo" && $results[0][$column] != "") {
                //     $profilePerc = $profilePerc + 10;
                //     $emergencyPerc = $emergencyPerc + 10;
                // }
                if ($key == "documentInfo") {
                    foreach ($userdata as $row) {

                        if ($row['dataLabel'] == $column) {
                            $dataArr = explode(",", $row['data']);
                            if (!in_array('Error', $dataArr) && $row['data'] != "") {
                                $profilePerc = $profilePerc + 5;
                                $documentPerc = $documentPerc + 5;
                            }
                        }
                    }
                }
            }
        }

        if(count($emergency_contacts)>0){
            $profilePerc = $profilePerc + 20;
            $emergencyPerc = $emergencyPerc + 20;
        }
        $curlResponse['profilePercentage'] = round($profilePerc);
        $curlResponse['personalPerc'] = round($personalPerc);
        $curlResponse['contactPerc'] = round($contactPerc);
        $curlResponse['employerPerc'] = round($employerPerc);
        $curlResponse['emergencyPerc'] = round($emergencyPerc);
        $curlResponse['documentPerc'] = round($documentPerc);
        echo json_encode($curlResponse);
    }

    public function verifyApplication($f3, $userID, $applicationID, $applicantID)
    {
        try {
            global $db;
            $curlResponse = [];
            $adminID = $f3->get('POST.userID');
            $update = true;

            //if post lets save
            $arrCheckList = [];
            $additionemployerInfo = '';
            $bankStatements = '';
            $signedEmployementContract = '';
            $payslips = '';
            $idDocuments = '';

            $additionalPersonalInfo = '';
            $additionalRentalReferenceInfo = '';
            $additionalOccupantsInfo = '';
            $additionalContactInfo = '';
            $additionalEmploymentInfo = '';
            $additionalEmergencyInfo = '';
            $additionalDocumentInfo = '';

            $query = "SELECT * FROM requests WHERE applicationID = :applicationID;";
            $vars = array(
                ":applicationID" => $applicationID
            );
            $requests = $db->exec($query, $vars);
            for ($i=0; $i < count($requests); $i++) { 
                if($requests[$i]['request'] == 'personal'){
                    $del = ',';
                    $additionalPersonalInfo = $additionalPersonalInfo.$del.str_replace(" ", "", $requests[$i]['title']);
                }

                if($requests[$i]['request'] == 'rentalReference'){
                    $del = ',';
                    $additionalRentalReferenceInfo = $additionalRentalReferenceInfo.$del.str_replace(" ", "", $requests[$i]['title']);
                }
    
                if($requests[$i]['request'] == 'contact'){
                    $del = ',';
                    $additionalContactInfo = $additionalContactInfo.$del.str_replace(" ", "", $requests[$i]['title']);
                }
    
                if($requests[$i]['request'] == 'employment'){
                    $del = ',';
                    $additionalEmploymentInfo = $additionalEmploymentInfo.$del.str_replace(" ", "", $requests[$i]['title']);
                }
    
                if($requests[$i]['request'] == 'emergency'){
                    $del = ',';
                    $additionalEmergencyInfo = $additionalEmergencyInfo.$del.str_replace(" ", "", $requests[$i]['title']);
                }
    
                if($requests[$i]['request'] == 'document'){
                    $del = ',';
                    $additionalDocumentInfo = $additionalDocumentInfo.$del.str_replace(" ", "_", $requests[$i]['title']);
                    for ($a=1; $a < 15; $a++) { 
                        $additionalDocumentInfo = $additionalDocumentInfo.$del.str_replace(" ", "_", $requests[$i]['title']).$a;
                    }
                }
            }

            $query = "SELECT * FROM children WHERE applicantID = :applicantID;";
            $vars = array(
                ":applicantID" => $applicantID
            );
            $children = $db->exec($query, $vars);

            if(count($children)>0){
                for ($child=0; $child < count($children); $child++) {
                    $del = ','; 
                    $additionalPersonalInfo = $additionalPersonalInfo.$del.'child'.($child+1).'FullName,child'.($child+1).'Age,child'.($child+1).'Disability';
                }
            }

            $query = "SELECT * FROM vehicles WHERE applicantID = :applicantID;";
            $vars = array(
                ":applicantID" => $applicantID
            );
            $vehicles = $db->exec($query, $vars);

            if(count($vehicles)>0){
                for ($vehicle=0; $vehicle < count($vehicles); $vehicle++) {
                    $del = ','; 
                    $additionalPersonalInfo = $additionalPersonalInfo.$del.'vehicle'.($vehicle+1).'Type,vehicle'.($vehicle+1).'Color,vehicle'.($vehicle+1).'RegistrationNumber';
                }
            }


            

            $arrCheckList["occupantsInfo"] = [];    
            $query = "SELECT * FROM occupants WHERE applicantID = :applicantID;";
            $vars = array(
                ":applicantID" => $applicantID
            );
            $occupants = $db->exec($query, $vars);

            if(count($occupants)>0){
                for ($occupant=0; $occupant < count($occupants);) {
                    $del = ','; 
                    $occupant++;
                    $additionalOccupantsInfo = $additionalOccupantsInfo.$del.'occupant'.($occupant).'FullName,occupant'.($occupant).'DateOfBirth';
                }
            }

            if(strlen($additionalOccupantsInfo) > 0){
                $additionalOccupantsInfo = explode( ',', $additionalOccupantsInfo );
                $arrCheckList["occupantsInfo"] = array_merge_recursive($arrCheckList["occupantsInfo"], $additionalOccupantsInfo);
                $arrCheckList["occupantsInfo"] = array_values($arrCheckList["occupantsInfo"]);
                $arrCheckList["occupantsInfo"] = array_filter($arrCheckList["occupantsInfo"]);
            }
            
            

            $arrCheckList["personalInfo"] = ["applicants", "occupants", "totalOccupants", "firstName", "lastName", "idPassportNo", "dob", "address", "maritalStatus", "maritalType", "disability", "pets", "children", "vehicles", "debtReview", "pendingJudgement", "bankName", "branchCode", "accountNumber", "accountType"];
            $additionalPersonalInfo = explode( ',', $additionalPersonalInfo );
            $arrCheckList["personalInfo"] = array_merge_recursive($arrCheckList["personalInfo"], $additionalPersonalInfo);
            $arrCheckList["personalInfo"] = array_values($arrCheckList["personalInfo"]);
            $arrCheckList["personalInfo"] = array_filter($arrCheckList["personalInfo"]);
            
            $arrCheckList["contactInfo"] = ["email", "phone", "altPhone"];
            if(strlen($additionalContactInfo) > 0){
                $additionalContactInfo = explode( ',', $additionalContactInfo );
                $arrCheckList["contactInfo"] = array_merge_recursive($arrCheckList["contactInfo"], $additionalContactInfo);
                $arrCheckList["contactInfo"] = array_values($arrCheckList["contactInfo"]);
                $arrCheckList["contactInfo"] = array_filter($arrCheckList["contactInfo"]);
            }

            $query = "SELECT * FROM applicants WHERE applicantID = :applicantID;";
            $vars = array(
                ":applicantID" => $applicantID
            );
            $applicants = $db->exec($query, $vars);

            if(count($applicants)>0){

                for ($a=0; $a < count($applicants);) { 
                    $delimeter = ",";
                    $a++;
                    $additionemployerInfo = $additionemployerInfo.$delimeter.'employmentTypeApplicant'.($a+1).',occupationApplicant'.($a+1).',employerApplicant'.($a+1).',employerContactApplicant'.($a+1).',employmentDurationApplicant'.($a+1).',totalIncomeApplicant'.($a+1).',totalExpensesApplicant'.($a+1);
    
                    if($bankStatements != ''){
                        $bankStatements = $bankStatements.',applicant'.($a+1).'BankStatement';
                    } else{
                        $bankStatements = $bankStatements.'applicant'.($a+1).'BankStatement';
                    }

                    if($signedEmployementContract != ''){
                        $signedEmployementContract = $signedEmployementContract.',applicant'.($a+1).'SignedEmployementContract';
                    } else{
                        $signedEmployementContract = $signedEmployementContract.'applicant'.($a+1).'SignedEmployementContract';
                    }
                    
                    for ($repeat=1; $repeat < 6; $repeat++) { 
                        $delimeter = ',';
                        $bankStatements = $bankStatements.$delimeter.'applicant'.($a+1).'BankStatement'.$repeat;
                    }
    
                    if($idDocuments != ''){
                        $idDocuments = $idDocuments.',applicant'.($a+1).'IdDocument';
                    } else{
                        $idDocuments = $idDocuments.'applicant'.($a+1).'IdDocument';
                    }
    
                    if($payslips != ''){
                        $payslips = $payslips.',applicant'.($a+1).'Payslip';
                    } else{
                        $payslips = $payslips.'applicant'.($a+1).'Payslip';
                    }
                    
                    for ($repeat=1; $repeat < 6; $repeat++) { 
                        $delimeter = ',';
                        $payslips = $payslips.$delimeter.'applicant'.($a+1).'Payslip'.$repeat;
                    }
                }
            }

            
            $additionemployerInfo = explode( ',', $additionemployerInfo );
            $arrCheckList["employerInfo"] = ["employmentType","occupation", "employer", "employerContact", "employmentDuration", "totalIncome", "totalExpenses", "occupationSpouse", "employerSpouse", "employerContactSpouse"];
            $arrCheckList["employerInfo"] = array_merge_recursive($arrCheckList["employerInfo"], $additionemployerInfo);
            $arrCheckList["employerInfo"] = array_values($arrCheckList["employerInfo"]);
            $arrCheckList["employerInfo"] = array_filter($arrCheckList["employerInfo"]);

           
            if(strlen($additionalEmploymentInfo) > 0){
                $additionalEmploymentInfo = explode( ',', $additionalEmploymentInfo );
                $arrCheckList["employerInfo"] = array_merge_recursive($arrCheckList["employerInfo"], $additionalEmploymentInfo);
                $arrCheckList["employerInfo"] = array_values($arrCheckList["employerInfo"]);
                $arrCheckList["employerInfo"] = array_filter($arrCheckList["employerInfo"]);
            }

            $query = "SELECT * FROM applicantdetails WHERE id = :applicantID;";
            $vars = array(
                ":applicantID" => $applicantID
            );
            $applicantdetails = $db->exec($query, $vars);
            $arrCheckList["rentalReferenceInfo"] = ["currentLandlord", "currentLandlordContact", "currentResident", "leaseAgreement", "leaseExpiredDate", "rentalAmount", "rentalDuration"];
            if($applicantdetails[0]['rentalType']=='previouslyRenting'){
                $arrCheckList["rentalReferenceInfo"] = ["previousLandlord", "previousLandlordContact", "signedAsSurety", "previousLeaseAgreement", "leaseAgreement", "leaseDuration", "rentalAmount", "rentalDuration"];
            } else if($applicantdetails[0]['rentalType']=='currentlyRenting'){
                $arrCheckList["rentalReferenceInfo"] = ["currentLandlord", "currentLandlordContact", "signedAsSurety",  "currentResident", "leaseAgreement", "leaseExpiredDate", "rentalAmount", "rentalDuration"];
            } else if($applicantdetails[0]['rentalType']=='firstTimeRenting'){
                $arrCheckList["rentalReferenceInfo"] = ["referenceName", "relationToReference", "referenceContact", "referenceEmail"];
            } else {
                $arrCheckList["rentalReferenceInfo"] = ["referenceName", "relationToReference", "referenceContact", "referenceEmail"];
            }

            if(strlen($additionalRentalReferenceInfo) > 0){
                $additionalRentalReferenceInfo = explode( ',', $additionalRentalReferenceInfo );
                $arrCheckList["rentalReferenceInfo"] = array_merge_recursive($arrCheckList["rentalReferenceInfo"], $additionalRentalReferenceInfo);
                $arrCheckList["rentalReferenceInfo"] = array_values($arrCheckList["rentalReferenceInfo"]);
                $arrCheckList["rentalReferenceInfo"] = array_filter($arrCheckList["rentalReferenceInfo"]);
            }

            $query = "SELECT * from emergencyContacts WHERE applicantID = :applicantID ORDER BY id ASC;";
            $vars = array(
                ':applicantID' =>  $applicantID,
            );
            $emergency_contacts = $db->exec($query, $vars);
            $curlResponse['emergency_contacts'] = json_encode($emergency_contacts);

            //$arrCheckList["emergencyInfo"] = ["nextOfKin", "nextOfKinContact"];
            $arrCheckList["emergencyInfo"] = [];
            for ($i=0; $i < count($emergency_contacts); $i++) { 
                array_push($arrCheckList["emergencyInfo"], "emergencyContact".($i+1)."FullName");
                array_push($arrCheckList["emergencyInfo"], "emergencyContact".($i+1)."PhoneNumber");
            }

            if(strlen($additionalEmergencyInfo) > 0){
                $additionalEmergencyInfo = explode( ',', $additionalEmergencyInfo );
                $arrCheckList["emergencyInfo"] = array_merge_recursive($arrCheckList["emergencyInfo"], $additionalEmergencyInfo);
                $arrCheckList["emergencyInfo"] = array_values($arrCheckList["emergencyInfo"]);
                $arrCheckList["emergencyInfo"] = array_filter($arrCheckList["emergencyInfo"]);
            }
            
           
            $bankStatements = explode( ',', $bankStatements );
            $signedEmployementContract = explode( ',', $signedEmployementContract );
            $payslips = explode( ',', $payslips );
            $idDocuments = explode( ',', $idDocuments );
            $arrCheckList["documentInfo"] = ["idDocument", "payslip", "bankStatement", 'payslip2', 'signedEmployementContract', 'bankStatement2', 'payslip3','bankStatement3', 'payslip4','bankStatement4', 'payslip5','bankStatement5', "idDocumentSpouse", "payslipSpouse", "bankStatementSpouse",'payslipSpouse2','bankStatementSpouse2', 'payslipSpouse3','bankStatementSpouse3', 'payslipSpouse4','bankStatementSpouse4', 'payslipSpouse5','bankStatementSpouse5', "proofOfPayment","creditReport","rentDeposit", 'marriageCertificate', "tpnIdDocument", "tpnBankDetails", 'divorceDecree', 'separationAffidavit','pendingJudgementDocument','debtReviewDocument'];
            $arrCheckList["documentInfo"] = array_merge_recursive($arrCheckList["documentInfo"], $bankStatements);
            $arrCheckList["documentInfo"] = array_merge_recursive($arrCheckList["documentInfo"], $signedEmployementContract);
            $arrCheckList["documentInfo"] = array_merge_recursive($arrCheckList["documentInfo"], $payslips);
            $arrCheckList["documentInfo"] = array_merge_recursive($arrCheckList["documentInfo"], $idDocuments);
            $arrCheckList["documentInfo"] = array_values($arrCheckList["documentInfo"]);
            $arrCheckList["documentInfo"] = array_filter($arrCheckList["documentInfo"]);

            if(strlen($additionalDocumentInfo) > 0){
                $additionalDocumentInfo = explode( ',', $additionalDocumentInfo );
                $arrCheckList["documentInfo"] = array_merge_recursive($arrCheckList["documentInfo"], $additionalDocumentInfo);
                $arrCheckList["documentInfo"] = array_values($arrCheckList["documentInfo"]);
                $arrCheckList["documentInfo"] = array_filter($arrCheckList["documentInfo"]);
            }

            //loop through array for the submitted form type
            foreach ($arrCheckList[$f3->get('POST.formType')] as $key => $column) {

                $isVerified = 'n';
                $update = false;

                //if the field is in post its a verification
                foreach ($f3->get('POST') as $feild => $value) {
                    if ($feild == $column) {
                        $isVerified = 'y';
                    }
                }

                //update or insert.
                $query = "SELECT * FROM userChecklist 
                WHERE userID = :userID 
                AND title = :title
                AND (applicationID = :applicationID OR applicationID IS NULL)";
                $varsArr = array(
                    ':userID' => $userID,
                    ':title' => $column,
                    ':applicationID' => $applicationID
                );
                $userdata = $db->exec($query, $varsArr);
                if (isset($userdata[0]['title'])) {
                    $update = true;
                }

                if ($update) {
                    $query = "UPDATE userChecklist SET approvedBy=:adminID, isVerified=:isVerified, dateUpdated=NOW() WHERE id=:id";
                    $varsArr = array(
                        ':adminID' => $adminID,
                        ':isVerified' => $isVerified,
                        ':id' => $userdata[0]['id'],
                    );
                    $userChecklist = $db->exec($query, $varsArr);
                    
                } else {
                    

                    if($title=="proofOfPayment"){
                        $query = "INSERT INTO userChecklist (userID, approvedBy, title, isVerified, applicationID, dateCreated, dateUpdated)
                        VALUES (:userID, :adminID, :title, :isVerified, :applicationID, :dateCreated, :dateUpdated)";
                        $varsArr = array(
                            ':userID' => $userID,
                            ':adminID' => $adminID,
                            ':title' => $column,
                            ':isVerified' => $isVerified,
                            ':applicationID' => $applicationID,
                            ':dateCreated' => date("Y-m-d H:i:s"),
                            ':dateUpdated' => date("Y-m-d H:i:s")
                        );
                        $userChecklist = $db->exec($query, $varsArr);
                    } else {
                        $query = "INSERT INTO userChecklist (userID, approvedBy, title, isVerified, dateCreated, dateUpdated)
                        VALUES (:userID, :adminID, :title, :isVerified, :dateCreated, :dateUpdated)";
                        $varsArr = array(
                            ':userID' => $userID,
                            ':adminID' => $adminID,
                            ':title' => $column,
                            ':isVerified' => $isVerified,
                            ':dateCreated' => date("Y-m-d H:i:s"),
                            ':dateUpdated' => date("Y-m-d H:i:s")
                        );
                        $userChecklist = $db->exec($query, $varsArr);
                    }

                   

                    
                }

                //Comments Submission
                if($column && $isVerified=="n"){

                    $type = "text";
                    if($f3->get('POST.formType')=="documentInfo"){
                        $type = "file";
                    }

                    $request = "additional";
                    if($f3->get('POST.formType')=="personalInfo"){
                        $request = "personal";
                    } else if($f3->get('POST.formType')=="contactInfo"){
                        $request = "contact";
                    } else if($f3->get('POST.formType')=="employerInfo"){
                        $request = "employment";
                    } else if($f3->get('POST.formType')=="rentalReferenceInfo"){
                        $request = "rentalReference";
                    } else if($f3->get('POST.formType')=="emergencyInfo"){
                        $request = "emergency";
                    } else if($f3->get('POST.formType')=="documentInfo"){
                        $request = "document";
                    }

                    $query = "SELECT * FROM comments WHERE applicationID = :applicationID AND title = :title";
                    $varsArr = array(
                        ':applicationID' => $applicationID,
                        ':title' => $column,
                    );
                    $comments = $db->exec($query, $varsArr);

                    if(count($comments)>0){
                        if($f3->get("POST.comment_$column")){
                            $query = "UPDATE comments SET message = :message, type = :type, request = :request, status = :status, dateUpdated=NOW() WHERE id=:id";
                            $varsArr = array(
                                ':message' => $f3->get("POST.comment_$column"),
                                ':request' => $request,
                                ':type' => $type,
                                ':status' => 'pending',
                                ':id' => $comments[0]['id'],
                            );
                            $save_comment = $db->exec($query, $varsArr);
                        }
                      
                    } else {
                        if($f3->get("POST.comment_$column")){
                            $query = "INSERT INTO comments (title, request, type, message, applicationID, dateCreated, dateUpdated) VALUES (:title, :request, :type, :message, :applicationID, NOW(), NOW());";
                            $vars = array(
                                ':title' => $column,
                                ':request' => $request,
                                ':type' => $type,
                                ':message' => $f3->get("POST.comment_$column"),
                                ':applicationID' => $applicationID
                            );
                            $save_comment = $db->exec($query, $vars);
                        }
                        
                    }

                    
                }
            }
            
            echo json_encode($curlResponse);
        } catch (Exception $exception) {
            echo json_encode($exception->getMessage());
        }
    }

    public function viewApplication($f3, $params)
    {
        global $db;
        $applicationID = $params['id'];
        $f3->set('applicationID', $applicationID);

        $query = "SELECT userID  FROM applications WHERE id = :id";
        $vars = array(
            ':id' => $applicationID,
        );
        $applications = $db->exec($query, $vars);
        $curlResponse['applications'] = json_encode($applications);

        $query = "SELECT * FROM applicantdetails WHERE userID = :userID";
        $vars = array(
            ':userID' => $applications[0]['userID']
        );
        $applicantdetails = $db->exec($query, $vars);
        $applicantID = $applicantdetails[0]['id'];

        $query = "SELECT * FROM occupants WHERE applicantID = :applicantID;";
        $vars = array(
            ":applicantID" => $applicantID
        );
        $occupants = $db->exec($query, $vars);

        $query = "SELECT * FROM requests WHERE applicationID = :applicationID";
        $vars = array(
            ':applicationID' => $applicationID
        );
        $requests = $db->exec($query, $vars);
        $curlResponse['requests'] = json_encode($requests);

        $query = "SELECT * FROM comments WHERE applicationID = :applicationID";
        $vars = array(
            ':applicationID' => $applicationID
        );
        $comments = $db->exec($query, $vars);
        $curlResponse['comments'] = json_encode($comments);

        if($f3->get("POST.applicantID") && !$f3->get("POST.request")){
            $userID = $f3->get("POST.applicantID");
            $this->verifyApplication($f3, $userID, $applicationID, $applicantID);
        }

        //Request Submission
        if($f3->get("POST.request")){
            $query = "INSERT INTO requests (request, title, type, message, applicationID, dateCreated, dateUpdated) VALUES (:request, :title, :type, :message, :applicationID, NOW(), NOW());";
            $vars = array(
                ':request' => $f3->get('POST.request'),
                ':title' => $f3->get('POST.title'),
                ':type' => $f3->get('POST.type'),
                ':message' => $f3->get('POST.message'),
                ':applicationID' => $applicationID
            );
            $save_request = $db->exec($query, $vars);
        }

        $query = "SELECT ad.*, uc.id as checkID, uc.title, uc.isVerified, xds.data AS xdsValue 
        from applicantdetails ad 
        LEFT JOIN userChecklist uc ON uc.userID = ad.userID 
        LEFT JOIN xdsdata xds ON ad.userID = xds.userID AND uc.title = xds.dataLabel 
        WHERE ad.userID = :userID";
        $vars = array(
            ':userID' => $applications[0]['userID'],
        );
        $results = $db->exec($query, $vars);
        $curlResponse['results'] = json_encode($results);

        $query = "SELECT dt.*, ad.maritalStatus, ad.id as applicantID, ad.applicants, ad.rentalType
        FROM applicantdetails ad LEFT JOIN applicantdata dt ON ad.userID = dt.userID WHERE ad.userID = :userID";
        $vars = array(
            ':userID' => $applications[0]['userID'],
        );
        $userdata = $db->exec($query, $vars);
        $curlResponse['userdata'] = json_encode($userdata);
        $applicantID = $userdata[0]['applicantID'];

        $query = "SELECT * FROM applicants WHERE applicantID = :applicantID";
        $vars = array(
            ':applicantID' => $applicantID
        );
        $applicants_data = $db->exec($query, $vars);
        $curlResponse['applicants_data'] = json_encode($applicants_data);

        $query = "SELECT COUNT(*) AS bankAccountVerification FROM userChecklist WHERE userID = :userID AND title =:title";
        $vars = array(
            ':userID' => $applications[0]['userID'],
            ':title' => "bankName"
        );
        $banking = $db->exec($query, $vars);
        $curlResponse['banking'] = json_encode($banking);

        $query = "SELECT * FROM applicants WHERE applicantID = :applicantID";
        $vars = array(
            ':applicantID' => $applicantID,
        );
        $applicants = $db->exec($query, $vars);
        $curlResponse['applicants'] = json_encode($applicants);

        $query = "SELECT * FROM occupants WHERE applicantID = :applicantID";
        $vars = array(
            ':applicantID' => $applicantID,
        );
        $occupants = $db->exec($query, $vars);
        $curlResponse['occupants'] = json_encode($occupants);

        $query = "SELECT * FROM children WHERE applicantID = :applicantID";
        $vars = array(
            ':applicantID' => $applicantID,
        );
        $children = $db->exec($query, $vars);
        $curlResponse['children'] = json_encode($children);

        $query = "SELECT * FROM vehicles WHERE applicantID = :applicantID";
        $vars = array(
            ':applicantID' => $applicantID,
        );
        $vehicles = $db->exec($query, $vars);
        $curlResponse['vehicles'] = json_encode($vehicles);

        $query = "SELECT *  from pets WHERE applicantID = :applicantID ORDER BY id ASC;";
        $vars = array(
            ':applicantID' => $results[0]['id'],
        );
        $pets = $db->exec($query, $vars);
        $curlResponse['pets'] = json_encode($pets);

        $query = "SELECT * from emergencyContacts WHERE applicantID = :applicantID ORDER BY id ASC;";
        $vars = array(
            ':applicantID' =>  $results[0]['id'],
        );
        $emergency_contacts = $db->exec($query, $vars);
        $curlResponse['emergency_contacts'] = json_encode($emergency_contacts);

        $addition = '';
        if(count($applicants) > 0){
            for ($a=0; $a < count($applicants); $a++) { 
                $delimeter = ",";
                $addition = $addition."".$delimeter." (SELECT data FROM xdsdata WHERE userID = :userID AND dataLabel = 'totalIncomeApplicant".($a+1)."') AS totalIncomeApplicant".($a+1).", (SELECT data FROM xdsdata WHERE userID = :userID AND dataLabel = 'totalExpensesApplicant".($a+1)."') AS totalExpensesApplicant".($a+1)." ";
            }
        }

        $query = "SELECT (SELECT data FROM xdsdata WHERE userID = :userID AND dataLabel = :income) AS totalIncome, (SELECT data FROM xdsdata WHERE userID = :userID AND dataLabel =:expenses) AS totalExpenses".$addition;
        $vars = array(
            ':userID' => $applications[0]['userID'],
            ':income' => "totalIncome",
            ':expenses' => "totalExpenses"
        );
        $income = $db->exec($query, $vars);
        $curlResponse['income'] = json_encode($income);

        $query = "SELECT (SELECT data FROM xdsdata WHERE userID = :userID AND dataLabel =:firstName) AS firstName, (SELECT data FROM xdsdata WHERE userID = :userID AND dataLabel =:lastName) AS lastName, (SELECT data FROM xdsdata WHERE userID = :userID AND dataLabel =:idPassportNo) AS idPassportNo, (SELECT data FROM xdsdata WHERE userID = :userID AND dataLabel =:dob) AS dob, (SELECT data FROM xdsdata WHERE userID = :userID AND dataLabel =:address) AS address, (SELECT data FROM xdsdata WHERE userID = :userID AND dataLabel =:maritalStatus) AS maritalStatus, (SELECT data FROM xdsdata WHERE userID = :userID AND dataLabel =:bankName) AS bankName, (SELECT data FROM xdsdata WHERE userID = :userID AND dataLabel =:branchCode) AS branchCode, (SELECT data FROM xdsdata WHERE userID = :userID AND dataLabel =:accountNumber) AS accountNumber, (SELECT data FROM xdsdata WHERE userID = :userID AND dataLabel =:accountType) AS accountType";
        $vars = array(
            ':userID' => $applications[0]['userID'],
            ':firstName' => "firstName",
            ':lastName' => "lastName",
            ':idPassportNo' => "idPassportNo",
            ':dob' => "dob",
            ':address' => "address",
            ':maritalStatus' => "maritalStatus",
            ':bankName' => "bankName",
            ':branchCode' => "branchCode",
            ':accountNumber' => "accountNumber",
            ':accountType' => "accountType"
        );
        $personal = $db->exec($query, $vars);
        $curlResponse['personal'] = json_encode($personal);

        $query = "SELECT * FROM screeningAPIs WHERE enabled = :enabled";
        $vars = array(
            ':enabled' => "e"
        );
        $screeningAPIs = $db->exec($query, $vars);
        $curlResponse['screeningAPIs'] = json_encode($screeningAPIs);

        //Update User Rent Trench Rent Trench
        if(count($applicants) > 0){
            $trench_attributes['rentTrench'] = 1;
            $partnerIncome = 0;
            for ($i=0; $i < count($applicants); $i++) { 
                $partnerIncome = $partnerIncome + $income[0]['totalIncomeApplicant'.($i+1)];
            }
            $income_total = floatval($income[0]['totalIncome']) + floatval($partnerIncome);

            $query = "SELECT * FROM `rentTrench` where minIncome <= :minIncome AND maxIncome >= :maxIncome AND deleted= :isDeleted";
            $vars = array(
                ':minIncome' => $income_total,
                ':maxIncome' => $income_total,
                ':isDeleted' => "n",
            );
            $rentTrench = $db->exec($query, $vars);
            if(count($rentTrench) > 0){
                $trench_attributes['rentTrench'] = $rentTrench[0]['id'];
                if($income_total > 15000) {
                    $trench_attributes['housing'] = 'affordable';
                } else if ($income_total < 15000){
                    $trench_attributes['housing'] = 'social';
                }
                $definition = $this->getDefinition($trench_attributes);
                $idArray = ['userID =?', $applications[0]['userID']];
                $info = $this->getRepository("applicantdetails")->updateRecord($idArray, $definition);
            }
        } else {
            $trench_attributes['rentTrench'] = 1;
            $query = "SELECT * FROM `rentTrench` where minIncome <= :minIncome AND maxIncome >= :maxIncome AND deleted= :isDeleted";
            $vars = array(
                ':minIncome' => $income[0]['totalIncome'],
                ':maxIncome' => $income[0]['totalIncome'],
                ':isDeleted' => "n",
            );
            $rentTrench = $db->exec($query, $vars);

            if(count($rentTrench) > 0){
                $trench_attributes['rentTrench'] = $rentTrench[0]['id'];
                if($income[0]['totalIncome'] > 15000) {
                    $trench_attributes['housing'] = 'affordable';
                } else if ($income[0]['totalIncome'] < 15000){
                    $trench_attributes['housing'] = 'social';
                }
                $definition = $this->getDefinition($trench_attributes);
                $idArray = ['userID =?', $applications[0]['userID']];
                $info = $this->getRepository("applicantdetails")->updateRecord($idArray, $definition);
            }
        }
        echo json_encode($curlResponse);
    }

    public function approveApplication($f3, $params)
    {
        if($f3->get("POST")){
            global $db;
            $this->authCheck($f3);
            $curlResponse = [];
            $applicationID = $f3->get("POST.applicationID");

            $query = "SELECT md.*, ad.firstName, ad.email, ap.userID  FROM applications ap
                        LEFT JOIN rentalproperties rt ON ap.id = rt.applicationID
                        LEFT JOIN metaData md ON md.mainID = rt.id
                        LEFT JOIN applicantdetails ad ON ad.userID = ap.userID
                        WHERE ap.id = :id";
            $vars = array(
                ':id' => $applicationID,
            );
            $applicationsData = $db->exec($query, $vars);

            $userID = $applicationsData[0]['userID'];


            $query = "UPDATE applications SET applicationStatus = :applicationStatus, unitID = :unitID  WHERE id = :id";
            $vars = array(
                ':unitID' => $f3->get("POST.unit"),
                ':applicationStatus' => "a",
                ':id' => $applicationID,
            );
            $applicationsUpdate = $db->exec($query, $vars);

            $query = "UPDATE units SET occupied= :occupied, applicantID = :applicantID WHERE id = :id";
            $vars = array(
                ':occupied' => "y",
                ':applicantID' => $userID,
                ':id' => $f3->get("POST.unit"),
            );
            $unitUpdate = $db->exec($query, $vars);
            $curlResponse['message'] = json_encode(array('msg' => 'Application approved successfully! Email has been sent to applicant', 'alert' => 'success'));
            echo json_encode($curlResponse);
        }

    }

    public function verifyProofOfPayment($f3, $params)
    {
        if($f3->get("POST")){
            global $db;
            $curlResponse = [];
            $applicationID = $f3->get("POST.applicationID");
            $verification = $f3->get("POST.verification");
            $screeningapiID = $f3->get("POST.screeningapiID");

            $query = "SELECT md.*, ad.firstName, ad.email, ap.userID  FROM applications ap
            LEFT JOIN rentalproperties rt ON ap.id = rt.applicationID
            LEFT JOIN metaData md ON md.mainID = rt.id
            LEFT JOIN applicantdetails ad ON ad.userID = ap.userID
            WHERE ap.id = :id";
            $vars = array(
                ':id' => $applicationID,
            );
            $applicationsData = $db->exec($query, $vars);

            $userID = $applicationsData[0]['userID'];
            $query = "INSERT INTO userChecklist (userID, approvedBy, title, isVerified, applicationID, dateCreated, dateUpdated)
                    VALUES (:userID, :approvedBy, :title, :isVerified, :applicationID, :dateCreated, :dateUpdated)";
            $vars = array(
                ':userID' =>$userID,
                ':approvedBy' => $f3->get('POST.userID'),
                ':title' => "proofOfPayment",
                ':isVerified' => $verification,
                ':applicationID' => $applicationID,
                ':dateCreated' => date("Y-m-d H:i:s"),
                ':dateUpdated' => date("Y-m-d H:i:s")
            );
            $userChecklist = $db->exec($query, $vars);

            $query = "UPDATE applications SET applicationFee = :applicationFee, dateUpdated = :dateUpdated WHERE id =:id";
            $vars = array(
                ':applicationFee' => 'paid',
                ':dateUpdated' => date("Y-m-d H:i:s"),
                ':id' => $applicationID
            );
            $applicationFee = $db->exec($query, $vars);

            if($verification == "y"){
                $params['userID'] =  $userID;
                $params['screeningapiID'] =  $screeningapiID;

                $query = "SELECT * FROM screeningAPIs WHERE id = :id";
                $vars = array(
                    ':id' => $screeningapiID,
                );
                $screeningAPI = $db->exec($query, $vars);
                //screening verification methods
                if($screeningAPI[0]['type'] == 'xds'){
                    $this->xdsVerification($f3, $params);
                } else if($screeningAPI[0]['type'] == 'tpn'){
                    $this->tpnVerification($f3, $params);
                }
                $curlResponse['verification'] = "y";
            }
            echo json_encode($curlResponse);
        }
    }

    public function re_run_application_verification($f3, $params)
    {
        global $db;
        $applicationID = $params['id'];
        $query = "SELECT md.*, ad.firstName, ad.email, ap.userID  FROM applications ap
        LEFT JOIN rentalproperties rt ON ap.id = rt.applicationID
        LEFT JOIN metaData md ON md.mainID = rt.id
        LEFT JOIN applicantdetails ad ON ad.userID = ap.userID
        WHERE ap.id = :id";
        $vars = array(
            ':id' => $applicationID,
        );
        $applicationsData = $db->exec($query, $vars);
        $curlResponse['applicationsData'] = json_encode($applicationsData);

        $params['userID'] = $applicationsData[0]['userID'];
        $query = "SELECT * FROM screeningAPIs WHERE type = :type ORDER BY dateCreated DESC LIMIT 1";
        $vars = array(
            ':type' => 'tpn',
        );
        $screeningAPI = $db->exec($query, $vars);
        $params['screeningapiID'] = $screeningAPI[0]['id'];
        $curlResponse['params'] = json_encode($params);
        
        //verification methods
        //xds verification
        if($screeningAPI[0]['type'] == 'xds'){
            $this->xdsVerification($f3, $params);
        } else if($screeningAPI[0]['type'] == 'tpn'){
            $this->tpnVerification($f3, $params);
        }
    }

    public function verifyDepositProofOfPayment($f3, $params)
    {
        if($f3->get("POST")){
            global $db;
            $applicationID = $f3->get("POST.applicationID");
            $verification = $f3->get("POST.verification");
            $curlResponse = [];

            $query = "SELECT md.*, ad.id AS applicantID, ad.firstName, ad.email, ap.userID, ap.complexID, ap.unitID, ap.propertyReference, ap.id AS applicationID FROM applications ap
            LEFT JOIN rentalproperties rt ON ap.id = rt.applicationID
            LEFT JOIN metaData md ON md.mainID = rt.id
            LEFT JOIN applicantdetails ad ON ad.userID = ap.userID
            WHERE ap.id = :id";
            $vars = array(
                ':id' => $applicationID,
            );
            $applicationsData = $db->exec($query, $vars);
            $curlResponse['applicationsData'] = json_encode($applicationsData);
            $userID = $applicationsData[0]['userID'];

            $status = 'a';
            $attributes = ["applicationStatus" => $status, "dateUpdated" => date("Y-m-d")];
            $definition = $this->getDefinition($attributes);
            $idArray = ['id = ?', $applicationID];

            $user = $this->getRepository('applications')->updateRecord($idArray, $definition);

            $query = "SELECT * FROM userChecklist WHERE title = :title AND userID = :userID";
            $vars = array(
                ':title' => 'rentDeposit',
                ':userID' => $userID
            );
            $rentcheck = $db->exec($query, $vars);
            $update = false;
            if(count($rentcheck) >0){
                $update = true;
            }

            if($update){
                $query = "UPDATE userChecklist SET userID = :userID, approvedBy = :approvedBy, title = :title, isVerified = :isVerified, dateUpdated = NOW() WHERE id = :id";
                $vars = array(':userID' =>$userID,
                            ':approvedBy' => $f3->get('POST.userID'),
                            ':title' => "rentDeposit",
                            ':isVerified' => $verification,
                            ':id' => $rentcheck[0]['id']
                        );
                $userChecklist = $db->exec($query, $vars);
            } else {

                $query = "INSERT INTO userChecklist (userID, approvedBy, title, isVerified, dateCreated, dateUpdated)
                        VALUES (:userID, :approvedBy, :title, :isVerified, NOW(), NOW())";
                $vars = array(':userID' =>$userID,
                            ':approvedBy' => $f3->get('POST.userID'),
                            ':title' => "rentDeposit",
                            ':isVerified' => $verification,
                        );
                $userChecklist = $db->exec($query, $vars);
            }

            $query = "SELECT * FROM complex WHERE id = :complexID";
            $vars = array(
                ':complexID' => $applicationsData[0]['complexID']
            );
            $complex = $db->exec($query, $vars);
            $curlResponse['complex'] = json_encode($complex);

            $query = "SELECT * FROM fixedCharges WHERE isDeleted = :isDeleted";
            $vars = array(
                ':isDeleted' => 'n',
            );
            $fixedChargeCosts = $db->exec($query, $vars);
            $curlResponse['fixedChargeCosts'] = json_encode($fixedChargeCosts);
            $fixedChargesString = '';
            $fixedCharges = '';

            foreach ($fixedChargeCosts as $key => $charge) {
                $totalfixedChargesCosts = $totalfixedChargesCosts + (float) $charge['cost'];
                $fixedCharges = $fixedCharges.' & '.$charge['chargeName']. ' R '.$charge['cost'];
                $fixedChargesString = $fixedChargesString. ' & '.$charge['chargeName'];
            }

            $query = "SELECT * FROM charges WHERE propertyReference = :propertyReference AND isDeleted = :isDeleted";
            $vars = array(
                ':propertyReference' => $applicationsData[0]['propertyReference'],
                ':isDeleted' => 'n',
            );
            $additionChargeCosts = $db->exec($query, $vars);
            $curlResponse['additionChargeCosts'] = json_encode($additionChargeCosts);
            $additionalString = '';
            $additional = '';

            foreach ($additionChargeCosts as $key => $charge) {
                $totalAdditionalCosts = $totalAdditionalCosts + (float) $charge['cost'];
                $additional = $additional.' & '.$charge['chargeName']. ' R '.$charge['cost'];
                $additionalString = $additionalString. ' & '.$charge['chargeName'];
            }

            $query = "SELECT * FROM rentalproperties WHERE applicantID = :applicantID AND applicationID = :applicationID";
            $vars = array(
                ':applicantID' => $applicationsData[0]['applicantID'],
                ':applicationID' => $applicationsData[0]['applicationID'],
            );
            $amount = $db->exec($query, $vars);
            $totalAmount = round((float) $amount[0]['rentDeposit'] + (float) $amount[0]['unitRental'] + (float) $amount[0]['lease'] + (float) $totalfixedChargesCosts + (float) $totalAdditionalCosts, 2);

            $query = "INSERT INTO invoices (userID, recipient, description, status, invoiceTotal, paid, invoiceType, dateCreated) 
                                VALUES (:userID, :recipient, :description, :status, :invoiceTotal, :paid, :invoiceType, NOW())";
            $vars = array(':userID' =>$userID,
                ':recipient' =>  "The Good People Data Company",
                ':description' => "Deposit/Lease & 1 Month Rental Invoice". $fixedChargesString."".$additionalString,
                ':status' =>  "paid",
                ':invoiceTotal' =>  $totalAmount,
                ':paid' =>  $totalAmount,
                ':invoiceType' =>  1,
            );
            $invoice = $db->exec($query, $vars);

            //Email Tenant Approval Email
            $userdata = [];
            foreach($applicationsData as $row){
                $userdata[$row["dataLabel"]] = $row["data"];
                $userdata["email"] = $row["email"];
                $userdata["firstName"] = $row["firstName"];
            }

            //invoice
            $query = "SELECT ad.id AS applicantID, ad.email, ad.firstName AS tenant_name, ad.lastName AS tenant_surname, ad.address AS tenant_address,
             ad.phone AS tenant_phone, ad.totalIncome, uc.title, uc.dateCreated AS issue_date, inv.recipient AS company_name, inv.description AS invoice_description, 
             inv.paid AS amount_total, inv.id AS invoice_number, ap.unitID, ap.complexID ,
             ap.propertyReference
             FROM applicantdetails ad 
             INNER JOIN userChecklist uc ON ad.userID = uc.userID 
             AND uc.title = :title AND uc.isVerified = :isVerified 
             INNER JOIN invoices inv ON ad.userID = inv.userID AND inv.invoiceType = :invoiceType 
             INNER JOIN applications ap ON ad.userID = ap.userID 
             WHERE ad.userID = :userID 
             ORDER BY ad.id DESC";
            $vars = array(
                ':userID' => $userID,
                ':title' => 'rentDeposit',
                ':isVerified' => 'y',
                ':invoiceType' => 1
            );
            $invoice = $db->exec($query, $vars);
            $curlResponse['depositInvoice'] = json_encode($invoice);

            $query = "INSERT INTO applicantunitallocation (name, applicantID, propertyReference, dateCreated, dateUpdated) VALUE (:name, :applicantID, :propertyReference, NOW(), NOW())";
            $vars = array(
                ':name' => $invoice[0]['tenant_name'],
                ':applicantID' => $invoice[0]['applicantID'],
                ':propertyReference' => $invoice[0]['propertyReference'],
            );
            $allocateUnit = $db->exec($query, $vars);

            $query = "SELECT * FROM rentalproperties WHERE propertyReference = :propertyReference";
            $vars = array(
                ':propertyReference' => $invoice[0]['propertyReference'],
            );
            $rentalproperty = $db->exec($query, $vars);
            $curlResponse['rentalproperty'] = json_encode($rentalproperty);

            $query = "SELECT * FROM paymentsettings";
            $paymentsettings = $db->exec($query);
            $curlResponse['paymentsettings'] = json_encode($paymentsettings);


            $query = "UPDATE rentalproperties SET occupied = :occupied, available = :available WHERE propertyReference = :propertyReference";
            $vars = array(
                ':occupied' => 'y',
                ':available' => 'n',
                ':propertyReference' => $invoice[0]['propertyReference'],
            );
            $occupied = $db->exec($query, $vars);
            echo json_encode($curlResponse);
        }
    }

    public function inviteTenant($f3)
    {
        global $db;
        $this->authCheck($f3);
        $curlResponse = [];

        $viewing = $f3->get("POST.viewingDate");
        $viewingDate = $f3->get('POST.visitDate');

        $applicationID = $f3->get("POST.id");
        $complexID = $f3->get("POST.complexID");

        $query = "UPDATE rentalproperties SET applicationID = :applicationID, dateUpdated = NOW() WHERE propertyReference = :propertyReference";
        $vars = array(
            ':applicationID' => $applicationID,
            ':propertyReference' => $f3->get("POST.propertyReference"),
        );
        $rentalproperties = $db->exec($query, $vars);

        $query = "SELECT md.*, ad.firstName, ad.email, ap.userID  FROM applications ap
                    LEFT JOIN rentalproperties rt ON ap.id = rt.applicationID
                    LEFT JOIN metaData md ON md.mainID = rt.id
                    LEFT JOIN applicantdetails ad ON ad.userID = ap.userID
                    WHERE ap.id = :id";
        $vars = array(
            ':id' => $applicationID,
        );
        $applicationsData = $db->exec($query, $vars);
        $curlResponse['applicationsData'] = json_encode($applicationsData);

        $query = "SELECT rp.*, pa.firstName, pa.lastName FROM rentalproperties rp
        LEFT JOIN propertyagents pa ON pa.id = rp.agentID
        WHERE rp.applicationID = :applicationID;";
        $vars = array(
            ':applicationID' => $applicationID,
        );
        $rentalproperty = $db->exec($query, $vars);
        $curlResponse['rentalproperty'] = json_encode($rentalproperty);

        $query = "SELECT * FROM visits WHERE propertyReference = :propertyReference AND applicationID = :applicationID ORDER BY id DESC";
        $vars = array(
            ':propertyReference' => $rentalproperty[0]['propertyReference'],
            ':applicationID' => $applicationID,
        );
        $visit = $db->exec($query, $vars);

        if(count($visit)>0){
            $query = "UPDATE visits SET viewingDate = :viewingDate WHERE id = :id";
            $vars = array(
                ':viewingDate' => $viewingDate,
                ':id' => $visit[0]['id'],
            );
            $viewing_visit = $db->exec($query, $vars);
        } else {
            $query = "INSERT INTO visits (propertyReference, userID, applicationID, viewingDate, dateCreated) VALUES (:propertyReference, :userID, :applicationID, :viewingDate, NOW())";
            $vars = array(
                ':propertyReference' => $rentalproperty[0]['propertyReference'],
                ':userID' => $applicationsData[0]['userID'],
                ':applicationID' => $applicationID,
                ':viewingDate' => $viewingDate
            );
            $viewing_visit = $db->exec($query, $vars);
        }
   
        $subject = "Unit Viewing Invitation";
        $userdata = [];
        foreach($applicationsData as $row){
            $userdata[$row["dataLabel"]] = $row["data"];
            $userdata["email"] = $row["email"];
            $userdata["firstName"] = $row["firstName"];
        }

        $query = "SELECT * FROM complex WHERE id = :complexID";
        $vars = array(
            ':complexID' => $complexID
        );
        $complex = $db->exec($query, $vars);
        $curlResponse['complex'] = json_encode($complex);


        $userdata["complexName"] = $complex[0]['region']. ' ' .$complex[0]['complexName'];
        $query = "SELECT * from applicantdetails WHERE userID = :userID";
        $vars = array(
            ':userID' => $applicationsData[0]['userID'],
        );
        $applicantdetails = $db->exec($query, $vars);        
        $userdata['tenantType'] = ucfirst($applicantdetails[0]['housing']);
        $curlResponse['applicantdetails'] = json_encode($complex);

        $status = 'i';
        $query = "UPDATE applications SET propertyReference = :propertyReference, applicationStatus = :applicationStatus, dateUpdated = NOW() WHERE id = :applicationID";
        $vars = array(
            ':propertyReference' => $rentalproperty[0]['propertyReference'],
            ':applicationStatus' => $status,
            ':applicationID' => $applicationID,
        );
        $invite = $db->exec($query, $vars);
        echo json_encode($curlResponse);
    }

    public function share_application($f3)
    {
        global $db;
        $curlResponse = [];
        $applicationID = $f3->get("POST.applicationID");

        $query = "SELECT ad.*, ap.id AS applicationID, ap.propertyReference, pd.dataType, pd.dataLabel, pd.data, ap.applicationStatus  
        FROM applications ap 
        LEFT JOIN applicantdetails ad ON ap.userID = ad.userID 
        LEFT JOIN applicantdata pd ON ap.userID = pd.userID 
        WHERE ap.id = :id";
        $vars = array(
            ':id' => $applicationID,
        );
        $applicationsData = $db->exec($query, $vars);
        $curlResponse['applicationsData'] = json_encode($applicationsData);

        $query = "SELECT * FROM rentalproperties WHERE applicationID = :applicationID";
        $vars = array(
            ':applicationID' => $applicationID,
        );
        $property_owner = $db->exec($query, $vars);
        $curlResponse['property_owner'] = json_encode($property_owner);
        echo json_encode($curlResponse);
    }

    public function generateCreditReport($f3, $params)
    {
        global $db;
        $logindetails['userID'] = $params['adminID'];
        $screeningapiID = $params['apiID'];

        $query = "SELECT *  from applicantdetails WHERE userID = :id";
        $vars = array(
            ':id' => $params['userID'],
        );
        $results = $db->exec($query, $vars);

        $query = "SELECT * FROM screeningAPIs WHERE id = :id";
        $vars = array(
            ':id' => $screeningapiID,
        );
        $screeningAPI = $db->exec($query, $vars);
        $logindetails['strUser'] = $screeningAPI[0]['username'];
        $logindetails['strPwd'] = $screeningAPI[0]['password'];
        $logindetails['SecurityCode'] = $screeningAPI[0]['securityCode'];
        $logindetails['tpnUrl'] = $screeningAPI[0]['url'];
        if($screeningAPI[0]['type']=='xds'){
            $ConnectTicket = $this->xdsLogin($f3, $logindetails);
        } else if($screeningAPI[0]['type']=='tpn'){
            $ConnectTicket = $this->tpnLogin($f3, $logindetails);
        }

        $query = "SELECT * from apiTickets WHERE connectTicket = :connectTicket";
        $vars = array(
            ':connectTicket' => $ConnectTicket,
        );
        $apiTickets = $db->exec($query, $vars);
        $ticketID = $apiTickets[0]['id'];

        $creditreportdetails['creditIDNo'] = $params['idnumber'];
        $creditreportdetails['idNumber'] = $params['idnumber'];
        $creditreportdetails['IDNumber'] = $params['idnumber'];
        $creditreportdetails['userID'] = $params['adminID'];
        $creditreportdetails['ConnectTicket'] = $ConnectTicket;
        $creditreportdetails['ticketID'] = $ticketID;

        $creditreportdetails["firstName"] = $results[0]["firstName"];
        $creditreportdetails["lastName"] = $results[0]["lastName"];
        $creditreportdetails['occupation'] = $results[0]["occupation"];
        $creditreportdetails['employer'] = $results[0]["employer"];
        $creditreportdetails['income'] = $results[0]["totalIncome"];
        $creditreportdetails['phone'] = $results[0]["phone"];
        $creditreportdetails['address'] = $results[0]["address"];
        $creditreportdetails['SecurityCode'] = $screeningAPI[0]['securityCode'];
        $creditreportdetails['tpnUrl'] = $screeningAPI[0]['url'];

        if($screeningAPI[0]['type']=='xds'){
            $getCreditReport = $this->getCreditReportPdf($f3, $creditreportdetails);
        } else if($screeningAPI[0]['type']=='tpn'){
            $getCreditReport = $this->tpnCreditReport($f3, $creditreportdetails);
        }

        if($getCreditReport=='Error While Processing the Request, Please contact TPN' || $getCreditReport=='Error While Processing the Request, Please contact XDS'){

        } else {
            $query = "UPDATE applicantdata SET data = :data, dateCreated = NOW(), dateUpdated = NOW() WHERE dataLabel = :dataLabel AND id = :id;";
            $vars = array(
                ':id' => $params['id'],
                ':data' => $getCreditReport,
                ':dataLabel' => 'creditReport',
            );
            $applicantdata = $db->exec($query, $vars);
            $updateID = $db->lastInsertId();
        }
        echo json_encode($updateID);
    }

    public function verifyBankingDetails($f3, $params)
    {
        global $db;
        $logindetails['userID'] = $params['userID'];
        $screeningapiID = $params['apiID'];

        $query = "SELECT *  from applicantdetails WHERE userID = :id";
        $vars = array(
            ':id' => $params['userID'],
        );
        $results = $db->exec($query, $vars);

        $query = "SELECT * FROM screeningAPIs WHERE id = :id";
        $vars = array(
            ':id' => $screeningapiID,
        );
        $screeningAPI = $db->exec($query, $vars);
        
        $logindetails['strUser'] = $screeningAPI[0]['username'];
        $logindetails['strPwd'] = $screeningAPI[0]['password'];
        $logindetails['SecurityCode'] = $screeningAPI[0]['securityCode'];
        $logindetails['tpnUrl'] = $screeningAPI[0]['url'];

        if($screeningAPI[0]['type']=='xds'){
            $ConnectTicket = $this->xdsLogin($f3, $logindetails);
        } else {
            $ConnectTicket = $this->tpnLogin($f3, $logindetails);
        }

        $query = "SELECT * from apiTickets WHERE connectTicket = :connectTicket";
        $vars = array(
            ':connectTicket' => $ConnectTicket,
        );
        $apiTickets = $db->exec($query, $vars);
        $ticketID = $apiTickets[0]['id'];

        $bankreportdetails['creditIDNo'] = $results[0]["idPassportNo"];
        $bankreportdetails['idNumber'] = $results[0]["idPassportNo"];
        $bankreportdetails['IDNumber'] = $results[0]["idPassportNo"];
        $bankreportdetails['userID'] = $params['userID'];
        $bankreportdetails['ConnectTicket'] = $ConnectTicket;
        $bankreportdetails['ticketID'] = $ticketID;
        $bankreportdetails['strUser'] = $screeningAPI[0]['username'];
        $bankreportdetails['strPwd'] = $screeningAPI[0]['password'];
        $bankreportdetails['SecurityCode'] = $screeningAPI[0]['securityCode'];
        $bankreportdetails['tpnUrl'] = $screeningAPI[0]['url'];
        $bankreportdetails["firstName"] = $results[0]["firstName"];
        $bankreportdetails["lastName"] = $results[0]["lastName"];
        $bankreportdetails['occupation'] = $results[0]["occupation"];
        $bankreportdetails['employer'] = $results[0]["employer"];
        $bankreportdetails['income'] = $results[0]["totalIncome"];
        $bankreportdetails['phone'] = $results[0]["phone"];
        $bankreportdetails['address'] = $results[0]["address"];

        $bankreportdetails['AccountNumber'] = $results[0]["accountNumber"];
        $bankreportdetails['Bank'] = $results[0]["bankName"];
        $bankreportdetails['Branch'] = "";
        $bankreportdetails['BranchCode'] = $results[0]["branchCode"];
        $bankreportdetails['AccountHolder'] = $results[0]["firstName"]. ' '.$results[0]["firstName"];
        if($results[0]["accountType"]=='CURRENTCHEQUEACCOUNT'){
            $type = 'Cheque/Current';
        } else if($results[0]["accountType"]=='SAVINGSACCOUNT'){
            $type = 'Savings';
        } else if($results[0]["accountType"]=='TRANSMISSION'){
            $type = 'Transmission';
        }
        $bankreportdetails['AccountType'] = $type;
        $getBankVerificationReport = $this->tpnBankVerification($f3, $bankreportdetails);

        if($getBankVerificationReport=='Error While Processing the Request, Please contact TPN'){

        } else {
            $query = "UPDATE applicantdata SET data = :data, dateCreated = NOW(), dateUpdated = NOW() WHERE dataLabel = :dataLabel AND id = :id;";
            $vars = array(
                ':id' => $params['id'],
                ':data' => $getBankVerificationReport,
                ':dataLabel' => 'tpnBankDetails',
            );
            $applicantdata = $db->exec($query, $vars);
            $updateID = $db->lastInsertId();

            echo json_encode($updateID);
        }
    }

    public function assignUnit($f3)
    {
        global $db;
        $this->authCheck($f3);
        $curlResponse = [];

        $applicationID = $f3->get("POST.applicationID");
        $complexID = $f3->get("POST.complexID");
        $unitID = $f3->get("POST.unit");
        $housing = $f3->get("POST.housing");

        $mainID = $f3->get("POST.propertyReferenceID");
        $resultUrl = (array) json_decode($f3->get('POST.files'));
        $docsArr = (array) json_decode($f3->get('POST.docsArr'));

        $output = array();
        $count = 0;
        foreach ($resultUrl as $fileUrl => $bool) {
            $update = false;
            $formFieldName = $docsArr[$count];

                $query = "INSERT INTO metaData (mainID, dataType, data, dataLabel, dateCreated, dateUpdated)
                    VALUES (:mainID, :dataType, :data, :dataLabel, NOW(), NOW())";
                $vars = array(
                    ':mainID' => $mainID,
                    ':dataType' => "document",
                    ':data' => $fileUrl,
                    ':dataLabel' => $formFieldName,
                );

            $docsData = $db->exec($query, $vars);
            $count++;
        }

        $status = 's';
        $propertyOwnerApproval = 'p';
        $attributes = ["propertyReference" => $f3->get("POST.propertyReference"), "propertyOwnerApproval" => $propertyOwnerApproval, "applicationStatus" => $status, "dateUpdated" => date("Y-m-d")];
        $definition = $this->getDefinition($attributes);
        $idArray = ['id = ?', $applicationID];
        $user = $this->getRepository('applications')->updateRecord($idArray, $definition);

        $query = "UPDATE rentalproperties SET propertyReference = :propertyReference, propertyDescription = :propertyDescription, agentFullName = :agentFullName, rentDeposit = :rentDeposit, lease = :lease, unitRental = :unitRental, ownerFullName = :ownerFullName, ownerEmail = :ownerEmail, ownerContactNumber = :ownerContactNumber, dateUpdated = NOW() WHERE applicationID = :applicationID";
        $vars = array(
            ':propertyReference' => $f3->get("POST.propertyReference"),
            ':propertyDescription' => $f3->get("POST.propertyDescription"),
            ':agentFullName' => $f3->get("POST.agentFullName"),
            ':rentDeposit' => $f3->get("POST.rentDeposit"),
            ':lease' => $f3->get("POST.lease"),
            ':unitRental' => $f3->get("POST.unitRental"),
            ':ownerFullName' => $f3->get("POST.ownerFullName"),
            ':ownerEmail' => $f3->get("POST.ownerEmail"),
            ':ownerContactNumber' => $f3->get("POST.ownerContactNumber"),
            ':applicationID' => $applicationID,
        );
        $rentalproperties = $db->exec($query, $vars);

        

        $query = "SELECT md.*, ad.firstName, ad.email, ap.userID  FROM applications ap
                    LEFT JOIN rentalproperties rt ON ap.id = rt.applicationID
                    LEFT JOIN metaData md ON md.mainID = rt.id
                    LEFT JOIN applicantdetails ad ON ad.userID = ap.userID
                    WHERE ap.id = :id";
        $vars = array(
            ':id' => $applicationID,
        );
        $applicationsData = $db->exec($query, $vars);
        $curlResponse['applicationsData'] = json_encode($applicationsData);

        $query = "SELECT * FROM complex WHERE id = :complexID";
        $vars = array(
            ':complexID' => $complexID
        );
        $complex = $db->exec($query, $vars);
        $curlResponse['complex'] = json_encode($complex);

        $query = "SELECT * FROM units WHERE id = :unitID";
        $vars = array(
            ':unitID' => $unitID
        );
        $unit = $db->exec($query, $vars);
        $curlResponse['unit'] = json_encode($unit);

        $query = "SELECT * FROM rentTrench WHERE id = :trenchID";
        $vars = array(
            ':trenchID' => $unit[0]['trenchID'],
        );
        $trench = $db->exec($query, $vars);
        $curlResponse['trench'] = json_encode($trench);

        $query = "UPDATE applicantdetails SET housing = :housing WHERE userID = :userID";
        $vars = array(
            ':housing' => $housing,
            ':userID' => $applicationsData[0]['userID'],
        );
        $update = $db->exec($query, $vars);

        $query = "SELECT * FROM applicantdetails WHERE userID = :userID";
        $vars = array(
            ':userID' => $applicationsData[0]['userID'],
        );
        $details = $db->exec($query, $vars);
        $curlResponse['details'] = json_encode($details);

        $query = "SELECT * FROM rentalproperties WHERE applicationID = :applicationID";
        $vars = array(
            ':applicationID' => $applicationID,
        );
        $rentalproperty = $db->exec($query, $vars);
        $curlResponse['rentalproperty'] = json_encode($rentalproperty);

        $query = "SELECT * FROM charges WHERE propertyReference = :propertyReference AND isDeleted = :isDeleted";
        $vars = array(
            ':propertyReference' => $rentalproperty[0]['propertyReference'],
            ':isDeleted' => 'n',
        );
        $additionChargeCosts = $db->exec($query, $vars);
        $curlResponse['additionChargeCosts'] = json_encode($additionChargeCosts);

        $query = "SELECT * FROM fixedCharges WHERE isDeleted = :isDeleted";
        $vars = array(
            ':isDeleted' => 'n',
        );
        $fixedChargeCosts = $db->exec($query, $vars);
        $curlResponse['fixedChargeCosts'] = json_encode($fixedChargeCosts);

        echo json_encode($curlResponse);
    }
}
