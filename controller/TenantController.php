<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class TenantController extends AppController
{

    public function spouse_consent($f3, $params)
    {
        global $db;
        #approve credit cheque
        $applicantID = $params['id'];
        $attributes['id'] = $applicantID;
        $attributes['consent'] = 'y'; 

        $query = "UPDATE applicants SET consent=:consent, dateUpdated=NOW() WHERE id=:applicantID";
        $varsArr = array(
            ':consent' => 'y',
            ':applicantID' => $applicantID
        );
        $applicant_consent = $db->exec($query, $varsArr);   
    }

    public function saveApplication($f3, $params, $applicantID, $fileUrl)
    {
        date_default_timezone_set('Africa/Johannesburg');
        global $db, $system_enviroment;

        $query = "SELECT * FROM rentalproperties WHERE propertyReference = :propertyReference AND deleted = :isDeleted;";
        $vars = array(
            ':propertyReference' => $f3->get("POST.propertyReference"),
            ':isDeleted' => 'n'
        );
        $rental_property = $db->exec($query, $vars);

        //SAVE APPLICATION
        $query = "INSERT INTO applications (userID, propertyReference, agentID, dateCreated, dateUpdated) VALUES (:userID, :propertyReference, :agentID, :dateCreated, :dateUpdated) ";
        $vars = array(
            ':userID' => $f3->get("POST.userID"),
            ':propertyReference' => $f3->get("POST.propertyReference"),
            ':agentID' => $rental_property[0]['agentID'],
            ':dateCreated' => date("Y-m-d H:i:s"),
            ':dateUpdated' => date("Y-m-d H:i:s")
        );
        $userdata = $db->exec($query, $vars);
        $applicationID = $db->lastInsertId();

        //UPDATE RENTAL PROPERTY WITH APPLICATION ID
        $query = "UPDATE rentalproperties SET applicationID = :applicationID, applicantID = :applicantID 
        WHERE id = :id;";
        $vars = array(
            ':applicationID' => $applicationID,
            ':applicantID' => $applicantID,
            ':id' => $rental_property[0]['id']
        );
        $update = $db->exec($query, $vars);

        //UPDATE APPLICATION DETAILS WITH NEW APPLICATION ID
        $query = "UPDATE applicantdetails SET applicationID = :applicationID, dateUpdated = :dateUpdated  
        WHERE userID = :userID;";
        $vars = array(
            ':applicationID' => $applicationID,
            ':dateUpdated' => date("Y-m-d H:i:s"),
            ':userID' => $f3->get("POST.userID")
        );
        $update_applicant_details = $db->exec($query, $vars);

        $query = "INSERT INTO applicantdata (userID, data, dataType, dataLabel, applicationID, dateCreated, dateUpdated)
        VALUES (:userID, :data, :dataType, :dataLabel, :applicationID, :dateCreated, :dateUpdated)";
        $vars = array(
            ':userID' => $f3->get("POST.userID"),
            ':data' => $fileUrl,
            ':dataType' => 'proofOfPayment',
            ':dataLabel' => 'proofOfPayment',
            ':applicationID' => $applicationID,
            ':dateCreated' => date("Y-m-d H:i:s"),
            ':dateUpdated' => date("Y-m-d H:i:s")
        );
        $applicantdata = $db->exec($query, $vars);

        $query = "INSERT INTO applicantdata (userID, data, dataType, dataLabel, applicationID, dateCreated, dateUpdated)
                    VALUES (:userID, :data, :dataType, :dataLabel, :applicationID, :dateCreated, :dateUpdated)";
        $vars = array(
            ':userID' => $f3->get("POST.userID"),
            ':data' => "Yes",
            ':dataType' => 'creditCheckConsent',
            ':dataLabel' => 'checkbox',
            ':applicationID' => $applicationID,
            ':dateCreated' => date("Y-m-d H:i:s"),
            ':dateUpdated' => date("Y-m-d H:i:s")
        );
        $applicantdata = $db->exec($query, $vars);

        $query = "INSERT INTO applicantdata (userID, data, dataType, dataLabel, applicationID, dateCreated, dateUpdated)
                    VALUES (:userID, :data, :dataType, :dataLabel, :applicationID, :dateCreated, :dateUpdated)";
        $vars = array(
            ':userID' => $f3->get("POST.userID"),
            ':data' => "Yes",
            ':dataType' => 'referenceCheck',
            ':dataLabel' => 'checkbox',
            ':applicationID' => $applicationID,
            ':dateCreated' => date("Y-m-d H:i:s"),
            ':dateUpdated' => date("Y-m-d H:i:s")
        );
        $applicantdata = $db->exec($query, $vars); 

        $query = "UPDATE userChecklist SET isVerified = :isVerified WHERE userID = :userID AND applicationID IS NULL;";
        $vars = array(
            ':isVerified' => 'n',
            ':userID' => $f3->get("POST.userID"),
        );
        $update = $db->exec($query, $vars);

        //INSERT INTO CHECKLIST
        $query = "INSERT INTO userChecklist (userID, approvedBy, title, isVerified, applicationID, dateCreated, dateUpdated)
                            VALUES (:userID, :approvedBy, :title, :isVerified, :applicationID, :dateCreated, :dateUpdated)";
        $vars = array(
            ':userID' => $f3->get("POST.userID"),
            ':approvedBy' => "onlinePortal",
            ':title' => "email",
            ':isVerified' => "y",
            ':applicationID' => $applicationID,
            ':dateCreated' => date("Y-m-d H:i:s"),
            ':dateUpdated' => date("Y-m-d H:i:s")
        );
        $userChecklist = $db->exec($query, $vars);

        $query = "INSERT INTO userChecklist (userID, approvedBy, title, isVerified, applicationID, dateCreated, dateUpdated)
                            VALUES (:userID, :approvedBy, :title, :isVerified, :applicationID, :dateCreated, :dateUpdated)";
        $vars = array(
            ':userID' => $f3->get("POST.userID"),
            ':approvedBy' => "onlinePortal",
            ':title' => "phone",
            ':isVerified' => "y",
            ':applicationID' => $applicationID,
            ':dateCreated' => date("Y-m-d H:i:s"),
            ':dateUpdated' => date("Y-m-d H:i:s")
        );
        $userChecklist = $db->exec($query, $vars);

        $query = "SELECT ap.id AS applicationID, cp.propertyReference as complexName, us.email, concat(ad.firstName, ' ', ad.lastName) AS firstName from applications ap 
        LEFT JOIN rentalproperties cp ON ap.id = cp.applicationID AND cp.deleted = :deleted
        LEFT JOIN users us ON ap.userID = us.id 
        LEFT JOIN applicantdetails ad ON ap.userID = ad.userID
        WHERE ap.userID = :id AND ap.id = :applicationID;";
        $vars = array(
            ':id' => $f3->get("POST.userID"),
            ':deleted' => 'n',
            ':applicationID' => $applicationID,
        );
        $user_details = $db->exec($query, $vars);

        $attributes = [];
        $attributes = $user_details[0];
        $subject = "New Application Submission";

        if($system_enviroment!='local'){
            $query = "SELECT * FROM propertyagents WHERE id = :agentID AND isDeleted= :isDeleted";
            $vars = array(
                ":agentID" => $rental_property[0]['agentID'],
                ':isDeleted' => "n"
            );
            $property_agent = $db->exec($query, $vars);
            
            if(count($property_agent)>0){
                if($property_agent[0]['email']){
                    $this->dispatchApplicationEmail($attributes, $subject, "application", $property_agent[0]['email']);
                }
            }

            $query = "SELECT * FROM applicationnotificationemails WHERE isDeleted= :isDeleted";
            $vars = array(
                ':isDeleted' => "n",
            );
            $applicationnotificationemails = $db->exec($query, $vars);

            foreach ($applicationnotificationemails as $send_mail) {
                $this->dispatchApplicationEmail($attributes, $subject, "application", $send_mail['email']); 
            }
        } else {
            $this->dispatchApplicationEmail($attributes, $subject, "application", 'mmupfururirwa@smartviewtechnology.co.za');
        }
    }

    public function submitApplication($f3, $userID, $complexID, $applicationFeeStatus)
    {
        date_default_timezone_set('Africa/Johannesburg'); //or change to whatever timezone you want
        global $db;
        $this->authCheck($f3);
        $query = "INSERT INTO applications (userID, complexID, applicationFee, dateCreated, dateUpdated) VALUES(:userID, :complexID, :applicationFeeStatus, :dateCreated, :dateUpdated) ";
        $vars = array(
            ':userID' => $userID,
            ':complexID' => $complexID,
            ':applicationFeeStatus' => $applicationFeeStatus,
            ':dateCreated' => date("Y-m-d H:i:s"),
            ':dateUpdated' => date("Y-m-d H:i:s")
        );
        $userdata = $db->exec($query, $vars);

        $f3->set('SESSION.message', array('msg' => 'Application saved successfully!', 'alert' => 'info'));
    }

    public function uploadDocuments($f3, $params)
    {
        global $db;
        $this->authCheck($f3);
        $query = "SELECT dt.*  FROM applicantdetails ad LEFT JOIN applicantdata dt ON ad.userID = dt.userID WHERE ad.userID = :userID";
        $vars = array(
            ':userID' => $f3->get("POST.userID"),
        );
        $userdata = $db->exec($query, $vars);
        $folder = "user_" . $f3->get("POST.userID");
        $docsArr = [];

        #Upload Documents
        foreach ($_FILES as $formFieldName => $value) {
            if ($value['name'] != '') {
                $resultUrl = $this->fileUpload($f3, $formFieldName, $folder);
                $docsArr[] = $formFieldName;
            }
        }
        $count = 0;
        foreach ($resultUrl as $fileUrl => $bool) {
            $update = false;
            $formFieldName = $docsArr[$count];

            foreach ($userdata as $row) {
                if ($row['dataLabel'] == $formFieldName) {
                    $update = true;
                }
            }
            if ($update) {
                $query = "UPDATE applicantdata SET data=:data WHERE userID = :userID AND dataLabel=:dataLabel";
                $vars = array(
                    ':data' => $fileUrl,
                    ':userID' => $f3->get("POST.userID"),
                    ':dataLabel' => $formFieldName,
                );
                $applicantdata = $db->exec($query, $vars);
            } else {
                $query = "INSERT INTO applicantdata (userID, data, dataType, dataLabel, dateCreated, dateUpdated)
                            VALUES (:userID, :data, :dataType, :dataLabel, :dateCreated, :dateUpdated)";
                $vars = array(
                    ':userID' => $f3->get("POST.userID"),
                    ':data' => $fileUrl,
                    ':dataType' => "document",
                    ':dataLabel' => $formFieldName,
                    ':dateCreated' => date("Y-m-d H:i:s"),
                    ':dateUpdated' => date("Y-m-d H:i:s")
                );
                $applicantdata = $db->exec($query, $vars);
            }
            $count++;
        }
    }

    public function createApplication($f3, $params)
    {
        date_default_timezone_set('Africa/Johannesburg'); //or change to whatever timezone you want
        global $db;
        $this->authCheck($f3);
        $query = "INSERT INTO applications (userID, propertyReference, agentID, dateCreated, dateUpdated) VALUES(:userID, :propertyReference, :agentID, :dateCreated, :dateUpdated) ";
        $vars = array(
            ':userID' => $f3->get("POST.userID"),
            ':propertyReference' => $f3->get("POST.propertyReference"),
            ':agentID' => $f3->get("POST.agentID"),
            ':dateCreated' => date("Y-m-d H:i:s"),
            ':dateUpdated' => date("Y-m-d H:i:s")
        );
        $application = $db->exec($query, $vars);
        $applicationID = $db->lastInsertId();

        $query = "SELECT * FROM propertyagents WHERE id = :agentID AND isDeleted = :isDeleted";
        $varsArr = array(
            ':agentID' => $f3->get("POST.agentID"),
            ':isDeleted' => 'n'
        );
        $property_agent = $db->exec($query, $varsArr); 

        $query = "UPDATE rentalproperties SET applicationID = :applicationID, propertyReference = :propertyReference, agentFullName = :agentFullName WHERE applicantID = :applicantID AND available = :available AND deleted = :deleted";
        $vars = array(
            ':applicationID' => $applicationID,
            ':propertyReference' => $f3->get("POST.propertyReference"),
            ':agentFullName' => $property_agent[0]['firstName']. " ".$property_agent[0]['lastName'],
            ':applicantID' => $f3->get("POST.applicantID"),
            ':available' => 'y',
            ':deleted' => 'n'
        );
        $property = $db->exec($query, $vars);
    }

    public function formPost($f3)
    {
        try {
            global $db;
            $userID = $f3->get('POST.userID');
            $attributes = $f3->get('POST');

            //get form fields
            $query = "SELECT * FROM fields WHERE formID = :formID ORDER BY id DESC";
            $vars = array(
                ':formID' => $attributes['formID'],
            );
            $fields = $db->exec($query, $vars);

            //save submission
            $query = "INSERT INTO `formsubmissions` (`formID`, `userID`, `dateCreated`)
            VALUES (:formID, :userID, :dateCreated)";
            $vars = array(
                ':formID' => $attributes['formID'],
                ':userID' => $userID,
                ':dateCreated' => date('Y-m-d H:i:s'),
            );
            $submission = $db->exec($query, $vars);
            $submissionID = $db->lastInsertId();

            for ($i = 0; $i < count($fields); $i++) {

                if ($fields[$i]['type'] == 'file') {

                    $folder = "user_" . $userID;
                    $formFieldName = 'file_upload';
                    $name = $attributes['field-' . $fields[$i]['id']];
                    $f3->set('POST.file_upload', "uploads/$folder/$name");
                    $resultUrl = $this->fileUpload($f3, $formFieldName, $folder);

                    //insert applicant-data
                    $query = "INSERT INTO `applicantdata` (`userID`, `dataType`, `data`, `fieldID`, `submissionID`, `dateCreated`)
                    VALUES (:userID, :dataType, :data, :fieldID, :submissionID, :dateCreated)";
                    $vars = array(
                        ':userID' => $userID,
                        ':dataType' => $fields[$i]['type'],
                        ':data' => $resultUrl,
                        ':fieldID' => $fields[$i]['id'],
                        ':submissionID' => $submissionID,
                        ':dateCreated' => date('Y-m-d H:i:s'),
                    );
                    $results = $db->exec($query, $vars);
                } else {
                    //insert applicant-data
                    $query = "INSERT INTO `applicantdata` (`userID`, `dataType`, `data`, `fieldID`, `submissionID`, `dateCreated`)
                    VALUES (:userID, :dataType, :data, :fieldID, :submissionID, :dateCreated)";
                    $vars = array(
                        ':userID' => $userID,
                        ':dataType' => $fields[$i]['type'],
                        ':data' => $attributes['field-' . $fields[$i]['id']],
                        ':fieldID' => $fields[$i]['id'],
                        ':submissionID' => $submissionID,
                        ':dateCreated' => date('Y-m-d H:i:s'),
                    );
                    $results = $db->exec($query, $vars);
                }
            }
        } catch (Exception $exception) {
            return $exception->getMessage();
        }
    }

    public function trainingInfo($f3)
    {
        try {
            global $db;
            $userID = $f3->get('POST.userID');
            $attributes = $f3->get('POST');

            //get form fields
            $query = "SELECT * FROM fields WHERE formID = :formID ORDER BY id DESC";
            $vars = array(
                ':formID' => $attributes['formID'],
            );
            $fields = $db->exec($query, $vars);

            //save submission
            $query = "INSERT INTO `formsubmissions` (`formID`, `userID`, `dateCreated`)
            VALUES (:formID, :userID, :dateCreated)";
            $vars = array(
                ':formID' => $attributes['formID'],
                ':userID' => $userID,
                ':dateCreated' => date('Y-m-d H:i:s'),
            );
            $submission = $db->exec($query, $vars);
            $submissionID = $db->lastInsertId();

            for ($i = 0; $i < count($fields); $i++) {

                if ($fields[$i]['type'] == 'file') {
                    $folder = "user_" . $userID;
                    $formFieldName = 'file_upload';
                    $name = $attributes['field-' . $fields[$i]['id']];
                    $f3->set('POST.file_upload', "uploads/$folder/$name");
                    $resultUrl = $this->fileUpload($f3, $formFieldName, $folder);

                    //insert applicant-data
                    $query = "INSERT INTO `applicantdata` (`userID`, `dataType`, `data`, `fieldID`, `submissionID`, `dateCreated`)
                    VALUES (:userID, :dataType, :data, :fieldID, :submissionID, :dateCreated)";
                    $vars = array(
                        ':userID' => $userID,
                        ':dataType' => $fields[$i]['type'],
                        ':data' => $resultUrl,
                        ':fieldID' => $fields[$i]['id'],
                        ':submissionID' => $submissionID,
                        ':dateCreated' => date('Y-m-d H:i:s'),
                    );
                    $results = $db->exec($query, $vars);
                } else {
                    //insert applicant-data
                    $query = "INSERT INTO `applicantdata` (`userID`, `dataType`, `data`, `fieldID`, `submissionID`, `dateCreated`)
                    VALUES (:userID, :dataType, :data, :fieldID, :submissionID, :dateCreated)";
                    $vars = array(
                        ':userID' => $userID,
                        ':dataType' => $fields[$i]['type'],
                        ':data' => $attributes['field-' . $fields[$i]['id']],
                        ':fieldID' => $fields[$i]['id'],
                        ':submissionID' => $submissionID,
                        ':dateCreated' => date('Y-m-d H:i:s'),
                    );
                    $results = $db->exec($query, $vars);
                }
            }
            $query = "UPDATE applicantdetails SET trainingComplete=:trainingComplete, dateUpdated=NOW() WHERE userID=:userID";
            $vars = array(
                ':trainingComplete' => 'y',
                ':userID' => $userID,
            );
            $update = $db->exec($query, $vars);
        } catch (Exception $exception) {
            return $exception->getMessage();
        }
    }

    public function dashboard($f3, $params)
    {
        date_default_timezone_set('Africa/Johannesburg'); //or change to whatever timezone you want
        global $db;
        $this->authCheck($f3);
        $curlResponse = [];

        #Reset Login Attempts
        $sql = "UPDATE `userLogins` SET `email` = :email, `loginResponse` = :loginResponse, `loginAttempts` = :loginAttempts, `loginTime` = :loginTime WHERE `email` = :email";
        $vars1 = array(
            ':email' => $f3->get("POST.email"),
            ':loginResponse' => "Login Successfull",
            ':loginAttempts' => 0,
            ':loginTime' => date('Y-m-d H:i:s'),
            ':email' => $f3->get("POST.email"),
        );
        $logins = $db->exec($sql, $vars1);

        #Get Property Agents List
        $sql = "SELECT * FROM propertyagents WHERE isDeleted = 'n';";
        $property_agents = $db->exec($sql);
        $curlResponse['agents'] = json_encode($property_agents);
        
        #Get Tenant Details
        $query = "SELECT * from applicantdetails WHERE userID = :id";
        $vars = array(
            ':id' => $f3->get("POST.userID"),
        );
        $applicant_details = $db->exec($query, $vars);
        $curlResponse['applicantdetails'] = json_encode($applicant_details);
        $applicantID = $applicant_details[0]['id'];
        $applicationID = $applicant_details[0]['applicationID'];

        if($applicant_details[0]['idPassportNo']==''){
            $curlResponse['idPassportNo'] =  "";
        } else if(!empty($applicant_details[0]['idPassportNo'])) {
            $query = "SELECT * FROM verifiedDocuments WHERE idNumber = :idNumber;";
            $vars = array(
                ':idNumber' => $applicant_details[0]['idPassportNo'],
            );
            $verifiedDocument = $db->exec($query, $vars);
            $curlResponse['verifiedDocument'] =  json_encode($verifiedDocument);
        }

        #Get Tenant Application
        if(is_null($applicationID)){
            $query = "SELECT * FROM applications WHERE userID = :userID ORDER BY id DESC;";
            $vars = array(
                ':userID' => $f3->get("POST.userID"),
            );
            $applicantdata = $db->exec($query, $vars);
            $curlResponse['applications'] = json_encode($applicantdata);

            $applicationID = $applicantdata[0]['id'];
        } else {
            $query = "SELECT * FROM applications WHERE id = :applicationID ORDER BY id DESC;";
            $vars = array(
                ':applicationID' => $applicationID,
            );
            $applicantdata = $db->exec($query, $vars);
            $curlResponse['applications'] = json_encode($applicantdata);

            $applicationID = $applicantdata[0]['id'];
        }
        

        #Get Tenant Application Additional Requests
        $query = "SELECT * FROM comments WHERE applicationID = :applicationID AND status = :status;";
        $vars = array(
            ':applicationID' => $applicationID,
            ':status' => 'pending'
        ); 
        $comments = $db->exec($query, $vars);
        $curlResponse['comments'] = json_encode($comments);

        if(count($comments)>0){
            for ($i=0; $i < count($comments); $i++) { 
                if($comments[$i]['title']){
                    $comments[$i]['title'] = strtoupper(preg_replace("([A-Z])", " $0", preg_replace("/[^a-zA-Z]+/", "", $comments[$i]['title'])). " ". preg_replace("/[^0-9]+/", "",$comments[$i]['title']));
                }   
            }
        }

        $query = "SELECT * FROM requests WHERE applicationID = :applicationID AND status = :status;";
        $vars = array(
            ':applicationID' => $applicationID,
            ':status' => 'pending'
        ); 
        $requests = $db->exec($query, $vars);

        $requests = array_merge($comments, $requests);
        $curlResponse['requests'] = json_encode($requests);

        
        #Get Tenant Training Form Submission
        $query = "SELECT *, (SELECT COUNT(id) FROM formsubmissions WHERE forms.id = formID AND userID = :userID) AS submissions FROM forms WHERE state = :value";
        $vars = array(
            ':userID' => $f3->get("POST.userID"),
            ':value' => 1,
        );
        $forms = $db->exec($query, $vars);
        $curlResponse['forms'] = json_encode($forms);

        if (sizeof($applicantdata) > 0) {
            $query = "SELECT *  FROM userChecklist WHERE userID = :userID";
            $vars = array(
                ':userID' => $f3->get("POST.userID"),
            );
            $userChecklist = $db->exec($query, $vars);
            $curlResponse['userChecklist'] = json_encode($userChecklist);

            if (sizeof($userChecklist) < 1 && $applicantdata[0]['applicationFee'] == "paid") {
                $params['userID'] = $f3->get("POST.userID");
            }

            $f3->set('hasApplied', 'y');
            $curlResponse['hasApplied'] = 'y';
            if ($applicantdata[0]['applicationStatus'] == 'a') {
                $f3->set('applicationStatus', 'Approved');
                $curlResponse['applicationStatus'] = "Approved";
            } else if ($applicantdata[0]['applicationStatus'] == 'p') {
                $f3->set('applicationStatus', 'Pending');
                $curlResponse['applicationStatus'] = "Pending";
            } else if ($applicantdata[0]['applicationStatus'] == 'd') {
                $f3->set('applicationStatus', 'Denied');
                $curlResponse['applicationStatus'] = "Denied";

            } else if ($applicantdata[0]['applicationStatus'] == 'i') {
                $f3->set('applicationStatus', 'Invited');
                $curlResponse['applicationStatus'] = "Invited";

            } else if ($applicantdata[0]['applicationStatus'] == 's') {
                $f3->set('applicationStatus', 'Assigned Unit');
                $curlResponse['applicationStatus'] = "Assigned Unit";
            } else if ($applicantdata[0]['applicationStatus'] == 'r') {
                $f3->set('applicationStatus', 'Rent Deposit');
                $curlResponse['applicationStatus'] = "Rent Deposit";
            } else if ($applicantdata[0]['applicationStatus'] == 't') {
                $f3->set('applicationStatus', 'Tenant Decline');
                $curlResponse['applicationStatus'] = "Tenant Decline";
            } 
        } else {
            $f3->set('hasApplied', 'n');
            $curlResponse['hasApplied'] = "n";
        }

        #Get Tenant Rental Property Fixed Charges
        $query = "SELECT * FROM fixedCharges WHERE isDeleted = :isDeleted";
        $vars = array(
            ':isDeleted' => 'n',
        );
        $fixedCharges = $db->exec($query, $vars);
        $curlResponse['fixedCharges'] = json_encode($fixedCharges);

        #Get Tenant Details
        $query = "SELECT dt.*, ad.id AS applicantID 
        FROM applicantdetails ad 
        LEFT JOIN applicantdata dt ON ad.userID = dt.userID AND (dt.applicationID = :applicationID OR dt.applicationID IS NULL)
        WHERE ad.userID = :userID AND (ad.applicationID = :applicationID OR ad.applicationID IS NULL);";
        $vars = array(
            ':userID' => $f3->get("POST.userID"),
            ':applicationID' => $applicationID
        );
        $userdata = $db->exec($query, $vars);
        $curlResponse['userdata'] = json_encode($userdata);

        #Get Tenant Rental Property
        $query = "SELECT * FROM rentalproperties WHERE applicantID = :applicantID AND applicationID = :applicationID AND deleted = :deleted";
        $vars = array(
            ':applicantID' => $applicantID,
            ':applicationID' => $applicationID,
            ':deleted' => 'n'
        );
        $rentalproperty = $db->exec($query, $vars);
        $curlResponse['rentalproperty'] = json_encode($rentalproperty);
        
        #Get Tenant Rental Property Additional Charges
        $query = "SELECT * FROM charges WHERE propertyReference = :propertyReference AND isDeleted = :isDeleted";
        $vars = array(
            ':propertyReference' => $rentalproperty[0]['propertyReference'],
            ':isDeleted' => 'n',
        );
        $charges = $db->exec($query, $vars);
        $curlResponse['charges'] = json_encode($charges);

        #Get Tenant Applicants
        $query = "SELECT * from applicants WHERE applicantID = :applicantID ORDER BY id ASC;";
        $vars = array(
            ':applicantID' => $applicantID,
        );
        $applicants = $db->exec($query, $vars);

        if(count($applicants) > 0){
            $consent_approved = 0;
            for ($applicant=0; $applicant < count($applicants); $applicant++) { 
                if($applicants[$applicant]['consent'] == 'y'){
                    $consent_approved++;
                }
            }

            #Check Partner/Applicats Credit Check Consent Approval
            if($consent_approved == count($applicants)){
                $query = "UPDATE applicantdetails SET partnerConsent=:partnerConsent, dateUpdated=NOW() WHERE userID=:userID";
                $varsArr = array(
                    ':partnerConsent' => 'y',
                    ':userID' => $f3->get("POST.userID"),
                );
                $consent = $db->exec($query, $varsArr); 
            }
        }
        $curlResponse['applicants'] = json_encode($applicants);

        #Profile Submission or Signed Document File Upload
        if ($f3->get('POST') && isset($_FILES)) {
            $folder = "user_" . $f3->get("POST.userID");

            #Checking it's a Application Submission
            if ($f3->get('POST.propertyReference')) {
                $application = true;
            } else {
                $application = false;
            }

            #Checking it's a Deposit Submission
            if ($f3->get('POST.rentDeposit')) {
                $deposit = true;
            } else {
                $deposit = false;
            }

            #File Upload Data
            $resultUrl = (array) json_decode($f3->get('POST.files'));
            $CheckList = (array) json_decode($f3->get('POST.docsArr'));
            $count = 0;
            foreach ($resultUrl as $fileUrl => $bool) {
                $update = false;
                $formFieldName = $CheckList[$count];
                foreach ($userdata as $row) {
                    if ($row['dataType'] == $formFieldName) {
                        $update = true;
                    }
                }

                #Update
                if ($update) {
                    //APPLICATION
                    if ($application) {
                        $query = "UPDATE applicantdata SET userID = :userID, data =:data, dataType =:dataType, dateUpdated = :dateUpdated 
                        WHERE dataLabel = :dataLabel AND applicationID = :applicationID";
                        $vars = array(
                            ':userID' => $f3->get("POST.userID"),
                            ':data' => $fileUrl,
                            ':dataType' => 'proofOfPayment',
                            ':dateUpdated' => date("Y-m-d H:i:s"),
                            ':dataLabel' => 'proofOfPayment',
                            ':applicationID' => $applicationID
                        );
                        $applicantdata = $db->exec($query, $vars);

                        //SUBMIT APPLICATION
                        $apply = $this->saveApplication($f3, $params, $applicantID, $fileUrl);
                    } 

                    #Deposit
                    else if($deposit) {
                        $query = "UPDATE applicantdata SET userID = :userID, data =:data, dataType =:dataType, dateUpdated = :dateUpdated WHERE dataLabel = :dataLabel";
                        $vars = array(
                            ':userID' => $f3->get("POST.userID"),
                            ':data' => $fileUrl,
                            ':dataType' => 'proofOfPayment',
                            ':dateUpdated' => date("Y-m-d H:i:s"),
                            ':dataLabel' => 'rentDeposit',
                        );
                        $applicantdata = $db->exec($query, $vars);
                    } 

                    #Signed Document Upload
                    else {
                        $query = "UPDATE applicantdata SET data=:data 
                        WHERE userID = :userID AND dataType=:dataType AND applicationID = :applicationID";
                        $vars = array(
                            ':data' => $fileUrl,
                            ':userID' => $f3->get("POST.userID"),
                            ':dataType' => $formFieldName,
                            ':applicationID' => $applicationID
                        );
                        $applicantdata = $db->exec($query, $vars);
                    }
                } 
                
                #Insert
                else {
                    #Application
                    if ($application) {
                        $apply = $this->saveApplication($f3, $params, $applicantID, $fileUrl);
                    } 

                    #Deposit
                    else if($deposit) {
                        $query = "INSERT INTO applicantdata (userID, data, dataType, dataLabel, dateCreated, dateUpdated)
                                    VALUES (:userID, :data, :dataType, :dataLabel, :dateCreated, :dateUpdated)";
                        $vars = array(
                            ':userID' => $f3->get("POST.userID"),
                            ':data' => $fileUrl,
                            ':dataType' => 'proofOfPayment',
                            ':dataLabel' => 'rentDeposit',
                            ':dateCreated' => date("Y-m-d H:i:s"),
                            ':dateUpdated' => date("Y-m-d H:i:s")
                        );
                        $applicantdata = $db->exec($query, $vars);
                    } 
                    
                    #Signed Document Upload
                    else {
                        $query = "INSERT INTO applicantdata (userID, data, dataType, dataLabel, applicationID, dateCreated, dateUpdated)
                                    VALUES (:userID, :data, :dataType, :dataLabel, :applicationID, :dateCreated, :dateUpdated)";
                        $vars = array(
                            ':userID' => $f3->get("POST.userID"),
                            ':data' => $fileUrl,
                            ':dataType' => $formFieldName,
                            ':dataLabel' => $formFieldName,
                            ':applicationID' => $applicationID,
                            ':dateCreated' => date("Y-m-d H:i:s"),
                            ':dateUpdated' => date("Y-m-d H:i:s")
                        );
                        $applicantdata = $db->exec($query, $vars);
                    }
                }
                $count++;
            }
        }

        #Get User Profile Checklist
        $query = "SELECT * FROM userChecklist WHERE userID = :id";
        $vars = array(
            ':id' => $f3->get("POST.userID"),
        );
        $checkList = $db->exec($query, $vars);
        $curlResponse['checkList'] = json_encode($checkList);

        $arrCheckList = [];
        $profilePerc = 0;
        $personalPerc = 0;
        $contactPerc = 0;
        $employerPerc = 0;
        $emergencyPerc = 0;
        $documentPerc = 0;
        $arrCheckList["personalInfo"] = ["firstName", "lastName", "idPassportNo", "dob" ,"maritalStatus", "address", "bankName", "branchCode", "accountNumber", "accountType"];
        $arrCheckList["contactInfo"] = ["email", "phone", "altPhone"];
        $arrCheckList["employerInfo"] = ["occupation", "employer", "employerContact", "totalIncome", "totalExpenses"];
        $arrCheckList["emergencyInfo"] = ["nextOfKin", "nextOfKinContact"];
        $arrCheckList["documentInfo"] = ["idDocument", "payslip", "bankStatement"];
        $documentVerification = 0;
        foreach ($arrCheckList["documentInfo"] as $column) {
            foreach ($checkList as $verify) {
                if ($verify['title'] == 'idDocument') {
                    if ($verify['isVerified'] == 'y') {
                        $curlResponse['idDocVerified'] =  "y";
                        $documentVerification++;
                    } else {
                        $curlResponse['idDocVerified'] =  "n";
                    }
                } else if ($verify['title'] == 'bankStatement') {
                    if ($verify['isVerified'] == 'y') {
                        $curlResponse['bankStatementVerified'] =  "y";
                        $documentVerification++;
                    } else {
                        $curlResponse['bankStatementVerified'] =  "n";
                    }
                } else if ($verify['title'] == 'payslip') {
                    if ($verify['isVerified'] == 'y') {
                        $curlResponse['payslipVerified'] =  "y";
                        $documentVerification++;
                    } else {
                        $curlResponse['payslipVerified'] =  "N";
                    }
                } else if ($verify['title'] == 'proofOfPayment') {
                    if ($verify['isVerified'] == 'y') {
                        $curlResponse['proofOfPaymentVerified'] =  "y";
                        $curlResponse['applicationFeeStatus'] =  "paid";
                    } else {
                        $curlResponse['proofOfPaymentVerified'] =  "n";
                        $curlResponse['applicationFeeStatus'] =  "unpaid";
                    }
                } else if ($verify['title'] == 'rentDeposit') {
                    if ($verify['isVerified'] == 'y') {
                        $curlResponse['rentDepositVerified'] =  "Paid";
                    } else {
                        $curlResponse['rentDepositVerified'] =  "Unpaid";
                    }
                }
            }
        }

        #GET APPLICANT DETAILS - AFTER UPDATES
        $query = "SELECT * FROM applicantdetails WHERE userID = :userID";
        $vars = array(
            ':userID' => $f3->get("POST.userID"),
        );
        $details = $db->exec($query, $vars);
        $applicantID = $details[0]['id'];
        $curlResponse['details'] = json_encode($details);
        $curlResponse['documentVerification'] =  $documentVerification;
        
        $signedDocuments = 0;
        //SIGRND DOCUMENTS CHECKLIST
        foreach ($arrCheckList as $key => $list) {
            foreach ($list as $column) {
                if ($key == "personalInfo" && $applicant_details[0][$column] != "") {
                    $profilePerc = $profilePerc + 2;
                    $personalPerc = $personalPerc + 2;
                }
                if ($key == "contactInfo" && $applicant_details[0][$column] != "") {
                    $profilePerc = $profilePerc + 6.66;
                    $contactPerc = $contactPerc + 6.66;
                }
                if ($key == "employerInfo" && $applicant_details[0][$column] != "") {
                    $profilePerc = $profilePerc + 4;
                    $employerPerc = $employerPerc + 4;
                }
                if ($key == "emergencyInfo" && $applicant_details[0][$column] != "") {
                    $profilePerc = $profilePerc + 10;
                    $emergencyPerc = $emergencyPerc + 10;
                }
                if ($key == "documentInfo") {
                    foreach ($userdata as $row) {

                        if ($row['dataType'] == $column) {
                            $dataArr = explode(",", $row['data']);
                            if (!in_array('Error', $dataArr) && $row['data'] != "") {
                                $profilePerc = $profilePerc + 6.66;
                                $documentPerc = $documentPerc + 6.66;
                            }
                        }

                        if ($row['dataType'] == 'leaseAgreement') {
                            $signedDocuments++;
                        } else if ($row['dataType'] == 'keySlip') {
                            $signedDocuments++;
                        }
                    }
                }
            }
        }
        if ($signedDocuments == 0) {
            $signedDocuments = 'no files';
        } else {
            $signedDocuments = 'uploaded';
        }
        $curlResponse['signedDocuments'] =  $signedDocuments;
        $curlResponse['profilePercentage'] =  round($profilePerc);
        $curlResponse['personalPerc'] =  round($personalPerc);
        $curlResponse['contactPerc'] =  round($contactPerc);
        $curlResponse['employerPerc'] =  round($employerPerc);
        $curlResponse['emergencyPerc'] =  round($emergencyPerc);
        $curlResponse['documentPerc'] =  round($documentPerc);
        $personalPerc = $personalPerc + 2;

        #Get Tenant Information
        $query = "SELECT * FROM users WHERE id = :userID";
        $vars = array(
            ':userID' => $f3->get("POST.userID"),
        );
        $userData = $db->exec($query, $vars);
        $curlResponse['users'] = json_encode($userData);
        
        #Get Tenant Rental Property
        $query = "SELECT * FROM rentalproperties WHERE applicantID = :applicantID OR applicationID = :applicationID AND deleted = :deleted AND available = :available";
        $vars = array(
            ':applicantID' => intval($applicantID),
            ':applicationID' => intval($applicationID),
            ':deleted' => 'n',
            ':available' => 'y'
        );
        $rentalproperties = $db->exec($query, $vars);
        $curlResponse['rentalproperties'] = json_encode($rentalproperties);

        #Get Payment Settings
        $query = "SELECT * FROM paymentsettings;";
        $paymentsettings = $db->exec($query);
        $curlResponse['paymentsettings'] = json_encode($paymentsettings);

        $query = "SELECT * FROM visits WHERE propertyReference = :propertyReference AND userID = :userID AND applicationID = :applicationID ORDER BY id DESC LIMIT 1;";
        $vars = array(
            ':propertyReference' => $rentalproperties[0]['propertyReference'],
            ':userID' => intval($f3->get("POST.userID")),
            ':applicationID' => intval($applicationID)
        );
        $visitDate = $db->exec($query, $vars);
        $curlResponse['visitDate'] = json_encode($visitDate);

        $query = "SELECT * FROM metaData WHERE mainID = :rentalpropertyID";
        $vars = array(
            ':rentalpropertyID' => intval($rentalproperties[0]['id']),
        );
        $property_documents = $db->exec($query, $vars);

        $query = "SELECT * from emergencyContacts WHERE applicantID = :applicantID ORDER BY id ASC;";
        $vars = array(
            ':applicantID' => intval($applicantID)
        );
        $emergency_contacts = $db->exec($query, $vars);
        $curlResponse['emergency_contacts'] = json_encode($emergency_contacts);


        $f3->set('property_documents', $property_documents);
        $curlResponse['property_documents'] = json_encode($property_documents);

        $f3->set("viewingDate", date('D jS, F Y', strtotime($visitDate[0]['viewingDate'])));
        $curlResponse['viewingDate'] =  date('D jS, F Y', strtotime($visitDate[0]['viewingDate']));
        echo json_encode($curlResponse);
        
    }

    public function onboarding($f3, $params)
    {
        global $db;
        $this->authCheck($f3);
        date_default_timezone_set('Africa/Johannesburg'); 

        $curlResponse = [];
        $query = "SELECT dt.*  FROM applicantdetails ad 
        LEFT JOIN applicantdata dt ON ad.userID = dt.userID 
        WHERE ad.userID = :userID";
        $vars = array(
            ':userID' => $f3->get("POST.userID"),
        );
        $userdata = $db->exec($query, $vars);
        $curlResponse['userdata'] = json_encode($userdata);

        if ($f3->get('POST')) {
            $folder = "user_" . $f3->get("POST.userID");

            if ($f3->get('POST.files')) {
                $count = 0;
                $resultUrl = (array) json_decode($f3->get('POST.files'));
                $docsArr = (array) json_decode($f3->get('POST.docsArr'));

                if (sizeof($resultUrl) > 0) {
                    foreach ($resultUrl as $fileUrl => $bool) {
                        $update = false;
                        $formFieldName = $docsArr[$count];

                        foreach ($userdata as $row) {
                            if ($row['dataType'] == $formFieldName) {
                                $update = true;
                            }
                        }
                        if ($update) {
                            $query = "UPDATE applicantdata SET data=:data WHERE userID = :userID AND dataType=:dataType";
                            $vars = array(
                                ':data' => $fileUrl,
                                ':userID' => $f3->get("POST.userID"),
                                ':dataType' => $formFieldName,
                            );
                            $applicantdata = $db->exec($query, $vars);
                            $f3->set('SESSION.message', array('msg' => 'Documents saved successfully!', 'alert' => 'info'));
                        } else {
                            $query = "INSERT INTO applicantdata (userID, data, dataType, dataLabel, dateCreated, dateUpdated)
                                VALUES (:userID, :data, :dataType, :dataLabel, :dateCreated, :dateUpdated)";
                            $vars = array(
                                ':userID' => $f3->get("POST.userID"),
                                ':data' => $fileUrl,
                                ':dataType' => $formFieldName,
                                ':dataLabel' => $formFieldName,
                                ':dateCreated' => date("Y-m-d H:i:s"),
                                ':dateUpdated' => date("Y-m-d H:i:s")
                            );
                            $applicantdata = $db->exec($query, $vars);
                            $f3->set('SESSION.message', array('msg' => 'Documents saved successfully!', 'alert' => 'info'));
                        }
                        $count++;
                    }
                }
            }
            $userID = $f3->get("POST.userID");
            $query = "SELECT * FROM `applicantdetails` where userID = :userID";
            $vars = array(
                ':userID' => $userID,
            );
            $user_profiles = $db->exec($query, $vars);

            $attributes = $f3->get('POST');
            if ($f3->get('POST.totalIncome') || $f3->get('POST.applicant2TotalIncome')) {

                $income = 0;
                if ((int) $f3->get('POST.applicants') > 0) {
                    $query = "SELECT * FROM `applicants` where applicantID = :applicantID";
                    $vars = array(
                        ':applicantID' => (int) $user_profiles[0]['id'],
                    );
                    $applicants = $db->exec($query, $vars);
                    
                    $applicant = 2;
                    for ($count=0; $count < count($applicants); $count++) { 

                        $occupation = 'applicant'.$applicant.'Occupation';
                        $employer = 'applicant'.$applicant.'Employer';
                        $employerContact = 'applicant'.$applicant.'EmployerContact';
                        $employmentDuration = 'applicant'.$applicant.'EmploymentDuration';
                        $grossIncome = 'applicant'.$applicant.'GrossIncome';
                        $totalIncome = 'applicant'.$applicant.'TotalIncome';
                        $additionalIncome = 'applicant'.$applicant.'AdditionalIncome';
                        $totalExpense = 'applicant'.$applicant.'TotalExpense';
                        $employmentType = 'applicant'.$applicant.'EmploymentType';
                        $otherEmploymentType = 'applicant'.$applicant.'OtherEmploymentType';
                        
                        $query = "UPDATE applicants SET occupation = :occupation, employer = :employer, employerContact = :employerContact, employmentDuration = :employmentDuration, 
                        grossIncome = :grossIncome, totalIncome = :totalIncome, additionalIncome = :additionalIncome, totalExpense = :totalExpense, employmentType =:employmentType, 
                        otherEmploymentType =:otherEmploymentType, dateUpdated = NOW() 
                        WHERE id =:id AND applicantID = :applicantID";
                        $vars = array(
                            ':occupation' => $f3->get("POST.$occupation"),
                            ':employer' => $f3->get("POST.$employer"),
                            ':employerContact' => $f3->get("POST.$employerContact"),
                            ':employmentDuration' => $f3->get("POST.$employmentDuration"),
                            ':grossIncome' => $f3->get("POST.$grossIncome"),
                            ':totalIncome' => $f3->get("POST.$totalIncome"),
                            ':additionalIncome' => $f3->get("POST.$additionalIncome"),
                            ':totalExpense' => $f3->get("POST.$totalExpense"),
                            ':employmentType' => $f3->get("POST.$employmentType"),
                            ':otherEmploymentType' => $f3->get("POST.$otherEmploymentType"),
                            ':id' => (int) $applicants[$count]['id'],
                            ':applicantID' => (int) $user_profiles[0]['id'],
                        );
                        $update_applicant = $db->exec($query, $vars);
                        $applicant ++;


                    }
                    
                    $query = "SELECT * FROM `applicants` where applicantID = :applicantID";
                    $vars = array(
                        ':applicantID' => (int) $user_profiles[0]['id'],
                    );
                    $applicants = $db->exec($query, $vars);

                    foreach ($applicants as $applicant) {
                        $income = $income + (float) $applicant['totalIncome'];
                    }
                    $income = round($income, 2);
                }
                
                $attributes['totalIncome'] = $f3->get('POST.totalIncome');
                $maxIncome = (float) $attributes['totalIncome'] + $income;

                $attributes['rentTrench'] = 1;
                $query = "SELECT * FROM `rentTrench` where minIncome <= :minIncome AND maxIncome >= :maxIncome AND deleted= :isDeleted";
                $vars = array(
                    ':minIncome' => $maxIncome,
                    ':maxIncome' => $maxIncome,
                    ':isDeleted' => "n",
                );
                $rentTrench = $db->exec($query, $vars);
                $curlResponse['rentTrench'] = json_encode($rentTrench);
                $attributes['rentTrench'] = $rentTrench[0]['id'];

                if($maxIncome > 15000) {
                    $attributes['housing'] = 'affordable';
                } else if ($maxIncome < 15000){
                    $attributes['housing'] = 'social';
                }
            }

            //applicants
            if ((int) $f3->get('POST.applicants') > 1 && $f3->get('POST.applicant2FullName')) {
                //clear created applicants
                $query = "SELECT * FROM `applicants` where applicantID = :applicantID ORDER BY id ASC;";
                $vars = array(
                    ':applicantID' => (int) $user_profiles[0]['id'],
                );
                $applicants = $db->exec($query, $vars);

                if(count($applicants) > 0){
                    $pos = 0;
                    for ($applicant=1; $applicant < (int) $f3->get('POST.applicants');) { 
                        //update applicants
                        $applicant++;
                        $fullName = 'applicant'.$applicant.'FullName';
                        $email = 'applicant'.$applicant.'Email';
                        $idNumber = 'applicant'.$applicant.'IdPassportNo';
                        $disability = 'applicant'.$applicant.'Disability';
    
                        $query = "UPDATE applicants SET applicantID = :applicantID, fullName = :fullName, email = :email, idNumber = :idNumber, disability = :disability, dateUpdated = NOW() WHERE id = :id;";
                        $vars = array(
                            ':applicantID' => (int) $user_profiles[0]['id'],
                            ':fullName' => $f3->get("POST.$fullName"),
                            ':email' => $f3->get("POST.$email"),
                            ':idNumber' => $f3->get("POST.$idNumber"),
                            ':disability' => $f3->get("POST.$disability"),
                            ':id' => $applicants[$pos]['id']
                        );
                        $update_applicant = $db->exec($query, $vars);

                        if($f3->get("POST.$email") != $applicants[$pos]['email']){

                            $spouse_data = [];
                            $spouse_data['firstName'] = $user_profiles[0]['firstName'].' '.$user_profiles[0]['lastName'];
                            $spouse_data['email'] = $f3->get("POST.$email");
                            $spouse_data['name'] = $f3->get("POST.$fullName");
                            $spouse_data['id'] = (int) $applicants[$pos]['id'];
                            $subject = 'Spouse/Partner Consent Request';
                
                            $this->dispatchEmail($spouse_data, $subject, "consent"); 
                        }
                        $pos++;
                    }
                } else {
                    for ($applicant=1; $applicant < (int) $f3->get('POST.applicants');) { 
                        //clear created applicants
                        $applicant++;
                        $fullName = 'applicant'.$applicant.'FullName';
                        $email = 'applicant'.$applicant.'Email';
                        $idNumber = 'applicant'.$applicant.'IdPassportNo';
                        $disability = 'applicant'.$applicant.'Disability';
    
                        $query = "INSERT INTO applicants (applicantID, fullName, email, idNumber, disability, dateCreated, dateUpdated) VALUES(:applicantID, :fullName, :email, :idNumber, :disability, :dateCreated, :dateUpdated)";
                        $vars = array(
                            ':applicantID' => (int) $user_profiles[0]['id'],
                            ':fullName' => $f3->get("POST.$fullName"),
                            ':email' => $f3->get("POST.$email"),
                            ':idNumber' => $f3->get("POST.$idNumber"),
                            ':disability' => $f3->get("POST.$disability"),
                            ':dateCreated' => date("Y-m-d H:i:s"),
                            ':dateUpdated' => date("Y-m-d H:i:s")
                        );
                        $save_applicant = $db->exec($query, $vars);
                        $applicantID = $db->lastInsertId();

                        $spouse_data = [];
                        $spouse_data['firstName'] = $user_profiles[0]['firstName'].' '.$user_profiles[0]['lastName'];
                        $spouse_data['email'] = $f3->get("POST.$email");
                        $spouse_data['name'] = $f3->get("POST.$fullName");
                        $spouse_data['id'] = (int) $applicantID;
                        $subject = 'Spouse/Partner Consent Request';
            
                        $this->dispatchEmail($spouse_data, $subject, "consent"); 
                    }
                }
            }

            //occupants
            if ((int) $f3->get('POST.occupants') > 0 && $f3->get('POST.occupant1FullName')) {
                //clear created occupants
                $query = "SELECT * FROM `occupants` where applicantID = :applicantID ORDER BY id ASC;";
                $vars = array(
                    ':applicantID' => (int) $user_profiles[0]['id'],
                );
                $occupants = $db->exec($query, $vars);

                if(count($occupants) > 0){
                    for ($occupant=1; $occupant <= (int) $f3->get('POST.occupants');) { 
                        //update occupants
                        $fullName = 'occupant'.$occupant.'FullName';
                        $dob = 'occupant'.$occupant.'DateOfBirth';

                        $query = "UPDATE occupants SET applicantID = :applicantID, fullName = :fullName, dob = :dob, dateUpdated = NOW() WHERE id = :id;";
                        $vars = array(
                            ':applicantID' => (int) $user_profiles[0]['id'],
                            ':fullName' => $f3->get("POST.$fullName"),
                            ':dob' => $f3->get("POST.$dob"),
                            ':id' => $occupants[$occupant-1]['id']
                        );
                        $update_occupant = $db->exec($query, $vars);
                        $occupant++;
                    }
                } else {
                    for ($occupant=1; $occupant <= (int) $f3->get('POST.occupants');) { 
                        //clear created occupants
                        $fullName = 'occupant'.$occupant.'FullName';
                        $dob = 'occupant'.$occupant.'DateOfBirth';
    
                        $query = "INSERT INTO occupants (applicantID, fullName, dob, dateCreated, dateUpdated) VALUES(:applicantID, :fullName, :dob, :dateCreated, :dateUpdated)";
                        $vars = array(
                            ':applicantID' => (int) $user_profiles[0]['id'],
                            ':fullName' => $f3->get("POST.$fullName"),
                            ':dob' => $f3->get("POST.$dob"),
                            ':dateCreated' => date("Y-m-d H:i:s"),
                            ':dateUpdated' => date("Y-m-d H:i:s")
                        );
                        $save_occupant = $db->exec($query, $vars);
                        $occupant++;
                    }
                }
            }

            //children
            if ((int) $f3->get('POST.children') > 0 && $f3->get('POST.child1FullName')) {
                //clear created applicants
                $query = "SELECT * FROM `children` where applicantID = :applicantID ORDER BY id ASC;";
                $vars = array(
                    ':applicantID' => (int) $user_profiles[0]['id'],
                );
                $children = $db->exec($query, $vars);

                if(count($children) > 0){
                    for ($child=0; $child < (int) $f3->get('POST.children'); $child++) { 
                        //update children
                        $fullName = 'child'.($child+1).'FullName';
                        $age = 'child'.($child+1).'Age';
                        $disability = 'child'.($child+1).'Disability';
    
                        $query = "UPDATE children SET applicantID = :applicantID, fullName = :fullName, age = :age, disability = :disability, dateUpdated = NOW() WHERE id = :id;";
                        $vars = array(
                            ':applicantID' => (int) $user_profiles[0]['id'],
                            ':fullName' => $f3->get("POST.$fullName"),
                            ':age' => $f3->get("POST.$age"),
                            ':disability' => $f3->get("POST.$disability"),
                            ':id' => $children[$child]['id'],
                        );
                        $update_child = $db->exec($query, $vars);
                    }
                } else {
                    for ($child=0; $child < (int) $f3->get('POST.children'); $child++) { 
                        //clear created children
                        $fullName = 'child'.($child+1).'FullName';
                        $age = 'child'.($child+1).'Age';
                        $disability = 'child'.($child+1).'Disability';
    
                        $query = "INSERT INTO children (applicantID, fullName, age, disability, dateCreated, dateUpdated) VALUES(:applicantID, :fullName, :age, :disability, :dateCreated, :dateUpdated)";
                        $vars = array(
                            ':applicantID' => (int) $user_profiles[0]['id'],
                            ':fullName' => $f3->get("POST.$fullName"),
                            ':age' => $f3->get("POST.$age"),
                            ':disability' => $f3->get("POST.$disability"),
                            ':dateCreated' => date("Y-m-d H:i:s"),
                            ':dateUpdated' => date("Y-m-d H:i:s")
                        );
                        $save_child = $db->exec($query, $vars);
                    }
                }
            }

            //vehicles
            if ((int) $f3->get('POST.vehicles') > 0 && $f3->get('POST.vehicle1Type')) {
                //clear created applicants
                $query = "SELECT * FROM `vehicles` where applicantID = :applicantID ORDER BY id ASC;";
                $vars = array(
                    ':applicantID' => (int) $user_profiles[0]['id'],
                );
                $vehicles = $db->exec($query, $vars);

                if(count($vehicles) > 0){
                    for ($vehicle=0; $vehicle < (int) $f3->get('POST.vehicles'); $vehicle++) { 
                        //update vehicles
                        $type = 'vehicle'.($vehicle+1).'Type';
                        $color = 'vehicle'.($vehicle+1).'Color';
                        $registrationNumber = 'vehicle'.($vehicle+1).'RegistrationNumber';
    
                        $query = "UPDATE vehicles SET applicantID = :applicantID, type = :type, color = :color, registrationNumber = :registrationNumber, dateUpdated = NOW() WHERE id = :id;";
                        $vars = array(
                            ':applicantID' => (int) $user_profiles[0]['id'],
                            ':type' => $f3->get("POST.$type"),
                            ':color' => $f3->get("POST.$color"),
                            ':registrationNumber' => $f3->get("POST.$registrationNumber"),
                            ':id' => $vehicles[$vehicle]['id']
                        );
                        $update_vehicle = $db->exec($query, $vars);
                    }
                } else {
                    for ($vehicle=0; $vehicle < (int) $f3->get('POST.vehicles'); $vehicle++) { 
                        //clear created vehicles
                        $type = 'vehicle'.($vehicle+1).'Type';
                        $color = 'vehicle'.($vehicle+1).'Color';
                        $registrationNumber = 'vehicle'.($vehicle+1).'RegistrationNumber';
    
                        $query = "INSERT INTO vehicles (applicantID, type, color, registrationNumber, dateCreated, dateUpdated) VALUES(:applicantID, :type, :color, :registrationNumber, :dateCreated, :dateUpdated)";
                        $vars = array(
                            ':applicantID' => (int) $user_profiles[0]['id'],
                            ':type' => $f3->get("POST.$type"),
                            ':color' => $f3->get("POST.$color"),
                            ':registrationNumber' => $f3->get("POST.$registrationNumber"),
                            ':dateCreated' => date("Y-m-d H:i:s"),
                            ':dateUpdated' => date("Y-m-d H:i:s")
                        );
                        $save_vehicle = $db->exec($query, $vars);
                    }
                }
            }

            //pets
            if ((int) $f3->get('POST.pets') > 0 && $f3->get('POST.pet1Size')) {
                //clear created applicants
                $query = "SELECT * FROM `pets` where applicantID = :applicantID ORDER BY id ASC;";
                $vars = array(
                    ':applicantID' => (int) $user_profiles[0]['id'],
                );
                $pets = $db->exec($query, $vars);

                if(count($pets) > 0){
                    for ($pet=0; $pet < (int) $f3->get('POST.pets'); $pet++) { 
                        //update pets
                        $type = 'pet'.($pet+1).'Type';
                        $breed = 'pet'.($pet+1).'Breed';
                        $size = 'pet'.($pet+1).'Size';
    
                        $query = "UPDATE pets SET applicantID = :applicantID, type = :type, breed = :breed, size = :size, dateUpdated = NOW() WHERE id = :id;";
                        $vars = array(
                            ':applicantID' => (int) $user_profiles[0]['id'],
                            ':type' => $f3->get("POST.$type"),
                            ':breed' => $f3->get("POST.$breed"),
                            ':size' => $f3->get("POST.$size"),
                            ':id' => $pets[$pet]['id']
                        );
                        $update_pet = $db->exec($query, $vars);
                    }
                } else {
                    for ($pet=0; $pet < (int) $f3->get('POST.pets'); $pet++) { 
                        //clear created pets
                        $type = 'pet'.($pet+1).'Type';
                        $breed = 'pet'.($pet+1).'Breed';
                        $size = 'pet'.($pet+1).'Size';
    
                        $query = "INSERT INTO pets (applicantID, type, breed, size, dateCreated, dateUpdated) VALUES(:applicantID, :type, :breed, :size, :dateCreated, :dateUpdated)";
                        $vars = array(
                            ':applicantID' => (int) $user_profiles[0]['id'],
                            ':type' => $f3->get("POST.$type"),
                            ':breed' => $f3->get("POST.$breed"),
                            ':size' => $f3->get("POST.$size"),
                            ':dateCreated' => date("Y-m-d H:i:s"),
                            ':dateUpdated' => date("Y-m-d H:i:s")
                        );
                        $save_pet = $db->exec($query, $vars);
                    }
                }
            }

            //emergency contacts
            if ((int) $f3->get('POST.emergencyContacts') > 0 && $f3->get('POST.emergencyContact1FullName')) {
                //clear created occupants
                $query = "DELETE FROM `emergencyContacts` where applicantID = :applicantID;";
                $vars = array(
                    ':applicantID' => (int) $user_profiles[0]['id'],
                );
                $clear = $db->exec($query, $vars);

                $query = "SELECT * FROM `emergencyContacts` where applicantID = :applicantID ORDER BY id ASC;";
                $vars = array(
                    ':applicantID' => (int) $user_profiles[0]['id'],
                );
                $emergencyContacts = $db->exec($query, $vars);

                if(count($emergencyContacts) > 0){
                    for ($emergencyContact=1; $emergencyContact <= (int) $f3->get('POST.emergencyContacts');) { 
                        //update emergencyContacts
                    
                        $fullName = 'emergencyContact'.$emergencyContact.'FullName';
                        $phoneNumber = 'emergencyContact'.$emergencyContact.'PhoneNumber';

                        $query = "UPDATE emergencyContacts SET applicantID = :applicantID, fullName = :fullName, phoneNumber = :phoneNumber, dateUpdated = :dateUpdated WHERE id = :id;";
                        $vars = array(
                            ':applicantID' => (int) $user_profiles[0]['id'],
                            ':fullName' => $f3->get("POST.$fullName"),
                            ':phoneNumber' => $f3->get("POST.$phoneNumber"),
                            ':dateUpdated' => date("Y-m-d H:i:s"),
                            ':id' => $emergencyContacts[$emergencyContact-1]['id']
                        );
                        $update_emergencyContact = $db->exec($query, $vars);
                        $emergencyContact++;
                    }
                } else {
                    for ($emergencyContact=1; $emergencyContact <= (int) $f3->get('POST.emergencyContacts');) { 
                        //clear created emergencyContacts
                        
                        $fullName = 'emergencyContact'.$emergencyContact.'FullName';
                        $phoneNumber = 'emergencyContact'.$emergencyContact.'PhoneNumber';

                        $query = "INSERT INTO emergencyContacts (applicantID, fullName, phoneNumber, dateCreated, dateUpdated) VALUES(:applicantID, :fullName, :phoneNumber, :dateCreated, :dateUpdated)";
                        $vars = array(
                            ':applicantID' => (int) $user_profiles[0]['id'],
                            ':fullName' => $f3->get("POST.$fullName"),
                            ':phoneNumber' => $f3->get("POST.$phoneNumber"),
                            ':dateCreated' => date("Y-m-d H:i:s"),
                            ':dateUpdated' => date("Y-m-d H:i:s")
                        );
                        $save_emergencyContact = $db->exec($query, $vars);
                        $emergencyContact++;
                    }
                }
            }

            $tenant_consent_agrrements = ['residenceCheck', 'employementCheck', 'creditCheck'];

            if (sizeof($tenant_consent_agrrements) > 0) {
                foreach ($tenant_consent_agrrements as $consent) {
                    $consent_update = false;
                    foreach ($userdata as $row) {
                        if ($row['dataType'] == $consent) {
                            $consent_update = true;
                        }
                    }

                    if ($consent_update) {
                        $query = "UPDATE applicantdata SET data=:data, dateUpdated = :dateUpdated WHERE userID = :userID AND dataType=:dataType";
                        $vars = array(
                            ':data' => "Yes",
                            ':userID' => $f3->get("POST.userID"),
                            ':dataType' => $consent,
                            ':dateUpdated' => date("Y-m-d H:i:s")
                        );
                        $applicantdata = $db->exec($query, $vars);
                    } else {
                        $query = "INSERT INTO applicantdata (userID, data, dataType, dataLabel, dateCreated, dateUpdated)
                            VALUES (:userID, :data, :dataType, :dataLabel, :dateCreated, :dateUpdated)";
                        $vars = array(
                            ':userID' => $f3->get("POST.userID"),
                            ':data' => "Yes",
                            ':dataType' => $consent,
                            ':dataLabel' => $consent,
                            ':dateCreated' => date("Y-m-d H:i:s"),
                            ':dateUpdated' => date("Y-m-d H:i:s")
                        );
                        $applicantdata = $db->exec($query, $vars);
                    } 
                }
            }

            if (count($user_profiles) > 0) {
                $update = true;
            } else {
                $attributes['userID'] = $userID;
                $update = false;
            }

            $definition = $this->getDefinition($attributes);
            $idArray = ['userID =?', $userID];

            if ($f3->get('POST.firstName')) {
                $userArray = ['id =?', $userID];

                $this->getRepository("users")->updateRecord($userArray, $definition);
            }
           
            if ($update) {
                $info = $this->getRepository("applicantdetails")->updateRecord($idArray, $definition);
            } else {
                $info = $this->getRepository("applicantdetails")->createRecord($definition);
            }
        }

        $query = "SELECT *  from applicantdetails WHERE userID = :id";
        $vars = array(
            ':id' => $f3->get("POST.userID"),
        );
        $applicantdetails = $db->exec($query, $vars);
        $curlResponse['applicantdetails'] = json_encode($applicantdetails);

        $query = "SELECT *  from applicants WHERE applicantID = :applicantID ORDER BY id ASC;";
        $vars = array(
            ':applicantID' => $applicantdetails[0]['id'],
        );
        $applicants = $db->exec($query, $vars);
        $curlResponse['applicants'] = json_encode($applicants);

        $query = "SELECT *  from occupants WHERE applicantID = :applicantID ORDER BY id ASC;";
        $vars = array(
            ':applicantID' => $applicantdetails[0]['id'],
        );
        $occupants = $db->exec($query, $vars);
        $curlResponse['occupants'] = json_encode($occupants);

        $query = "SELECT *  from children WHERE applicantID = :applicantID ORDER BY id ASC;";
        $vars = array(
            ':applicantID' => $applicantdetails[0]['id'],
        );
        $children = $db->exec($query, $vars);
        $curlResponse['children'] = json_encode($children);

        $query = "SELECT *  from vehicles WHERE applicantID = :applicantID ORDER BY id ASC;";
        $vars = array(
            ':applicantID' => $applicantdetails[0]['id'],
        );
        $vehicles = $db->exec($query, $vars);
        $curlResponse['vehicles'] = json_encode($vehicles);

        $query = "SELECT *  from pets WHERE applicantID = :applicantID ORDER BY id ASC;";
        $vars = array(
            ':applicantID' => $applicantdetails[0]['id'],
        );
        $pets = $db->exec($query, $vars);
        $curlResponse['pets'] = json_encode($pets);

        $query = "SELECT * from emergencyContacts WHERE applicantID = :applicantID ORDER BY id ASC;";
        $vars = array(
            ':applicantID' => $applicantdetails[0]['id'],
        );
        $emergency_contacts = $db->exec($query, $vars);
        $curlResponse['emergency_contacts'] = json_encode($emergency_contacts);

        echo json_encode($curlResponse);
    }

    public function profile($f3, $params)
    {
        global $db;
        $this->authCheck($f3);

        $curlResponse = [];
        $query = "SELECT dt.*  FROM applicantdetails ad 
        LEFT JOIN applicantdata dt ON ad.userID = dt.userID 
        WHERE ad.userID = :userID";
        $vars = array(
            ':userID' => $f3->get("POST.userID"),
        );
        $userdata = $db->exec($query, $vars);
        $curlResponse['userdata'] = json_encode($userdata);

        if ($f3->get('POST')) {
            $folder = "user_" . $f3->get("POST.userID");

            if ($f3->get('POST.files')) {

                $count = 0;
                $resultUrl = (array) json_decode($f3->get('POST.files'));
                $docsArr = (array) json_decode($f3->get('POST.docsArr'));

                if (sizeof($resultUrl) > 0) {
                    foreach ($resultUrl as $fileUrl => $bool) {
                        $update = false;
                        $formFieldName = $docsArr[$count];
                        foreach ($userdata as $row) {
                            if ($row['dataType'] == $formFieldName) {
                                $update = true;
                            }
                        }

                        if ($update) {
                            $query = "UPDATE applicantdata SET data=:data WHERE userID = :userID AND dataType=:dataType";
                            $vars = array(
                                ':data' => $fileUrl,
                                ':userID' => $f3->get("POST.userID"),
                                ':dataType' => $formFieldName,
                            );
                            $applicantdata = $db->exec($query, $vars);

                            //GET USER APPLICATIONS
                            $query = "SELECT * FROM applications WHERE userID = :userID ORDER BY id DESC;";
                            $vars = array(
                                ':userID' => $f3->get("POST.userID"),
                            );
                            $application = $db->exec($query, $vars);
                            //GET USER APPLICATION REQUESTS
                            $request_field = str_replace("_", " ", $formFieldName);
                            $query = "SELECT * FROM requests WHERE applicationID = :applicationID AND title = :title AND request = :request;";
                            $vars = array(
                                ':applicationID' => $application[0]['id'],
                                ':title' => $request_field,
                                ':request' => 'document'
                            ); 
                            $requests = $db->exec($query, $vars);

                            if(count($requests) > 0){
                                $query = "UPDATE requests SET status = :status, submission = :submission, dateUpdated = NOW() WHERE id = :id;";
                                $vars = array(
                                    ':status' => 'completed',
                                    ':submission' => $fileUrl,
                                    ':id' => $requests[0]['id']
                                );
                                $update_request = $db->exec($query, $vars);
                            }

                            if($request_field){

                                if($request_field == "occupants"){
                                    $request_field = "totalOccupants";
                                }

                                if($request_field == "children"){
                                    $request_field = "childOccupants";
                                }

                                if($request_field == "applicants"){
                                    $request_field = "adultOccupants";
                                }

                                $query = "SELECT * FROM comments WHERE applicationID = :applicationID AND title = :title";
                                $varsArr = array(
                                    ':applicationID' => $applicationID,
                                    ':title' => $request_field,
                                );
                                $comments = $db->exec($query, $varsArr);
            
                                if(count($comments)>0){
                               
                                    $query = "UPDATE comments SET status = :status, dateUpdated=NOW() WHERE id=:id";
                                    $varsArr = array(
                                        ':status' => 'completed',
                                        ':id' => $comments[0]['id'],
                                    );
                                    $save_comment = $db->exec($query, $varsArr);
                            
                                }
                                
                            }

                        } else {
                            $query = "INSERT INTO applicantdata (userID, data, dataType, dataLabel, dateCreated, dateUpdated)
                                VALUES (:userID, :data, :dataType, :dataLabel, :dateCreated, :dateUpdated)";
                            $vars = array(
                                ':userID' => $f3->get("POST.userID"),
                                ':data' => $fileUrl,
                                ':dataType' => $formFieldName,
                                ':dataLabel' => $formFieldName,
                                ':dateCreated' => date("Y-m-d H:i:s"),
                                ':dateUpdated' => date("Y-m-d H:i:s")
                            );
                            $applicantdata = $db->exec($query, $vars);

                            //GET USER APPLICATIONS
                            $query = "SELECT * FROM applications WHERE userID = :userID ORDER BY id DESC;";
                            $vars = array(
                                ':userID' => $f3->get("POST.userID"),
                            );
                            $application = $db->exec($query, $vars);
                            //GET USER APPLICATION REQUESTS
                            $request_field = str_replace("_", " ", $formFieldName);
                            $query = "SELECT * FROM requests WHERE applicationID = :applicationID AND title = :title;";
                            $vars = array(
                                ':applicationID' => $application[0]['id'],
                                ':title' => $request_field
                            ); 
                            $requests = $db->exec($query, $vars);

                            if(count($requests) > 0){
                                $query = "UPDATE requests SET status = :status, submission = :submission, dateUpdated = NOW() WHERE id = :id;";
                                $vars = array(
                                    ':status' => 'completed',
                                    ':submission' => $fileUrl,
                                    ':id' => $requests[0]['id']
                                );
                                $update_request = $db->exec($query, $vars);
                            }
                        }
                        $count++;
                    }
                }
            }
            $userID = $f3->get("POST.userID");
            $query = "SELECT * FROM `applicantdetails` where userID = :userID";
            $vars = array(
                ':userID' => $userID,
            );
            $user_profiles = $db->exec($query, $vars);

            $attributes = $f3->get('POST');
            if ($f3->get('POST.totalIncome') || $f3->get('POST.applicant2TotalIncome')) {

                $income = 0;
                if ((int) $f3->get('POST.applicants') > 0) {
                    $query = "SELECT * FROM `applicants` where applicantID = :applicantID";
                    $vars = array(
                        ':applicantID' => (int) $user_profiles[0]['id'],
                    );
                    $applicants = $db->exec($query, $vars);
                    
                    $applicant = 2;
                    for ($count=0; $count < count($applicants); $count++) { 
                        $occupation = 'applicant'.$applicant.'Occupation';
                        $employer = 'applicant'.$applicant.'Employer';
                        $employerContact = 'applicant'.$applicant.'EmployerContact';
                        $employmentDuration = 'applicant'.$applicant.'EmploymentDuration';
                        $grossIncome = 'applicant'.$applicant.'GrossIncome';
                        $totalIncome = 'applicant'.$applicant.'TotalIncome';
                        $additionalIncome = 'applicant'.$applicant.'AdditionalIncome';
                        $totalExpense = 'applicant'.$applicant.'TotalExpense';
                        $employmentType = 'applicant'.$applicant.'EmploymentType';
                        $otherEmploymentType = 'applicant'.$applicant.'OtherEmploymentType';
                        
                        $query = "UPDATE applicants SET occupation = :occupation, employer = :employer, employerContact = :employerContact, employmentDuration = :employmentDuration, 
                        grossIncome = :grossIncome, totalIncome = :totalIncome, additionalIncome = :additionalIncome, totalExpense = :totalExpense, employmentType =:employmentType, 
                        otherEmploymentType =:otherEmploymentType, dateUpdated = NOW() 
                        WHERE id =:id AND applicantID = :applicantID";
                        $vars = array(
                            ':occupation' => $f3->get("POST.$occupation"),
                            ':employer' => $f3->get("POST.$employer"),
                            ':employerContact' => $f3->get("POST.$employerContact"),
                            ':employmentDuration' => $f3->get("POST.$employmentDuration"),
                            ':grossIncome' => $f3->get("POST.$grossIncome"),
                            ':totalIncome' => $f3->get("POST.$totalIncome"),
                            ':additionalIncome' => $f3->get("POST.$additionalIncome"),
                            ':totalExpense' => $f3->get("POST.$totalExpense"),
                            ':employmentType' => $f3->get("POST.$employmentType"),
                            ':otherEmploymentType' => $f3->get("POST.$otherEmploymentType"),
                            ':id' => (int) $applicants[$count]['id'],
                            ':applicantID' => (int) $user_profiles[0]['id'],
                        );
                        $update_applicant = $db->exec($query, $vars);
                        $applicant ++;
                    }
                    
                    $query = "SELECT * FROM `applicants` where applicantID = :applicantID";
                    $vars = array(
                        ':applicantID' => (int) $user_profiles[0]['id'],
                    );
                    $applicants = $db->exec($query, $vars);

                    foreach ($applicants as $applicant) {
                        $income = $income + (float) $applicant['totalIncome'];
                    }
                    $income = round($income, 2);
                }
                $attributes['totalIncome'] = $f3->get('POST.totalIncome');
                $maxIncome = (float) $attributes['totalIncome'] + $income;

                $attributes['rentTrench'] = 1;
                $query = "SELECT * FROM `rentTrench` where minIncome <= :minIncome AND maxIncome >= :maxIncome AND deleted= :isDeleted";
                $vars = array(
                    ':minIncome' => $maxIncome,
                    ':maxIncome' => $maxIncome,
                    ':isDeleted' => "n",
                );
                $rentTrench = $db->exec($query, $vars);
                $curlResponse['rentTrench'] = json_encode($rentTrench);
                $attributes['rentTrench'] = $rentTrench[0]['id'];

                if($maxIncome > 15000) {
                    $attributes['housing'] = 'affordable';
                } else if ($maxIncome < 15000){
                    $attributes['housing'] = 'social';
                }
            }

            //applicants
            if ((int) $f3->get('POST.applicants') > 1 && $f3->get('POST.applicant2FullName')) {
                //clear created applicants
                $query = "SELECT * FROM `applicants` where applicantID = :applicantID ORDER BY id ASC;";
                $vars = array(
                    ':applicantID' => (int) $user_profiles[0]['id'],
                );
                $applicants = $db->exec($query, $vars);

                if(count($applicants) > 0){
                    // for ($applicant=2; $applicant < (int) $f3->get('POST.applicants'); $applicant++) {
                    $pos = 0;
                    for ($applicant=1; $applicant < (int) $f3->get('POST.applicants');) {  
                        //update applicants
                        $applicant++;
                        $fullName = 'applicant'.$applicant.'FullName';
                        $email = 'applicant'.$applicant.'Email';
                        $idNumber = 'applicant'.$applicant.'IdPassportNo';
                        $disability = 'applicant'.$applicant.'Disability';
    
                        $query = "UPDATE applicants SET applicantID = :applicantID, fullName = :fullName, email = :email, idNumber = :idNumber, disability = :disability, dateUpdated = NOW() WHERE id = :id;";
                        $vars = array(
                            ':applicantID' => (int) $user_profiles[0]['id'],
                            ':fullName' => $f3->get("POST.$fullName"),
                            ':email' => $f3->get("POST.$email"),
                            ':idNumber' => $f3->get("POST.$idNumber"),
                            ':disability' => $f3->get("POST.$disability"),
                            ':id' => $applicants[$pos]['id']
                        );
                        $update_applicant = $db->exec($query, $vars);

                        if($f3->get("POST.$email") != $applicants[$pos]['email']){

                            $spouse_data = [];
                            $spouse_data['firstName'] = $user_profiles[0]['firstName'].' '.$user_profiles[0]['lastName'];
                            $spouse_data['email'] = $f3->get("POST.$email");
                            $spouse_data['name'] = $f3->get("POST.$fullName");
                            $spouse_data['id'] = (int) $applicants[$pos]['id'];
                            $subject = 'Spouse/Partner Consent Request';
                
                            $this->dispatchEmail($spouse_data, $subject, "consent"); 
                        }

                        $pos++;
                    }
                } else {
                    // for ($applicant=2; $applicant < (int) $f3->get('POST.applicants'); $applicant++) { 
                    for ($applicant=1; $applicant < (int) $f3->get('POST.applicants');) { 
                        //clear created applicants
                        $applicant++;
                        $fullName = 'applicant'.$applicant.'FullName';
                        $email = 'applicant'.$applicant.'Email';
                        $idNumber = 'applicant'.$applicant.'IdPassportNo';
                        $disability = 'applicant'.$applicant.'Disability';
    
                        $query = "INSERT INTO applicants (applicantID, fullName, email, idNumber, disability, dateCreated, dateUpdated) VALUES(:applicantID, :fullName, :email, :idNumber, :disability, :dateCreated, :dateUpdated)";
                        $vars = array(
                            ':applicantID' => (int) $user_profiles[0]['id'],
                            ':fullName' => $f3->get("POST.$fullName"),
                            ':email' => $f3->get("POST.$email"),
                            ':idNumber' => $f3->get("POST.$idNumber"),
                            ':disability' => $f3->get("POST.$disability"),
                            ':dateCreated' => date("Y-m-d H:i:s"),
                            ':dateUpdated' => date("Y-m-d H:i:s")
                        );
                        $save_applicant = $db->exec($query, $vars);

                        $spouse_data = [];
                        $spouse_data['firstName'] = $user_profiles[0]['firstName'].' '.$user_profiles[0]['lastName'];
                        $spouse_data['email'] = $f3->get("POST.$email");
                        $spouse_data['name'] = $f3->get("POST.$fullName");
                        $spouse_data['id'] = (int) $applicantID;
                        $subject = 'Spouse/Partner Consent Request';
            
                        $this->dispatchEmail($spouse_data, $subject, "consent"); 
                    }
                }
            }

            //occupants
            if ((int) $f3->get('POST.occupants') > 0 && $f3->get('POST.occupant1FullName')) {
                //clear created occupants
                $query = "SELECT * FROM `occupants` where applicantID = :applicantID ORDER BY id ASC;";
                $vars = array(
                    ':applicantID' => (int) $user_profiles[0]['id'],
                );
                $occupants = $db->exec($query, $vars);

                if(count($occupants) > 0){
                    for ($occupant=1; $occupant <= (int) $f3->get('POST.occupants');) { 
                        //update occupants
                       
                        $fullName = 'occupant'.$occupant.'FullName';
                        $dob = 'occupant'.$occupant.'DateOfBirth';

                        $query = "UPDATE occupants SET applicantID = :applicantID, fullName = :fullName, dob = :dob, dateUpdated = NOW() WHERE id = :id;";
                        $vars = array(
                            ':applicantID' => (int) $user_profiles[0]['id'],
                            ':fullName' => $f3->get("POST.$fullName"),
                            ':dob' => $f3->get("POST.$dob"),
                            ':id' => $occupants[$occupant-1]['id']
                        );
                        $update_occupant = $db->exec($query, $vars);
                        $occupant++;
                    }
                } else {
                    for ($occupant=1; $occupant <= (int) $f3->get('POST.occupants');) { 
                        //clear created occupants
                        
                        $fullName = 'occupant'.$occupant.'FullName';
                        $dob = 'occupant'.$occupant.'DateOfBirth';
    
                        $query = "INSERT INTO occupants (applicantID, fullName, dob, dateCreated, dateUpdated) VALUES(:applicantID, :fullName, :dob, :dateCreated, :dateUpdated)";
                        $vars = array(
                            ':applicantID' => (int) $user_profiles[0]['id'],
                            ':fullName' => $f3->get("POST.$fullName"),
                            ':dob' => $f3->get("POST.$dob"),
                            ':dateCreated' => date("Y-m-d H:i:s"),
                            ':dateUpdated' => date("Y-m-d H:i:s")
                        );
                        $save_occupant = $db->exec($query, $vars);
                        $occupant++;
                    }
                }
            }
            
            //children
            if ((int) $f3->get('POST.children') > 0 && $f3->get('POST.child1FullName')) {
                //clear created applicants
                $query = "SELECT * FROM `children` where applicantID = :applicantID ORDER BY id ASC;";
                $vars = array(
                    ':applicantID' => (int) $user_profiles[0]['id'],
                );
                $children = $db->exec($query, $vars);

                if(count($children) > 0){
                    for ($child=0; $child < (int) $f3->get('POST.children'); $child++) { 
                        //update children
                        $fullName = 'child'.($child+1).'FullName';
                        $age = 'child'.($child+1).'Age';
                        $disability = 'child'.($child+1).'Disability';
    
                        $query = "UPDATE children SET applicantID = :applicantID, fullName = :fullName, age = :age, disability = :disability, dateUpdated = NOW() WHERE id = :id;";
                        $vars = array(
                            ':applicantID' => (int) $user_profiles[0]['id'],
                            ':fullName' => $f3->get("POST.$fullName"),
                            ':age' => $f3->get("POST.$age"),
                            ':disability' => $f3->get("POST.$disability"),
                            ':id' => $children[$child]['id']
                        );
                        $update_child = $db->exec($query, $vars);
                    }
                } else {
                    for ($child=0; $child < (int) $f3->get('POST.children'); $child++) { 
                        //clear created children
                        $fullName = 'child'.($child+1).'FullName';
                        $age = 'child'.($child+1).'Age';
                        $disability = 'child'.($child+1).'Disability';
    
                        $query = "INSERT INTO children (applicantID, fullName, age, disability, dateCreated, dateUpdated) VALUES(:applicantID, :fullName, :age, :disability, :dateCreated, :dateUpdated)";
                        $vars = array(
                            ':applicantID' => (int) $user_profiles[0]['id'],
                            ':fullName' => $f3->get("POST.$fullName"),
                            ':age' => $f3->get("POST.$age"),
                            ':disability' => $f3->get("POST.$disability"),
                            ':dateCreated' => date("Y-m-d H:i:s"),
                            ':dateUpdated' => date("Y-m-d H:i:s")
                        );
                        $save_child = $db->exec($query, $vars);
                    }
                }
            }

            //vehicles
            if ((int) $f3->get('POST.vehicles') > 0 && $f3->get('POST.vehicle1Type')) {
                //clear created applicants
                $query = "SELECT * FROM `vehicles` where applicantID = :applicantID ORDER BY id ASC;";
                $vars = array(
                    ':applicantID' => (int) $user_profiles[0]['id'],
                );
                $vehicles = $db->exec($query, $vars);

                if(count($vehicles) > 0){
                    for ($vehicle=0; $vehicle < (int) $f3->get('POST.vehicles'); $vehicle++) { 
                        //update vehicles
                        $type = 'vehicle'.($vehicle+1).'Type';
                        $color = 'vehicle'.($vehicle+1).'Color';
                        $registrationNumber = 'vehicle'.($vehicle+1).'RegistrationNumber';
    
                        $query = "UPDATE vehicles SET applicantID = :applicantID, type = :type, color = :color, registrationNumber = :registrationNumber, dateUpdated = NOW() WHERE id = :id;";
                        $vars = array(
                            ':applicantID' => (int) $user_profiles[0]['id'],
                            ':type' => $f3->get("POST.$type"),
                            ':color' => $f3->get("POST.$color"),
                            ':registrationNumber' => $f3->get("POST.$registrationNumber"),
                            ':id' => $vehicles[$vehicle]['id']
                        );
                        $update_vehicle = $db->exec($query, $vars);
                    }
                } else {
                    for ($vehicle=0; $vehicle < (int) $f3->get('POST.vehicles'); $vehicle++) { 
                        //clear created vehicles
                        $type = 'vehicle'.($vehicle+1).'Type';
                        $color = 'vehicle'.($vehicle+1).'Color';
                        $registrationNumber = 'vehicle'.($vehicle+1).'RegistrationNumber';
    
                        $query = "INSERT INTO vehicles (applicantID, type, color, registrationNumber, dateCreated, dateUpdated) VALUES(:applicantID, :type, :color, :registrationNumber, :dateCreated, :dateUpdated)";
                        $vars = array(
                            ':applicantID' => (int) $user_profiles[0]['id'],
                            ':type' => $f3->get("POST.$type"),
                            ':color' => $f3->get("POST.$color"),
                            ':registrationNumber' => $f3->get("POST.$registrationNumber"),
                            ':dateCreated' => date("Y-m-d H:i:s"),
                            ':dateUpdated' => date("Y-m-d H:i:s")
                        );
                        $save_vehicle = $db->exec($query, $vars);
                    }
                }
            }

            //pets
            if ((int) $f3->get('POST.pets') > 0 && $f3->get('POST.pet1Type')) {
                //clear created applicants
                $query = "SELECT * FROM `pets` where applicantID = :applicantID ORDER BY id ASC;";
                $vars = array(
                    ':applicantID' => (int) $user_profiles[0]['id'],
                );
                $pets = $db->exec($query, $vars);

                if(count($pets) > 0){
                    for ($pet=0; $pet < (int) $f3->get('POST.pets'); $pet++) { 
                        //update pets
                        $type = 'pet'.($pet+1).'Type';
                        $breed = 'pet'.($pet+1).'Breed';
                        $size = 'pet'.($pet+1).'Size';
    
                        $query = "UPDATE pets SET applicantID = :applicantID, type = :type, breed = :breed, size = :size, dateUpdated = NOW() WHERE id = :id;";
                        $vars = array(
                            ':applicantID' => (int) $user_profiles[0]['id'],
                            ':type' => $f3->get("POST.$type"),
                            ':breed' => $f3->get("POST.$breed"),
                            ':size' => $f3->get("POST.$size"),
                            ':id' => $pets[$pet]['id']
                        );
                        $update_pet = $db->exec($query, $vars);
                    }
                } else {
                    for ($pet=0; $pet < (int) $f3->get('POST.pets'); $pet++) { 
                        //clear created pets
                        $type = 'pet'.($pet+1).'Type';
                        $breed = 'pet'.($pet+1).'Breed';
                        $size = 'pet'.($pet+1).'Size';
    
                        $query = "INSERT INTO pets (applicantID, type, breed, size, dateCreated, dateUpdated) VALUES(:applicantID, :type, :breed, :size, :dateCreated, :dateUpdated)";
                        $vars = array(
                            ':applicantID' => (int) $user_profiles[0]['id'],
                            ':type' => $f3->get("POST.$type"),
                            ':breed' => $f3->get("POST.$breed"),
                            ':size' => $f3->get("POST.$size"),
                            ':dateCreated' => date("Y-m-d H:i:s"),
                            ':dateUpdated' => date("Y-m-d H:i:s")
                        );
                        $save_pet = $db->exec($query, $vars);
                    }
                }
            }
            
            //emergency contacts
            if ((int) $f3->get('POST.emergencyContacts') > 0 && $f3->get('POST.emergencyContact1FullName')) {
                //clear created occupants
                $query = "DELETE FROM `emergencyContacts` where applicantID = :applicantID;";
                $vars = array(
                    ':applicantID' => (int) $user_profiles[0]['id'],
                );
                $clear = $db->exec($query, $vars);

                $query = "SELECT * FROM `emergencyContacts` where applicantID = :applicantID ORDER BY id ASC;";
                $vars = array(
                    ':applicantID' => (int) $user_profiles[0]['id'],
                );
                $emergencyContacts = $db->exec($query, $vars);

                if(count($emergencyContacts) > 0){
                    for ($emergencyContact=1; $emergencyContact <= (int) $f3->get('POST.emergencyContacts');) { 
                        //update emergencyContacts
                       
                        $fullName = 'emergencyContact'.$emergencyContact.'FullName';
                        $phoneNumber = 'emergencyContact'.$emergencyContact.'PhoneNumber';

                        $query = "UPDATE emergencyContacts SET applicantID = :applicantID, fullName = :fullName, phoneNumber = :phoneNumber, dateUpdated = :dateUpdated WHERE id = :id;";
                        $vars = array(
                            ':applicantID' => (int) $user_profiles[0]['id'],
                            ':fullName' => $f3->get("POST.$fullName"),
                            ':phoneNumber' => $f3->get("POST.$phoneNumber"),
                            ':dateUpdated' => date("Y-m-d H:i:s"),
                            ':id' => $emergencyContacts[$emergencyContact-1]['id']
                        );
                        $update_emergencyContact = $db->exec($query, $vars);
                        $emergencyContact++;
                    }
                } else {
                    for ($emergencyContact=1; $emergencyContact <= (int) $f3->get('POST.emergencyContacts');) { 
                        //clear created emergencyContacts
                        
                        $fullName = 'emergencyContact'.$emergencyContact.'FullName';
                        $phoneNumber = 'emergencyContact'.$emergencyContact.'PhoneNumber';
    
                        $query = "INSERT INTO emergencyContacts (applicantID, fullName, phoneNumber, dateCreated, dateUpdated) VALUES(:applicantID, :fullName, :phoneNumber, :dateCreated, :dateUpdated)";
                        $vars = array(
                            ':applicantID' => (int) $user_profiles[0]['id'],
                            ':fullName' => $f3->get("POST.$fullName"),
                            ':phoneNumber' => $f3->get("POST.$phoneNumber"),
                            ':dateCreated' => date("Y-m-d H:i:s"),
                            ':dateUpdated' => date("Y-m-d H:i:s")
                        );
                        $save_emergencyContact = $db->exec($query, $vars);
                        $emergencyContact++;
                    }
                }
            }

            $query = "SELECT * FROM `emergencyContacts` where applicantID = :applicantID ORDER BY id ASC;";
            $vars = array(
                ':applicantID' => (int) $user_profiles[0]['id'],
            );
            $emergencyContacts = $db->exec($query, $vars);

            //additional infomation
            //GET USER APPLICATIONS
            $query = "SELECT * FROM applications WHERE userID = :userID ORDER BY id DESC;";
            $vars = array(
                ':userID' => $userID,
            );
            $application = $db->exec($query, $vars);

            if (count($user_profiles) > 0) {
                $update = true;
            } else {
                $attributes['userID'] = $userID;
                $update = false;
            }
            $definition = $this->getDefinition($attributes);
            $idArray = ['userID =?', $userID];

            if ($f3->get('POST.firstName')) {
                $userArray = ['id =?', $userID];

                $this->getRepository("users")->updateRecord($userArray, $definition);
            }

            if ($update) {
                $info = $this->getRepository("applicantdetails")->updateRecord($idArray, $definition);
            } else {
                $info = $this->getRepository("applicantdetails")->createRecord($definition);
            }

            if(count($emergencyContacts)>0 && count($emergencyContacts)==2){
                $query = "DELETE FROM `comments` where applicationID = :applicationID AND (title = :title OR title = :title1);";
                $vars = array(
                    ':applicationID' => $application[0]['id'],
                    ':title' => "emergencyContact3FullName",
                    ':title1' => "emergencyContact3PhoneNumber",
                );
                $clear = $db->exec($query, $vars);
            }
            if(count($emergencyContacts)>0 && count($emergencyContacts)==1){
                $query = "DELETE FROM `comments` where applicationID = :applicationID AND (title = :title OR title = :title1 OR title = :title2 OR title = :title3);";
                $vars = array(
                    ':applicationID' => $application[0]['id'],
                    ':title' => "emergencyContact2FullName",
                    ':title1' => "emergencyContact2PhoneNumber",
                    ':title2' => "emergencyContact3FullName",
                    ':title3' => "emergencyContact3PhoneNumber",
                );
                $clear = $db->exec($query, $vars);
            }
            
            foreach ($f3->get('POST') as $key => $value) {

                if($key && $value){
                    $request_field = $key;
                    if($request_field == "occupants"){
                        $request_field = "totalOccupants";
                    }

                    if($request_field == "children"){
                        $request_field = "childOccupants";
                    }

                    if($request_field == "applicants"){
                        $request_field = "adultOccupants";
                    }
                   
                    $query = "SELECT * FROM comments WHERE applicationID = :applicationID AND title = :title";
                    $varsArr = array(
                        ':applicationID' => $application[0]['id'],
                        ':title' => $request_field,
                    );
                    $comments = $db->exec($query, $varsArr);

                    if($request_field=='adultOccupants' && count($comments)==0){
                        $request_field = "applicants";
                        $query = "SELECT * FROM comments WHERE applicationID = :applicationID AND title = :title";
                        $varsArr = array(
                            ':applicationID' => $application[0]['id'],
                            ':title' => $request_field,
                        );
                        $comments = $db->exec($query, $varsArr);
                    }
                    
                    if(count($comments)>0){
                
                        $query = "UPDATE comments SET status = :status, dateUpdated=NOW() WHERE id=:id";
                        $varsArr = array(
                            ':status' => 'completed',
                            ':id' => $comments[0]['id'],
                        );
                        $save_comment = $db->exec($query, $varsArr);
                
                    }
                    
                }

            }
            //GET USER APPLICATION REQUESTS
            $query = "SELECT * FROM requests WHERE applicationID = :applicationID;";
            $vars = array(
                ':applicationID' => $application[0]['id']
            ); 
            $requests = $db->exec($query, $vars);

            foreach ($requests as $request) {
                if($requests['request']!='document'){
                    $request_field = str_replace(" ", "_", $request['title']);
                    if($f3->get("POST.$request_field")){
                        $query = "UPDATE requests SET status = :status, submission = :submission, dateUpdated = NOW() WHERE id = :id;";
                        $vars = array(
                            ':status' => 'completed',
                            ':submission' => $f3->get("POST.$request_field"),
                            ':id' => $request['id']
                        );
                        $update_request = $db->exec($query, $vars);
                    }
                } 
            }
        }

        $query = "SELECT * from applicantdetails WHERE userID = :id";
        $vars = array(
            ':id' => $f3->get("POST.userID"),
        );
        $checklistData = $db->exec($query, $vars);
        $curlResponse['checklistData'] = json_encode($checklistData);
        $curlResponse['applicantdetails'] = json_encode($checklistData);

        $query = "SELECT * from applicants WHERE applicantID = :applicantID ORDER BY id ASC;";
        $vars = array(
            ':applicantID' => $checklistData[0]['id'],
        );
        $applicants = $db->exec($query, $vars);
        $curlResponse['applicants'] = json_encode($applicants);

        $query = "SELECT *  from occupants WHERE applicantID = :applicantID ORDER BY id ASC;";
        $vars = array(
            ':applicantID' => $checklistData[0]['id'],
        );
        $occupants = $db->exec($query, $vars);
        $curlResponse['occupants'] = json_encode($occupants);

        $query = "SELECT *  from children WHERE applicantID = :applicantID ORDER BY id ASC;";
        $vars = array(
            ':applicantID' => $checklistData[0]['id'],
        );
        $children = $db->exec($query, $vars);
        $curlResponse['children'] = json_encode($children);

        $query = "SELECT *  from vehicles WHERE applicantID = :applicantID ORDER BY id ASC;";
        $vars = array(
            ':applicantID' => $checklistData[0]['id'],
        );
        $vehicles = $db->exec($query, $vars);
        $curlResponse['vehicles'] = json_encode($vehicles);

        $query = "SELECT *  from pets WHERE applicantID = :applicantID ORDER BY id ASC;";
        $vars = array(
            ':applicantID' => $checklistData[0]['id'],
        );
        $pets = $db->exec($query, $vars);
        $curlResponse['pets'] = json_encode($pets);

        $query = "SELECT * from emergencyContacts WHERE applicantID = :applicantID ORDER BY id ASC;";
        $vars = array(
            ':applicantID' => $checklistData[0]['id'],
        );
        $emergency_contacts = $db->exec($query, $vars);
        $curlResponse['emergency_contacts'] = json_encode($emergency_contacts);

        //GET USER APPLICATIONS
        $query = "SELECT * FROM applications WHERE userID = :userID ORDER BY id DESC;";
        $vars = array(
            ':userID' => $userID,
        );
        $application = $db->exec($query, $vars);

        //GET USER APPLICATION REQUESTS
        $query = "SELECT * FROM comments WHERE applicationID = :applicationID AND status = :status;";
        $vars = array(
            ':applicationID' => $application[0]['id'],
            ':status' => 'pending'
        ); 
        $comments = $db->exec($query, $vars);
        $curlResponse['comments'] = json_encode($comments);

        if(count($comments)>0){
            for ($i=0; $i < count($comments); $i++) { 
                if($comments[$i]['title']){
                    $comments[$i]['title'] = strtoupper(preg_replace("([A-Z])", " $0", preg_replace("/[^a-zA-Z]+/", "", $comments[$i]['title'])). " ". preg_replace("/[^0-9]+/", "",$comments[$i]['title']));
                }   
            }
        }

        $query = "SELECT * FROM requests WHERE applicationID = :applicationID AND status = :status;";
        $vars = array(
            ':applicationID' => $application[0]['id'],
            ':status' => 'pending'
        ); 
        $requests = $db->exec($query, $vars);
        $curlResponse['requests'] = json_encode($requests);

        echo json_encode($curlResponse);
    }

    public function viewApplications($f3)
    {
        date_default_timezone_set('Africa/Johannesburg'); //or change to whatever timezone you want
        global $db;
        $this->authCheck($f3);
        $curlResponse = [];
        $query = "SELECT ap.*  FROM applications ap LEFT JOIN applicantdetails ad ON ap.userID = ad.userID WHERE ap.userID = :userID AND ap.isDeleted = :isDeleted";
        $vars = array(
            ':userID' => $f3->get("POST.userID"),
            ':isDeleted' => 'n',
        );
        $applicantdata = $db->exec($query, $vars);
        $curlResponse['applicantdata'] = json_encode($applicantdata);

        //GET USER APPLICATIONS
        $query = "SELECT * FROM applications WHERE userID = :userID ORDER BY id DESC;";
        $vars = array(
            ':userID' => $f3->get("POST.userID"),
        );
        $applicantdata = $db->exec($query, $vars);
        $curlResponse['applications'] = json_encode($applicantdata);

        //GET USER APPLICATION REQUESTS
        $query = "SELECT * FROM comments WHERE applicationID = :applicationID AND status = :status;";
        $vars = array(
            ':applicationID' => $applicantdata[0]['id'],
            ':status' => 'pending'
        ); 
        $comments = $db->exec($query, $vars);
        $curlResponse['comments'] = json_encode($comments);

        if(count($comments)>0){
            for ($i=0; $i < count($comments); $i++) { 
                if($comments[$i]['title']){
                    $comments[$i]['title'] = strtoupper(preg_replace("([A-Z])", " $0", preg_replace("/[^a-zA-Z]+/", "", $comments[$i]['title'])). " ". preg_replace("/[^0-9]+/", "",$comments[$i]['title']));
                }   
            }
        }

        $query = "SELECT * FROM requests WHERE applicationID = :applicationID AND status = :status;";
        $vars = array(
            ':applicationID' => $applicantdata[0]['id'],
            ':status' => 'pending'
        ); 
        $requests = $db->exec($query, $vars);
        $curlResponse['requests'] = json_encode($requests);
        echo json_encode($curlResponse);
    }

    public function viewRequests($f3)
    {
        date_default_timezone_set('Africa/Johannesburg'); //or change to whatever timezone you want
        $userID = $f3->get('POST.userID');
        global $db;
        $curlResponse = [];
        $query = "SELECT *, (SELECT COUNT(id) FROM formsubmissions WHERE forms.id = formID AND userID = :userID) AS submissions FROM forms WHERE state = :value";
        $vars = array(
            ':userID' => $userID,
            ':value' => 1,
        );
        $forms = $db->exec($query, $vars);
        $curlResponse['forms'] = json_encode($forms);

        //GET USER APPLICATIONS
        $query = "SELECT * FROM applications WHERE userID = :userID ORDER BY id DESC;";
        $vars = array(
            ':userID' => $f3->get("POST.userID"),
        );
        $applicantdata = $db->exec($query, $vars);
        $curlResponse['applications'] = json_encode($applicantdata);

        //GET USER APPLICATION REQUESTS
        $query = "SELECT * FROM comments WHERE applicationID = :applicationID AND status = :status;";
        $vars = array(
            ':applicationID' => $applicantdata[0]['id'],
            ':status' => 'pending'
        ); 
        $comments = $db->exec($query, $vars);
        $curlResponse['comments'] = json_encode($comments);

        if(count($comments)>0){
            for ($i=0; $i < count($comments); $i++) { 
                if($comments[$i]['title']){
                    $comments[$i]['title'] = strtoupper(preg_replace("([A-Z])", " $0", preg_replace("/[^a-zA-Z]+/", "", $comments[$i]['title'])). " ". preg_replace("/[^0-9]+/", "",$comments[$i]['title']));
                }   
            }
        }

        $query = "SELECT * FROM requests WHERE applicationID = :applicationID AND status = :status;";
        $vars = array(
            ':applicationID' => $applicantdata[0]['id'],
            ':status' => 'pending'
        ); 
        $requests = $db->exec($query, $vars);
        $curlResponse['requests'] = json_encode($requests);
        echo json_encode($curlResponse);
    }

    public function viewDocuments($f3)
    {
        date_default_timezone_set('Africa/Johannesburg'); //or change to whatever timezone you want
        global $db;
        $this->authCheck($f3);
        $curlResponse = [];

        //GET USER APPLICATIONS
        $query = "SELECT * FROM applications WHERE userID = :userID ORDER BY id DESC;";
        $vars = array(
            ':userID' => $f3->get("POST.userID"),
        );
        $applicantdata = $db->exec($query, $vars);
        $curlResponse['applications'] = json_encode($applicantdata);

        //GET USER APPLICATION REQUESTS
        $query = "SELECT * FROM comments WHERE applicationID = :applicationID AND status = :status;";
        $vars = array(
            ':applicationID' => $applicantdata[0]['id'],
            ':status' => 'pending'
        ); 
        $comments = $db->exec($query, $vars);
        $curlResponse['comments'] = json_encode($comments);

        if(count($comments)>0){
            for ($i=0; $i < count($comments); $i++) { 
                if($comments[$i]['title']){
                    $comments[$i]['title'] = strtoupper(preg_replace("([A-Z])", " $0", preg_replace("/[^a-zA-Z]+/", "", $comments[$i]['title'])). " ". preg_replace("/[^0-9]+/", "",$comments[$i]['title']));
                }   
            }
        }

        $query = "SELECT * FROM requests WHERE applicationID = :applicationID AND status = :status;";
        $vars = array(
            ':applicationID' => $applicantdata[0]['id'],
            ':status' => 'pending'
        ); 
        $requests = $db->exec($query, $vars);
        $curlResponse['requests'] = json_encode($requests);

        $extra_documents = '';
        for ($i=0; $i < count($requests); $i++) { 
            if($requests[$i]['request'] == 'document' && !empty($requests[$i]['submission'])){
                $extra_documents = $extra_documents."'".str_replace(' ', '_', $requests[$i]['title'])."',";
                for ($a=1; $a < 15; $a++) { 
                     $extra_documents = $extra_documents."'".str_replace(' ', '_', $requests[$i]['title']).$a."',";
                }
            }
        }
        
        $query = "SELECT * FROM applicantdata WHERE userID = :userID 
        AND dataLabel 
        IN(".$extra_documents."
            'leaseAgreement','leaseAgreement', 'leaseAgreement1', 'leaseAgreement2', 'leaseAgreement3', 'leaseAgreement4', 'leaseAgreement5','leaseAgreement6', 'leaseAgreement7', 'leaseAgreement8', 'leaseAgreement9',
            'leaseAgreement10', 'leaseAgreement11', 'leaseAgreement12', 'leaseAgreement13', 'leaseAgreement14', 'leaseAgreement15', 'leaseAgreement16', 'leaseAgreement17', 'leaseAgreement18', 'leaseAgreement19',
            'keySlip','idDocument','payslip','bankStatement', 'payslip1','bankStatement1', 'payslip2','bankStatement2', 'payslip3','bankStatement3', 'payslip4','bankStatement4', 'payslip5','bankStatement5',
            'applicant1IdDocument', 'applicant1BankStatement', 'applicant1Payslip', 'applicant1BankStatement1', 'applicant1Payslip1', 'applicant1BankStatement2', 'applicant1Payslip2', 'applicant1BankStatement3', 'applicant1Payslip3', 'applicant1BankStatement4', 'applicant1Payslip4', 'applicant1BankStatement5', 'applicant1Payslip5',
            'applicant2IdDocument', 'applicant2BankStatement', 'applicant2Payslip', 'applicant2BankStatement1', 'applicant2Payslip1', 'applicant2BankStatement2', 'applicant2Payslip2', 'applicant2BankStatement3', 'applicant2Payslip3', 'applicant2BankStatement4', 'applicant2Payslip4', 'applicant2BankStatement5', 'applicant2Payslip5',
            'applicant3IdDocument', 'applicant3BankStatement', 'applicant3Payslip', 'applicant3BankStatement1', 'applicant3Payslip1', 'applicant3BankStatement2', 'applicant3Payslip2', 'applicant3BankStatement3', 'applicant3Payslip3', 'applicant3BankStatement4', 'applicant3Payslip4', 'applicant3BankStatement5', 'applicant3Payslip5',
            'applicant4IdDocument', 'applicant4BankStatement', 'applicant4Payslip', 'applicant4BankStatement1', 'applicant4Payslip1', 'applicant4BankStatement2', 'applicant4Payslip2', 'applicant4BankStatement3', 'applicant4Payslip3', 'applicant4BankStatement4', 'applicant4Payslip4', 'applicant4BankStatement5', 'applicant4Payslip5',
            'applicant5IdDocument', 'applicant5BankStatement', 'applicant5Payslip', 'applicant5BankStatement1', 'applicant5Payslip1', 'applicant5BankStatement2', 'applicant5Payslip2', 'applicant5BankStatement3', 'applicant5Payslip3', 'applicant5BankStatement4', 'applicant5Payslip4', 'applicant5BankStatement5', 'applicant5Payslip5',
            'applicant6IdDocument', 'applicant6BankStatement', 'applicant6Payslip', 'applicant6BankStatement1', 'applicant6Payslip1', 'applicant6BankStatement2', 'applicant6Payslip2', 'applicant6BankStatement3', 'applicant6Payslip3', 'applicant6BankStatement4', 'applicant6Payslip4', 'applicant6BankStatement5', 'applicant6Payslip5',
            'applicant7IdDocument', 'applicant7BankStatement', 'applicant7Payslip', 'applicant7BankStatement1', 'applicant7Payslip1', 'applicant7BankStatement2', 'applicant7Payslip2', 'applicant7BankStatement3', 'applicant7Payslip3', 'applicant7BankStatement4', 'applicant7Payslip4', 'applicant7BankStatement5', 'applicant7Payslip5',
            'applicant8IdDocument', 'applicant8BankStatement', 'applicant8Payslip', 'applicant8BankStatement1', 'applicant8Payslip1', 'applicant8BankStatement2', 'applicant8Payslip2', 'applicant8BankStatement3', 'applicant8Payslip3', 'applicant8BankStatement4', 'applicant8Payslip4', 'applicant8BankStatement5', 'applicant8Payslip5',
            'idDocumentSpouse','payslipSpouse','bankStatementSpouse','payslipSpouse1','bankStatementSpouse1','payslipSpouse2','bankStatementSpouse2', 'payslipSpouse3','bankStatementSpouse3', 
            'payslipSpouse4','bankStatementSpouse4', 'payslipSpouse5','bankStatementSpouse5', 'proofOfPayment', 'marriageCertificate', 'divorceDecree', 
            'separationAffidavit','pendingJudgementDocument','debtReviewDocument', 'signedEmployementContract', 'applicant1SignedEmployementContract', 'applicant2SignedEmployementContract', 'applicant3SignedEmployementContract'
        ) ORDER BY id ASC";
        $vars = array(
            ':userID' => $f3->get("POST.userID"),
        );
        $documents = $db->exec($query, $vars);

        $newDocsArr = [];
        foreach ($documents as $document) {
            $title = preg_replace("([A-Z])", " $0", $document['dataLabel']);

            $newDocsArr[] = [
                "id" => $document['id'], 
                "dataType" => $document['dataType'],
                "dataLabel" => $document['dataLabel'], 
                "data" => $document['data'], 
                "title" => $title, 
                "applicationID" => $document['applicationID'], 
                'dateUpdated' => $document['dateUpdated']
            ];
        }
        $curlResponse['documents'] = json_encode($newDocsArr);
        echo json_encode($curlResponse);
    }

    public function invoices($f3, $params)
    {
        
    }
}