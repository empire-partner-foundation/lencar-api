<?php

require_once 'AppController.php';

class AjaxController extends AppController
{

    public function convert($hours, $minutes)
    {
        return $hours + round($minutes / 60, 2);
    }

    public function get_user_messages($f3, $params)
    {
        global $db;
        $this->authCheck($f3);
        $curlResponse = [];

        $query = "SELECT * FROM messages WHERE (userID = :userID OR userID = :toUserID) AND (toUserID = :toUserID OR toUserID = :userID) ORDER BY dateTime ASC";
        $vars = array(
            ':userID' => $f3->get('POST.userID'),
            ':toUserID' => $params['toUserID']
        );
        $messages = $db->exec($query, $vars);
        $curlResponse['messages'] = json_encode($messages);
        echo json_encode($curlResponse);
    }

    public function get_unread_messages($f3, $params)
    {
        global $db;
        $this->authCheck($f3);
        $curlResponse = [];

        $query = "SELECT * FROM messages WHERE toUserID = :toUserID AND isRead = :isRead ORDER BY dateTime ASC";
        $vars = array(
            ':toUserID' => $f3->get('POST.userID'),
            ':isRead' => 'n'
        );
        $messages = $db->exec($query, $vars);
        $curlResponse['messages'] = json_encode($messages);
        echo json_encode($curlResponse);
    }

    public function update_unread_messages($f3, $params)
    {
        global $db;
        $this->authCheck($f3);
        $curlResponse = [];

        $query = "UPDATE messages SET isRead = :isRead WHERE toUserID = :userID AND isDeleted = :isDeleted;";
        $vars = array(
            ':isRead' => 'y',
            ':userID' => $f3->get('POST.userID'),
            ':isDeleted' => 'n'
        );
        $read = $db->exec($query, $vars);
    }
    
    public function create_message($f3, $params)
    {
        try {
            #change to whatever timezone you want
            date_default_timezone_set('Africa/Johannesburg');
            $this->authCheck($f3);
            $attributes = $f3->get('POST');
            $attributes['dateTime'] = date("Y-m-d H:i:s");
            $attributes['isDeleted'] = 'n';
            $attributes['isRead'] = 'n';
            $definition = $this->getDefinition($attributes);
            $message = $this->getRepository("messages")->createRecord($definition);
        } catch (Exception $exception) {
            echo json_encode($exception->getMessage());
        }
    }

    public function get_agent_properties($f3, $params)
    {
        try {
            global $db;
            $query = "SELECT * FROM rentalproperties WHERE agentID = :agentID AND deleted = :deleted AND available = :available ORDER BY id ASC";
            $vars = array(
                ':agentID' => $f3->get('POST.agentID'),
                ':deleted' => 'n',
                ':available' => 'y'
            );
            $rental_properties = $db->exec($query, $vars);
            $curlResponse['rental_properties'] = json_encode($rental_properties);

            echo json_encode($curlResponse);
        } catch (Exception $exception) {
            echo json_encode($exception->getMessage());
        }
    }

    public function get_property_agents($f3, $params)
    {
        try {
            global $db;
            
            $query = "SELECT pa.* FROM propertyagents pa 
            LEFT JOIN rentalproperties rp ON rp.propertyReference = :propertyReference AND rp.deleted = :isDeleted AND rp.available = :available
            WHERE pa.id = rp.agentID AND pa.isDeleted = :isDeleted 
            GROUP BY pa.id 
            ORDER BY pa.id ASC";
            $vars = array(
                ':propertyReference' => $f3->get('POST.propertyReference'),
                ':isDeleted' => 'n',
                ':available' => 'y'
            );
            $agents = $db->exec($query, $vars);
            $curlResponse['agents'] = json_encode($agents);

            echo json_encode($curlResponse);
        } catch (Exception $exception) {
            echo json_encode($exception->getMessage());
        }
    }

    public function get_property_admin_fees($f3, $params)
    {
        try {
            global $db;
            $query = "SELECT * FROM rentalproperties WHERE deleted = :isDeleted AND propertyReference = :propertyReference ORDER BY id DESC LIMIT 1";
            $vars = array(
                ':propertyReference' => $f3->get('POST.propertyReference'),
                ':isDeleted' => 'n'
            );
            $rental_property = $db->exec($query, $vars);
            $curlResponse['rental_property'] = json_encode($rental_property);

            $query = "SELECT * FROM fixedCharges WHERE isDeleted = :isDeleted;";
            $vars = array(
                ':isDeleted' => 'n'
            );
            $fixed_charges = $db->exec($query, $vars);
            $curlResponse['fixed_charges'] = json_encode($fixed_charges);

            $query = "SELECT * FROM charges WHERE isDeleted = :isDeleted AND propertyReference = :propertyReference;";
            $vars = array(
                ':isDeleted' => 'n',
                ':propertyReference' => $f3->get('POST.propertyReference'),
            );
            $charges = $db->exec($query, $vars);
            $curlResponse['charges'] = json_encode($charges);

            $query = "SELECT ad.totalIncome, (SELECT SUM(a.totalIncome) FROM applicants a WHERE a.applicantID = ad.id) AS additionalIncome
            FROM applicantdetails ad
            WHERE ad.userID = :userID;";
            $vars = array(
                ':userID' => $f3->get('POST.userID'),
            );
            $total_income = $db->exec($query, $vars);
            $curlResponse['total_income'] = json_encode($total_income);

            

            echo json_encode($curlResponse);
        } catch (Exception $exception) {
            echo json_encode($exception->getMessage());
        }
    }
    
    public function updateApplication($f3, $params)
    {
        global $db;
        $applicationID = $params['applicationID'];
        $userID = $params['userID'];

        if($params['action'] == 'approve') {
            $query = "UPDATE applicantdetails SET unitApproved=:unitApproved, dateUpdated=NOW() WHERE userID=:userID";
            $vars = array(
                ':unitApproved' => 'y',
                ':userID' => $userID,
            );
            $update = $db->exec($query, $vars);

            $status = 'r';
            $attributes = ["applicationStatus" => $status, "dateUpdated" => date("Y-m-d")];
            $definition = $this->getDefinition($attributes);
            $idArray = ['id = ?', $applicationID];
            $application = $this->getRepository('applications')->updateRecord($idArray, $definition);

            $query = "SELECT id, firstName FROM applicantdetails
                    WHERE userID = :userID";
            $vars = array(
                ':userID' => $userID,
            );
            $applicant = $db->exec($query, $vars);
            echo 'Unit Approved Successfully';
        } 
        else if ($params['action'] == 'update') {
            $query = "UPDATE applicantdetails SET housing=:housing, dateUpdated=NOW() WHERE userID=:userID";
            $vars = array(
                ':housing' => 'affordable',
                ':userID' => $userID,
            );
            $update = $db->exec($query, $vars);
            echo 'Housing Group Updated Successfully';
        } 
        else if ($params['action'] == 'decline') {
            $status = 't';
            $attributes = ["applicationStatus" => $status, "dateUpdated" => date("Y-m-d")];
            $definition = $this->getDefinition($attributes);
            $idArray = ['id = ?', $applicationID];
            $application = $this->getRepository('applications')->updateRecord($idArray, $definition);

            $query = "UPDATE applicantdetails SET unitApproved=:unitApproved, dateUpdated=NOW() WHERE userID=:userID";
            $vars = array(
                ':unitApproved' => 'n',
                ':userID' => $userID,
            );
            $update = $db->exec($query, $vars);
            echo 'Unit Declined Successfully';
        }
    }

    public function form_fields($f3, $params)
    {
        global $db;
        $this->authCheck($f3);
        $curlResponse = [];

        $query = "SELECT * FROM fields WHERE formID = :formID ORDER BY id DESC";
        $vars = array(
            ':formID' => $f3->get('POST.formID'),
        );
        $fields = $db->exec($query, $vars);
        $curlResponse['fields'] = json_encode($fields);
        echo json_encode($curlResponse);
    }

    public function field_options($f3, $params)
    {
        global $db;
        $this->authCheck($f3);
        $curlResponse = [];

        $query = "SELECT * FROM fieldOptions WHERE fieldID = :fieldID ORDER BY id DESC";
        $vars = array(
            ':fieldID' => $f3->get('POST.fieldID'),
        );
        $options = $db->exec($query, $vars);
        $curlResponse['options'] = json_encode($options);
        echo json_encode($curlResponse);
    }

    public function formsave($f3, $params)
    {
        try {
            global $db;
            $this->authCheck($f3);
            $formtype = $params['formtype'];
            $userID = $f3->get('POST.userID');

            $query = "SELECT * FROM `applicantdetails` where userID = :userID";
            $vars = array(
                ':userID' => $userID,
            );
            $user_profiles = $db->exec($query, $vars);

            $attributes = $f3->get('POST');
            if ($params['formtype'] == "employmentInfo") {
                $attributes['rentTrench'] = 1;
                $query = "SELECT * FROM `rentTrench` where minIncome <= :minIncome AND maxIncome >= :maxIncome AND deleted= :isDeleted";
                $vars = array(
                    ':minIncome' => $attributes['totalIncome'],
                    ':maxIncome' => $attributes['totalIncome'],
                    ':isDeleted' => "n",
                );
                $results = $db->exec($query, $vars);
                $attributes['rentTrench'] = $results[0]['id'];
            }

            if (count($user_profiles) > 0) {
                $update = true;
            } else {
                $attributes['userID'] = $userID;
                $update = false;
            }

            $definition = $this->getDefinition($attributes);
            $idArray = ['userID =?', $userID];
            if ($f3->get('POST')) {
                if ($params['formtype'] == "personalInfo") {
                    $userArray = ['id =?', $userID];
                    $this->getRepository("users")->updateRecord($userArray, $definition);
                }
                if ($update) {
                    $info = $this->getRepository("applicantdetails")->updateRecord($idArray, $definition);
                } else {
                    $info = $this->getRepository("applicantdetails")->createRecord($definition);
                }
                echo "Info saved successfully!";
            } else {
                echo "No information was saved!";
            }
        } catch (Exception $exception) {
            echo json_encode($exception->getMessage());
        }
    }

    public function verifyForm($f3, $params)
    {
        try {
            global $db;
            $this->authCheck($f3);
            $formType = $params['formtype'];
            $adminID = $f3->get('POST.userID');
            $update = true;

            //if post lets save
            if ($f3->get('POST')) {
                $arrCheckList = [];
                $arrCheckList["personalInfo"] = ["firstName", "lastName", "idPassportNo", "dob", "address", "maritalStatus", "bankName", "branchCode", "accountNumber", "accountType"];
                $arrCheckList["contactInfo"] = ["email", "phone", "altPhone"];
                $arrCheckList["employerInfo"] = ["occupation", "employer", "employerContact", "totalIncome", "totalExpenses"];
                $arrCheckList["emergencyInfo"] = ["nextOfKin", "nextOfKinContact"];
                $arrCheckList["documentInfo"] = ["idDocument", "payslip", "bankStatement", "proofOfPayment"];

                //loop through array for the submitted form type
                foreach ($arrCheckList[$formType] as $column) {
                    $isVerified = 'n';
                    $update = false;

                    //if the field is in post its a verification
                    foreach ($f3->get('POST') as $feild => $value) {
                        if ($feild == $column) {
                            $isVerified = 'y';
                        }
                    }

                    //update or insert.
                    $query = "SELECT * FROM userChecklist WHERE userID = :userID AND title = :title";
                    $vars = array(
                        ':userID' => $f3->get("POST.userID"),
                        ':title' => $column,
                    );
                    $userdata = $db->exec($query, $vars);
                    if (isset($userdata[0]['title'])) {
                        $update = true;
                    }

                    if ($update) {
                        $query = "UPDATE userChecklist SET approvedBy=:adminID, isVerified=:isVerified, dateUpdated=NOW() WHERE id=:id";
                        $vars = array(
                            ':adminID' => $adminID,
                            ':isVerified' => $isVerified,
                            ':id' => $userdata[0]['id'],
                        );
                        $userChecklist = $db->exec($query, $vars);
                    } else {
                        $query = "INSERT INTO userChecklist (userID, approvedBy, title, isVerified, dateCreated, dateUpdated)
                                          VALUES (:userID, :adminID, :title, :isVerified, NOW(), NOW())";
                        $vars = array(
                            ':userID' => $f3->get("POST.userID"),
                            ':adminID' => $adminID,
                            ':title' => $column,
                            ':isVerified' => $isVerified,
                        );
                        $userChecklist = $db->exec($query, $vars);
                    }

                    if ($column == "proofOfPayment") {
                        $params['userID'] = $f3->get("POST.userID");
                        $this->xdsVerification($f3, $params);
                    }
                }
                echo "Info saved successfully!";
                $f3->set('SESSION.message', array('msg' => 'Data saved successfully!', 'alert' => 'info'));
            } else {
                echo "No information was saved!";
            }
        } catch (Exception $exception) {
            echo json_encode($exception->getMessage());
        }
    }

    public function field_state($f3, $params)
    {
        global $db;
        $this->authCheck($f3);
        $curlResponse = [];
        $formID = $f3->get('POST.formID');
        $state = $f3->get('POST.state');

        $query = "UPDATE forms SET state = :state WHERE id = :formID";
        $vars = array(
            ':state' => $state,
            ':formID' => $formID,
        );
        $update = $db->exec($query, $vars);
        $curlResponse['options'] = json_encode($state);
        echo json_encode($curlResponse);
    }

    public function create_form($f3, $params)
    {
        try {
            global $db;
            $this->authCheck($f3);
            $userID = $f3->get('POST.userID');
            $update = false;
            $attributes = $f3->get('POST');
            $definition = $this->getDefinition($attributes);
            $idArray = ['userID =?', $userID];
            if ($f3->get('POST')) {
                $form = $f3->get('POST.form');
                $data = json_decode($form, true);

                //create form
                $query = "INSERT INTO `forms` (`title`, `description`, `state`, `dateCreated`)
                VALUES (:title, :description, :state, :dateCreated)";
                $vars = array(
                    ':title' => $data['name'],
                    ':description' => $data['description'],
                    ':state' => 1,
                    ':dateCreated' => date('Y-m-d H:i:s'),
                );
                $results = $db->exec($query, $vars);
                $formID = $db->lastInsertId();

                //create fields
                for ($i = 0; $i < count($data['formData']); $i++) {
                    $query2 = "INSERT INTO `fields` (`formID`, `title`, `type`, `dateCreated`)
                    VALUES (:formID, :title, :type, :dateCreated)";
                    $vars2 = array(
                        ':formID' => $formID,
                        ':title' => $data['formData'][$i]['label'],
                        ':type' => $data['formData'][$i]['type'],
                        ':dateCreated' => date('Y-m-d H:i:s'),
                    );
                    $results2 = $db->exec($query2, $vars2);
                    $fieldID = $db->lastInsertId();

                    if ($data['formData'][$i]['type'] == "select") {
                        # create field options
                        for ($x = 0; $x < count($data['formData'][$i]['options']); $x++) {
                            $query3 = "INSERT INTO `fieldOptions` (`fieldID`, `title`, `value`, `dateCreated`)
                            VALUES (:fieldID, :title, :value, :dateCreated)";
                            $vars3 = array(
                                ':fieldID' => $fieldID,
                                ':title' => $data['formData'][$i]['options'][$x]['label'],
                                ':value' => $data['formData'][$i]['options'][$x]['value'],
                                ':dateCreated' => date('Y-m-d H:i:s'),
                            );
                            $results3 = $db->exec($query3, $vars3);
                        }
                    }
                }
                echo 'Form created successfully!';
            } else {
                echo "No Form was created!";
            }
        } catch (Exception $exception) {
            echo json_encode($exception->getMessage());
        }
    }

    public function update_form($f3, $params)
    {
        try {
            global $db;
            $this->authCheck($f3);
            $userID = $f3->get('POST.userID');
            $form = $f3->get('POST.form');
            $data = json_decode($form, true);

            $deletedFields = $f3->get('POST.deleted');
            $delete = json_decode($deletedFields, true);

            if (count($delete['fields']) > 0) {
                for ($a = 0; $a < count($delete['fields']); $a++) {
                    $delet_query = "DELETE FROM fieldOptions WHERE fieldID = :fieldID";
                    $vars_delete = array(
                        ':fieldID' => $delete['fields'][$a]['id'],
                    );
                    $result = $db->exec($delet_query, $vars_delete);

                    $delet_query2 = "DELETE FROM fields WHERE id = :fieldID";
                    $vars_delete2 = array(
                        ':fieldID' => $delete['fields'][$a]['id'],
                    );
                    $result2 = $db->exec($delet_query2, $vars_delete2);
                }
            }

            if (count($data['formData']) > 0) {
                for ($i = 0; $i < count($data['formData']); $i++) {
                    $query2 = "INSERT INTO `fields` (`formID`, `title`, `type`, `dateCreated`)
                    VALUES (:formID, :title, :type, :dateCreated)";
                    $vars2 = array(
                        ':formID' => $data['formID'],
                        ':title' => $data['formData'][$i]['label'],
                        ':type' => $data['formData'][$i]['type'],
                        ':dateCreated' => date('Y-m-d H:i:s'),
                    );
                    $results2 = $db->exec($query2, $vars2);
                    $fieldID = $db->lastInsertId();

                    if ($data['formData'][$i]['type'] == "select") {
                        # create field options
                        for ($x = 0; $x < count($data['formData'][$i]['options']); $x++) {
                            $query3 = "INSERT INTO `fieldOptions` (`fieldID`, `title`, `value`, `dateCreated`)
                            VALUES (:fieldID, :title, :value, :dateCreated)";
                            $vars3 = array(
                                ':fieldID' => $fieldID,
                                ':title' => $data['formData'][$i]['options'][$x]['label'],
                                ':value' => $data['formData'][$i]['options'][$x]['value'],
                                ':dateCreated' => date('Y-m-d H:i:s'),
                            );
                            $results3 = $db->exec($query3, $vars3);
                        }
                    }
                }
            }
            echo 'Form updated successfully!';
        } catch (Exception $exception) {
            echo json_encode($exception->getMessage());
        }
    }

    public function adminFormSave($f3, $params)
    {
        try {
            $this->authCheck($f3);
            $formtype = $params['formtype'];
            $userID = $f3->get('POST.userID');
            $update = true;

            $attributes = $f3->get('POST');
            $definition = $this->getDefinition($attributes);
            $idArray = ['userID =?', $userID];
            if ($f3->get('POST')) {

                if ($update) {
                    $info = $this->getRepository("applicantdetails")->updateRecord($idArray, $definition);
                } else {
                    $info = $this->getRepository("applicantdetails")->createRecord($definition);
                }
                echo "Info saved successfully!";
                $f3->set('SESSION.message', array('msg' => 'Data saved successfully!', 'alert' => 'info'));
            } else {
                echo "No information was saved!";
            }
        } catch (Exception $exception) {
            echo json_encode($exception->getMessage());
        }
    }

    public function getApplicationVerification($f3, $params)
    {
        global $db;
        $this->authCheck($f3);
        $curlResponse = [];
        $applicationID = $params['applicationID'];

        $query = "SELECT userID  FROM applications WHERE id = :id";
        $vars = array(
            ':id' => $applicationID,
        );
        $applications = $db->exec($query, $vars);
        $curlResponse['applications'] = json_encode($applications);

        $query = "SELECT ad.*, uc.id as checkID, uc.title, uc.isVerified from applicantdetails ad LEFT JOIN userChecklist uc ON uc.userID = ad.userID WHERE ad.userID = :userID";
        $vars = array(
            ':userID' => $applications[0]['userID'],
        );
        $results = $db->exec($query, $vars);
        $curlResponse['results'] = json_encode($results);

        $query = "SELECT dt.*, ad.id AS applicantID FROM applicantdetails ad LEFT JOIN applicantdata dt ON ad.userID = dt.userID WHERE ad.userID = :userID";
        $vars = array(
            ':userID' => $applications[0]['userID'],
        );
        $userdata = $db->exec($query, $vars);
        $curlResponse['userdata'] = json_encode($userdata);

        $query = "SELECT * from emergencyContacts WHERE applicantID = :applicantID ORDER BY id ASC;";
        $vars = array(
            ':applicantID' => $userdata[0]['applicantID'],
        );
        $emergency_contacts = $db->exec($query, $vars);
        $curlResponse['emergency_contacts'] = json_encode($emergency_contacts);

        $arrCheckList = [];
        $getApplicationVerification = [];
        $arrCheckList["personalInfo"] = ["firstName", "lastName", "idPassportNo", "dob", "address", "maritalStatus", "bankName", "branchCode", "accountNumber", "accountType"];
        $arrCheckList["contactInfo"] = ["email", "phone", "altPhone"];
        $arrCheckList["employerInfo"] = ["occupation", "employer", "employerContact", "totalIncome", "totalExpenses"];
        $arrCheckList["emergencyInfo"] = [];
        for ($i=0; $i < count($emergency_contacts); $i++) { 
            array_push($arrCheckList["emergencyInfo"], "emergencyContact".($i+1)."FullName");
            array_push($arrCheckList["emergencyInfo"], "emergencyContact".($i+1)."PhoneNumber");
        }
        $arrCheckList["documentInfo"] = ["idDocument", "payslip", "bankStatement", "proofOfPayment"];
        $isColumn = [];
        foreach ($arrCheckList as $key => $arrlist) {

            foreach ($arrlist as $column) {
                $isVerified = 'p';
                $val = '';
                $title = $column;

                foreach ($results as $list) {
                    array_push($isColumn, $column);
                    if ($list["title"] == $column) {
                        $isVerified = $list["isVerified"];
                    }
                    if (isset($list[$column])) {
                        $val = $list[$column];
                    }
                }
                if ($key == "personalInfo") {
                    $getApplicationVerification[] = ["title" => $title, "value" => $val, "isVerified" => $isVerified, "applicantData" => $results[0]["applicantData"]];
                }
                if ($key == "contactInfo") {
                    array_push($isColumn, $column);

                    $getApplicationVerification[] = ["title" => $title, "value" => $results[0][$column], "isVerified" => $isVerified, "applicantData" => $results[0]["applicantData"]];
                }
                if ($key == "employerInfo") {
                    array_push($isColumn, $column);

                    $getApplicationVerification[] = ["title" => $title, "value" => $results[0][$column], "isVerified" => $isVerified, "applicantData" => $results[0]["applicantData"]];
                }
                if ($key == "emergencyInfo") {
                    array_push($isColumn, $column);

                    $getApplicationVerification[] = ["title" => $title, "value" => $results[0][$column], "isVerified" => $isVerified, "applicantData" => $results[0]["applicantData"]];
                }
                if ($key == "documentInfo") {
                    array_push($isColumn, $column);
                    $getApplicationVerification[] = ["title" => $title, "value" => $results[0][$column], "isVerified" => $isVerified, "applicantData" => $results[0]["applicantData"]];
                }
            }
        }

        echo json_encode($curlResponse);
    }

    public function approveApplication($f3, $params)
    {
        global $db;
        $this->authCheck($f3);

        $applicationID = $params['applicationID'];
        $curlResponse = [];

        $query = "SELECT ap.userID, cp.complexName  FROM applications ap
                    LEFT JOIN complex cp ON cp.id = ap.complexID
                    WHERE ap.id = :id";
        $vars = array(
            ':id' => $applicationID,
        );
        $applications = $db->exec($query, $vars);
        $curlResponse['applications'] = json_encode($applications);

        //check application percentage
        $query = "SELECT dt.*  FROM applicantdetails ad 
        LEFT JOIN applicantdata dt ON ad.userID = dt.userID AND (dt.applicationID = :applicationID OR dt.applicationID IS NULL)
        WHERE ad.userID = :userID;";
        $vars = array(
            ':userID' => $applications[0]['userID'],
            ':applicationID' => $applicationID
        );
        $userdata = $db->exec($query, $vars);
        $curlResponse['userdata'] = json_encode($userdata);

        $query = "SELECT ad.*, uc.id as checkID, uc.title, uc.isVerified 
        FROM applicantdetails ad 
        LEFT JOIN userChecklist uc ON uc.userID = ad.userID AND (uc.applicationID = :applicationID OR uc.applicationID IS NULL)
        WHERE ad.userID = :userID";
        $vars = array(
            ':userID' => $applications[0]['userID'],
            ':applicationID' => $applicationID
        );
        $results = $db->exec($query, $vars);
        $curlResponse['results'] = json_encode($results);

        $query = "SELECT rentTrench from applicantdetails WHERE userID = :userID";
        $vars = array(
            ':userID' => $applications[0]['userID'],
        );
        $rentTrench = $db->exec($query, $vars);
        $curlResponse['rentTrench'] = json_encode($rentTrench);

        $query = "SELECT * FROM rentTrench WHERE id = :id LIMIT 1;";
        $vars = array(
            ':id' => $rentTrench[0]['rentTrench'],
        );
        $tenantTrench = $db->exec($query, $vars);

        $query = "SELECT * from rentTrench WHERE maxIncome < :minIncome OR id = :id ORDER BY maxIncome DESC LIMIT 2";
        $vars = array(
            ':minIncome' => $tenantTrench[0]['minIncome'],
            ':id' => $rentTrench[0]['rentTrench'],
        );
        $trenchs = $db->exec($query, $vars);
        $curlResponse['trenchs'] = json_encode($trenchs);
        $curlResponse['tenantTrench'] = $rentTrench[0]['rentTrench'];


        $query = "SELECT * FROM units WHERE trenchID = :trenchID AND occupied = :occupied";
        $vars = array(
            ':trenchID' => $rentTrench[0]['rentTrench'],
            ':occupied' => "n",

        );
        $units = $db->exec($query, $vars);
        $curlResponse['units'] = json_encode($units);

        $query = "SELECT * FROM rentalproperties WHERE applicationID = :applicationID AND deleted = :deleted";
        $vars = array(
            ':applicationID' => $applicationID,
            ':deleted' => 'n'
        );
        $rentalproperty = $db->exec($query, $vars);
        $curlResponse['rentalproperty'] = json_encode($rentalproperty);

        $query = "SELECT * FROM propertyagents WHERE isDeleted = :isDeleted";
        $vars = array(
            ':isDeleted' => 'n',
        );
        $agents = $db->exec($query, $vars);
        $curlResponse['agents'] = json_encode($agents);

        $query = "SELECT * from emergencyContacts WHERE applicantID = :applicantID ORDER BY id ASC;";
        $vars = array(
            ':applicantID' =>  $results[0]['id'],
        );
        $emergency_contacts = $db->exec($query, $vars);
        $curlResponse['emergency_contacts'] = json_encode($emergency_contacts);


        echo json_encode($curlResponse);
    }

    public function getpropertyReferenceCharges($f3, $params)
    {
        global $db;
        $propertyReferenceID = $params['propertyReferenceID'];
        $curlResponse = [];
        $query = "SELECT * FROM rentalproperties WHERE id = :id";
        $vars = array(
            ':id' => $propertyReferenceID,
        );
        $rentalProperty = $db->exec($query, $vars);
        $curlResponse['rentalProperty'] = json_encode($rentalProperty);

        $query = "SELECT * FROM charges
                    WHERE propertyReference = :propertyReference";
        $vars = array(
            ':propertyReference' => $rentalProperty[0]['propertyReference'],
        );
        $charges = $db->exec($query, $vars);
        $curlResponse['charges'] = json_encode($charges);
        echo json_encode($curlResponse);
    }

    public function addCharge($f3)
    {
        global $db, $ip;
        $curlResponse = [];
        $query = "INSERT INTO charges (propertyReference, chargeName, chargeDescription, cost, dateCreated) 
                                VALUES (:propertyReference, :chargeName, :chargeDescription, :cost, NOW())";
        $vars = array(
            ':propertyReference' =>  $f3->get('POST.propertyReference'),
            ':chargeName' => $f3->get('POST.chargeName'),
            ':chargeDescription' =>  $f3->get('POST.chargeDescription'),
            ':cost' =>  (float) $f3->get('POST.cost'),
        );
        $charge = $db->exec($query, $vars);
    }

    public function getProofOfPayment($f3, $params)
    {
        global $db;
        $this->authCheck($f3);
        $curlResponse = [];
        $applicationID = $params['applicationID'];

        $query = "SELECT userID FROM applications
                    WHERE id = :id";
        $vars = array(
            ':id' => $applicationID,
        );
        $applications = $db->exec($query, $vars);
        $curlResponse['applications'] = json_encode($applications);

        //check application percentage
        $query = "SELECT *  FROM applicantdata 
        WHERE userID = :userID AND dataType IN ('proofOfPayment', 'onlinePayment') AND applicationID = :applicationID";
        $vars = array(
            ':userID' => $applications[0]['userID'],
            ':applicationID' => $applicationID
        );
        $applicantdata = $db->exec($query, $vars);
        $curlResponse['applicantdata'] = json_encode($applicantdata);

        $query = "SELECT * FROM screeningAPIs WHERE enabled = :enabled";
        $vars = array(
            ':enabled' => 'e',
        );
        $screeningAPIs = $db->exec($query, $vars);
        $curlResponse['screeningAPIs'] = json_encode($screeningAPIs);
        echo json_encode($curlResponse);
    }
    
    public function getRentalProofOfPayment($f3, $params)
    {
        global $db;
        $this->authCheck($f3);
        $applicationID = $params['applicationID'];
        $curlResponse = [];

        $query = "SELECT userID FROM applications
                    WHERE id = :id";
        $vars = array(
            ':id' => $applicationID,
        );
        $applications = $db->exec($query, $vars);
        $curlResponse['applications'] = json_encode($applications);

        //check application percentage
        $query = "SELECT *  FROM applicantdata WHERE userID = :userID AND dataType = :dataType AND dataLabel = :dataLabel";
        $vars = array(
            ':userID' => $applications[0]['userID'],
            ':dataType' => "proofOfPayment",
            ':dataLabel' => 'rentDeposit'

        );
        $applicantdata = $db->exec($query, $vars);
        $curlResponse['applicantdata'] = json_encode($applicantdata);
        echo json_encode($curlResponse);
    }


    public function getComplexInfo($f3, $params)
    {
        global $db, $ip;
        $this->authCheck($f3);
        $curlResponse = [];
        $complexID = $params['complexID'];
        $query = "SELECT applicationFee, bankName, bankAccount, branch, accountType, transactionCharge FROM complex
                    WHERE id = :id";
        $vars = array(
            ':id' => $complexID,
        );
        $complexinfo = $db->exec($query, $vars);
        $curlResponse['complexinfo'] = json_encode($complexinfo);

        echo json_encode($curlResponse);
    }

    public function availableUnits($f3, $params)
    {
        global $db;
        $this->authCheck($f3);
        $curlResponse = [];
        $complexID = $params['complexID'];
        $applicationID = $params['applicationID'];
        $approvalData = [];

        $query = "SELECT ap.userID, cp.complexName  FROM applications ap
                    LEFT JOIN complex cp ON cp.id = ap.complexID
                    WHERE ap.id = :id";
        $vars = array(
            ':id' => $applicationID,
        );
        $applications = $db->exec($query, $vars);
        $curlResponse['applications'] = json_encode($applications);

        $query = "SELECT rentTrench from applicantdetails WHERE userID = :userID";
        $vars = array(
            ':userID' => $applications[0]['userID'],
        );
        $rentTrench = $db->exec($query, $vars);
        $curlResponse['rentTrench'] = json_encode($rentTrench);

        $query = "SELECT * FROM units WHERE trenchID = :trenchID AND occupied = :occupied AND complexID = :complexID";
        $vars = array(
            ':trenchID' => $rentTrench[0]['rentTrench'],
            ':occupied' => "n",
            ':complexID' => $complexID
        );
        $units = $db->exec($query, $vars);
        $curlResponse['units'] = json_encode($units);
        $approvalData['units'] = $units;
        echo json_encode($curlResponse);
    }

    public function affordableUnits($f3, $params)
    {
        global $db;
        $this->authCheck($f3);
        $curlResponse = [];
        $complexID = $params['complexID'];
        $applicationID = $params['applicationID'];
        $approvalData = [];

        $query = "SELECT ap.userID, cp.complexName  FROM applications ap
                    LEFT JOIN complex cp ON cp.id = ap.complexID
                    WHERE ap.id = :id";
        $vars = array(
            ':id' => $applicationID,
        );
        $applications = $db->exec($query, $vars);
        $curlResponse['applications'] = json_encode($applications);

        $query = "SELECT id from rentTrench WHERE minIncome > :minIncome";
        $vars = array(
            ':minIncome' => 15000,
        );
        $rentTrench = $db->exec($query, $vars);
        $curlResponse['rentTrench'] = json_encode($rentTrench);

        $affordableUnits = array();
        for ($i=0; $i < count($rentTrench); $i++) { 
            $query = "SELECT * FROM units WHERE trenchID = :trenchID AND occupied = :occupied AND complexID = :complexID";
            $vars = array(
                ':trenchID' => $rentTrench[$i]['id'],
                ':occupied' => "n",
                ':complexID' => $complexID
            );
            $results = $db->exec($query, $vars);
            $affordableUnits = array_merge_recursive($affordableUnits, $results);
        }
        $curlResponse['affordableUnits'] = json_encode($affordableUnits);
        echo json_encode($curlResponse);
    }

    public function trenchUnits($f3, $params)
    {
        global $db;
        $this->authCheck($f3);
        $curlResponse = [];
        $complexID = $params['complexID'];
        $applicationID = $params['applicationID'];
        $trenchID = $params['trenchID'];
        $approvalData = [];

        $query = "SELECT ap.userID, cp.complexName  FROM applications ap
                    LEFT JOIN complex cp ON cp.id = ap.complexID
                    WHERE ap.id = :id";
        $vars = array(
            ':id' => $applicationID,
        );
        $applications = $db->exec($query, $vars);
        $curlResponse['applications'] = json_encode($applications);

        $query = "SELECT id from rentTrench WHERE id = :id";
        $vars = array(
            ':id' => $trenchID,
        );
        $rentTrench = $db->exec($query, $vars);
        $curlResponse['rentTrench'] = json_encode($rentTrench);

        $trenchUnits = array();
        for ($i=0; $i < count($rentTrench); $i++) { 
            $query = "SELECT * FROM units WHERE trenchID = :trenchID AND occupied = :occupied AND complexID = :complexID";
            $vars = array(
                ':trenchID' => $rentTrench[$i]['id'],
                ':occupied' => "n",
                ':complexID' => $complexID
            );
            $results = $db->exec($query, $vars);
            $trenchUnits = array_merge_recursive($trenchUnits, $results);
        }
        $curlResponse['trenchUnits'] = json_encode($trenchUnits);
        echo json_encode($curlResponse);
    }

}
