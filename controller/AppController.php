<?php

require_once 'AppDefinition.php';

require_once 'AppRepository.php';

require_once 'utilities/Auth.php';

require_once 'utilities/Emailer.php';

require_once 'utilities/Response.php';

require_once 'passwordController.php';

require_once 'utilities/PasswordGenerator.php';

require_once 'SendGrid/SendGrid.php';
require_once 'SendGrid/Mail.php';
require_once 'Twilio/src/Twilio/autoload.php';


//use SoapClient;
use Twilio\Rest\Client;

// Save The Keys In Your Configuration File
define('FIRSTKEY', 'Lk5Uz3slx3BrAghS1aaW5AYgWZRV0tIX5eI0yPchFz4=');
define('SECONDKEY', 'EZ44mFi3TlAey1b2w4Y7lVDuqO+SRxGXsa7nctnr/JmMrA2vN6EJhrvdVZbxaQs5jpSe34X3ejFK/o9+Y5c83w==');


abstract class AppController
{

    protected $f3;

    public function __construct()
    {
        $f3 = Base::instance();
        $this->f3 = $f3;

        if (Auth::user($f3)) {
            $f3->set('auth', array('user' => Auth::user($f3)));
        }
    }

    /** 
     * Get header Authorization
     * */
    public function getAuthorizationHeader(){
        $headers = null;
        if (isset($_SERVER['Authorization'])) {
            $headers = trim($_SERVER["Authorization"]);
        }
        else if (isset($_SERVER['HTTP_AUTHORIZATION'])) { 
            $headers = trim($_SERVER["HTTP_AUTHORIZATION"]);
        } elseif (function_exists('apache_request_headers')) {
            $requestHeaders = apache_request_headers();
            // Server-side fix for bug in old Android versions (a nice side-effect of this fix means we don't care about capitalization for Authorization)
            $requestHeaders = array_combine(array_map('ucwords', array_keys($requestHeaders)), array_values($requestHeaders));
            //print_r($requestHeaders);
            if (isset($requestHeaders['Authorization'])) {
                $headers = trim($requestHeaders['Authorization']);
            }
        }
        return $headers;
    }
    /**
    * get access token from header
    * */
    public function getBearerToken() {
        $headers = $this->getAuthorizationHeader();
        // HEADER: Get the access token from the header
        if (!empty($headers)) {
            if (preg_match('/Bearer\s(\S+)/', $headers, $matches)) {
                return $matches[1];
            }
        }
        return null;
    }

    protected function authCheck($f3)
    {
        //$f3->set('SESSION.count', 1);
        // if (!Auth::user($f3)) {
        //     $f3->set('SESSION.message', array('msg' => 'You must login to continue', 'alert' => 'danger'));

        //     echo json_encode(array('msg' => 'You must login to continue', 'alert' => 'danger'));
        //     //$f3->reroute('/login');
        // }
        // if ($f3->get('SESSION.LAST_ACTIVITY') + 3600 < time() && $f3->get('SESSION.login') != 'login') {
        //     $f3->set('SESSION.login', 'login');
        //     $f3->set('SESSION.message', array('msg' => 'You must login to continue', 'alert' => 'danger'));
        //     echo json_encode(array('msg' => 'You must login to continue', 'alert' => 'danger'));
 
        //     //$f3->reroute('/login');
        // }

        // $authToken = $this->getBearerToken();
            
        $token = "080042cad6356ad5dc0a720c18b53b8e53d4c274";

        if($f3->get('POST.SECRECT_KEY') != $token){
            
            echo json_encode(array('msg' => 'You must login to continue', 'alert' => 'danger'));
            return false;

        }
        

    }



    public function fileUpload($f3, $formFieldName, $folder)
    {

        if (!file_exists("uploads/$folder")) {
            mkdir("uploads/$folder", 0777, true);
            chmod("uploads/$folder", 0777);
        }
        $f3->set('UPLOADS', "uploads/$folder/"); // don't forget to set an Upload directory, and make it writable!
        $web = \Web::instance();
        //$mime = $web->mime($file); // returns 'image/jpeg'
        $overwrite = true; // set to true, to overwrite an existing file; Default: false
        $slug = true; // rename file to filesystem-friendly version

        $files = $web->receive(function ($file, $formFieldName) {

            //var_dump($file);
            /* looks like:
            array(5) {
            ["name"] =>     string(19) "csshat_quittung.png"
            ["type"] =>     string(9) "image/png"
            ["tmp_name"] => string(14) "/tmp/php2YS85Q"
            ["error"] =>    int(0)
            ["size"] =>     int(172245)
            } */

            //$extArr = ['jpg', 'jpeg', 'png', 'pdf'];
            $ext = explode(".", $file['name']);

            // // $file['name'] already contains the slugged name now
            // if(!in_array($ext, $fileExt)) // if ext not support
            //     return "This uploaded file is not valid"; //, return false will skip moving it

            // maybe you want to check the file size
            if ($file['size'] > (5 * 1024 * 1024)) // if bigger than 5 MB
            {
                return "Error, your file is too large.";
            }
            // this file is not valid, return false will skip moving it
            // everything went fine, hurray!
            return true; // allows the file to be moved from php tmp dir to your defined upload dir
        }, true, function ($fileBaseName, $formFieldName) {
            // build new file name from base name or input field name
            $ext = substr($fileBaseName,-5);
            $extArr = explode(".",$ext);

            return $formFieldName.".".$extArr[1];
        }, $overwrite, $slug
        );

        return $files;
    }

    // Generates a strong password of N length containing at least one lower case letter,
    // one uppercase letter, one digit, and one special character. The remaining characters
    // in the password are chosen at random from those four sets.
    //
    // The available characters in each set are user friendly - there are no ambiguous
    // characters such as i, l, 1, o, 0, etc. This, coupled with the $add_dashes option,
    // makes it much easier for users to manually type or speak their passwords.
    //
    // Note: the $add_dashes option will increase the length of the password by
    // floor(sqrt(N)) characters.

    protected function generateStrongPassword($length = 9, $add_dashes = false, $available_sets = 'luds')
    {
        $sets = array();
        if(strpos($available_sets, 'l') !== false)
            $sets[] = 'abcdefghjkmnpqrstuvwxyz';
        if(strpos($available_sets, 'u') !== false)
            $sets[] = 'ABCDEFGHJKMNPQRSTUVWXYZ';
        if(strpos($available_sets, 'd') !== false)
            $sets[] = '23456789';
        if(strpos($available_sets, 's') !== false)
            $sets[] = '!@#$%&*?';

        $all = '';
        $password = '';
        foreach($sets as $set)
        {
            $password .= $set[array_rand(str_split($set))];
            $all .= $set;
        }

        $all = str_split($all);
        for($i = 0; $i < $length - count($sets); $i++)
            $password .= $all[array_rand($all)];

        $password = str_shuffle($password);

        if(!$add_dashes)
            return $password;

        $dash_len = floor(sqrt($length));
        $dash_str = '';
        while(strlen($password) > $dash_len)
        {
            $dash_str .= substr($password, 0, $dash_len) . '-';
            $password = substr($password, $dash_len);
        }
        $dash_str .= $password;
        return $dash_str;
    }

    public function send_email($f3, $attributes, $subject, $message)
    {
        $sendgrid_api_key = "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";
        //$sendgrid_api_key = "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";
        $headers = array(
            "Authorization: Bearer $sendgrid_api_key",
            "Content-Type: application/json"
        );

        $data = array(
            "personalizations" => array(
                array(
                    "to" => array(
                        array(
                            "email" => $attributes['email'],
                            "name" => $attributes['firstName']
                        )
                    )
                )
            ),
            "from" => array(
                "email" => "tmsapplications@remax-townandcountry.co.za"
            ),
            "subject" => $subject,
            "content" => array(
                array(
                    "type" => "text/html",
                    "value" => $message
                )
            )
        );

        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://api.sendgrid.com/v3/mail/send",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => json_encode($data),
            CURLOPT_HTTPHEADER => $headers,
        ));
        $response = curl_exec($curl);
        curl_close($curl);
        return $response;
    }

    protected function dispatchApplicationEmail($attributes, $subject, $template, $send_to)
    {
        $message = file_get_contents("ui/emailTemplates/$template.php");

        $host_url = 'tms.remax-townandcountry.co.za';
        if($_SERVER['HTTP_HOST'] == "remaxstage.smartview.co.za"){
            $host_url = 'tmsstage.remax-townandcountry.co.za';
        } else {
            $host_url = 'tms.remax-townandcountry.co.za';
        }

        //Replace the codetags with the message contents
        $replacements = array(
            '({email})' => $attributes['email'],
            '({password})' => $attributes['password'],
            '({spouse_name})' => $attributes['name'],
            '({user})' => $attributes['user'],
            '({ticket})' => $attributes['ticket'],
            '({firstName})' => $attributes['firstName'],
            '({complexName})' => $attributes['complexName'],
            '({spouse_consent_url})' => 'https://' . $host_url.'/consent/'.$attributes['id'],
            '({support_email})' => 'support@remax-townsandcountry.co.za',
            '({login_url})' => 'https://' . $host_url."/login",
            '({help_url})' => 'https://' . $host_url,
            '({admin_url})' => 'https://'.$host_url.'/login',
        );
        $message = preg_replace(array_keys($replacements), array_values($replacements), $message);
        try {
            $attributes['email'] = $send_to;
            $response = $this->send_email($f3, $attributes, $subject, $message);
        } catch (Exception $e) {
            echo 'Caught exception: ' . $e->getMessage() . "\n";
        }
    }

    protected function dispatchEmail($attributes, $subject, $template)
    {
        $message = file_get_contents("ui/emailTemplates/$template.php");
        
        $host_url = 'tms.remax-townandcountry.co.za';
        if($_SERVER['HTTP_HOST'] == "remaxstage.smartview.co.za"){
            $host_url = 'tmsstage.remax-townandcountry.co.za';
        } else {
            $host_url = 'tms.remax-townandcountry.co.za';
        }
        
        //Replace the codetags with the message contents
        $replacements = array(
            '({email})' => $attributes['email'],
            '({password})' => $attributes['password'],
            '({property_owner})' => $attributes['property_owner'],
            '({property_reference})' => $attributes['property_reference'],
            '({spouse_name})' => $attributes['name'],
            '({firstName})' => $attributes['firstName'],
            '({viewingDate})' => $attributes['viewingDate'],
            '({tenantType})' => $attributes['tenantType'],
            '({unitName})' => $attributes['unitName'],
            '({unitDescription})' => $attributes['unitDescription'],
            '({unitSize})' => $attributes['unitSize'],
            '({rental})' => $attributes['rental'],
            '({deposit})' => $attributes['deposit'],
            '({complexName})' => $attributes['complexName'],
            '({spouse_consent_url})' => 'https://' . $host_url. '/consent/'.$attributes['id'],
            '({support_email})' => 'support@zelriprop.talentindx.co.za',
            '({login_url})' => 'https://' . $host_url. "/login",
            '({help_url})' => 'https://' . $host_url,
            '({welcomeLetter_url})' => 'https://' . $host_url. "/".$attributes['welcomeLetter'],
            // '({keyslip_url})' => 'http://' . $host_url. "/".$attributes['keyslip'],
            '({leaseAgreement_url})' => 'https://' . $host_url. "/".$attributes['leaseAgreement'],
            '({approvalLetter_url})' => 'https://' . $host_url. "/".$attributes['approvalLetter'],
            '({action_url})' => 'https://' . $host_url . "/activate"."/".$attributes['verificationCode'],
        );

        $message = preg_replace(array_keys($replacements), array_values($replacements), $message);

        // $sendgrid = new SendGrid\SendGrid('keenangeorge', 'Charnte#1');

        // // Make a message object
        // $email = new SendGrid\Mail();

        // $email->setFrom("support@remax-townandcountry.co.za", "Zelriprop Team");
        // $email->setSubject($subject);
        // $email->addTo($attributes['email'], $attributes['firstName']);
        // $email->setCcs(array());
        // $email->setBccs(array());
        // //$email->setText("and easy to do anywhere, even with PHP");
        // $email->setHtml($message);
        
        try {
            $response = $this->send_email($f3, $attributes, $subject, $message);
            //$response = $sendgrid->send($email);
            //print $response->statusCode() . "\n";
            //print_r($response->headers());
            //print $response->body() . "\n";
        } catch (Exception $e) {
            echo 'Caught exception: ' . $e->getMessage() . "\n";
        }
    }

    protected function createInvoice($attributes, $invoice_title, $invoice_number,  $template)
    {
        $message = file_get_contents("ui/invoiceTemplates/$template.php");
        $subject = $invoice_title;

        //Replace the codetags with the message contents
        $replacements = array(
            '({firstName})'             => $attributes['tenant_name'],
            "({company_logo})"          => "http://tms.smartviewtechnology.co.za/static/img/user/invoicelogo.jpg",
            "({company_name})"          => "Zelri Properties",
            "({company_address})"       => "518 Paul Kruger St, Pretoria Central, Pretoria, 0002",
            "({company_city_zip_state})"=> " ● 0002 Pretoria Central, Pretoria",
            "({company_phone_fax})"     => "012 343 4548",
            "({company_email_web})"     => "https://www.zelriprop.co.za ● https://www.zelriprop.co.za",
            "({issue_date_label})"      => "Issue Date: ",
            "({issue_date})"            => $attributes['issue_date'],
            "({bill_to_label})"         => "Bill to",
            "({client_name})"           => $attributes['tenant_name'],
            "({client_address})"        => $attributes['tenant_address'],
            "({client_phone_fax})"      => $attributes['tenant_phone'],
            "({client_email})"          => $attributes['email'],
            "({invoice_title})"         => $invoice_title,
            "({invoice_number})"        => "#".$invoice_number,
            "({item_row_number_label})" => "",
            "({item_description_label})"=> "Description",
            "({item_price_label})"      => "Cost",
            "({item_line_total_label})" => "Linetotal",
            "({item_row_number})"       => 1,
            "({item_description})"      => $attributes['invoice_description'],
            "({item_price})"            => 'Deposit R'.$attributes['invoice_deposit_price'] . ' + Lease R' . $attributes['invoice_lease_price'],
            "({item_line_total})"       => "R ".$attributes['amount_total'],
            "({amount_total_label})"    => "Total",
            "({amount_total})"          => "R ".$attributes['amount_total'],
            "({amount_paid_label})"     => "Paid",
            "({amount_paid})"           => "R ".$attributes['amount_total'],
            "({terms_label})"           => "Terms & Notes",
            "({terms})"                 => $attributes['tenant_name'].", thank you very much. We really appreciate your business."
        );
     
        $message = preg_replace(array_keys($replacements), array_values($replacements), $message);

        // $sendgrid = new SendGrid\SendGrid('keenangeorge', 'Charnte#1');

        // // Make a message object
        // $email = new SendGrid\Mail();

        // $email->setFrom("support@remax-townandcountry.co.za", "Zelriprop Team");
        // $email->setSubject($subject);
        // $email->addTo($attributes['email'], $attributes['firstName']);
        // $email->setCcs(array());
        // $email->setBccs(array());
        //$email->setText("and easy to do anywhere, even with PHP");

        // var_dump($message);exit();
        //$email->setHtml($message);
        
        try {
            $response = $this->send_email($f3, $attributes, $subject, $message);
           // $response = $sendgrid->send($email);
            //print $response->statusCode() . "\n";
            //print_r($response->headers());
            //print $response->body() . "\n";
        } catch (Exception $e) {
            echo 'Caught exception: ' . $e->getMessage() . "\n";
        }
    }

    protected function secured_encrypt($data)
    {
        $first_key = base64_decode(FIRSTKEY);
        $second_key = base64_decode(SECONDKEY);

        $method = "aes-256-cbc";
        $iv_length = openssl_cipher_iv_length($method);
        $iv = openssl_random_pseudo_bytes($iv_length);

        $first_encrypted = openssl_encrypt($data, $method, $first_key, OPENSSL_RAW_DATA, $iv);
        $second_encrypted = hash_hmac('sha3-512', $first_encrypted, $second_key, TRUE);

        $output = base64_encode($iv.$second_encrypted.$first_encrypted);
        return $output;
    }

    protected function secured_decrypt($input)
    {
        $first_key = base64_decode(FIRSTKEY);
        $second_key = base64_decode(SECONDKEY);
        $mix = base64_decode($input);

        $method = "aes-256-cbc";
        $iv_length = openssl_cipher_iv_length($method);

        $iv = substr($mix, 0, $iv_length);
        $second_encrypted = substr($mix, $iv_length, 64);
        $first_encrypted = substr($mix, $iv_length+64);

        $data = openssl_decrypt($first_encrypted, $method, $first_key, OPENSSL_RAW_DATA, $iv);
        $second_encrypted_new = hash_hmac('sha3-512', $first_encrypted, $second_key, TRUE);

        if (hash_equals($second_encrypted, $second_encrypted_new))
            return $data;

        return false;
    }

    protected function viewMyInvoice($attributes, $invoice_title, $invoice_number,  $template)
    {
        $message = file_get_contents("ui/invoiceTemplates/$template.php");
        $subject = $invoice_title;

        //Replace the codetags with the message contents
        $replacements = array(
            '({firstName})'             => $attributes['tenant_name'],
            "({company_logo})"          => "http://tms.smartviewtechnology.co.za/static/img/user/invoicelogo.jpg",
            "({company_name})"          => "Zelri Properties",
            "({company_address})"       => "518 Paul Kruger St, Pretoria Central, Pretoria, 0002",
            "({company_city_zip_state})"=> " ● 0002 Pretoria Central, Pretoria",
            "({company_phone_fax})"     => "012 343 4548",
            "({company_email_web})"     => "https://www.zelriprop.co.za ● https://www.zelriprop.co.za",
            "({issue_date_label})"      => "Issue Date: ",
            "({issue_date})"            => $attributes['issue_date'],
            "({bill_to_label})"         => "Bill to",
            "({client_name})"           => $attributes['tenant_name'],
            "({client_address})"        => $attributes['tenant_address'],
            "({client_phone_fax})"      => $attributes['tenant_phone'],
            "({client_email})"          => $attributes['email'],
            "({invoice_title})"         => $invoice_title,
            "({invoice_number})"        => "#".$invoice_number,
            "({item_row_number_label})" => "",
            "({item_description_label})"=> "Description",
            "({item_price_label})"      => "Cost",
            "({item_line_total_label})" => "Linetotal",
            "({item_row_number})"       => 1,
            "({item_description})"      => $attributes['invoice_description'],
            "({item_price})"            => 'Deposit R'.$attributes['invoice_deposit_price'] . ' + Lease R' . $attributes['invoice_lease_price'],
            "({item_line_total})"       => "R ".$attributes['amount_total'],
            "({amount_total_label})"    => "Total",
            "({amount_total})"          => "R ".$attributes['amount_total'],
            "({amount_paid_label})"     => "Paid",
            "({amount_paid})"           => "R ".$attributes['amount_total'],
            "({terms_label})"           => "Terms & Notes",
            "({terms})"                 => $attributes['tenant_name'].", thank you very much. We really appreciate your business."
        );
     
        $message = preg_replace(array_keys($replacements), array_values($replacements), $message);
        print_r($message);
    }
    
/*
    public function xdsLogin($f3, $params){
        global $db, $xdsUrl;

        $userID = $params['userID'];
        $date = date("Y-m-d");
        $query = "SELECT * FROM apiTickets WHERE userID=:userID AND type = :type AND dateCreated = :dateCreated";
        $vars = array(
            ':userID' => $userID,
            ':type' => 'xds',
            ':dateCreated' => $date,
        );
        $xdsTickets = $db->exec($query, $vars);

        if(sizeof($xdsTickets) > 0){
            if($xdsTickets[0]['usagesRemaining'] > 0){
                $connectTicket = $xdsTickets[0]['connectTicket'];
                
            }
            else{
                $connectTicket = "XDS usage limit has been reached";
            }
        }
        else{

            $strUser = $params['strUser'];
            $strPwd = $params['strPwd'];
            $client = new \SoapClient ($xdsUrl); 
            $parameters = array ("strUser"=>$strUser, "strPwd"=>$strPwd); 
            $configQry = $client->Login ($parameters);

            $connectTicket = $configQry->LoginResult;

            $query = "INSERT INTO apiTickets (userID, type, connectTicket, usagesRemaining, dateCreated)
                        VALUES (:userID, :type, :connectTicket, :usagesRemaining, NOW())";
            $vars = array(
                ':userID' =>  $userID,
                ':type' => 'xds',
                ':connectTicket' => $connectTicket,
                ':usagesRemaining' => 50,
            );
            $xdsTickets = $db->exec($query, $vars);
        }

        return $connectTicket;
    }

    public function validateID($f3, $params){
        global $db, $xdsUrl;
        $ConnectTicket =  $params['ConnectTicket'];

        $IdNumber = $params['IDNumber'];
        $FirstName = "";
        $Surname = "";
        $BirthDate = "";
        $YourReference = "";
        $VoucherCode = "";
        $client = new \SoapClient($xdsUrl);
        $parameters = array (
            "ConnectTicket"=>$ConnectTicket,"IdNumber"=>$IdNumber,"PassportNo"=>"","FirstName"=>$FirstName,
            "Surname"=>$Surname,"BirthDate"=>$BirthDate,"YourReference"=>$YourReference,
            "VoucherCode"=>$VoucherCode);
        $ConnectIDVerification = $client->ConnectIDVerification($parameters);
        $ConnectIDVerification = new SimpleXMLElement($ConnectIDVerification->ConnectIDVerificationResult);
        
        $params['apiQuery'] = "ConnectIDVerification";
        $params['apiType'] = "xds";
        $this->apiLogging($f3, $params);

        return $ConnectIDVerification;
    }

    public function validateMaritalStatus($f3, $params){
        global $db, $xdsUrl;
        $ConnectTicket =  $params['ConnectTicket'];

        $IdNumber = $params['IDNumber'];
        $ProductId = 39;
        $EnquiryReason = "";
        $FirstName = "";
        $Surname = "";
        $BirthDate = "";
        $YourReference = "";
        $VoucherCode = "";
        $client = new \SoapClient($xdsUrl);
        $parameters = array ("ConnectTicket"=>$ConnectTicket,"EnquiryReason"=>$EnquiryReason,"ProductId"=>$ProductId,"IdNumber"=>$IdNumber,"PassportNo"=>"","FirstName"=>$FirstName,"Surname"=>$Surname,"BirthDate"=>$BirthDate,"YourReference"=>$YourReference,"VoucherCode"=>$VoucherCode);

        $ConnectConsumerMatch = $client->ConnectConsumerMatch($parameters);
        $ConnectConsumerMatch = new SimpleXMLElement($ConnectConsumerMatch->ConnectConsumerMatchResult);
        //echo $ConnectConsumerMatch->ConsumerDetails->EnquiryID;
       

        //ConnectGetResult
        $EnquiryID = $ConnectConsumerMatch->ConsumerDetails->EnquiryID;
        $EnquiryResultID = $ConnectConsumerMatch->ConsumerDetails->EnquiryResultID;
        $client = new \SoapClient ($xdsUrl);
        $parameters = array (
            "ConnectTicket"=>$ConnectTicket,"EnquiryID"=>$EnquiryID,"EnquiryResultID"=>$EnquiryResultID,
            "ProductID"=>$ProductId);
        $ConnectGetResult = $client->ConnectGetResult($parameters);
        $ConnectGetResult = new SimpleXMLElement($ConnectGetResult->ConnectGetResultResult);

        $params['apiQuery'] = "validateMaritalStatus";
        $params['apiType'] = "xds";
        $this->apiLogging($f3, $params);

        return $ConnectGetResult;
    }

    public function getCreditReportPdf($f3, $params){
        global $db, $xdsUrl;

        $IdNumber = $params['creditIDNo'];
        $userID = $params['userID'];
        $ConnectTicket = $params['ConnectTicket'];

        $client = new \SoapClient ($xdsUrl);
        $parameters = array (
            "ConnectTicket"=>$ConnectTicket,"IdNumber"=>$IdNumber, "PassportNo"=>"");
        $ConnectGetConsumerCreditReportBinary = $client->ConnectGetConsumerCreditReportBinary($parameters);
        $pdfString = $ConnectGetConsumerCreditReportBinary->ConnectGetConsumerCreditReportBinaryResult;

        $params['apiQuery'] = "getCreditReportPdf";
        $params['apiType'] = "xds";
        $this->apiLogging($f3, $params);


        if (strlen($pdfString) < 1000) {
            $pdfString = "Error While Processing the Request, Please contact XDS";

            return $pdfString;
        } else {
            $pdfString = base64_encode($pdfString);
            //$uploadFolder ="reports/user_$userID";
            return $pdfString;
            // if (!is_dir($uploadFolder)) {
            //     mkdir($uploadFolder, 0777, true);
            // }
            // $reportFile = $uploadFolder."/creditReport_$IdNumber.pdf";

            // if(file_put_contents($reportFile, base64_decode($data))){

            //     return $reportFile;
            // }
        }
         
    }

    public function getCreditReport($f3, $params){
        global $db, $xdsUrl;
        $ConnectTicket =  $params['ConnectTicket'];

        $IdNumber = $params['IDNumber'];
        $ProductId = 15;
        $EnquiryReason = "";
        $FirstName = "";
        $Surname = "";
        $BirthDate = "";
        $YourReference = "";
        $VoucherCode = "";
        $client = new \SoapClient($xdsUrl);
        $parameters = array ("ConnectTicket"=>$ConnectTicket,"EnquiryReason"=>$EnquiryReason,"ProductId"=>$ProductId,"IdNumber"=>$IdNumber,"PassportNo"=>"","FirstName"=>$FirstName,"Surname"=>$Surname,"BirthDate"=>$BirthDate,"YourReference"=>$YourReference,"VoucherCode"=>$VoucherCode);

        $ConnectConsumerMatch = $client->ConnectConsumerMatch($parameters);
        $ConnectConsumerMatch = new SimpleXMLElement($ConnectConsumerMatch->ConnectConsumerMatchResult);
        //echo $ConnectConsumerMatch->ConsumerDetails->EnquiryID;

        //ConnectGetResult
        $EnquiryID = $ConnectConsumerMatch->ConsumerDetails->EnquiryID;
        $EnquiryResultID = $ConnectConsumerMatch->ConsumerDetails->EnquiryResultID;
        $client = new \SoapClient ($xdsUrl);
        $parameters = array (
            "ConnectTicket"=>$ConnectTicket,"EnquiryID"=>$EnquiryID,"EnquiryResultID"=>$EnquiryResultID,
            "ProductID"=>$ProductId);
        $ConnectGetResult = $client->ConnectGetResult($parameters);
        $ConnectGetResult = new SimpleXMLElement($ConnectGetResult->ConnectGetResultResult);

        $params['apiQuery'] = "getCreditFullReport";
        $params['apiType'] = "xds";
        $this->apiLogging($f3, $params);

        return $ConnectGetResult;
    }

    public function getCreditReportSummary($f3, $params){
        global $db, $xdsUrl;
        $ConnectTicket =  $params['ConnectTicket'];

        $IdNumber = $params['IDNumber'];
        $ProductId = 132;
        $EnquiryReason = "";
        $FirstName = "";
        $Surname = "";
        $BirthDate = "";
        $YourReference = "";
        $VoucherCode = "";
        $client = new \SoapClient($xdsUrl);
        $parameters = array ("ConnectTicket"=>$ConnectTicket,"EnquiryReason"=>$EnquiryReason,"ProductId"=>$ProductId,"IdNumber"=>$IdNumber,"PassportNo"=>"","FirstName"=>$FirstName,"Surname"=>$Surname,"BirthDate"=>$BirthDate,"YourReference"=>$YourReference,"VoucherCode"=>$VoucherCode);

        $ConnectConsumerMatch = $client->ConnectConsumerMatch($parameters);
        $ConnectConsumerMatch = new SimpleXMLElement($ConnectConsumerMatch->ConnectConsumerMatchResult);

        //ConnectGetResult
        $EnquiryID = $ConnectConsumerMatch->ConsumerDetails->EnquiryID;
        $EnquiryResultID = $ConnectConsumerMatch->ConsumerDetails->EnquiryResultID;
        $client = new \SoapClient ($xdsUrl);
        $parameters = array (
            "ConnectTicket"=>$ConnectTicket,"EnquiryID"=>$EnquiryID,"EnquiryResultID"=>$EnquiryResultID,
            "ProductID"=>$ProductId);
        $ConnectGetResult = $client->ConnectGetResult($parameters);
        $ConnectGetResult = new SimpleXMLElement($ConnectGetResult->ConnectGetResultResult);

        $params['apiQuery'] = "getCreditReportSummary";
        $params['apiType'] = "xds";
        $this->apiLogging($f3, $params);

        return $ConnectGetResult;
    }

    public function getAccountVerification($f3, $params){
        global $db, $xdsUrl;

        $ConnectTicket =  $params['ConnectTicket'];
        $VerificationType = 'Individual';        
        $Entity ="None";
        $Initials = $params['initials'];
        $SurName = $params['lastName'];
        $IDNo = $params['IDNumber'];
        $IDType = "SID";
        $CompanyName = "";
        $Reg1 = "";
        $Reg2 = "";
        $Reg3 = "";
        $TrustNo ="";
        $AccNo = $params['accountNumber'];
        $BranchCode = $params['branchCode'];
        $Acctype = $params['accountType'];
        $BankName = $params['bankName'];
        $client = new \SoapClient ($xdsUrl);
        $parameters = array (
            "ConnectTicket"=>$ConnectTicket,"VerificationType"=>$VerificationType,"Entity"=>$Entity,"Initials"=>$Initials,"SurName"=>$SurName,
            "IDNo"=>$IDNo,"IDType"=>"SID",  "CompanyName"=>"","Reg1"=>"","Reg2"=>"","Reg3"=>"","TrustNo"=>"","AccNo"=>$AccNo,
            "BranchCode"=>$BranchCode,"Acctype"=>$Acctype,"BankName"=>$BankName,
            "VoucherCode"=>"","YourReference"=>"");
        $ConnectAccountVerificationRealTime = $client->ConnectAccountVerificationRealTime($parameters);
        $ConnectAccountVerificationRealTime = new SimpleXMLElement($ConnectAccountVerificationRealTime->ConnectAccountVerificationRealTimeResult);

        $EnquiryLogID = "70395789";//$ConnectAccountVerificationRealTime->ReferenceNo;
        $client = new \SoapClient ($xdsUrl);
        $parameters = array (
            "ConnectTicket"=>$ConnectTicket,"EnquiryLogID"=>$EnquiryLogID);
        $ConnectGetAccountVerificationResult = $client->ConnectGetAccountVerificationResult($parameters);
        $ConnectGetAccountVerificationResult = new SimpleXMLElement($ConnectGetAccountVerificationResult->ConnectGetAccountVerificationResultResult);

        $params['apiQuery'] = "ConnectAccountVerificationRealTime";
        $params['apiType'] = "xds";
        $this->apiLogging($f3, $params);

        return $ConnectGetAccountVerificationResult;
    }

    public function getIncomeEstimate($f3, $params){
        global $db, $xdsUrl;

        $ConnectTicket =  $params['ConnectTicket'];
        $IdNumber = $params['IDNumber'];
        $ProductId = 132;
        $EnquiryReason = "";
        $FirstName = "";
        $Surname = "";
        $BirthDate = "";
        $YourReference = "";
        $VoucherCode = "";
        $MaritalStatus = "";
        $SpouseFirstName = "";
        $SpouseSurname = "";
        $ResidentialAddressLine1 = "";
        $ResidentialAddressLine2 = "";
        $ResidentialAddressLine3 = "";
        $ResidentialAddressLine4 = "";
        $ResidentialPostalCode = "";
        $PostalAddressLine1 = "";
        $PostalAddressLine2 = "";
        $PostalAddressLine3 = "";
        $PostalAddressLine4 = "";
        $PostalPostalCode = "";
        $HomeTelCode = "";
        $HomeTelNo = "";

        $client = new \SoapClient ($xdsUrl);

        $parameters = array ("ConnectTicket"=>$ConnectTicket,"EnquiryReason"=>$EnquiryReason,"ProductId"=>$ProductId,"IdNumber"=>$IdNumber,"PassportNo"=>"","FirstName"=>$FirstName,"Surname"=>$Surname,"BirthDate"=>$BirthDate,"MaritalStatus"=>$MaritalStatus,"SpouseFirstName"=>$SpouseFirstName,"SpouseSurname"=>$SpouseSurname,"ResidentialAddressLine1"=>$ResidentialAddressLine1,"ResidentialAddressLine2"=>$ResidentialAddressLine2,"ResidentialAddressLine3"=>$ResidentialAddressLine3,"ResidentialAddressLine4"=>$ResidentialAddressLine4,"ResidentialPostalCode"=>$ResidentialPostalCode,"PostalAddressLine1"=>$PostalAddressLine1,"PostalAddressLine2"=>$PostalAddressLine2,"PostalAddressLine3"=>$PostalAddressLine3,"PostalAddressLine4"=>$PostalAddressLine4,"PostalPostalCode"=>$PostalPostalCode,"HomeTelCode"=>$HomeTelCode,"HomeTelNo"=>$HomeTelNo,"WorkTelCode"=>$BirthDate,"WorkTelNo"=>$BirthDate,"CellularCode"=>$BirthDate,"CellularNo"=>$BirthDate,"EmailAddress"=>$BirthDate,"TotalNetMonthlyIncome"=>$BirthDate,"Employer"=>$BirthDate,"JobTitle"=>$BirthDate,"YourReference"=>$YourReference,"VoucherCode"=>$VoucherCode);

        $ConnectConsumerMatch = $client->ConnectConsumerMatch($parameters);
        $ConnectConsumerMatch = new SimpleXMLElement($ConnectConsumerMatch->ConnectConsumerMatchResult);

        //ConnectGetResult
        $EnquiryID = $ConnectConsumerMatch->ConsumerDetails->EnquiryID;
        $EnquiryResultID = $ConnectConsumerMatch->ConsumerDetails->EnquiryResultID;
        $client = new \SoapClient ($xdsUrl);
        $parameters = array (
            "ConnectTicket"=>$ConnectTicket,"EnquiryID"=>$EnquiryID,"EnquiryResultID"=>$EnquiryResultID,
            "ProductID"=>$ProductId);
        $ConnectGetResult = $client->ConnectGetResult($parameters);
        $ConnectGetResult = new SimpleXMLElement($ConnectGetResult->ConnectGetResultResult);

        $params['apiQuery'] = "getIncomeEstimate";
        $params['apiType'] = "xds";
        $this->apiLogging($f3, $params);

        return $ConnectGetResult;
    }

    public function xdsVerification($f3, $params)
    {
        date_default_timezone_set('Africa/Johannesburg'); //or change to whatever timezone you want
        global $db;
        $this->authCheck($f3);
        $xdsData = array();

        $query = "SELECT * from applicantdetails WHERE userID = :id";
        $vars = array(
            ':id' => $params['userID'],
        );
        $results = $db->exec($query, $vars);

        $query = "SELECT * from applicants WHERE applicantID = :applicantID";
        $vars = array(
            ':applicantID' => $results[0]['id'],
        );
        $applicants = $db->exec($query, $vars);

        $params["IDNumber"] = $results[0]["idPassportNo"];
        $params["initials"] = substr($results[0]["lastName"], 0,1);
        $params["lastName"] = $results[0]["lastName"];

        $params["accountNumber"] = $results[0]["accountNumber"];
        $params["branchCode"] = $results[0]["branchCode"];
        $params["bankName"] = $results[0]["bankName"];
        $params["accountType"] = $results[0]["accountType"];

        $params["maritalStatus"] = $results[0]["maritalStatus"];
        $params["spouseIdPassportNo"] = $results[0]["spouseIdPassportNo"];

        $query = "SELECT * from screeningAPIs WHERE id = :id";
        $vars = array(
            ':id' => $params['screeningapiID'],
        );
        $screeningAPI = $db->exec($query, $vars);

        $params['strUser'] = $screeningAPI[0]['username'];
        $params['strPwd'] = $screeningAPI[0]['password'];

        $verifiedData = [];
        $xdsLabels = [];

        array_push($xdsLabels, "firstName");
        array_push($xdsLabels, "lastName");
        array_push($xdsLabels, "idPassportNo");
        array_push($xdsLabels, "dob");
        array_push($xdsLabels, "address");
        array_push($xdsLabels, "maritalStatus");
        array_push($xdsLabels, "accountNumber");
        array_push($xdsLabels, "branchCode");
        array_push($xdsLabels, "accountType");
        array_push($xdsLabels, "bankName"); 
        array_push($xdsLabels, "totalIncome");
        array_push($xdsLabels, "totalExpenses"); 

        $ConnectTicket = $this->xdsLogin($f3, $params);

        $params['ConnectTicket'] = $ConnectTicket;

        $query = "SELECT * from apiTickets WHERE connectTicket = :connectTicket";
        $vars = array(
            ':connectTicket' => $ConnectTicket,
        );
        $xdsTickets = $db->exec($query, $vars);
        $params['ticketID'] = $xdsTickets[0]['id'];

        ///////////////////////////////////////////////////////////////////////////////////////////////////////
        //verify ID
        $idVerificationResponse = $this->validateID($f3, $params);
        $xdsData['idPassportNo'] =  $idVerificationResponse->ConsumerDetails->Idno;
        $xdsData['dob'] =  $idVerificationResponse->ConsumerDetails->Idno;
        $xdsData['firstName'] =  $idVerificationResponse->ConsumerDetails->FirstName;
        $xdsData['lastName'] =   $idVerificationResponse->ConsumerDetails->Surname;
        $xdsData['address'] = '';
        $xdsData['maritalStatus'] = '';
        $xdsData['accountNumber'] =  '';
        $xdsData['branchCode'] =  '';
        $xdsData['accountType'] =  '';
        $xdsData['bankName'] =  '';
        $xdsData['totalIncome'] =  '';
        $xdsData['totalExpenses'] =  '';
        $xdsData['totalIncomeSpouse'] =  '';
        $xdsData['totalExpensesSpouse'] =  '';

        for ($count=0; $count < count($applicants); $count++) {
            $$applicant = $count+1;
            $xdsData["totalIncomePartner$applicant"] = '';
            $xdsData["totalExpensesPartner$applicant"] = '';
        }

        //ID of person is Deceased Status
        if ($idVerificationResponse->ConsumerDetails->DeceasedStatus == "Active") {
            $firstName = strtolower($results[0]["firstName"]);
            $lastName = strtolower($results[0]["lastName"]);
            $idPassportNo = strtolower($results[0]["idPassportNo"]);

            if (strtolower($idVerificationResponse->ConsumerDetails->FirstName) == $firstName) {
                array_push($verifiedData, "firstName");
            }

            if (strtolower($idVerificationResponse->ConsumerDetails->Surname) == $lastName) {
                array_push($verifiedData, "lastName");
            }
            //////////////////////////////////////////////////////////////////////////////////////////////////////

            if($idVerificationResponse->ConsumerDetails->Idno == $idPassportNo){
                array_push($verifiedData, "idPassportNo");
                array_push($verifiedData, "dob");

                //get income estimates
                $incomeEstimates = $this->getIncomeEstimate($f3, $params);
                if(isset($incomeEstimates->ConsumerAffordability)){
                    $xdsData['totalIncome'] =  $incomeEstimates->ConsumerAffordability->PredictedIncome;
                    $xdsData['totalExpenses'] =  $incomeEstimates->ConsumerAffordability->TotalPredictedExpenses;
                } else {
                    $xdsData['totalIncome'] = $incomeEstimates->Error;
                    $xdsData['totalExpenses'] = $incomeEstimates->Error;
                }

                //getCredit Report for Main User
                $params['creditIDNo'] = $results[0]["idPassportNo"];
                $getCreditReportPdf = $this->getCreditReportPdf($f3, $params);

                $query = "INSERT INTO applicantdata (userID, data, dataType, dataLabel, dateCreated, dateUpdated)
                                    VALUES (:userID, :data, :dataType, :dataLabel, :dateCreated, :dateUpdated)";
                $vars = array(
                    ':userID' => $params['userID'],
                    ':data' => $getCreditReportPdf,
                    ':dataType' => 'document',
                    ':dataLabel' => 'creditReport',
                    ':dateCreated' => date("Y-m-d H:i:s"),
                    ':dateUpdated' => date("Y-m-d H:i:s")
                );
                $applicantdata = $db->exec($query, $vars);

                //////////////////////////////////////////////////////////////////////////////////////////////////////
                //verify Marital Status
                $validateMaritalStatus = $this->validateMaritalStatus($f3, $params);

                if(isset($validateMaritalStatus->ConsumerDetail->MaritalStatusDesc)){

                    if (strtolower($validateMaritalStatus->ConsumerDetail->MaritalStatusDesc) == strtolower($results[0]["maritalStatus"])) {
                        array_push($verifiedData, "maritalStatus");
                        $xdsData['maritalStatus'] =  $validateMaritalStatus->ConsumerDetail->MaritalStatusDesc;
                    } else {
                        $xdsData['maritalStatus'] =  $validateMaritalStatus->ConsumerDetail->MaritalStatusDesc;
                    }
                }

                //credit Report for Spouse/Partners

                if($results[0]["applicants"] != "1"){

                    for ($count=0; $count < count($applicants); $count++) { 
                        //income estimator
                        $applicant = $count + 1;
                        $params['IDNumber'] = $applicants[$count]["idNumber"];
                        $incomeEstimates = $this->getIncomeEstimate($f3, $params);

                        if(isset($incomeEstimates->ConsumerAffordability)){
                            $xdsData["totalIncomePartner$applicant"] =  $incomeEstimates->ConsumerAffordability->PredictedIncome;
                            $xdsData["totalExpensesPartner$applicant"] =  $incomeEstimates->ConsumerAffordability->TotalPredictedExpenses;
                        } else {
                            $xdsData["totalIncomePartner$applicant"] = $incomeEstimates->Error;
                            $xdsData["totalExpensesPartner$applicant"] = $incomeEstimates->Error;
                        }

                        //credit report
                        $params['creditIDNo'] = $applicants[$count]["idNumber"];
                        array_push($xdsLabels, "idPassportNopartner$applicant");
                        $xdsData["idPassportNopartner$applicant"] =  $applicants[$count]["idNumber"];

                        $getCreditReportPdf = $this->getCreditReportPdf($f3, $params);
                        $query = "INSERT INTO applicantdata (userID, data, dataType, dataLabel, dateCreated, dateUpdated)
                                    VALUES (:userID, :data, :dataType, :dataLabel, :dateCreated, :dateUpdated)";
                        $vars = array(
                            ':userID' => $params['userID'],
                            ':data' => $getCreditReportPdf,
                            ':dataType' => 'document',
                            ':dataLabel' => 'creditReport',
                            ':dateCreated' => date("Y-m-d H:i:s"),
                            ':dateUpdated' => date("Y-m-d H:i:s")
                        );
                        $applicantdata = $db->exec($query, $vars);
                    }
  
                    //if status still says married from DHA
                    // if($results[0]["maritalStatus"] != "Married" && $validateMaritalStatus->ConsumerDetail->MaritalStatusDesc == "Married") {
                    //     $getCreditReportPdf = $this->getCreditReportPdf($f3, $params);
                    //     $query = "INSERT INTO applicantdata (userID, data, dataType, dataLabel, dateCreated, dateUpdated)
                    //                 VALUES (:userID, :data, :dataType, :dataLabel, :dateCreated, :dateUpdated)";
                    //     $vars = array(
                    //         ':userID' => $params['userID'],
                    //         ':data' => $getCreditReportPdf,
                    //         ':dataType' => 'document',
                    //         ':dataLabel' => 'creditReport',
                    //     );
                    //     $applicantdata = $db->exec($query, $vars);
                    // }
                    
                    // //if return spouse ID is different from one on db
                    // if($results[0]["maritalStatus"] == "Married" && $validateMaritalStatus->ConsumerMaritalStatusEnquiry->SpouseIDno !=  $params['creditIDNo']) {
                    //     $getCreditReportPdf = $this->getCreditReportPdf($f3, $params);
                    //     $query = "INSERT INTO applicantdata (userID, data, dataType, dataLabel, dateCreated, dateUpdated)
                    //                 VALUES (:userID, :data, :dataType, :dataLabel, :dateCreated, :dateUpdated)";
                    //     $vars = array(
                    //         ':userID' => $params['userID'],
                    //         ':data' => $getCreditReportPdf,
                    //         ':dataType' => 'document',
                    //         ':dataLabel' => 'creditReport',
                    //     );
                    //     $applicantdata = $db->exec($query, $vars);
                    // }


                }
            }
            ///////////////////////////////////////////////////////////////////////////////////////////////////////
            //get Verify Bank Details
            $getAccountVerification = $this->getAccountVerification($f3, $params);
            if (isset($getAccountVerification->ResultFile->ACCOUNTNUMBER) && $getAccountVerification->ResultFile->ACCOUNTNUMBER == $params["accountNumber"]) {
                array_push($verifiedData, "accountNumber");
                array_push($verifiedData, "branchCode");
                array_push($verifiedData, "accountType");
                array_push($verifiedData, "bankName");       
                $xdsData['accountNumber'] =  $getAccountVerification->ResultFile->ACCOUNTNUMBER;
                $xdsData['branchCode'] =  $params["branchCode"];
                $xdsData['accountType'] =  $params["accountType"];
                $xdsData['bankName'] =  $params["bankName"];
            }

            $update = false;

            $adminID = "xdsApi";

            foreach ($verifiedData as $column) {

                $query = "SELECT * FROM userChecklist WHERE userID = :userID AND title = :title";
                $vars = array(
                    ':userID' => $params['userID'],
                    ':title' => $column,
                );
                $userdata = $db->exec($query, $vars);

                if (sizeof($userdata) > 0) {
                    $update = true;
                }

                //$dataArr = explode("_", $f3->get("POST.$column"));
                if ($update) {
                    $query = "UPDATE userChecklist SET approvedBy=:adminID, isVerified=:isVerified WHERE id=:id";
                    $vars = array(
                        ':adminID' => $adminID,
                        ':isVerified' => "y",
                        ':id' => $userdata[0]['id'],
                    );
                    $userChecklist = $db->exec($query, $vars);
                } else {
                    $query = "INSERT INTO userChecklist (userID, approvedBy, title, isVerified, dateCreated, dateUpdated)
                            VALUES (:userID, :approvedBy, :title, :isVerified, :dateCreated, :dateUpdated)";
                    $vars = array(
                        ':userID' => $params['userID'],
                        ':approvedBy' => $adminID,
                        ':title' => $column,
                        ':isVerified' => "y",
                        ':dateCreated' => date("Y-m-d H:i:s"),
                        ':dateUpdated' => date("Y-m-d H:i:s")
                    );
                    $userChecklist = $db->exec($query, $vars);
                }

            }
        }

        $updatexdsData = false;
        foreach ($xdsLabels as $column) {

            $query = "SELECT * FROM xdsdata WHERE userID = :userID AND dataLabel = :dataLabel";
            $vars = array(
                ':userID' => $params['userID'],
                ':dataLabel' => $column,
            );
            $userdata = $db->exec($query, $vars);

            if (sizeof($userdata) > 0) {
                $updatexdsData = true;
            }

            if ($updatexdsData) {

                $query = "UPDATE xdsdata SET data=:data WHERE userID=:userID AND dataLabel = :dataLabel";
                $vars = array(
                    ':data' => $xdsData[$column],
                    ':userID' => $params['userID'],
                    ':dataLabel' => $column,
                );
                $xdsdata = $db->exec($query, $vars);
            } else {
                $query = "INSERT INTO xdsdata (userID, dataType, dataLabel, data, dateCreated, dateUpdated)
                        VALUES (:userID, :dataType, :dataLabel, :data, :dateCreated, :dateUpdated)";
                $vars = array(
                    ':userID' => $params['userID'],
                    ':dataType' => 'xdsReturns',
                    ':dataLabel' => $column,
                    ':data' => $xdsData[$column],
                    ':dateCreated' => date("Y-m-d H:i:s"),
                    ':dateUpdated' => date("Y-m-d H:i:s")
                );
                $xdsdata = $db->exec($query, $vars);
            }
            
        }
    }

    public function apiLogging($f3, $params){
        global $db;

        $IdNumber = $params['IDNumber'];

        $query = "INSERT INTO apiReports (userID, ticketID, idNumber, apiQuery, apiType, dateCreated)
                                VALUES (:userID, :ticketID, :idNumber, :apiQuery, :apiType, :dateCreated)";
        $vars = array(
            ':userID' =>  $params['userID'],
            ':ticketID' => $params['ticketID'],
            ':idNumber' => $IdNumber,
            ':apiQuery' => $params['apiQuery'],
            ':apiType' => $params['apiType'],
            ':dateCreated' => date("Y-m-d H:i:s")
        );
        $xdsReports = $db->exec($query, $vars);
    }

    protected function sms($params)
    {
        // Your Account SID and Auth Token from twilio.com/console
        $sid = 'AC01b613c9da13165af03421b674d8d15c';
        $token = 'e5ba1ab962329ebc35bacaef051091fc';
        $client = new Client($sid, $token);

        
        // Use the client to do fun stuff like send text messages!
        $client->messages->create(
            // the number you'd like to send the message to
            '+27731590660',
            [
                // A Twilio phone number you purchased at twilio.com/console
                'from' => '+12063397764',
                // the body of the text message you'd like to send
                'body' => "Hey Nimrod! Good luck on the bar exam!"//$params['body'];
            ]
        );
    }
*/
    //TPN Verifications
    public function tpnLogin($f3, $params){
        global $db;
        if($params['tpnUrl'] == 'https://test-webservices.tpn.co.za'){
            $tpnUrl = $params['tpnUrl'].'/CreditCheck/Security.asmx?WSDL';
        } else {
            $params['SecurityCode'] = 'tgpdc84512uat';
            $tpnUrl = "https://".$params['tpnUrl'].'/security.asmx?WSDL';
        }
        $userID = $params['userID'];
        $date = date("Y-m-d");
        $query = "SELECT * FROM apiTickets WHERE userID=:userID AND type = :type AND dateCreated = :dateCreated";
        $vars = array(
            ':userID' => $userID,
            ':type' => 'tpn',
            ':dateCreated' => $date,
        );
        $tpnTickets = $db->exec($query, $vars);

        if(sizeof($tpnTickets) > 0 && !empty($tpnTickets[0]['connectTicket'])){
            if($tpnTickets[0]['usagesRemaining'] > 0){
                $connectTicket = $tpnTickets[0]['connectTicket'];
                
            }
            else{
                $connectTicket = "TPN usage limit has been reached";
                //echo json_encode(array('status' => 'error', 'message' => "TPN usage limit has been reached"));
            }
        }
        else{
            $client = new \SoapClient ($tpnUrl); 
            
            $parameters['SecurityBlock'] = array(
                "SecurityCode"=>$params['SecurityCode'], 
                "AuthToken"=>"", 
                "OutputFormat"=>"links", 
                "LoginCode"=>$params['strUser'], 
                "Password"=>$params['strPwd'], 
                "SessionId"=>"");
            $configQry = (array) $client->GenerateAuthToken($parameters);
            $data = (array) $configQry['GenerateAuthTokenResult'];
            $connectTicket = new SimpleXMLElement($data['any']);

            $connectTicket = $connectTicket->token;

            if(strlen($connectTicket) > 1){
                $query = "INSERT INTO apiTickets (userID, type, connectTicket, usagesRemaining, dateCreated)
                            VALUES (:userID, :type, :connectTicket, :usagesRemaining, NOW())";
                $vars = array(
                    ':userID' =>  $userID,
                    ':type' => 'tpn',
                    ':connectTicket' => $connectTicket,
                    ':usagesRemaining' => 50,
                );
                $tpnTickets = $db->exec($query, $vars);
            }
        }

        return $connectTicket;
        
    }

    public function tpnCanAccessCredex($f3, $params){
        global $db;
        if($params['tpnUrl'] == 'https://test-webservices.tpn.co.za'){
            $url = $params['tpnUrl'].'/CreditCheck/Consumer.asmx?WSDL';
        } else {
            $params['SecurityCode'] = 'tgpdc84512uat';
            $url = $params['tpnUrl'].'/consumer.asmx?WSDL';
        }

        $token = $params['token'];

        $client = new \SoapClient ($url); 
        $parameters['SecurityBlock'] = array(
                "SecurityCode"=>$params['SecurityCode'], 
                "AuthToken"=>$token, 
                "OutputFormat"=>"links", 
                "LoginCode"=>$params['strUser'], 
                "Password"=>$params['strPwd'], 
                "SessionId"=>""); 

        $configQry = (array) $client->CanAccessCredex($parameters);

        $data = (array) $configQry['CanAccessCredexResult'];

        return $data['any'];
    }

    public function tpnValidateIDMaritalStatus($f3, $params){
        global $db, $tpnUrl;
        if($params['tpnUrl'] == 'http://webservices.tpn.co.za' || $params['tpnUrl'] == 'https://webservices.tpn.co.za'){
            $url = $params['tpnUrl'].'/CreditCheck/Consumer.asmx?WSDL';
        } else {
            $params['SecurityCode'] = 'remaxtandc4962uat';
            $url = $params['tpnUrl'].'/consumer.asmx?WSDL';
        }

        $client = new \SoapClient ($url); 
        $token = $params['token'];

        $IdNumber = $params['IDNumber'];
        $FirstName = $params["firstName"];
        $Surname = $params["lastName"];
        $username = $params['strUser'];
        $password = $params['strPwd'];

        $parameters = array(
            "SecurityBlock"=>array(
                "SecurityCode"=>$params['SecurityCode'], 
                "AuthToken"=>$token, 
                "OutputFormat"=>"links", 
                "LoginCode"=>$username, 
                "Password"=>$password , 
                "SessionId"=>""), 
            "RsaIdNumber"=>$IdNumber, 
            "FirstName"=>$FirstName, 
            "Surname"=>$Surname ); 

        $configQry = (array) $client->ConsumerIDVerification($parameters);

        $data = (array) $configQry['ConsumerIDVerificationResult'];

        $ConsumerIDVerificationResult = new SimpleXMLElement($data['any']);

        $params['apiQuery'] = "ConnectIDVerification";
        $params['apiType'] = "tpn";
        $this->apiLogging($f3, $params);
        
        return $ConsumerIDVerificationResult;
    }

    public function tpnBankVerification($f3, $params){
        global $db, $tpnUrl;
        if($params['tpnUrl'] == 'http://webservices.tpn.co.za' || $params['tpnUrl'] == 'https://webservices.tpn.co.za'){
            $url = $params['tpnUrl'].'/CreditCheck/Consumer.asmx?WSDL';
        } else {
            $params['SecurityCode'] = 'remaxtandc4962uat';
            $url = $params['tpnUrl'].'/consumer.asmx?WSDL';
        }

        $client = new \SoapClient ($url); 
        $token = $params['ConnectTicket'];
        $parameters = array(
            "SecurityBlock"=>array(
                "SecurityCode"=>$params['SecurityCode'], 
                "AuthToken"=>$token, 
                "OutputFormat"=>"links", 
                "LoginCode"=>$params['strUser'], 
                "Password"=>$params['strPwd'] , 
                "SessionId"=>""
            ),
            "ModuleList" => ['TPN', '5874'],
            "ConsumerBlock"=>array(
                "NationalID"=>$params['creditIDNo'], 
                "RSAid"=>"Yes", 
                "DateOfBirth"=>$params['dob'],
                "Surname"=>$params['lastName'], 
                "FirstName"=>$params['firstName'] , 
                "CurrentAddress"=> array(
                    "Line1"=>$params['address'], 
                    "Suburb"=>$params['address'], 
                    "City"=>$params['address'], 
                    "Province"=>"WesternCape",
                    "PostalCode"=>"7640",
                ),
                "EnquiryReason"=>"40", 
                "CellNumber"=>$params['phone'], 
                "WorkNumber"=> array(
                    "Code"=>27, 
                    "Number"=>123, 
                ),
                "Occupation" => $params['occupation'],
                "Employer" => $params['employer'],
                "CreditAmount" => 4000,
                "CredexDetails"=> array(
                    "ScoreVersion"=>"credex", 
                    "NetIncome"=>(int) $params['income'], 
                    "TotalInstallment"=>4000, 
                    "Contributors"=>0, 
                    "InstallmentPortion"=>48, 
                ),
                "BankCodeDetails"=> array(
                    "AccountNumber"=> $params['AccountNumber'], 
                    "Bank"=> $params['Bank'],
                    "Branch"=> "",
                    "BranchCode"=> $params['BranchCode'], 
                    "AccountHolder"=> $params['AccountHolder'], 
                    "AccountType"=> $params['AccountType'],
                    "SpecialInstructions"=>"?", 
                    "CreditAmount"=>3000, 
                    "Terms"=>"?", 
                ),
            ),
            "EnquiryBlock"=>array(
                "ContactName"=>"Tester", 
                "ContactNumber"=>"0731590660", 
                "EmailAddress"=>"nmutandwa@smartviewtechnology.co.za", 
            ),
    
        );

        $configQry = $client->ConsumerEnquiry($parameters);
        $data = (array) $configQry->ConsumerEnquiryResult;
        $tpnBankVerification = new SimpleXMLElement($data['any']);

        if(isset($tpnBankVerification->PdfURL)){
            $tpnBankVerification = $tpnBankVerification->PdfURL;
        } else {
            $tpnBankVerification = "Error While Processing the Request, Please contact TPN";
        }

        $params['apiQuery'] = "ConnectAccountVerificationRealTime";
        $params['apiType'] = "tpn";
        $this->apiLogging($f3, $params);
        
        return $tpnBankVerification;
    }

    public function tpnCreditReport($f3, $params){
        global $db, $tpnUrl;
	// var_dump($params['tpnUrl'];
	// exit;
        if($params['tpnUrl'] == 'https://test-webservices.tpn.co.za'){
            $url = $params['tpnUrl'].'/CreditCheck/Consumer.asmx?WSDL';
        } else {
            $params['SecurityCode'] = 'tgpdc84512uat';
            $url = $params['tpnUrl'].'/consumer.asmx?WSDL';
        }

        $client = new \SoapClient ($url); 
        $token = $params['ConnectTicket'];
	// var_dump($token);
	// exit;
        $parameters = array(
            "SecurityBlock"=>array(
                "SecurityCode"=>$params['SecurityCode'], 
                "AuthToken"=>$token, 
                "OutputFormat"=>"links", 
                "LoginCode"=>$params['strUser'], 
                "Password"=>$params['strPwd'] , 
                "SessionId"=>""
            ),
            "ModuleList" => ['TPN','TPN_RentCheck'],
            "ConsumerBlock"=>array(
                "NationalID"=>$params['creditIDNo'], 
                "RSAid"=>"Yes", 
                "DateOfBirth"=>$params['dob'],
                "Surname"=>$params['lastName'], 
                "FirstName"=>$params['firstName'] , 
                "CurrentAddress"=> array(
                    "Line1"=>$params['address'], 
                    "Suburb"=>$params['address'], 
                    "City"=>$params['address'], 
                    "Province"=>"WesternCape",
                    "PostalCode"=>"7640",
                ),
                "EnquiryReason"=>"40", 
                "CellNumber"=>$params['phone'], 
                "WorkNumber"=> array(
                    "Code"=>27, 
                    "Number"=>123, 
                ),
                "Occupation" => $params['occupation'],
                "Employer" => $params['employer'],
                "CreditAmount" => 4000,
                "CredexDetails"=> array(
                    "ScoreVersion"=>"credex", 
                    "NetIncome"=>$params['income'], 
                    "TotalInstallment"=>4000, 
                    "Contributors"=>0, 
                    "InstallmentPortion"=>48, 
                ),
                "BankCodeDetails"=> array(
                    "AccountNumber"=>"", 
                    "Bank"=>"", 
                    "Branch"=>"", 
                    "BranchCode"=>"", 
                    "AccountHolder"=>"", 
                    "AccountType"=>"", 
                    "SpecialInstructions"=>"", 
                    "CreditAmount"=>3000, 
                    "Terms"=>"", 
                ),
            ),
            "EnquiryBlock"=>array(
                "ContactName"=>"Tester", 
                "ContactNumber"=>"0731590660", 
                "EmailAddress"=>"nmutandwa@smartviewtechnology.co.za", 
            ),
    
        ); 

        $configQry = (array) $client->ConsumerEnquiry($parameters);

        $data = (array) $configQry['ConsumerEnquiryResult'];
        $tpnCreditReport = new SimpleXMLElement($data['any']);

        if(isset($tpnCreditReport->PdfURL)){
            $tpnCreditReport = $tpnCreditReport->PdfURL;
        } else {
            $tpnCreditReport = "Error While Processing the Request, Please contact TPN";
        }
        
        $params['apiQuery'] = "getCreditReportPdf";
        $params['apiType'] = "tpn";
        $this->apiLogging($f3, $params);
	// var_dump($tpnCreditReport);
	// exit;        
        return $tpnCreditReport;
    }

    public function tpnVerification($f3, $params)
    {
        date_default_timezone_set('Africa/Johannesburg'); //or change to whatever timezone you want
        global $db;
        $this->authCheck($f3);

        $verifiedData = [];

        $query = "SELECT * from applicantdetails WHERE userID = :id";
        $vars = array(
            ':id' => $params['userID'],
        );
        $results = $db->exec($query, $vars);

        $query = "SELECT * from applicants WHERE applicantID = :applicantID";
        $vars = array(
            ':applicantID' => $results[0]['id'],
        );
        $applicants = $db->exec($query, $vars);

        $query = "SELECT * from screeningAPIs WHERE id = :id";
        $vars = array(
            ':id' => $params['screeningapiID'],
        );
        $screeningAPI = $db->exec($query, $vars);
        $params['SecurityCode'] = $screeningAPI[0]['securityCode'];
        $params['tpnUrl'] = $screeningAPI[0]['url'];
        $params["IDNumber"] = $results[0]["idPassportNo"];
        $params["initials"] = substr($results[0]["lastName"], 0,1);
        $params["firstName"] = $results[0]["firstName"];
        $params["lastName"] = $results[0]["lastName"];

        $params['strUser'] = $screeningAPI[0]['username'];
        $params['strPwd'] = $screeningAPI[0]['password'];

        $ConnectTicket = $this->tpnLogin($f3, $params);
        $params['ConnectTicket'] = $ConnectTicket;

        $query = "SELECT * from apiTickets WHERE connectTicket = :connectTicket";
        $vars = array(
            ':connectTicket' => $ConnectTicket,
        );
        $tpnTickets = $db->exec($query, $vars);
        $params['ticketID'] = $tpnTickets[0]['id'];


        ///////////////////////////////////////////////////////////////////////////////////////////////////////
        //verify ID
        $tpnValidateID = $this->tpnValidateIDMaritalStatus($f3, $params);
        if(isset($tpnValidateID->PdfURL)){
            //ID Number Exists

            $query = "SELECT * FROM applicantdata WHERE userID = :userID AND dataType = :dataType AND dataLabel = :dataLabel ORDER BY id DESC";
            $vars = array(
                ':userID' => $params['userID'],
                ':dataType' => 'document',
                ':dataLabel' => 'tpnIdDocument'
            );
            $tpn_document = $db->exec($query, $vars);

            if(count($tpn_document)>0){
                $query = "UPDATE applicantdata SET data = :data, dateUpdated = :dateUpdated WHERE id = :id";
                $vars = array(
                    ':data' => $tpnValidateID->PdfURL,
                    ':dateUpdated' => date("Y-m-d H:i:s"),
                    ':id' => $tpn_document[0]['id']
                );
                $applicantdata = $db->exec($query, $vars);
            } else {
                $query = "INSERT INTO applicantdata (userID, data, dataType, dataLabel, dateCreated, dateUpdated)
                VALUES (:userID, :data, :dataType, :dataLabel, :dateCreated, :dateUpdated)";
                $vars = array(
                    ':userID' => $params['userID'],
                    ':data' => $tpnValidateID->PdfURL,
                    ':dataType' => 'document',
                    ':dataLabel' => 'tpnIdDocument',
                    ':dateCreated' => date("Y-m-d H:i:s"),
                    ':dateUpdated' => date("Y-m-d H:i:s")
                );
                $applicantdata = $db->exec($query, $vars);
            }
            
            $query = "SELECT * FROM applicantdata WHERE userID = :userID AND dataType = :dataType AND dataLabel = :dataLabel ORDER BY id DESC";
            $vars = array(
                ':userID' => $params['userID'],
                ':dataType' => 'document',
                ':dataLabel' => 'tpnBankDetails'
            );
            $tpn_document = $db->exec($query, $vars);

            if(count($tpn_document)>0){
                $query = "UPDATE applicantdata SET data = :data, dateUpdated = :dateUpdated WHERE id = :id";
                $vars = array(
                    ':data' => 'TPN',
                    ':dateUpdated' => date("Y-m-d H:i:s"),
                    ':id' => $tpn_document[0]['id']
                );
                $applicantdata = $db->exec($query, $vars);
            } else {
                $query = "INSERT INTO applicantdata (userID, data, dataType, dataLabel, dateCreated, dateUpdated)
                        VALUES (:userID, :data, :dataType, :dataLabel, :dateCreated, :dateUpdated)";
                $vars = array(
                    ':userID' => $params['userID'],
                    ':data' => 'TPN',
                    ':dataType' => 'document',
                    ':dataLabel' => 'tpnBankDetails',
                    ':dateCreated' => date("Y-m-d H:i:s"),
                    ':dateUpdated' => date("Y-m-d H:i:s")
                );
                $applicantdata = $db->exec($query, $vars);
            }

            $query = "SELECT * FROM applicantdata WHERE userID = :userID AND dataType = :dataType AND dataLabel = :dataLabel ORDER BY id DESC";
            $vars = array(
                ':userID' => $params['userID'],
                ':dataType' => 'document',
                ':dataLabel' => 'creditReport'
            );
            $tpn_document = $db->exec($query, $vars);
            
            if(count($tpn_document)>0){
                $query = "DELETE FROM applicantdata WHERE userID = :userID AND dataType = :dataType AND dataLabel = :dataLabel";
                $vars = array(
                    ':userID' => $params['userID'],
                    ':dataType' => 'document',
                    ':dataLabel' => 'creditReport'
                );
                $delete_credit_reports = $db->exec($query, $vars);

                $query = "INSERT INTO applicantdata (userID, data, dataType, dataLabel, dateCreated, dateUpdated)
                VALUES (:userID, :data, :dataType, :dataLabel, :dateCreated, :dateUpdated)";
                $vars = array(
                ':userID' => $params['userID'],
                ':data' => 'TPN',
                ':dataType' => 'document',
                ':dataLabel' => 'creditReport',
                ':dateCreated' => date("Y-m-d H:i:s"),
                ':dateUpdated' => date("Y-m-d H:i:s")
                );
                $applicantdata = $db->exec($query, $vars);
            } else {
                $query = "INSERT INTO applicantdata (userID, data, dataType, dataLabel, dateCreated, dateUpdated)
                VALUES (:userID, :data, :dataType, :dataLabel, :dateCreated, :dateUpdated)";
                $vars = array(
                ':userID' => $params['userID'],
                ':data' => 'TPN',
                ':dataType' => 'document',
                ':dataLabel' => 'creditReport',
                ':dateCreated' => date("Y-m-d H:i:s"),
                ':dateUpdated' => date("Y-m-d H:i:s")
                );
                $applicantdata = $db->exec($query, $vars);
            }

            array_push($verifiedData, "idPassportNo");
            array_push($verifiedData, "creditReport");

            if(count($applicants) > 0){
                for ($i=0; $i < count($applicants); $i++) { 
                    array_push($verifiedData, "idPassportNoPartner".($i+1));
                    array_push($verifiedData, "creditReport");
                    $query = "INSERT INTO applicantdata (userID, data, dataType, dataLabel, dateCreated, dateUpdated)
                                VALUES (:userID, :data, :dataType, :dataLabel, :dateCreated, :dateUpdated)";
                    $vars = array(
                        ':userID' => $params['userID'],
                        ':data' => 'TPN',
                        ':dataType' => 'document',
                        ':dataLabel' => 'creditReport',
                        ':dateCreated' => date("Y-m-d H:i:s"),
                        ':dateUpdated' => date("Y-m-d H:i:s")
                    );
                    $applicantdata = $db->exec($query, $vars);
                }
            }
            $update = false;

            $adminID = "tpnApi";

            foreach ($verifiedData as $column) {

                $query = "SELECT * FROM userChecklist WHERE userID = :userID AND title = :title";
                $vars = array(
                    ':userID' => $params['userID'],
                    ':title' => $column,
                );
                $userdata = $db->exec($query, $vars);

                if (sizeof($userdata) > 0) {
                    $update = true;
                }
                if ($update) {
                    $query = "UPDATE userChecklist SET approvedBy=:adminID, isVerified=:isVerified WHERE id=:id";
                    $vars = array(
                        ':adminID' => $adminID,
                        ':isVerified' => "n",
                        ':id' => $userdata[0]['id'],
                    );
                    $userChecklist = $db->exec($query, $vars);
                } else {
                    $query = "INSERT INTO userChecklist (userID, approvedBy, title, isVerified, dateCreated, dateUpdated)
                            VALUES (:userID, :approvedBy, :title, :isVerified, :dateCreated, :dateUpdated)";
                    $vars = array(
                        ':userID' => $params['userID'],
                        ':approvedBy' => $adminID,
                        ':title' => $column,
                        ':isVerified' => "n",
                        ':dateCreated' => date("Y-m-d H:i:s"),
                        ':dateUpdated' => date("Y-m-d H:i:s")
                    );
                    $userChecklist = $db->exec($query, $vars);
                }

            }
            
        }
    }   

    protected function getRepository($table)
    {
        return new AppRepository($table);
    }

    protected function getDefinition($attributes)
    {
        return new AppDefinition($attributes);
    }

}
