<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class RentalPropertyController extends AppController
{
    /*
    * Housing Units
    * Shop Units
    */
    public function viewUnits($f3)
    {
        global $db;
        $curlResponse = [];

        $query = "SELECT userType, agentID FROM users WHERE id = :userID AND isDeleted=:isDeleted";
        $vars = array(
            ':userID' => $f3->get("POST.userID"),
            ':isDeleted' => 'n',
        );
        $user_profile = $db->exec($query, $vars);

        $filter_applications_by_agent = '';
        $filter_applications_by_agent_2 = '';
        if($user_profile[0]['userType']=='3' && !empty($user_profile[0]['agentID'])){
            $filter_applications_by_agent = 'AND agentID = '.$user_profile[0]['agentID'];
            $filter_applications_by_agent_2 = 'AND u.agentID = '.$user_profile[0]['agentID'];
        }

        $query = "SELECT COUNT(*) as totalUnits FROM rentalproperties WHERE deleted=:deleted AND propertyReference IS NOT NULL $filter_applications_by_agent";
        $vars = array(
            ':deleted' => 'n',
        );
        $totalUnits = $db->exec($query, $vars);
        $curlResponse['totalUnits'] = $totalUnits[0]['totalUnits'];

        $query = "SELECT * FROM propertyagents WHERE isDeleted=:isDeleted";
        $vars = array(
            ':isDeleted' => 'n',
        );
        $property_agents = $db->exec($query, $vars);
        $curlResponse['property_agents'] = json_encode($property_agents);

        $query = "SELECT COUNT(*) as occupiedUnits FROM rentalproperties WHERE deleted=:deleted AND occupied=:occupied AND propertyReference IS NOT NULL $filter_applications_by_agent";
        $vars = array(
            ':deleted' => 'n',
            ':occupied' => 'y',
        );
        $occupiedUnits = $db->exec($query, $vars);
        $curlResponse['occupiedUnits'] = $occupiedUnits[0]['occupiedUnits'];
        $curlResponse['availableUnits'] = $totalUnits[0]['totalUnits'] - $occupiedUnits[0]['occupiedUnits'];

        $query = "SELECT COUNT(*) as onholdUnits FROM rentalproperties WHERE deleted=:deleted AND occupied=:occupied AND propertyReference IS NOT NULL $filter_applications_by_agent";
        $vars = array(
            ':deleted' => 'n',
            ':occupied' => 'h',
        );
        $units = $db->exec($query, $vars);
        $curlResponse['onholdUnits'] = $units[0]['onholdUnits'];

        $query = "SELECT * FROM complex WHERE deleted=:deleted";
        $vars = array(
            ':deleted' => 'n',
        );
        $complex = $db->exec($query, $vars);
        $f3->set('complex', $complex);
        $curlResponse['complex'] = json_encode($complex);

        $query = "SELECT * FROM rentTrench WHERE deleted=:deleted";
        $vars = array(
            ':deleted' => 'n',
        );
        $rentTrench = $db->exec($query, $vars);
        $f3->set('rentTrench', $rentTrench);
        $curlResponse['rentTrench'] = json_encode($rentTrench);

        $query = "SELECT u.*, CONCAT(pa.firstName, ' ', pa.lastName) as agentFullName, (SELECT COUNT(*) FROM charges ch WHERE ch.propertyReference = u.propertyReference AND ch.isDeleted = :deleted) as charges FROM rentalproperties u 
        LEFT JOIN propertyagents pa ON pa.id = u.agentID AND pa.isDeleted = :deleted
        WHERE u.deleted = :deleted AND u.propertyReference IS NOT NULL $filter_applications_by_agent_2";
        $vars = array(
            ':deleted' => 'n',
        );
        $units = $db->exec($query, $vars);
        $curlResponse['units'] = json_encode($units);
        echo json_encode($curlResponse);
    }

    public function createUnit($f3)
    {
        try {
            $this->authCheck($f3);
            $f3->set('POST.dateCreated', date("Y-m-d"));
            $f3->set('POST.dateUpdated', date("Y-m-d"));
            $attributes = $f3->get('POST');
            $attributes['occupied'] = "n";

            $definition = $this->getDefinition($attributes);
            $this->getRepository('units')->createRecord($definition);
        } catch (Exception $exception) {
            echo json_encode($exception->getMessage());
        }
    }

    public function deleteUnit($f3, $params)
    {
        try {
            $this->authCheck($f3);
            $attributes = ["deleted" => 'y', "dateUpdated" => date("Y-m-d")];
            $definition = $this->getDefinition($attributes);
            $idArray = ['id = ?', $params['id']];
            $user = $this->getRepository('rentalproperties')->updateRecord($idArray, $definition);
        } catch (Exception $exception) {
            echo json_encode($exception->getMessage());
        }
    }

    public function updateUnit($f3, $params)
    {

        try {
            global $db;
            $this->authCheck($f3);
            $curlResponse = [];

            if ($f3->get('POST.propertyReference')) {
                $f3->set('POST.dateUpdated', date("Y-m-d"));
                $attributes = $f3->get('POST');
                $definition = $this->getDefinition($attributes);

                $idArray = ['id = ?', $params['id']];
                $units = $this->getRepository('rentalproperties')->updateRecord($idArray, $definition);
            }

            $query = "SELECT * FROM complex WHERE deleted = :deleted";
            $vars = array(
                ':deleted' => "n",
            );
            $complex = $db->exec($query, $vars);
            $f3->set('complex', $complex);
            $curlResponse['complex'] = json_encode($complex);

            $query = "SELECT * FROM rentTrench WHERE deleted = :deleted";
            $vars = array(
                ':deleted' => "n",
            );
            $rentTrench = $db->exec($query, $vars);
            $f3->set('rentTrench', $rentTrench);
            $curlResponse['rentTrench'] = json_encode($rentTrench);

            $query = "SELECT u.* FROM rentalproperties u WHERE u.id = :unitID AND deleted = :deleted";
            $vars = array(
                ':unitID' => $params['id'],
                ':deleted' => 'n',
            );
            $units = $db->exec($query, $vars);
            $curlResponse['units'] = json_encode($units);

            echo json_encode($curlResponse);
        } catch (Exception $exception) {
            echo json_encode($exception->getMessage());
        }
    }

    public function uploadDocuments($f3, $params)
    {
        global $db;
        $this->authCheck($f3);
        $curlResponse = [];
        $dataArr = (array) json_decode($f3->get('POST.dataArr'));
        
        for ($i=0; $i < sizeof($dataArr); $i++) { 
            
            if(!empty($dataArr[$i]->propertyReference)){

                $query = "SELECT * FROM rentalproperties WHERE propertyReference = :propertyReference AND deleted = :deleted";
                $vars = array(
                    ':propertyReference' => $dataArr[$i]->propertyReference,
                    ':deleted' => 'n',
                );
                $property = $db->exec($query, $vars);

                if(count($property)==0){
                    $query = "INSERT INTO `rentalproperties` (`propertyReference`, `propertyDescription`, `unitRental`, `lease`, `rentDeposit`, `agentID`, `dateCreated`, `dateUpdated`, `deleted`, `available`) 
                    VALUES (:propertyReference, :propertyDescription, :unitRental, :lease, :rentDeposit, :agentID, :dateCreated, :dateUpdated, :deleted, :available)";
                    $vars = array(
                        ':propertyReference' => $dataArr[$i]->propertyReference,
                        ':propertyDescription' => $dataArr[$i]->propertyDescription,
                        ':unitRental' => (int) $dataArr[$i]->unitRental,
                        ':lease' => (int) $dataArr[$i]->lease,
                        ':rentDeposit' => (int) $dataArr[$i]->rentDeposit,
                        ':agentID' => (int) $dataArr[$i]->agentID,
                        ':dateCreated' => date("Y-m-d H:i:s"),
                        ':dateUpdated' => date("Y-m-d H:i:s"),
                        ':deleted' => 'n',
                        ':available' => 'y'
                    );
                    $curlResponse['sixe'.$i] = $vars;
                    $save = $db->exec($query, $vars);
                }
            }
           
        }
        echo json_encode($curlResponse);
    }

    public function viewUnitCharges($f3, $params)
    {
        global $db;
        $propertyReference  = $params['propertyReference'];
        $curlResponse = [];
    
        $query = "SELECT c.* FROM charges c LEFT JOIN rentalproperties rp ON c.propertyReference = rp.propertyReference WHERE c.propertyReference = :propertyReference AND c.isDeleted = :isDeleted";
        $vars = array(
            ':propertyReference' =>  $propertyReference,
            ':isDeleted' => 'n',
        );
        $charges = $db->exec($query, $vars);
        $curlResponse['propertyReference'] = $charges[0]['propertyReference'];
        $curlResponse['charges'] = json_encode($charges);

        echo json_encode($curlResponse);
    }

    public function deleteUnitCharges($f3, $params)
    {
        global $db;
        $chargeID  = $params['id'];
        $query = "UPDATE charges SET isDeleted = :isDeleted, dateUpdated = NOW() WHERE id = :id;";
        $vars = array(
            ':isDeleted' => 'y',
            ':id' => (int) $chargeID,
        );
        $deletecharge = $db->exec($query, $vars);
    }

    public function uploadPropertyDocuments($f3)
    {
        global $db;
        //property Documents
        if ($f3->get('POST.files')) {
            $resultUrl = (array) json_decode($f3->get('POST.files'));
            $docsArr = (array) json_decode($f3->get('POST.docsArr'));
            $mainID = $f3->get("POST.propertyReferenceID");

            $count = 0;
            if (sizeof($resultUrl) > 0) {
                foreach ($resultUrl as $fileUrl => $bool) {
                    $update = false;
                    $formFieldName = $docsArr[$count];

                    $query = "SELECT * FROM metaData WHERE mainID = :mainID";
                    $vars = array(
                        ':mainID' => $mainID,
                    );
                    $propertyDocs = $db->exec($query, $vars);

                    foreach ($propertyDocs as $row) {
                        if ($row['dataLabel'] == $formFieldName) {
                            $update = true;
                        }
                    }

                    if ($update) {
                        $query = "UPDATE metaData SET data=:data, dateUpdated = NOW() WHERE mainID = :mainID AND dataLabel=:dataLabel";
                        $vars = array(
                            ':data' => $fileUrl,
                            ':mainID' => $mainID,
                            ':dataLabel' => $formFieldName,
                        );
                        $docsData = $db->exec($query, $vars);
                    } else {
                        $query = "INSERT INTO metaData (mainID, datatype, data, dataLabel, dateCreated, dateUpdated)
                                VALUES (:mainID, :datatype, :data, :dataLabel, NOW(), NOW())";
                        $vars = array(
                            ':mainID' => $mainID,
                            ':datatype' => "document",
                            ':data' => $fileUrl,
                            ':dataLabel' => $formFieldName,
                        );
                        $docsData = $db->exec($query, $vars);
                    }
                    $count++;
                }
            }
        }
    }

    public function editUnitCharges($f3)
    {
        global $db;
        //$this->authCheck($f3);
        $query = "UPDATE charges SET chargeName = :chargeName, chargeDescription = :chargeDescription, cost = :cost, dateUpdated = NOW() WHERE id = :id;";
        $vars = array(
            ':chargeName' => $f3->get('POST.chargeName'),
            ':chargeDescription' => $f3->get('POST.chargeDescription'),
            ':cost' => (float) $f3->get('POST.cost'),
            ':id' => (int) $f3->get('POST.chargeID'),
        );
        $editCharge = $db->exec($query, $vars);
    }

    function additionalCharges($f3)
    {
        global $db;
        $curlResponse = [];

        $query = "SELECT * FROM charges WHERE propertyReference = :propertyReference AND chargeName = :chargeName AND chargeDescription = :chargeDescription AND cost = :cost";
        $vars = array(
            ':propertyReference' =>  $f3->get('POST.propertyReference'),
            ':chargeName' => $f3->get('POST.chargeName'),
            ':chargeDescription' =>  $f3->get('POST.chargeDescription'),
            ':cost' =>  (float) $f3->get('POST.cost'),
        );
        $charges = $db->exec($query, $vars);

        if(count($charges)==0){
            $query = "INSERT INTO charges (propertyReference, chargeName, chargeDescription, cost, dateCreated) 
            VALUES (:propertyReference, :chargeName, :chargeDescription, :cost, NOW())";
            $vars = array(
            ':propertyReference' =>  $f3->get('POST.propertyReference'),
            ':chargeName' => $f3->get('POST.chargeName'),
            ':chargeDescription' =>  $f3->get('POST.chargeDescription'),
            ':cost' =>  (float) $f3->get('POST.cost'),
            );
            $charge = $db->exec($query, $vars);
        }

       
    }
}