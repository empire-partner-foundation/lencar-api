<?php 
 //Data, connection, auth// request data from the form
// function mainLogin() {

 $soapUrl = 'https://test-webservices.tpn.co.za/security.asmx'; // asmx URL of WSDL

 // xml post structure

 $securityCode = "tgpdc84512uat";
 $loginCode = "tgpdc01";
 $passWord = "58Yo#N#w8u";

 $xml_post_string = '<?xml version="1.0" encoding="utf-8"?>
 <soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
   <soap:Body>
     <GenerateAuthToken xmlns="http://tpn.co.za/">
       <SecurityBlock>
         <SecurityCode>'.$securityCode.'</SecurityCode>
         <AuthToken></AuthToken>
         <OutputFormat>links</OutputFormat>
         <LoginCode>'.$loginCode.'</LoginCode>
         <Password>'.$passWord.'</Password>
         <SessionId></SessionId>
       </SecurityBlock>
     </GenerateAuthToken>
   </soap:Body>
 </soap:Envelope>';   // data from the form, e.g. some ID number

    $headers = array(
                 "Content-type: text/xml;charset=\"utf-8\"",
                 "Accept: text/xml",
                 "Cache-Control: no-cache",
                 "Pragma: no-cache",
                 "SOAPAction: http://tpn.co.za/GenerateAuthToken", 
                 "Content-length: ".strlen($xml_post_string),
             ); //SOAPAction: your op URL

     $url = $soapUrl;

     // PHP cURL  for https connection with auth
     $ch = curl_init();
     curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);
     curl_setopt($ch, CURLOPT_URL, $url);
     curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); // username and password - declared at the top of the doc
     curl_setopt($ch, CURLOPT_TIMEOUT, 10);
     curl_setopt($ch, CURLOPT_POST, true);
     curl_setopt($ch, CURLOPT_POSTFIELDS, $xml_post_string); // the SOAP request
     curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

     // converting
     $response = curl_exec($ch); 
     curl_close($ch);


     // converting
     $response1 = str_replace("<soap:Body>","",$response);
     $response2 = str_replace("</soap:Body>","",$response1);

     // convertingc to XML
     $parser = simplexml_load_string($response2);
     // user $parser to get your data out of XML response and to display it. 
     $xml_obj = json_encode($parser);
     $data = (array) json_decode($xml_obj, TRUE);
     echo $data["GenerateAuthTokenResponse"]["GenerateAuthTokenResult"]["result"]["token"];
     //$data = (array) $xml_obj['GenerateAuthTokenResult'];
     // echo var_dump($data);
//	}
     //mainLogin();
 ?>
