<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class PropertyOwnerController extends AppController
{

    public function property_owner($f3)
    {
        global $db;
        $this->authCheck($f3);
        $curlResponse = [];

        //GET USER APPLICATIONS
        $query = "SELECT * FROM applications WHERE id = :id AND propertyReference = :propertyReference";
        $vars = array(
            ':id' => $f3->get("POST.applicationID"),
            ':propertyReference' => $f3->get("POST.propertyReference"),
        );
        $applicantdata = $db->exec($query, $vars);
        $curlResponse['application'] = json_encode($applicantdata);

        //GET USER APPLICATION REQUESTS
        $query = "SELECT * FROM requests WHERE applicationID = :applicationID;";
        $vars = array(
            ':applicationID' => $applicantdata[0]['id']
        ); 
        $requests = $db->exec($query, $vars);
        $curlResponse['requests'] = json_encode($requests);

        $query = "SELECT ad.*, ap.propertyReference, pd.dataType, pd.dataLabel, pd.data, ap.applicationStatus  
        FROM applications ap 
        LEFT JOIN applicantdetails ad ON ap.userID = ad.userID 
        LEFT JOIN applicantdata pd ON ap.userID = pd.userID AND (pd.applicationID = :applicationID OR pd.applicationID IS NULL)
        WHERE ap.id = :applicationID";
        $vars = array(
            ':applicationID' => $applicantdata[0]['id'],
        );
        $applicationsData = $db->exec($query, $vars);
        $curlResponse['applicationsData'] = json_encode($applicationsData);

        $extra_documents = '';
        for ($i=0; $i < count($requests); $i++) { 
            if($requests[$i]['request'] == 'document' && !empty($requests[$i]['submission'])){
                $extra_documents = $extra_documents."'".str_replace(' ', '_', $requests[$i]['title'])."',";
                for ($a=1; $a < 15; $a++) { 
                     $extra_documents = $extra_documents."'".str_replace(' ', '_', $requests[$i]['title']).$a."',";
                }
            }
        }
        $userID = $applicantdata[0]['userID'];
        $query = "SELECT * FROM applicantdata WHERE userID = :userID AND (applicationID = :applicationID OR applicationID IS NULL)
        AND dataLabel 
        IN(".$extra_documents."
            'leaseAgreement','leaseAgreement', 'leaseAgreement1', 'leaseAgreement2', 'leaseAgreement3', 'leaseAgreement4', 'leaseAgreement5','leaseAgreement6', 'leaseAgreement7', 'leaseAgreement8', 'leaseAgreement9',
            'leaseAgreement10', 'leaseAgreement11', 'leaseAgreement12', 'leaseAgreement13', 'leaseAgreement14', 'leaseAgreement15', 'leaseAgreement16', 'leaseAgreement17', 'leaseAgreement18', 'leaseAgreement19',
            'keySlip','idDocument','payslip','bankStatement', 'payslip1','bankStatement1', 'payslip2','bankStatement2', 'payslip3','bankStatement3', 'payslip4','bankStatement4', 'payslip5','bankStatement5',
            'partner1IdDocument', 'partner1BankStatement', 'partner1Payslip', 'partner1BankStatement1', 'partner1Payslip1', 'partner1BankStatement2', 'partner1Payslip2', 'partner1BankStatement3', 'partner1Payslip3', 'partner1BankStatement4', 'partner1Payslip4', 'partner1BankStatement5', 'partner1Payslip5',
            'partner2IdDocument', 'partner2BankStatement', 'partner2Payslip', 'partner2BankStatement1', 'partner2Payslip1', 'partner2BankStatement2', 'partner2Payslip2', 'partner2BankStatement3', 'partner2Payslip3', 'partner2BankStatement4', 'partner2Payslip4', 'partner2BankStatement5', 'partner2Payslip5',
            'partner3IdDocument', 'partner3BankStatement', 'partner3Payslip', 'partner3BankStatement1', 'partner3Payslip1', 'partner3BankStatement2', 'partner3Payslip2', 'partner3BankStatement3', 'partner3Payslip3', 'partner3BankStatement4', 'partner3Payslip4', 'partner3BankStatement5', 'partner3Payslip5',
            'partner4IdDocument', 'partner4BankStatement', 'partner4Payslip', 'partner4BankStatement1', 'partner4Payslip1', 'partner4BankStatement2', 'partner4Payslip2', 'partner4BankStatement3', 'partner4Payslip3', 'partner4BankStatement4', 'partner4Payslip4', 'partner4BankStatement5', 'partner4Payslip5',
            'partner5IdDocument', 'partner5BankStatement', 'partner5Payslip', 'partner5BankStatement1', 'partner5Payslip1', 'partner5BankStatement2', 'partner5Payslip2', 'partner5BankStatement3', 'partner5Payslip3', 'partner5BankStatement4', 'partner5Payslip4', 'partner5BankStatement5', 'partner5Payslip5',
            'partner6IdDocument', 'partner6BankStatement', 'partner6Payslip', 'partner6BankStatement1', 'partner6Payslip1', 'partner6BankStatement2', 'partner6Payslip2', 'partner6BankStatement3', 'partner6Payslip3', 'partner6BankStatement4', 'partner6Payslip4', 'partner6BankStatement5', 'partner6Payslip5',
            'partner7IdDocument', 'partner7BankStatement', 'partner7Payslip', 'partner7BankStatement1', 'partner7Payslip1', 'partner7BankStatement2', 'partner7Payslip2', 'partner7BankStatement3', 'partner7Payslip3', 'partner7BankStatement4', 'partner7Payslip4', 'partner7BankStatement5', 'partner7Payslip5',
            'partner8IdDocument', 'partner8BankStatement', 'partner8Payslip', 'partner8BankStatement1', 'partner8Payslip1', 'partner8BankStatement2', 'partner8Payslip2', 'partner8BankStatement3', 'partner8Payslip3', 'partner8BankStatement4', 'partner8Payslip4', 'partner8BankStatement5', 'partner8Payslip5',
            'idDocumentSpouse','payslipSpouse','bankStatementSpouse','payslipSpouse1','bankStatementSpouse1','payslipSpouse2','bankStatementSpouse2', 'payslipSpouse3','bankStatementSpouse3', 'payslipSpouse4','bankStatementSpouse4', 'payslipSpouse5','bankStatementSpouse5', 'marriageCertificate', 'divorceDecree', 'separationAffidavit','pendingJudgementDocument','debtReviewDocument', 'creditReport','rentDeposit', 'marriageCertificate', 'tpnIdDocument', 'tpnBankDetails'
        ) ORDER BY id ASC";
        $vars = array(
            ':userID' => $userID,
            ':applicationID' => $f3->get("POST.applicationID")
        );
        $documents = $db->exec($query, $vars);

        $newDocsArr = [];
        foreach ($documents as $document) {
            $title = preg_replace("([A-Z])", " $0", $document['dataLabel']);

            $newDocsArr[] = ["id" => $document['id'], "dataType" => $document['dataType'],
                "dataLabel" => $document['dataLabel'], "data" => $document['data'], "title" => $title, 'dateUpdated' => $document['dateUpdated']];
        }
        $curlResponse['documents'] = json_encode($newDocsArr);
        echo json_encode($curlResponse);
    }

    public function property_owner_response($f3, $params)
    {
        try {
            global $db;
            $applicationID = $params['applicationID'];
            $action = $params['action'];
            
            if($action=="decline"){
                $status = 'd';
                $propertyOwnerApproval = 'd';
                $attributes = ["applicationStatus" => $status, "propertyOwnerApproval" => $propertyOwnerApproval, "dateUpdated" => date("Y-m-d")];
            } else if($action=='accept') {
                $status = $params['status'];
                $propertyOwnerApproval = 'a';
                $attributes = ["propertyOwnerApproval" => $propertyOwnerApproval, "dateUpdated" => date("Y-m-d")];
            }
            
            $definition = $this->getDefinition($attributes);

            $idArray = ['id = ?', $applicationID];

            $user = $this->getRepository('applications')->updateRecord($idArray, $definition);

            if($action=="accept"){
                $query = "SELECT md.*, ad.id AS applicantID, ad.firstName, ad.email, ap.userID, ap.complexID, ap.unitID, ap.propertyReference, ap.id AS applicationID FROM applications ap
                LEFT JOIN rentalproperties rt ON ap.id = rt.applicationID
                LEFT JOIN metaData md ON md.mainID = rt.id
                LEFT JOIN applicantdetails ad ON ad.userID = ap.userID
                WHERE ap.id = :id";
                $vars = array(
                    ':id' => $applicationID,
                );
                $applicationsData = $db->exec($query, $vars);
                $userID = $applicationsData[0]['userID'];
                $status = 'a';
                $attributes = ["applicationStatus" => $status, "dateUpdated" => date("Y-m-d")];
                $definition = $this->getDefinition($attributes);
                $idArray = ['id = ?', $applicationID];

                $user = $this->getRepository('applications')->updateRecord($idArray, $definition);

                //Email Tenant Approval Email
                $userdata = [];
                foreach($applicationsData as $row){
                    $userdata[$row["dataLabel"]] = $row["data"];
                    $userdata["email"] = $row["email"];
                    $userdata["firstName"] = $row["firstName"];
                }
                $subject = "Application Approved";
                $this->dispatchEmail($userdata, $subject, "property_owner_approval");
                //invoice
                $query = "SELECT ad.id AS applicantID, ad.email, ad.firstName AS tenant_name, ad.lastName AS tenant_surname, 
                ad.address AS tenant_address, ap.propertyReference,
                ad.phone AS tenant_phone, ad.totalIncome, uc.title, uc.dateCreated AS issue_date,
                ap.unitID, ap.complexID, ap.propertyReference
                FROM applicantdetails ad 
                INNER JOIN userChecklist uc ON ad.userID = uc.userID 
                AND uc.title = :title AND uc.isVerified = :isVerified
                INNER JOIN applications ap ON ad.userID = ap.userID 
                WHERE ad.userID = :userID 
                ORDER BY ad.id DESC";
                $vars = array(
                    ':userID' => $userID,
                    ':title' => 'proofOfPayment',
                    ':isVerified' => 'y'
                );
                $invoice = $db->exec($query, $vars);
                $query = "INSERT INTO applicantunitallocation (name, applicantID, propertyReference, dateCreated, dateUpdated) VALUE (:name, :applicantID, :propertyReference, NOW(), NOW())";
                $vars = array(
                    ':name' => $invoice[0]['tenant_name'],
                    ':applicantID' => $invoice[0]['applicantID'],
                    ':propertyReference' => $invoice[0]['propertyReference'],
                );
                $allocateUnit = $db->exec($query, $vars);

                $query = "SELECT * FROM rentalproperties WHERE propertyReference = :propertyReference";
                $vars = array(
                    ':propertyReference' => $invoice[0]['propertyReference'],
                );
                $rentalproperty = $db->exec($query, $vars);
                $curlResponse['rentalproperty'] = json_encode($rentalproperty);

                $query = "SELECT * FROM propertyagents WHERE id = :agentID";
                $vars = array(
                    ':agentID' => $rentalproperty[0]['agentID'],
                );
                $propertyagent = $db->exec($query, $vars);
                $curlResponse['propertyagent'] = json_encode($propertyagent);

                $rentalpropertydata = [];
                foreach($propertyagent as $row){
                    $rentalpropertydata["email"] = $row["email"];
                    $rentalpropertydata["firstName"] = $row["firstName"];
                    $rentalpropertydata["lastName"] = $row["lastName"];
                    $rentalpropertydata["property_owner"] = $rentalproperty[0]["ownerFullName"];
                    $rentalpropertydata["property_reference"] = $rentalproperty[0]["propertyReference"];
                }
                $subject = "Application Approved";
                $this->dispatchEmail($rentalpropertydata, "Property Owner Approval", "agent_approval_notification");

                $query = "SELECT * FROM paymentsettings";
                $paymentsettings = $db->exec($query);
    
                $query = "UPDATE rentalproperties SET occupied = :occupied, available = :available WHERE propertyReference = :propertyReference";
                $vars = array(
                    ':occupied' => 'y',
                    ':available' => 'n',
                    ':propertyReference' => $invoice[0]['propertyReference'],
                );
                $occupied = $db->exec($query, $vars);
            } else if($action=="decline"){
                $query = "SELECT md.*, ad.id AS applicantID, ad.firstName, ad.email, ap.userID, ap.complexID, ap.unitID, ap.propertyReference, ap.id AS applicationID FROM applications ap
                LEFT JOIN rentalproperties rt ON ap.id = rt.applicationID
                LEFT JOIN metaData md ON md.mainID = rt.id
                LEFT JOIN applicantdetails ad ON ad.userID = ap.userID
                WHERE ap.id = :id";
                $vars = array(
                    ':id' => $applicationID,
                );
                $applicationsData = $db->exec($query, $vars);
                $userID = $applicationsData[0]['userID'];

                //Email Tenant Approval Email
                $userdata = [];
                foreach($applicationsData as $row){
                    $userdata[$row["dataLabel"]] = $row["data"];
                    $userdata["email"] = $row["email"];
                    $userdata["firstName"] = $row["firstName"];
                }
                $subject = "Application Declined";
                $this->dispatchEmail($userdata, $subject, "property_owner_decline");
            }
            $curlResponse = array('msg' => 'Application updated successfully!', 'alert' => 'success');
            echo json_encode($curlResponse);
        } catch (Exception $exception) {
            echo json_encode($exception->getMessage());
        }
    }
}