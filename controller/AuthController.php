<?php

// header('Access-Control-Allow-Origin: *');

// header('Access-Control-Allow-Methods: GET, POST');

// header("Access-Control-Allow-Headers: X-Requested-With");
//date_default_timezone_set('Africa/Johannesburg');

require_once 'AppController.php';
require_once 'passwordController.php';

class AuthController extends AppController
{
    public function loginPage($f3)
    {
        if ($f3->get('SESSION.USER.id')) {
            $f3->reroute('/user/dashboard');
        } else {
            $f3->set('content', 'auth/login.htm');
            echo View::instance()->render('auth_template.htm');
        }
    }

    public function userLogins($f3, $attributes)
    {
        global $db;

        $sql = "SELECT * FROM userLogins where email = :email AND date(loginTime) >= curdate() ORDER BY id DESC LIMIT 1;";
        $vars = array(
            ':email' => $attributes['email'],
        );
        $attempts = $db->exec($sql, $vars);
        
    

        if(count($attempts) > 0){
            $userLogin['loginAttempts'] = $attempts[0]['loginAttempts'] + 1;
            $update = true;
        } else {
            $userLogin['loginAttempts'] = 1;
            $update = false;
        }
        

        $userLogin['email'] = $attributes['email'];
        $userLogin['loginResponse'] = $attributes['loginResponse'];
        $userLogin['ipAddress'] = $attributes['ipAddress'];
        $userLogin['loginLocation'] = $attributes['loginLocation'];

        if($update) {

            if($attempts[0]['loginAttempts'] <= 3){
                $sql = "UPDATE `userLogins` SET `email` = :email, `loginResponse` = :loginResponse, `ipAddress` = :ipAddress, `loginLocation` = :loginLocation, `loginAttempts` = :loginAttempts, `loginTime` = :loginTime WHERE date(loginTime) >= curdate() AND `id` = :id";
                $vars1 = array(
                    ':email' => $userLogin['email'],
                    ':loginResponse' => $userLogin['loginResponse'],
                    ':ipAddress' => $userLogin['ipAddress'],
                    ':loginLocation' => $userLogin['loginLocation'],
                    ':loginAttempts' => $userLogin['loginAttempts'],
                    ':loginTime' => date('Y-m-d H:i:s'),
                    ':id' => $attempts[0]['id'],
                );
                $logins = $db->exec($sql, $vars1);
            }
        } else {

            if($attempts[0]['loginAttempts'] <= 3){
                $sql = "INSERT INTO `userLogins` (`email`,`loginResponse`,`ipAddress`,`loginLocation`,`loginAttempts`,`loginTime`)
                VALUES (:email, :loginResponse, :ipAddress, :loginLocation, :loginAttempts, :loginTime)";
                $vars1 = array(
                    ':email' => $userLogin['email'],
                    ':loginResponse' => $userLogin['loginResponse'],
                    ':ipAddress' => $userLogin['ipAddress'],
                    ':loginLocation' => $userLogin['loginLocation'],
                    ':loginAttempts' => $userLogin['loginAttempts'],
                    ':loginTime' => date('Y-m-d H:i:s'),
                );
                $logins = $db->exec($sql, $vars1);
            }
        }

        if($userLogin['loginAttempts'] == 3){
            $sql = "UPDATE `users` SET `userState` = :userState, `dateUpdated` = :dateUpdated WHERE `email` = :email";
            $vars1 = array(
                ':userState' => 0,
                ':dateUpdated' => date('Y-m-d H:i:s'),
                ':email' => $userLogin['email'],
            );
            $deactivateaccount = $db->exec($sql, $vars1);

            $sql = "UPDATE `userLogins` SET `email` = :email, `loginResponse` = :loginResponse, `ipAddress` = :ipAddress, `loginLocation` = :loginLocation, `loginAttempts` = :loginAttempts, `loginTime` = :loginTime WHERE date(loginTime) >= curdate() AND `id` = :id";
            $vars1 = array(
                ':email' => $userLogin['email'],
                ':loginResponse' => "Account Disabled",
                ':ipAddress' => $userLogin['ipAddress'],
                ':loginLocation' => $userLogin['loginLocation'],
                ':loginAttempts' => 3,
                ':loginTime' => date('Y-m-d H:i:s'),
                ':id' => $attempts[0]['id'],
            );
            $logins = $db->exec($sql, $vars1);
        }

        return $userLogin['loginAttempts'];
    }

    public function loginPost($f3)
    {
        try {
            global $db;
            $attributes = $f3->get('POST');
            $user = $this->authenticate($attributes);
            $loginAttempts = 1;
            $curlResponse = [];

            $userverifiedstate = $this->getRepository('users')->getByAttribute('email', $attributes['email']);
            $curlResponse['isVerified'] = json_encode($userverifiedstate[0]['isVerified']);

            if (!$user) {
                $attributes['loginResponse'] = "Incorrect password/email";
                $attributes['userID'] = $user['id'];
                $loginAttempts = $this->userLogins($f3, $attributes);
                
            } elseif ($user[0]['isVerified'] == '0') {

            } elseif ($user[0]['userState'] == '0') {

            }

            $sql = "SELECT pmLevel FROM users where email = :email;";
            $vars = array(
                ':email' => $attributes['email'],
            );
            $results = $db->exec($sql, $vars);
            $curlResponse['pmLevel'] = json_encode($results[0]['pmLevel']);
            $curlResponse['loginAttempts'] = json_encode($loginAttempts);
            
            $now = time(); // or your date as well
            $your_date = strtotime($user[0]['dateCreated']);
            $datediff = $now - $your_date;
            $days = round($datediff / (60 * 60 * 24));

            $newArr = ["id"=>$user[0]['id'],"firstName"=>$user[0]['firstName'], "userState"=>$user[0]['userState'],
                        "lastName"=>$user[0]['lastName'], "email"=>$user[0]['email'],"phone"=>$user[0]['phone'], 
                        "pmLevel"=>$user[0]['pmLevel'], "isVerified"=>$user[0]['isVerified'], "days" => $days, "userType" => $user[0]['userType'], "agentID" => $user[0]['agentID'], "reset" => $user[0]['reset']];

            Auth::createSession($f3, $user[0]);

            $curlResponse['user'] = json_encode($newArr);
            echo json_encode($curlResponse);
        } catch (Exception $exception) {
            return $exception->getMessage();
        }
    }

    public function property_login($f3)
    {
        try {
            global $db;
            $attributes = $f3->get('POST');
            $loginAttempts = 1;
            $curlResponse = [];

            $sql = "SELECT * FROM applications WHERE id = :applicationID AND propertyReference = :propertyReference;";
            $vars = array(":applicationID" => $attributes['applicationID'], ':propertyReference' => $attributes['propertyReference']);
            $application = $db->exec($sql, $vars);

            $response = array();
            if(count($application)>0){
                $response = array(
                    "property" => '1',
                    "reference" => $application[0]['propertyReference']
                );
            } else {
                $response = array(
                    "property" => '0',
                    "reference" => ''
                );
            }
            $curlResponse['rental_property'] = json_encode($response);
            $curlResponse['user'] = json_encode($newArr);
            echo json_encode($curlResponse);
        } catch (Exception $exception) {
            return $exception->getMessage();
        }
    }

    public function resendOTPPost($f3)
    {
        global $db;
        $attributes = $f3->get('POST');
        $phoneNumber = $attributes['phone'];

        $query = "UPDATE `users` SET `phone` = :phone, `dateUpdated` = :dateUpdated WHERE `id` = :id;";
        $vars = array(
            ':id' => $attributes['userID'],
            ':phone' => $phoneNumber,
            ':dateUpdated' => date('Y-m-d H:i:s'),
        );
        $results = $db->exec($query, $vars);
    }

    private function authenticate($attributes) {

        $user = $this->getRepository('users')->getByAttribute('email', $attributes['email']);
        $password = hash('sha256', $attributes['password']);
        
        if ($user[0]['password'] === $password) {

            return $user;
        }

        return false;
    }

    public function agents($f3)
    {
        global $db;
        $curlResponse = [];

        $sql = "SELECT * FROM propertyagents WHERE isDeleted = 'n';";
        $results = $db->exec($sql);
        $curlResponse['agents'] = json_encode($results);

        $query = "SELECT * FROM rentalproperties WHERE deleted = :deleted AND available = :available GROUP BY propertyReference ORDER BY id ASC";
        $vars = array(
            ':deleted' => 'n',
            ':available' => 'y'
        );
        $rental_properties = $db->exec($query, $vars);
        $curlResponse['rental_properties'] = json_encode($rental_properties);
        
        echo json_encode($curlResponse);
    }
    

    public function registerPost($f3)
    {

        try {
            $attributes = $f3->get('POST');
            $curlResponse = [];
            $validate = true;

            if ($attributes['agreement'] != "1") {
                $validate = false;
            }

            // if ($attributes['policy'] != "1") {
            //     $validate = false;
            // }

            // if ($attributes['information_consent'] != "1") {
            //     $validate = false;
            // }

            // if ($attributes['infringement_consent'] != "1") {
            //     $validate = false;
            // }

            // if ($attributes['service_acknowledgement'] != "1") {
            //     $validate = false;
            // }

            // if ($attributes['third_party_access_consent'] != "1") {
            //     $validate = false;
            // }

            // if ($attributes['personal_information_consent'] != "1") {
            //     $validate = false;
            // }

            if ($attributes['email'] == "") {
                $validate = false;
            }

            $user = $this->getRepository('users')->getByAttribute('email', $attributes['email']);
            if ($user) {
                $curlResponse['user'] = json_encode($user);
                $validate = false;
            }

            if ($attributes['phone'] == "") {
                $validate = false;
            }

            if( !preg_match( "/^\+27[0-9]{9}$/", $attributes['phone'] ) ){
                $validate = false;
            }

            if ($attributes['firstName'] == "") {
                $validate = false;
            }

            if ($attributes['lastName'] == "") {
                $validate = false;
            }

            // if ($attributes['agentID'] == "") {
            //     $validate = false;
            // }

            // if ($attributes['propertyReference'] == "") {
            //     $validate = false;
            // }

            if ($attributes['password'] != $attributes['password_confirm']) {
                $validate = false;

            }

            if ($validate) {
                $this->createAccount($f3, $attributes);
                $subject = "Welcome to Tenant Onboarding";
                $curlResponse['result'] = array('msg' => 'Please check your email to verify your account.', 'alert' => 'success');
            } else {
            }
            // echo json_encode($curlResponse);
        } catch (Exception $exception) {
            return $exception->getMessage();
        }

    }

    public function createAccount($f3, $attributes)
    {
        global $db;
        date_default_timezone_set('Africa/Johannesburg');
        $password = $password = hash('sha256', $f3->get('POST.password')); //create_hash(trim($attributes['password']));
        

        $query = "INSERT INTO `users` (`email`, `pmLevel`, `userType`,`userState`,`isVerified`,`password`,`verificationCode`,`firstName`,`lastName`,`phone`,`dateCreated`,`dateUpdated`)
        VALUES (:email, :pmLevel, :userType, :userState, :isVerified, :password, :verificationCode, :firstName, :lastName, :phone, :dateCreated, :dateUpdated)";
        $vars = array(
            ':email' => $attributes['email'],
            ':pmLevel'=> 2,
            ':userType' => 2,
            ':userState' => 1,
            ':isVerified' => 0,
            ':password' => $password,
            ':verificationCode' => $attributes['verificationCode'],
            ':firstName' => $attributes['firstName'],
            ':lastName' => $attributes['lastName'],
            ':phone' => $attributes['phone'],
            ':dateCreated' => date('Y-m-d H:i:s'),
            ':dateUpdated' => date('Y-m-d H:i:s'),
        );
        $results = $db->exec($query, $vars);
        $insertedID = $db->lastInsertId();

        $user = $this->getRepository('applicantdetails')->getByAttribute('userID', $insertedID);
        if ($user) {
        }
        else{
            $sql = "INSERT INTO `applicantdetails` (`email`,`phone`,`altPhone`,`firstName`,`lastName`,`userID`,`dateCreated`,`dateUpdated`)
            VALUES (:email, :phone, :altPhone, :firstName, :lastName, :userID, :dateCreated, :dateUpdated)";
            $vars1 = array(
                ':email' => $attributes['email'],
                ':phone' => $attributes['phone'],
                ':altPhone' => $attributes['phone'],
                ':userID' => $insertedID,
                ':firstName' => $attributes['firstName'],
                ':lastName' => $attributes['lastName'],
                ':dateCreated' => date('Y-m-d H:i:s'),
                ':dateUpdated' => date('Y-m-d H:i:s'),
            );
            $results = $db->exec($sql, $vars1);
            $applicantID = $db->lastInsertId();

            // $sql = "SELECT * FROM rentalproperties where agentID = :agentID AND propertyReference = :propertyReference AND deleted = :deleted AND available = :available";
            // $vars = array(
            //     ':agentID' => $attributes['agentID'],
            //     ':propertyReference' => $attributes['propertyReference'],
            //     ':deleted' =>'n',
            //     ':available' => 'y'
            // );
            // $propertyReference = $db->exec($sql, $vars);

            // if(count($propertyReference)>0){
            //     $sql = "UPDATE `rentalproperties` SET applicantID = :applicantID, dateUpdated = :dateUpdated WHERE id = :id";
            //     $vars1 = array(
            //         ':applicantID' => $applicantID,
            //         ':dateUpdated' => date("Y-m-d H:i:s"),
            //         ':id' => $propertyReference[0]['id'],
                    
            //     );
            //     $results = $db->exec($sql, $vars1);
            // }
        }
    }

    public function verifyAccount($f3, $params, $attributes)
    {
        global $db;

        $sql = "SELECT email FROM users where email = :email AND verificationCode = :verificationCode";
        $vars = array(
            ':email' => $params['email'],
            ':verificationCode' => $attributes['verificationCode'],
        );
        $results = $db->exec($sql, $vars);

        if (count($results) > 0) {
            $sql = "UPDATE users SET isVerified = :isVerified where email = :email AND verificationCode = :verificationCode";
            $vars = array(
                ':email' => $params['email'],
                ':isVerified' => 1,
                ':verificationCode' => $attributes['verificationCode'],
            );
            $results = $db->exec($sql, $vars);
            $f3->set('SESSION.count', 1);
            $f3->set('SESSION.message', array('msg' => 'Account Verified, Please Log In.', 'alert' => 'success'));
            $f3->reroute("/login");

        } else {
            $f3->set('SESSION.count', 1);
            $f3->set('SESSION.message', array('msg' => 'Incorrect Verification Code', 'alert' => 'danger'));
            $f3->reroute("/verify/" . $params['email']);
        }

    }


    public function activate($f3, $params) {
        try {

            global $db;

            $code = $params['code'];
            $f3->set('POST.isVerified', 1);
            $attributes = $f3->get('POST');
            $definition = $this->getDefinition($attributes);

            $sql = "SELECT * FROM users where verificationCode = :verificationCode";
            $vars = array(
                ':verificationCode' => $code,
            );
            $user = $db->exec($sql, $vars);

            if($user[0]['isVerified']==1){
                $curlResponse['user'] = array('msg' => 'Your profile has been activated successfully! Please log in to your account', 'alert' => 'success');
            } else{
                $sql = "UPDATE users SET isVerified = :isVerified where verificationCode = :verificationCode";
                $vars = array(
                    ':isVerified' => 1,
                    ':verificationCode' => $code,
                );
                $user = $db->exec($sql, $vars);

                $sql = "SELECT * FROM users where verificationCode = :verificationCode";
                $vars = array(
                    ':verificationCode' => $code,
                );
                $user = $db->exec($sql, $vars);

                if ($user[0]['isVerified']==1) {
                    $curlResponse['user'] = array('msg' => 'Your profile has been activated successfully! Please log in to your account', 'alert' => 'success');
                } else {
                    $curlResponse['user'] = array('msg' => 'Your profile has been not been activated successfully! Please contact support', 'alert' => 'danger');
                }
            }
            echo json_encode($curlResponse);

            //$f3->reroute("/login");
        } catch (Exception $exception) {
            echo json_encode($exception->getMessage());
        }
    }

    public function verifyPage($f3, $params)
    {
        try {
            if ($f3->get('SESSION.USER.id')) {
                $f3->reroute('/user/dashboard');
            }
            else{
                $attributes = $f3->get('POST');
                $this->verifyAccount($f3, $params, $attributes);
                $f3->set('email', $params['email']);
                $f3->set('content', 'auth/verify.htm');
                echo View::instance()->render('auth_template.htm');
            }
            
        } catch (Exception $exception) {
            return $exception->getMessage();
        }
    }

    public function verifyPost($f3, $params)
    {
        try {
            $attributes = $f3->get('POST');
            $this->verifyAccount($f3, $params, $attributes);
        } catch (Exception $exception) {
            return $exception->getMessage();
        }
    }

    public function passwordResetPage($f3)
    {
        if ($f3->get('SESSION.USER.id')) {
            $f3->reroute('/user/dashboard');
        } else {
            $f3->set('email', $params['email']);
            $f3->set('content', 'auth/resetPassword.htm');
            echo View::instance()->render('auth_template.htm');
        }
    }

    public function passwordResetPost($f3)
    {
        global $db;
        try {
            $curlResponse = [];
        
            $attributes = $f3->get('POST');
            $f3->set('SESSION.count', 1);

            $sql = "SELECT email, firstName FROM users where email = :email";
            $vars = array(
                ':email' => $attributes['email']
            );
            $user = $db->exec($sql, $vars);
            $curlResponse['user'] = json_encode($user);

            // if (!$user) {
            //     $f3->set('SESSION.emailError', 'An account with that email already exists.');
            //     $f3->reroute('/login');
            // }

            $newPassword = $this->generateStrongPassword(8);

            $curlResponse['password'] = $newPassword;

            $attributes['firstName'] = $user[0]['firstName'];
            $attributes['password'] =  $newPassword;

            $password = hash('sha256', $newPassword); //create_hash(trim($attributes['password']));

            $sql = "UPDATE users SET password = :password, reset = :reset where email = :email";
            $vars = array(
                ':email' => $attributes['email'],
                ':password' => $password,
                ':reset' => 1
            );
            $results = $db->exec($sql, $vars);

            echo json_encode($curlResponse);

            

            // $subject = "Tenant Onboarding";

            // $this->dispatchEmail($attributes, $subject, "passwordReset");

            // $f3->set('SESSION.message', array('msg' => 'Please check your email for your new password.', 'alert' => 'success'));
            // $f3->reroute('/login');
        } catch (Exception $exception) {
            return $exception->getMessage();
        }
        //$f3->reroute('/login');
    }


    public function passwordUpdatePost($f3)
    {

        $attributes = $f3->get('POST');
        $curlResponse = [];

        //Clear Past Error Messages
        $f3->set('SESSION.passwordError', null);
        $f3->set('SESSION.message', array('error' => null, 'alert' => 'danger'));

        $validate = true;
        if ($attributes['password'] != $attributes['password_confirm']) {
            $f3->set('SESSION.passwordError', 'Passwords do not match');
            $validate = false;
        }

        if ($validate) {
            $attributes['reset'] = 0;
            $id = $this->updatePassword($f3, $attributes);
            $curlResponse['updateID'] = json_encode($id);
        } else {

        }

        echo json_encode($curlResponse);

    }

    public function updatePassword($f3, $attributes)
    {
        global $db;

        $password = hash('sha256', $attributes['password']); //create_hash(trim($attributes['password']));
        
        $query = "UPDATE `users` SET `password` = :password, `reset` = :reset, `dateCreated` = :dateCreated, `dateUpdated` = :dateUpdated WHERE `id` = :id;";
        $vars = array(
            ':id' => $attributes['id'],
            ':password' => $password,
            ':reset' => 0,
            ':dateCreated' => date('Y-m-d H:i:s'),
            ':dateUpdated' => date('Y-m-d H:i:s'),
        );
        $results = $db->exec($query, $vars);
        $insertedID = $db->lastInsertId();

        return $insertedID;
    }



    public function passwordChangePage($f3, $params)
    {
        if ($f3->get('SESSION.USER.id')) {
            $f3->reroute('/user/dashboard');
        } else {
            $f3->set('email', $params['email']);
            $f3->set('content', 'auth/changePassword.htm');
            echo View::instance()->render('auth_template.htm');
        }
    }

    public function passwordChangePost($f3, $params)
    {
        try {
            $attributes = $f3->get('POST');
            $this->verifyAccount($f3, $params, $attributes);
        } catch (Exception $exception) {
            return $exception->getMessage();
        }
    }

    public function logout($f3)
    {
        Auth::destroySession($f3);
        $f3->reroute("/login");
    }

}
