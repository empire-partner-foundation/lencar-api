<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class AdminController extends AppController
{

    public function dashboard($f3, $params)
    {
        global $db;
        $this->authCheck($f3);
        $curlResponse = [];

        $sql = "UPDATE `userLogins` SET `email` = :email, `loginResponse` = :loginResponse, `loginAttempts` = :loginAttempts, `loginTime` = :loginTime WHERE `email` = :email";
        $vars1 = array(
            ':email' => $f3->get("POST.email"),
            ':loginResponse' => "Login Successfull",
            ':loginAttempts' => 0,
            ':loginTime' => date('Y-m-d H:i:s'),
            ':email' => $f3->get('POST.email'),
        );
        $logins = $db->exec($sql, $vars1);

        $query = "SELECT userType, agentID FROM users WHERE id = :userID AND isDeleted=:isDeleted";
        $vars = array(
            ':userID' => $f3->get("POST.userID"),
            ':isDeleted' => 'n',
        );
        $user_profile = $db->exec($query, $vars);

        $filter_applications_by_agent = '';
        if($user_profile[0]['userType']=='3' && !empty($user_profile[0]['agentID'])){
            $filter_applications_by_agent = 'AND agentID = '.$user_profile[0]['agentID'];
        }

        $query = "SELECT COUNT(*) as totalUnits FROM rentalproperties WHERE deleted=:deleted $filter_applications_by_agent";
        $vars = array(
            ':deleted' => 'n',
        );
        $totalUnits = $db->exec($query, $vars);
        $curlResponse['totalUnits'] = $totalUnits[0]['totalUnits'];

        $query = "SELECT COUNT(*) as occupiedUnits FROM rentalproperties WHERE deleted=:deleted AND occupied=:occupied $filter_applications_by_agent";
        $vars = array(
            ':deleted' => 'n',
            ':occupied' => 'y',
        );
        $occupiedUnits = $db->exec($query, $vars);
        $curlResponse['occupiedUnits'] = $occupiedUnits[0]['occupiedUnits'];
        $curlResponse['availableUnits'] = $totalUnits[0]['totalUnits'] - $occupiedUnits[0]['occupiedUnits'];

        $query = "SELECT COUNT(*) as totalApplications  FROM applications WHERE isDeleted = :isDeleted $filter_applications_by_agent";
        $vars = array(
            ':isDeleted' => 'n',
        );
        $applicantdata = $db->exec($query, $vars);

        $f3->set('totalApplications', $applicantdata[0]['totalApplications']);
        $curlResponse['totalApplications'] = $applicantdata[0]['totalApplications'];

        $query = "SELECT COUNT(*) as approvedApplications  FROM applications WHERE isDeleted = :isDeleted AND applicationStatus = :status $filter_applications_by_agent";
        $vars = array(
            ':status' => "a",
            ':isDeleted' => 'n',
        );
        $applicantdata = $db->exec($query, $vars);
        $curlResponse['approvedApplications'] = $applicantdata[0]['approvedApplications'];

        $query = "SELECT COUNT(*) as declinedApplications  FROM applications WHERE isDeleted = :isDeleted AND applicationStatus = :status $filter_applications_by_agent";
        $vars = array(
            ':status' => "d",
            ':isDeleted' => 'n',
        );
        $applicantdata = $db->exec($query, $vars);
        $curlResponse['declinedApplications'] = $applicantdata[0]['declinedApplications'];

        $query = "SELECT COUNT(*) as pendingApplications  FROM applications WHERE isDeleted = :isDeleted AND applicationStatus = :status $filter_applications_by_agent";
        $vars = array(
            ':status' => "p",
            ':isDeleted' => 'n',
        );
        $applicantdata = $db->exec($query, $vars);
        $curlResponse['pendingApplications'] = $applicantdata[0]['pendingApplications'];

        $query = "SELECT COUNT(*) as onholdUnits FROM units WHERE deleted=:deleted AND occupied=:occupied";
        $vars = array(
            ':deleted' => 'n',
            ':occupied' => 'h',
        );
        $units = $db->exec($query, $vars);
        $curlResponse['onholdUnits'] = $units[0]['onholdUnits'];

        $query = "SELECT *  FROM users WHERE id = :userID";
        $vars = array(
            ':userID' => $f3->get("SESSION.USER.id"),
        );
        $userData = $db->exec($query, $vars);
        $curlResponse['userDataEmail'] = $userData[0]['email'];
        $curlResponse['isNew'] = $userData[0]['isNew'];
        echo json_encode($curlResponse);
    }

    public function viewXdsLogs($f3)
    {
        $curlResponse = [];
        $this->authCheck($f3);
        global $db;
        $query = "SELECT * FROM apiReports";
        $results = $db->exec($query, $vars);
        $curlResponse['xdslogs'] = json_encode($results);
        echo json_encode($curlResponse);
    }

    public function viewBackupLogs($f3)
    {
        $curlResponse = [];
        $this->authCheck($f3);
        global $db;
        $query = "SELECT * FROM backupLogs";
        $results = $db->exec($query);
        $curlResponse['backuplogs'] = json_encode($results);
        echo json_encode($curlResponse);
    }

    public function createAgent($f3){
        global $db;
        $query = "INSERT INTO propertyagents (firstName, lastName, phone, email, dateCreated, dateUpdated) VALUES (:firstName, :lastName, :phone, :email, NOW(), NOW());";
        $vars = array(
            ':firstName' => $f3->get('POST.firstName'),
            ':lastName' => $f3->get('POST.lastName'),
            ':phone' => $f3->get('POST.phone'),
            ':email' => $f3->get('POST.email'),
        );
        $agents = $db->exec($query, $vars);
    }

    public function deleteAgent($f3, $params)
    {
        global $db;
        $agentID  = $params['agentID'];
        $query = "UPDATE propertyagents SET isDeleted = :isDeleted, dateUpdated = NOW() WHERE id = :id;";
        $vars = array(
            ':isDeleted' => 'y',
            ':id' => (int) $agentID,
        );
        $deleteagent = $db->exec($query, $vars);
    }

    public function updateAgent($f3, $params)
    {
        try {
            global $db;
            $curlResponse = [];
            if ($f3->get('POST')) {
                $attributes = $f3->get('POST');
                $definition = $this->getDefinition($attributes);
                $idArray = ['id = ?', $params['id']];
                $user = $this->getRepository('propertyagents')->updateRecord($idArray, $definition);
            }

            $query = "SELECT * FROM propertyagents WHERE id = :agentID";
            $vars = array(
                ':agentID' => $params['id'],
            );
            $users = $db->exec($query, $vars);
            $curlResponse['POST'] = json_encode($users);
            echo json_encode($curlResponse);
        } catch (Exception $exception) {
            echo json_encode($exception->getMessage());
        }
    }

    public function view_application_email_notifications_users_list($f3)
    {
        global $db;
        $curlResponse = [];

        $query = "SELECT * FROM applicationnotificationemails WHERE isDeleted= :isDeleted";
        $vars = array(
            ':isDeleted' => "n",
        );
        $notification_user_emails = $db->exec($query, $vars);
        $curlResponse['notification_user_emails'] = json_encode($notification_user_emails);

        echo json_encode($curlResponse);
    }

    public function create_application_notification_email($f3)
    {
        try {
            global $db;
            $attributes = $f3->get('POST');
  
            $query = "SELECT * FROM applicationnotificationemails WHERE email= :email";
            $vars = array(
                ':email' => $attributes['email']
            );
            $applicationnotificationemails = $db->exec($query, $vars);
            if (count($applicationnotificationemails)>0) {

                if($applicationnotificationemails[0]['isDeleted']=='y'){
                    $attributes = [];
                    $attributes['firstName'] = $f3->get('POST.firstName');
                    $attributes['lastName'] = $f3->get('POST.lastName');
                    $attributes['email'] = $f3->get('POST.email');
                    $attributes['isDeleted'] = 'n';
                    $attributes['dateUpdated'] = date('Y-m-d H:i:s');
                    $definition = $this->getDefinition($attributes);
                    $idArray = ['id = ?', $applicationnotificationemails[0]['id']];
                    $update = $this->getRepository('applicationnotificationemails')->updateRecord($idArray, $definition);
                } else {
                    $curlResponse = array('msg' => 'An notification email with that email already exists.', 'alert' => 'error');
                }
                
            } else {
                $query = "INSERT INTO `applicationnotificationemails` (`firstName`,`lastName`, `email`, `dateCreated`)
                VALUES ( :firstName, :lastName, :email, :dateCreated)";
                $vars = array(
                    ':firstName' => $attributes['firstName'],
                    ':lastName' => $attributes['lastName'],
                    ':email' => $attributes['email'],
                    ':dateCreated' => date('Y-m-d H:i:s')
                );
                $save = $db->exec($query, $vars);
                $curlResponse = array('msg' => 'Notification email created successfully!', 'alert' => 'success');
            }
            echo json_encode($curlResponse);

        } catch (Exception $exception) {
            echo json_encode($exception->getMessage());
        }
    }

    public function edit_application_notification_email($f3)
    {
        try {
            global $db;
            $attributes = $f3->get('POST');
            $query = "SELECT * FROM applicationnotificationemails WHERE email= :email AND isDeleted = :isDeleted";
            $vars = array(
                ':email' => $attributes['email'],
                ':isDeleted' => 'n'
            );
            $applicationnotificationemails = $db->exec($query, $vars);

            if (count($applicationnotificationemails)>0) {

                if($applicationnotificationemails[0]['id']==$attributes['notificationEmailID']){
                    $attributes = [];
                    $attributes['firstName'] = $f3->get('POST.firstName');
                    $attributes['lastName'] = $f3->get('POST.lastName');
                    $attributes['email'] = $f3->get('POST.email');
                    $attributes['dateUpdated'] = date('Y-m-d H:i:s');
                    $definition = $this->getDefinition($attributes);
                    $idArray = ['id = ?', $f3->get('POST.notificationEmailID')];
                    $update = $this->getRepository('applicationnotificationemails')->updateRecord($idArray, $definition);
                    $curlResponse = array('msg' => 'Notification email updated successfully!', 'alert' => 'success');
                } else {
                    $curlResponse = array('msg' => 'An notification email with that email already exists.', 'alert' => 'error');
                }
                
            } else {
                $attributes = [];
                $attributes['firstName'] = $f3->get('POST.firstName');
                $attributes['lastName'] = $f3->get('POST.lastName');
                $attributes['email'] = $f3->get('POST.email');
                $attributes['dateUpdated'] = date('Y-m-d H:i:s');
                $definition = $this->getDefinition($attributes);
                $idArray = ['id = ?', $f3->get('POST.notificationEmailID')];
                $update = $this->getRepository('applicationnotificationemails')->updateRecord($idArray, $definition);
                $curlResponse = array('msg' => 'Notification email updated successfully!', 'alert' => 'success');
            }
            echo json_encode($curlResponse);

        } catch (Exception $exception) {
            echo json_encode($exception->getMessage());
        }
    }

    public function delete_application_notification_email($f3, $params)
    {
        $attributes = ["isDeleted" => 'y', "dateUpdated" => date("Y-m-d")];
        $definition = $this->getDefinition($attributes);
        $idArray = ['id = ?', $params['id']];
        $user = $this->getRepository('applicationnotificationemails')->updateRecord($idArray, $definition);
        $f3->set('SESSION.message', array('msg' => 'Notification email deleted successfully!', 'alert' => 'success'));
    }

    public function viewUsers($f3)
    {
        global $db;
        $curlResponse = [];
        $this->authCheck($f3);
        $query = "SELECT * FROM users WHERE pmLevel= :pmLevel AND isDeleted= :isDeleted";
        $vars = array(
            ':pmLevel' => 1,
            ':isDeleted' => "n",
        );
        $results = $db->exec($query, $vars);
        $curlResponse['users'] = json_encode($results);

        $sql = "SELECT * FROM propertyagents WHERE isDeleted = 'n';";
        $property_agents = $db->exec($sql);
        $curlResponse['property_agents'] = json_encode($property_agents);

        echo json_encode($curlResponse);
    }

    public function createAccount($f3, $attributes)
    {
        global $db;
        if(!isset($attributes['userType'])){
            $attributes['userType'] = 1;
        }

        if(!isset($attributes['userType'])){
            $attributes['userType'] = 1;
        }

        

        $password = hash('sha256', $attributes['password']); 

        if($attributes['agentID']){
            $query = "INSERT INTO `users` (`email`, `pmLevel`, `userType`,`userState`,`isVerified`,`agentID`,`password`,`verificationCode`,`firstName`,`lastName`,`phone`,`dateCreated`,`dateUpdated`)
            VALUES (:email, :pmLevel, :userType, :userState, :isVerified, :agentID, :password, :verificationCode, :firstName, :lastName, :phone, :dateCreated, :dateUpdated)";
            $vars = array(
                ':email' => $attributes['email'],
                ':pmLevel' => 1,
                ':userType' => $attributes['userType'],
                ':userState' => 1,
                ':isVerified' => 1,
                ':agentID' => $attributes['agentID'],
                ':password' => $password,
                ':verificationCode' => $attributes['verificationCode'], 
                ':firstName' => $attributes['firstName'],
                ':lastName' => $attributes['lastName'],
                ':phone' => $attributes['phone'],
                ':dateCreated' => date('Y-m-d H:i:s'),
                ':dateUpdated' => date('Y-m-d H:i:s'),
            );
            $results = $db->exec($query, $vars);
        } else {
            $query = "INSERT INTO `users` (`email`, `pmLevel`, `userType`,`userState`,`isVerified`,`password`,`verificationCode`,`firstName`,`lastName`,`phone`,`dateCreated`,`dateUpdated`)
            VALUES (:email, :pmLevel, :userType, :userState, :isVerified, :password, :verificationCode, :firstName, :lastName, :phone, :dateCreated, :dateUpdated)";
            $vars = array(
                ':email' => $attributes['email'],
                ':pmLevel' => 1,
                ':userType' => $attributes['userType'],
                ':userState' => 1,
                ':isVerified' => 1,
                ':password' => $password,
                ':verificationCode' => $attributes['verificationCode'], 
                ':firstName' => $attributes['firstName'],
                ':lastName' => $attributes['lastName'],
                ':phone' => $attributes['phone'],
                ':dateCreated' => date('Y-m-d H:i:s'),
                ':dateUpdated' => date('Y-m-d H:i:s'),
            );
            $results = $db->exec($query, $vars);
        }

        
    }

    public function createUser($f3)
    {
        try {
            $this->authCheck($f3);
            $f3->set('POST.dateCreated', date("Y-m-d"));
            $f3->set('POST.dateUpdated', date("Y-m-d"));
            $attributes = $f3->get('POST');
            $user = $this->getRepository('users')->getByAttribute('email', $attributes['email']);
            if ($user) {
                $f3->set('SESSION.emailError', 'An account with that email already exists.');
                $curlResponse = array('msg' => 'An account with that email already exists.', 'alert' => 'error');
            } else {
                $definition = $this->getDefinition($attributes);
                $this->createAccount($f3, $attributes);
                $curlResponse = array('msg' => 'User created successfully!', 'alert' => 'success');
            }

            echo json_encode($curlResponse);

        } catch (Exception $exception) {
            echo json_encode($exception->getMessage());
        }
    }
    public function updateUser($f3, $params)
    {
        try {
            global $db;
            $this->authCheck($f3);
            $curlResponse = [];

            if ($f3->get('POST')) {
                $f3->set('POST.dateUpdated', date("Y-m-d"));
                $users = $this->getRepository('users')->getByAttribute('id', $params['id']);

                if (!empty($f3->get('POST.password'))) {
                    $password = hash('sha256', $f3->get('POST.password'));
                    $f3->set('POST.password', $password);
                } else {
                    $f3->set('POST.password', $users[0]['password']);
                }
                $attributes = $f3->get('POST');
                $definition = $this->getDefinition($attributes);

                $idArray = ['id = ?', $params['id']];
                $user = $this->getRepository('users')->updateRecord($idArray, $definition);
                $curlResponse = array('msg' => 'User updated successfully!', 'alert' => 'success');

            }
            $query = "SELECT * FROM users WHERE id = :userID";
            $vars = array(
                ':userID' => $params['id'],
            );
            $users = $db->exec($query, $vars);
            $curlResponse['POST'] = json_encode($users);
            echo json_encode($curlResponse);
        } catch (Exception $exception) {
            echo json_encode($exception->getMessage());
        }
    }

    public function deleteUser($f3, $params)
    {
        try {
            $this->authCheck($f3);
            $attributes = ["isDeleted" => 'y', "dateUpdate" => date("Y-m-d")];
            $definition = $this->getDefinition($attributes);
            $idArray = ['id = ?', $params['id']];

            $user = $this->getRepository('users')->updateRecord($idArray, $definition);
            $curlResponse = array('msg' => 'User deleted successfully!', 'alert' => 'success');
        } catch (Exception $exception) {
            echo json_encode($exception->getMessage());
        }
    }

    public function paymentSettings($f3)
    {
        global $db;
        $curlResponse = [];
        if($f3->get('POST.update')){
            $query = "UPDATE paymentsettings SET applicationFee = :applicationFee, bankName = :bankName, bankAccount = :bankAccount, accountType = :accountType, branch = :branch, transactionCharge = :transactionCharge, payfast_merchant_id = :payfast_merchant_id, payfast_merchant_key = :payfast_merchant_key, dateUpdated = NOW() WHERE id = :id";
            $vars = array(
                ':applicationFee' => $f3->get("POST.applicationFee"),
                ':bankName' => $f3->get("POST.bankName"),
                ':bankAccount' => $f3->get("POST.bankAccount"),
                ':accountType' => $f3->get("POST.accountType"),
                ':branch' => $f3->get("POST.branch"),
                ':transactionCharge' => $f3->get("POST.transactionCharge"),
                ':payfast_merchant_id' => $f3->get("POST.payfast_merchant_id"),
                ':payfast_merchant_key' => $f3->get("POST.payfast_merchant_key"),
                ':id' => 1,
            );
            $paymentsettings_update = $db->exec($query, $vars);
        }

        $query = "SELECT * FROM paymentsettings";
        $paymentsettings = $db->exec($query);
        $curlResponse['paymentsettings'] = json_encode($paymentsettings);
        echo json_encode($curlResponse);
    }

    public function viewTenants($f3)
    {
        $this->authCheck($f3);
        global $db;
        $curlResponse = [];

        $query = "SELECT distinct(au.applicantID), au.id AS tenantUnitID, ad.firstName, ad.lastName, ad.email, 
        ad.phone, ad.userID, ap.unitID, ap.complexID, ap.dateUpdated, au.propertyReference, CONCAT(pa.firstName, ' ', pa.lastName) AS agentFullName, 
        rp.ownerFullName, rp.ownerContactNumber
        FROM applicantunitallocation au
        INNER JOIN applicantdetails ad ON  au.applicantID = ad.id
        INNER JOIN applications ap ON ad.userID = ap.userID
        INNER JOIN rentalproperties rp ON ap.propertyReference = rp.propertyReference
        LEFT JOIN propertyagents pa ON pa.id = ap.agentID
        WHERE au.id > :id ORDER BY au.id DESC";
        $vars = array(
            ':id' => 0,
        );
        $tenants = $db->exec($query, $vars);

        $f3->set('tenant', $tenants);

        $curlResponse['tenants'] = json_encode($tenants);

        echo json_encode($curlResponse);
    }

    public function viewApplicants($f3)
    {
        global $db;
        $this->authCheck($f3);
        $curlResponse = [];
        $query = "SELECT * FROM users WHERE pmLevel = :pmLevel AND isDeleted = :isDeleted";
        $vars = array(
            ':pmLevel' => 2,
            ':isDeleted' => 'n',
        );
        $applicants = $db->exec($query, $vars);
        $curlResponse['applicants'] = json_encode($applicants);
        echo json_encode($curlResponse);
    }

    public function updateAppliantProfile($f3, $params)
    {
        global $db;
        $this->authCheck($f3);
        $curlResponse = [];
        $query = "UPDATE users SET userState = :userState WHERE id = :userID";
        $vars = array(
            ':userID' => $params['userID'],
            ':userState' => $params['userState'],
        );
        $update = $db->exec($query, $vars);
    }

    public function upgradeApplication($f3, $params)
    {
        $this->authCheck($f3);
        global $db;

        //update application -> move back to pending
        $applicationID = $params['applicationID'];
        $userID = $params['userID'];

        $status = 'p';
        $attributes = ["complexID" => '', "unitID" => '', "applicationStatus" => $status, "dateUpdated" => date("Y-m-d")];
        $definition = $this->getDefinition($attributes);
        $idArray = ['id = ?', $applicationID];

        $user = $this->getRepository('applications')->updateRecord($idArray, $definition);

        $query = "UPDATE applicantdetails SET housing = :housing WHERE userID = :userID";
        $vars = array(
            ':housing' => 'affordable',
            ':userID' => $userID
        );
        $update = $db->exec($query, $vars);

        $f3->set('SESSION.message', array('msg' => 'Application updated successfully!', 'alert' => 'success'));  
        $curlResponse = array('msg' => 'Application updated successfully!', 'alert' => 'success');

        echo json_encode($curlResponse);
        //$f3->reroute('/admin/applications');
    }

    public function createDocuments($f3)
    {
        $this->authCheck($f3);
        global $db;
        try {
            $f3->set('POST.dateCreated', date("Y-m-d"));
            $f3->set('POST.dateUpdated', date("Y-m-d"));
            $attributes = $f3->get('POST');
            $formFieldName = "document";
            $resultUrl = $this->fileUpload($f3, $formFieldName, $folder);
            $CheckList = ["idDocument", "bankStatement", "payslip"];
            $count = 0;

            foreach ($resultUrl as $fileUrl => $bool) {
                $query = "INSERT INTO documents (title, documentUrl, dateCreated, dateUpdated)
                                VALUES (:title, :documentUrl, NOW(), NOW())";
                $vars = array(
                    ':title' =>  $f3->get('POST.title'),
                    ':documentUrl' => $fileUrl
                );
                $documents = $db->exec($query, $vars);
            }

            $f3->set('SESSION.message', array('msg' => 'documents created successfully!', 'alert' => 'success'));
            $f3->reroute("/admin/documents");
        } catch (Exception $exception) {
            echo json_encode($exception->getMessage());
        }
    }

    public function deleteDocuments($f3, $params)
    {
        $this->authCheck($f3);
        $attributes = ["deleted"=>'y', "dateUpdated"=>date("Y-m-d")];
        $definition = $this->getDefinition($attributes);
        try { 
            $idArray = ['id = ?',$params['id']];

            $user = $this->getRepository('documents')->updateRecord($idArray, $definition);
            $f3->set('SESSION.message', array('msg' => 'Document deleted successfully!', 'alert' => 'success'));

            $f3->reroute("/admin/documents");
        } catch (Exception $exception) {
            echo json_encode($exception->getMessage());
        }
        
    }

    public function updateComplex($f3, $params)
    {
        $this->authCheck($f3);
        try {
            if($f3->get('POST')){
                $f3->set('POST.dateUpdated', date("Y-m-d"));
                $attributes = $f3->get('POST');
                $definition = $this->getDefinition($attributes);

                $idArray = ['id = ?',$params['id']];

                $user = $this->getRepository('documents')->updateRecord($idArray, $definition);

                $f3->reroute("/admin/documents");
            }

            $users = $this->getRepository('documents')->getByAttribute('id', $params['id']);
            $f3->set('POST', $users[0]);

            $f3->set('content', '../ui/admin/documents/updateDocuments.html');
            echo \Template::instance()->render('admin_template.htm');
            
        } catch (Exception $exception) {
            echo json_encode($exception->getMessage());
        }
    }

    public function viewForms($f3)
    {
        global $db;
        $curlResponse = [];
        $query = "SELECT *, IF(state=1, 'checked', '') AS status, (SELECT COUNT(id) FROM formsubmissions WHERE forms.id = formID) AS results FROM forms WHERE id > :value";
        $vars = array(
            ':value' => 0,
        );
        $forms = $db->exec($query, $vars);

        $query = "SELECT * FROM formsubmissions";
        $submissions = $db->exec($query);

        $query = "SELECT * FROM forms WHERE state = 1";
        $active_forms = $db->exec($query);

        $query = "SELECT * FROM forms WHERE state = 0";
        $inactive_forms = $db->exec($query);

        $curlResponse['forms'] = json_encode($forms);
        $curlResponse['submissions'] = json_encode($submissions);
        $curlResponse['active_forms'] = json_encode($active_forms);
        $curlResponse['inactive_forms'] = json_encode($inactive_forms);
        echo json_encode($curlResponse);
    }

    public function viewResults($f3, $params)
    {
        //get form submissions
        global $db;
        $curlResponse = [];
    
        $query = "SELECT * FROM fields WHERE formID = :formID ORDER BY id ASC";
        $vars = array(
            ':formID' => $params['id'],
        );
        $fields = $db->exec($query, $vars);
        $curlResponse['fields'] = json_encode($fields);

        $query = "SELECT fm.*, us.email, us.firstName, us.lastName FROM formsubmissions fm LEFT JOIN users us ON fm.userID = us.id WHERE fm.formID = :formID ORDER BY fm.id ASC";
        $vars = array(
            ':formID' => $params['id'],
        );
        $submissions = $db->exec($query, $vars);
        $curlResponse['submissions'] = json_encode($submissions);
        $rows = array();
        $user_rows = array();
        for ($i = 0; $i < count($submissions); $i++) {

            for ($x = 0; $x < count($fields); $x++) {
                # code...
                $query = "SELECT * FROM applicantdata WHERE fieldID = :fieldID AND submissionID = :submissionID ORDER BY id ASC";
                $vars = array(
                    ':fieldID' => $fields[$x]['id'],
                    ':submissionID' => $submissions[$i]['id'],
                );
                $user_data = $db->exec($query, $vars);
                if(sizeof($user_data) > 0) $rows[$x] = $user_data;
            }
            if(sizeof($rows) > 0) $user_rows[$i] = $rows;
        }
        $curlResponse['user_rows'] = json_encode($user_rows);
        echo json_encode($curlResponse);
    }
}
