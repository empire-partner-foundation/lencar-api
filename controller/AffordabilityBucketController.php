<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class AffordabilityBucketController extends AppController
{
    /*
    * viewTrench
    */
    public function viewTrench($f3)
    {
        global $db;
        $curlResponse = [];
        $query = "SELECT * FROM rentTrench WHERE deleted = :deleted";
            $vars = array(
                ':deleted' => "n",
            );
            $rentTrench = $db->exec($query, $vars);
        $f3->set('data', $rentTrench);
        $curlResponse['rentTrench'] = json_encode($rentTrench);
        echo json_encode($curlResponse);
    }

    public function createTrench($f3)
    {
        try {
            $this->authCheck($f3);
            $f3->set('POST.dateCreated', date("Y-m-d"));
            $f3->set('POST.dateUpdated', date("Y-m-d"));
            $attributes = $f3->get('POST');
            $definition = $this->getDefinition($attributes);
            $this->getRepository('rentTrench')->createRecord($definition);
            $f3->set('SESSION.message', array('msg' => 'rentTrench created successfully!', 'alert' => 'success'));
        } catch (Exception $exception) {
            echo json_encode($exception->getMessage());
        }
    }

    public function deleteTrench($f3, $params)
    {
        try {
            $this->authCheck($f3);
            $attributes = ["deleted" => 'y', "dateUpdated" => date("Y-m-d")];
            $definition = $this->getDefinition($attributes);

            $idArray = ['id = ?', $params['id']];

            $user = $this->getRepository('rentTrench')->updateRecord($idArray, $definition);
            $f3->set('SESSION.message', array('msg' => 'rentTrench deleted successfully!', 'alert' => 'success'));

            //$f3->reroute("/admin/trench");
        } catch (Exception $exception) {
            echo json_encode($exception->getMessage());
        }
    }

    public function updateTrench($f3, $params)
    {
        try {
            global $db;
            $this->authCheck($f3);
            $curlResponse = [];

            if ($f3->get('POST.title')) {
                $f3->set('POST.dateUpdated', date("Y-m-d"));
                $attributes = $f3->get('POST');
                $definition = $this->getDefinition($attributes);

                $idArray = ['id = ?', $params['id']];
                $user = $this->getRepository('rentTrench')->updateRecord($idArray, $definition);
            }
            $query = "SELECT * FROM rentTrench WHERE id = :id";
            $vars = array(
                ':id' => $params['id'],
            );
            $rentTrench = $db->exec($query, $vars);
            $curlResponse['rentTrench'] = json_encode($rentTrench);

            echo json_encode($curlResponse);
        } catch (Exception $exception) {
            echo json_encode($exception->getMessage());
        }
    }
}