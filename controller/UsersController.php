<?php

require_once 'AppController.php';

class UsersController extends AppController
{
    public function docs_validation($f3)
    {
        global $db;
        $curlResponse = [];
        $attributes = $f3->get('POST');

        $query = "SELECT * FROM verifiedDocuments WHERE fileName = :fileName ORDER BY id DESC LIMIT 1";
        $vars = array(
            ':fileName' => $f3->get('POST.filename'),
        );
        $files = $db->exec($query, $vars);

        if(count($files) > 0){
            $query = "UPDATE verifiedDocuments SET `fileName` = :fileName, `idNumber` = :idNumber, `response` = :response, `dateUpdated` = NOW() WHERE `id` = :id";
            $vars = array(
                ':fileName' => $f3->get('POST.filename'),
                ':idNumber' => $f3->get('POST.id'),
                ':response' => "OCR Completed Succesfully",
                ':id' => $files[0]['id'],
            );
            $file = $db->exec($query, $vars);
            $saveID = "Document Updated";

        } else {
            $query = "INSERT INTO verifiedDocuments (`fileName`, `idNumber`, `response`, `dateCreated`, `dateUpdated`) VALUES (:fileName, :idNumber, :response, :dateCreated, :dateUpdated)";
            $vars = array(
                ':fileName' => $f3->get('POST.filename'),
                ':idNumber' => $f3->get('POST.id'),
                ':response' => "OCR Completed Succesfully",
                ':dateCreated' => date("Y-m-d H:i:s"),
                ':dateUpdated' => date("Y-m-d H:i:s")
            );
            $insert = $db->exec($query, $vars);
            $saveID = "Document Saved";
        }
        
        $curlResponse['response'] = json_encode($saveID);
        echo json_encode($curlResponse);
    }

    public function verifyIDDocument($f3, $params)
    {
        global $db;
        $curlResponse = [];
        $userID = $params['id'];
        $column = 'idDocument';
        $isVerified = 'y';

        //update or insert.
        $query = "SELECT * FROM userChecklist WHERE userID = :userID AND title = :title";
        $varsArr = array(
            ':userID' => $userID,
            ':title' => $column,
        );
        $userdata = $db->exec($query, $varsArr);
        if (isset($userdata[0]['title'])) {
            $update = true;
        }

        if ($update) {
            $query = "UPDATE userChecklist SET approvedBy=:adminID, isVerified=:isVerified, dateUpdated=NOW() WHERE id=:id";
            $varsArr = array(
                ':adminID' => 'OCR',
                ':isVerified' => $isVerified,
                ':id' => $userdata[0]['id'],
            );
            $userChecklist = $db->exec($query, $varsArr);
            $saveID = 'Verified';
        } else {
            $query = "INSERT INTO userChecklist (userID, approvedBy, title, isVerified, dateCreated, dateUpdated)
                              VALUES (:userID, :adminID, :title, :isVerified, :dateCreated, :dateUpdated)";
            $varsArr = array(
                ':userID' => $userID,
                ':adminID' => 'OCR',
                ':title' => $column,
                ':isVerified' => $isVerified,
                ':dateCreated' => date("Y-m-d H:i:s"),
                ':dateUpdated' => date("Y-m-d H:i:s")
            );
            $userChecklist = $db->exec($query, $varsArr);
            $saveID = 'Verified';
        }

        $curlResponse['response'] = json_encode($saveID);
        echo json_encode($curlResponse);
    }

    public function basicUpload($f3, $formFieldName, $Files)
    {
        $folder = "user_" . $f3->get("POST.userID");
        $target_dir = "uploads/$folder/";
        if (!is_dir("$target_dir")) {
            mkdir("$target_dir", 0777, true);
            chmod("$target_dir", 0777);
        }

        $target_file = $target_dir . basename($_FILES[$formFieldName]["name"]);
        $uploadOk = 1;
        $fileType = strtolower(pathinfo($target_file, PATHINFO_EXTENSION));
        // Check if image file is a actual image or fake image
        // if (isset($_POST["submit"])) {
        //     $check = getimagesize($_FILES[$formFieldName]["tmp_name"]);
        //     if ($check !== false) {
        //         echo "File is an image - " . $check["mime"] . ".";
        //         $uploadOk = 1;
        //     } else {
        //         echo "File is not an image.";
        //         $uploadOk = 0;
        //     }
        // }
        // Check if file already exists
        if (file_exists($target_file)) {
            //echo "Error, file already exists.";
            //$uploadOk = 0;
        }
        // Check file size
        if ($_FILES[$formFieldName]["size"] > 500000) {
            return "Error, your file is too large.";
            $uploadOk = 0;
        }
        // Allow certain file formats
        if ($fileType != "jpg" && $fileType != "png" && $fileType != "jpeg"
            && $fileType != "pdf") {
            return "Error, only JPG, JPEG, PNG & PDF files are allowed.";
            $uploadOk = 0;
        }
        // Check if $uploadOk is set to 0 by an error
        if ($uploadOk == 0) {
            return "Error, your file was not uploaded.";
            // if everything is ok, try to upload file
        } else {
            if (move_uploaded_file($_FILES[$formFieldName]["tmp_name"], $target_file)) {
                return $target_file;
            } else {
                return "Error, there was an error uploading your file.";
            }
        }
    }

    
}
