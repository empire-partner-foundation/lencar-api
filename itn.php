<?php
require 'controller/SendGrid.php';

/**
 * Notes:
 * - All lines with the suffix "// DEBUG" are for debugging purposes and
 *   can safely be removed from live code.
 * - Remember to set PAYFAST_SERVER to LIVE for production/live site
 * - If there is a passphrase set on the sandbox account, include it on line 68
 */
// General defines
define( 'PAYFAST_SERVER', 'TEST' ); // Whether to use "sandbox" test server or live server
define( 'USER_AGENT', 'Mozilla/4.0 (compatible; MSIE 5.01; Windows NT 5.0)' ); // User Agent for cURL

// Error Messages
define( 'PF_ERR_AMOUNT_MISMATCH', 'Amount mismatch' );
define( 'PF_ERR_BAD_SOURCE_IP', 'Bad source IP address' );
define( 'PF_ERR_CONNECT_FAILED', 'Failed to connect to PayFast' );
define( 'PF_ERR_BAD_ACCESS', 'Bad access of page' );
define( 'PF_ERR_INVALID_SIGNATURE', 'Security signature mismatch' );
define( 'PF_ERR_CURL_ERROR', 'An error occurred executing cURL' );
define( 'PF_ERR_INVALID_DATA', 'The data received is invalid' );
define( 'PF_ERR_UNKNOWN', 'Unknown error occurred' );

// General Messages
define( 'PF_MSG_OK', 'Payment was successful' );
define( 'PF_MSG_FAILED', 'Payment has failed' );

// Notify PayFast that information has been received
header( 'HTTP/1.0 200 OK' );
flush();
date_default_timezone_set('Africa/Johannesburg'); //or change to whatever timezone you want
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
$env = $_SERVER['HTTP_HOST'];
$ip = gethostbyname($env);
$enviroment = 'testing';

if(strpos($env, '127.168.20.199') > -1){

    //LOCAL
    $dsn = 'mysql:host=localhost;dbname=remaxstagetenantmanagement;port=3306';
    $user = 'root';
    $password = '';

    try {
        $db = new PDO($dsn, $user, $password);
    } catch (PDOException $e) {
        echo 'Connection failed: ' . $e->getMessage();
    }

    $enviroment = 'local';

} else if(strpos($ip, '127.0.0.1') > -1){

    //PRODUCTION
    $options = array(
                       PDO::MYSQL_ATTR_SSL_VERIFY_SERVER_CERT => false,
                       PDO::MYSQL_ATTR_SSL_KEY    =>'/var/www/certs/client-key.pem',
                       PDO::MYSQL_ATTR_SSL_CERT=>'/var/www/certs/client-cert.pem',
                       PDO::MYSQL_ATTR_SSL_CA    =>'/var/www/certs/ca.pem');
                       
    $dsn = 'mysql:host=127.0.0.1;port=3306;dbname=remaxtenantmanagement';
    $user = 'admin';
    $password = 'NJnorxms99@VZIlr2ukrG';

    try {
        $db = new PDO($dsn, $user, $password, $options);
    } catch (PDOException $e) {
        echo 'Connection failed: ' . $e->getMessage();
    }

    $enviroment = 'prod';
} else if(strpos($env, 'remaxstage') > -1){

    //STAGE
    $options = array(
                       PDO::MYSQL_ATTR_SSL_VERIFY_SERVER_CERT => false,
                       PDO::MYSQL_ATTR_SSL_KEY    =>'/var/www/certs/client-key.pem',
                       PDO::MYSQL_ATTR_SSL_CERT=>'/var/www/certs/client-cert.pem',
                       PDO::MYSQL_ATTR_SSL_CA    =>'/var/www/certs/ca.pem');

    $dsn = 'mysql:host=192.168.20.188;port=3306;dbname=remaxstagetenantmanagement';
    $user = 'root';
    $password = 'NJnorxms99@VZIlr2ukrG';

    try {
        $db = new PDO($dsn, $user, $password, $options);
    } catch (PDOException $e) {
        echo 'Connection failed: ' . $e->getMessage();
    }
   
    $enviroment = 'stage';
} else if(strpos($env, 'tms.remax-townandcountry.co.za') > -1){
    //Test
    $options = array(
                       PDO::MYSQL_ATTR_SSL_VERIFY_SERVER_CERT => false,
                       PDO::MYSQL_ATTR_SSL_KEY    =>'/var/www/certs/client-key.pem',
                       PDO::MYSQL_ATTR_SSL_CERT=>'/var/www/certs/client-cert.pem',
                       PDO::MYSQL_ATTR_SSL_CA    =>'/var/www/certs/ca.pem');

    $dsn = 'mysql:host=192.168.20.188;port=3306;dbname=remaxtenantmanagement';
    $user = 'root';
    $password = 'NJnorxms99@VZIlr2ukrG';

    try {
        $db = new PDO($dsn, $user, $password, $options);
    } catch (PDOException $e) {
        echo 'Connection failed: ' . $e->getMessage();
    }
    $enviroment = 'testing';
} else {

    $options = array(
                    PDO::MYSQL_ATTR_SSL_VERIFY_SERVER_CERT => false,
                    PDO::MYSQL_ATTR_SSL_KEY    =>'/var/www/certs/client-key.pem',
                    PDO::MYSQL_ATTR_SSL_CERT=>'/var/www/certs/client-cert.pem',
                    PDO::MYSQL_ATTR_SSL_CA    =>'/var/www/certs/ca.pem');
                    
    $dsn = 'mysql:host=192.168.20.188;port=3306;dbname=remaxtenantmanagement';
    $user = 'root';
    $password = 'NJnorxms99@VZIlr2ukrG';

    try {
        $db = new PDO($dsn, $user, $password, $options);
    } catch (PDOException $e) {
        echo 'Connection failed: ' . $e->getMessage();
    }

    $enviroment = 'prod';
}

// Variable initialization
$pfError = false;
$pfErrMsg = '';
$dateTime = date('Y-m-d H:i:s', time());
$output = ''; // DEBUG
$pfParamString = '';
//$pfHost = ( PAYFAST_SERVER == 'LIVE' ) ? 'www.payfast.co.za' : 'sandbox.payfast.co.za';
// Sandbox
//$pfHost = 'sandbox.payfast.co.za';
// Production
// $pfHost = 'www.payfast.co.za';

if ($enviroment == 'stage' || $enviroment == 'testing' || $enviroment == 'local') {
    $pfHost = 'sandbox.payfast.co.za';
    $passPhrase = "";
} else if ($enviroment == 'prod') {
    $pfHost = 'www.payfast.co.za';
    $passPhrase = "";
}

$pfData = array();
$output = "ITN Response Received\n\n";

//// Dump the submitted variables and calculate security signature
if ( !$pfError )
{
    $output .= "Posted Variables:\n"; // DEBUG

    // Strip any slashes in data
    foreach ( $_POST as $key => $val )
    {
        $pfData[$key] = stripslashes( $val );
        $output .= "$key = $val\n";
    }

    // Dump the submitted variables and calculate security signature
    foreach ( $pfData as $key => $val )
    {
        if ( $key != 'signature' )
        {
            $pfParamString .= $key . '=' . urlencode( $val ) . '&';
        }

    }

    // Remove the last '&' from the parameter string
    $pfParamString = substr( $pfParamString, 0, -1 );
    $pfTempParamString = $pfParamString;

    // If a passphrase has been set in the PayFast Settings, include it in the signature string.
    //$passPhrase = '6qM9lTEATRRNHVIpFcbB'; //You need to get this from a constant or stored in your website/database
    if ( !empty( $passPhrase ) )
    {
        $pfTempParamString .= '&passphrase=' . urlencode( $passPhrase );
    }
    $signature = md5( $pfTempParamString );

    $result = ( $_POST['signature'] == $signature );

    $output .= "\nSecurity Signature:\n"; // DEBUG
    $output .= "- posted     = " . $_POST['signature'] . "\n"; // DEBUG
    $output .= "- calculated = " . $signature . "\n"; // DEBUG
    $output .= "- result     = " . ( $result ? 'SUCCESS' : 'FAILURE' ) . "\n"; // DEBUG

    if( !$result ){
        $pfError =  true;
        $pfErrMsg = PF_ERR_INVALID_SIGNATURE;
    }
}

//// Verify source IP
if ( !$pfError )
{
    $validHosts = array(
        'www.payfast.co.za',
        'sandbox.payfast.co.za',
        'w1w.payfast.co.za',
        'w2w.payfast.co.za',
    );

    $validIps = array();

    foreach ( $validHosts as $pfHostname )
    {
        $ips = gethostbynamel( $pfHostname );

        if ( $ips !== false )
        {
            $validIps = array_merge( $validIps, $ips );
        }
    }

    // Remove duplicates
    $validIps = array_unique( $validIps );

    if ( !in_array( $_SERVER['REMOTE_ADDR'], $validIps ) )
    {
        $pfError = true;
        $pfErrMsg = PF_ERR_BAD_SOURCE_IP;
    }
}

//// Connect to server to validate data received
if ( !$pfError )
{
    // Use cURL (If it's available)
    if ( function_exists( 'curl_init' ) )
    {
        $output .= "\nUsing cURL\n"; // DEBUG

        // Create default cURL object
        $ch = curl_init();

        // Base settings
        $curlOpts = array(
            // Base options
            CURLOPT_USERAGENT => USER_AGENT, // Set user agent
            CURLOPT_RETURNTRANSFER => true, // Return output as string rather than outputting it
            CURLOPT_HEADER => false, // Don't include header in output
            CURLOPT_SSL_VERIFYHOST => 2,
            CURLOPT_SSL_VERIFYPEER => 1,

            // Standard settings
            CURLOPT_URL => 'https://' . $pfHost . '/eng/query/validate',
            CURLOPT_POST => true,
            CURLOPT_POSTFIELDS => $pfParamString,
        );
        curl_setopt_array( $ch, $curlOpts );

        // Execute CURL
        $res = curl_exec( $ch );
        curl_close( $ch );

        if ( $res === false )
        {
            $pfError = true;
            $pfErrMsg = PF_ERR_CURL_ERROR;
        }
    }
    // Use fsockopen
    else
    {
        $output .= "\nUsing fsockopen\n"; // DEBUG

        // Construct Header
        $header = "POST /eng/query/validate HTTP/1.0\r\n";
        $header .= "Host: " . $pfHost . "\r\n";
        $header .= "Content-Type: application/x-www-form-urlencoded\r\n";
        $header .= "Content-Length: " . strlen( $pfParamString ) . "\r\n\r\n";

        // Connect to server
        $socket = fsockopen( 'ssl://' . $pfHost, 443, $errno, $errstr, 10 );

        // Send command to server
        fputs( $socket, $header . $pfParamString );

        // Read the response from the server
        $res = '';
        $headerDone = false;

        while ( !feof( $socket ) )
        {
            $line = fgets( $socket, 1024 );

            // Check if we are finished reading the header yet
            if ( strcmp( $line, "\r\n" ) == 0 )
            {
                // read the header
                $headerDone = true;
            }
            // If header has been processed
            else if ( $headerDone )
            {
                // Read the main response
                $res .= $line;
            }
        }
    }
}

// Get data from server
if ( !$pfError )
{
    // Parse the returned data
    $lines = explode( "\n", $res );

    $output .= "\nValidate response from server:\n"; // DEBUG

    foreach ( $lines as $line ) // DEBUG
    {
        $output .= $line . "\n";
    }
    // DEBUG
}

// Interpret the response from server
if ( !$pfError )
{
    // Get the response from PayFast (VALID or INVALID)
    $result = trim( $lines[0] );

    $output .= "\nResult = " . $result; // DEBUG

    // If the transaction was valid
    if ( strcmp( $result, 'VALID' ) == 0 )
    {
        //SUCCESSFUL PAYMENT

        function send_email($attributes, $subject, $message)
        {
            $sendgrid_api_key = "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";
            $headers = array(
                "Authorization: Bearer $sendgrid_api_key",
                "Content-Type: application/json"
            );

            $data = array(
                "personalizations" => array(
                    array(
                        "to" => array(
                            array(
                                "email" => $attributes['email'],
                                "name" => $attributes['firstName']
                            )
                        )
                    )
                ),
                "from" => array(
                    "email" => "tmsapplications@remax-townandcountry.co.za"
                ),
                "subject" => $subject,
                "content" => array(
                    array(
                        "type" => "text/html",
                        "value" => $message
                    )
                )
            );

            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_URL => "https://api.sendgrid.com/v3/mail/send",
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "POST",
                CURLOPT_POSTFIELDS => json_encode($data),
                CURLOPT_HTTPHEADER => $headers,
            ));
            $response = curl_exec($curl);
            curl_close($curl);
            return $response;
        }
        
        /* Connect to a MySQL database using driver invocation */

        $userID = $_POST['custom_str4'];
        $applicantID = $_POST['custom_str8'];
        $propertyAgentID = $_POST['custom_str7'];
        $propertyReference = $_POST['custom_str5'];
        $m_payment_id = $_POST['m_payment_id'];

       
        if ($_POST['custom_str6']=='deposit') {
            $deposit = true;
        } else {
            $deposit = false;
        }

        if($_POST['custom_str1'] == 'deposit') {
            $deposit = true;
        } else {
            $deposit = false;
        }

        if ($deposit) {

            $totalAmount = ((float) $_POST['custom_str3']);
            $complexID = $_POST['custom_str2'];

            $sth = $db->prepare('INSERT INTO billingInfo (userID,transactionAmount,info, errorLog,dateTime) VALUES (:userID,:transactionAmount,:info,:errorLog, :dateTime);');
            $sth->execute(array(':userID' => $userID, ':transactionAmount' => $_POST['custom_str3'], ':info' => "success", ':errorLog' => "", ':dateTime' => date("Y-m-d H:i:s")));    

            $fetch_results = $db->prepare('SELECT * from applications WHERE userID = :id');
            $fetch_results->execute(array(':id' =>$userID));
            $unit = $fetch_results->fetchAll();

            $fetch_results = $db->prepare('SELECT * FROM fixedCharges WHERE isDeleted = :isDeleted');
            $fetch_results->execute(array(
                ':isDeleted' => 'n',
            ));
            $fixedChargeCosts = $fetch_results->fetchAll();

            $totalFixedCosts = 0;
            $fixed_charge_string = '';
            foreach ($fixedChargeCosts as $key => $charge) {
                $totalFixedCosts = $totalFixedCosts + ((float) $charge['cost']);
                $fixedcharges = $fixedcharges.' '.$charge['chargeName'].' R '.$charge['cost'];
                $fixed_charge_string = $fixed_charge_string. ' & '.$charge['chargeName'];
                $fixed_charges = $fixed_charges.' + '.$charge['chargeName'].' R '.$charge['cost'];
            }

            $fetch_results = $db->prepare('SELECT * FROM charges WHERE propertyReference = :propertyReference AND isDeleted = :isDeleted');
            $fetch_results->execute(array(
                ':propertyReference' => $unit[0]['propertyReference'],
                ':isDeleted' => 'n',
            ));
            $additionChargeCosts = $fetch_results->fetchAll();

            $totalAdditionalCosts = 0;
            $charge_string = '';
            foreach ($additionChargeCosts as $key => $charge) {
                $totalAdditionalCosts = $totalAdditionalCosts + ((float) $charge['cost']);
                $charges = $charges.' '.$charge['chargeName'].' R '.$charge['cost'];
                $charge_string = $charge_string. ' & '.$charge['chargeName'];
                $additional_charges = $additional_charges.' + '.$charge['chargeName'].' R '.$charge['cost'];
            }

            $sth = $db->prepare('INSERT INTO applicantdata (userID, dataType, dataLabel, data, dateCreated, dateUpdated) 
                                VALUES (:userID, :dataType, :dataLabel, :data, :dateCreated, :dateUpdated)');
            $sth->execute(array(':userID' =>$userID,
                            ':dataType' =>  "onlinePayment",
                            ':dataLabel' => "rentDeposit",
                            ':data' =>  $m_payment_id,
                            ':dateCreated' => date("Y-m-d H:i:s"),
                            ':dateUpdated' => date("Y-m-d H:i:s")
                        ));

            $sth = $db->prepare('INSERT INTO invoices (userID, recipient, description, status, invoiceTotal, paid, invoiceType, dateCreated) 
                                VALUES (:userID, :recipient, :description, :status, :invoiceTotal, :paid, :invoiceType, :dateCreated)');
            $sth->execute(array(':userID' =>$userID,
                            ':recipient' =>  "The Good People Data Company",
                            ':description' => "Deposit/Lease & 1st Month Rental Invoice".$charge_string.' & Online Payment Fee',
                            ':status' =>  "paid",
                            ':invoiceTotal' =>  $totalAmount,
                            ':paid' =>  $totalAmount,
                            ':invoiceType' =>  1,
                            ':dateCreated' => date("Y-m-d H:i:s")
                        ));
                        

            $fetch_results = $db->prepare('SELECT *  from userChecklist WHERE title = :title AND userID = :userID');
            $fetch_results->execute(array(':title' => 'rentDeposit', ':userID' =>$userID));
            $results = $fetch_results->fetchAll();

            if(count($results) > 0){
                $sth = $db->prepare('UPDATE userChecklist SET approvedBy = :approvedBy, isVerified=:isVerified, dateUpdated=:dateUpdated WHERE id = :id');
                $sth->execute(array(
                    ':approvedBy' => "onlinePayment",
                    ':isVerified' => "y",
                    ':dateUpdated' => date("Y-m-d H:i:s"),
                    ':id' =>$results[0]['id']
                ));
            } else {

                $sth = $db->prepare('INSERT INTO userChecklist (userID, approvedBy, title, isVerified, dateCreated, dateUpdated)
                                    VALUES (:userID, :approvedBy, :title, :isVerified, :dateCreated, :dateUpdated)');
                $sth->execute(array(':userID' =>$userID,
                    ':approvedBy' => "onlinePayment",
                    ':title' => "rentDeposit",
                    ':isVerified' => "y",
                    ':dateCreated' => date("Y-m-d H:i:s"),
                    ':dateUpdated' => date("Y-m-d H:i:s")
                ));
            }

            $sth = $db->prepare('UPDATE applications SET applicationStatus = :applicationStatus WHERE userID = :userID');
            $sth->execute(array(
                ':applicationStatus' => "a",
                ':userID' =>$userID
            ));

            $subject = 'Application Approval';

            $fetch_results = $db->prepare('SELECT * from applications WHERE userID = :id');
            $fetch_results->execute(array(':id' =>$userID));
            $results = $fetch_results->fetchAll();

            $applicationID = $results[0]['id'];
            $fetch_results = $db->prepare('SELECT md.*, ad.id AS applicantID, ad.firstName, ad.email, ap.userID, ap.complexID, ap.unitID, ap.propertyReference, ap.id AS applicationID FROM applications ap
                        LEFT JOIN rentalproperties rt ON ap.id = rt.applicationID
                        LEFT JOIN metaData md ON md.mainID = rt.id
                        LEFT JOIN applicantdetails ad ON ad.userID = ap.userID
                        WHERE ap.id = :id');
            $fetch_results->execute(array(':id' =>$applicationID));
            $applicationsData = $fetch_results->fetchAll();
                            
            $userdata = [];

            foreach($applicationsData as $row){

                $userdata[$row["dataLabel"]] = $row["data"];
                $userdata["email"] = $row["email"];
                $userdata["firstName"] = $row["firstName"];
            }

            $fetch_results = $db->prepare('SELECT * from paymentsettings');
            $fetch_results->execute();
            $paymentsettings = $fetch_results->fetchAll();

            // Connect to your SendGrid account
            $sendgrid = new SendGrid\SendGrid('keenangeorge', 'Charnte#1');

            // Make a message object
            $mail = new SendGrid\Mail();

            $message = file_get_contents('ui/emailTemplates/approval.php');
            //Replace the codetags with the message contents
            $replacements = array(
                '({email})' => $userdata['email'],
                '({firstName})' => $userdata['firstName'],
                '({support_email})' => 'support@remax.talentindx.co.za',
                '({login_url})' => "https://tms.remax-townandcountry.co.za/login",
                '({help_url})' => 'https://tms.remax-townandcountry.co.za',
                '({additionalCharges})' => "Additional Charges:",
                '({charges})' => $charges,
                '({bankName})' => $paymentsettings[0]['bankName'],
                '({accountType})' => $paymentsettings[0]['accountType'],
                '({bankAccount})' => $paymentsettings[0]['bankAccount'],
                '({branch})' => $paymentsettings[0]['branch']
            );

            $message = preg_replace( array_keys( $replacements ), array_values( $replacements ), $message );
            send_email($userdata, $subject, $message);

            ///////////////////////////////////////////////INVOICE/////////////////////////////////////////////////
            $message = file_get_contents("ui/invoiceTemplates/deposit.php");

            $fetch_results = $db->prepare('SELECT ad.id AS applicantID, ap.id AS applicationID, ad.email, ad.firstName AS tenant_name, ad.lastName AS tenant_surname, ad.address AS tenant_address,
             ad.phone AS tenant_phone, ad.totalIncome, uc.title, uc.dateCreated AS issue_date, inv.recipient AS company_name, inv.description AS invoice_description, 
             inv.paid AS amount_total, inv.id AS invoice_number, ap.unitID, ap.complexID ,
             ap.propertyReference
             FROM applicantdetails ad 
             INNER JOIN userChecklist uc ON ad.userID = uc.userID 
             AND uc.title = :title AND uc.isVerified = :isVerified 
             INNER JOIN invoices inv ON ad.userID = inv.userID AND inv.invoiceType = :invoiceType 
             INNER JOIN applications ap ON ad.userID = ap.userID 
             WHERE ad.userID = :userID 
             ORDER BY ad.id DESC');
            $fetch_results->execute(array(
                ':userID' => $userID,
                ':title' => 'rentDeposit',
                ':isVerified' => 'y',
                ':invoiceType' => 1
            ));
            $invoice = $fetch_results->fetchAll();

            $sth = $db->prepare('INSERT INTO applicantunitallocation (name, applicantID, propertyReference, dateCreated, dateUpdated) VALUE (:name, :applicantID, :propertyReference, :dateCreated, :dateUpdated)');
            $sth->execute(array(
                ':name' => $invoice[0]['tenant_name'],
                ':applicantID' => $invoice[0]['applicantID'],
                ':propertyReference' => $invoice[0]['propertyReference'],
                ':dateCreated' => date("Y-m-d H:i:s"),
                ':dateUpdated' => date("Y-m-d H:i:s")
            ));


            $fetch_results = $db->prepare('SELECT * FROM rentalproperties WHERE propertyReference = :propertyReference AND applicationID = :applicationID AND applicantID = :applicantID');
            $fetch_results->execute(array(
                ':propertyReference' => $invoice[0]['propertyReference'],
                ':applicationID' => $invoice[0]['applicationID'],
                ':applicantID' => $invoice[0]['applicantID'],
            ));
            $allocatedUnit = $fetch_results->fetchAll();

            $fetch_results = $db->prepare('SELECT * FROM rentTrench WHERE id = :trenchID');
            $fetch_results->execute(array(
                ':trenchID' => $allocatedUnit[0]['trenchID']
            ));
            $trench = $fetch_results->fetchAll();

            $attributes['email'] = $invoice[0]['email'];
            $attributes['tenant_name'] = $invoice[0]['tenant_name']." ". $invoice[0]['tenant_surname'];
            $attributes['tenant_address'] = $invoice[0]['tenant_address'];
            $attributes['tenant_phone'] = $invoice[0]['tenant_phone'];
            $attributes['invoice_description'] = $invoice[0]['invoice_description']. " Property Web Refeference ".$allocatedUnit[0]['propertyReference']. " Payment";
            $attributes['invoice_deposit_price'] = ((float) $allocatedUnit[0]['rentDeposit']);
            $lease = ((float) $allocatedUnit[0]['lease']);
            $attributes['invoice_lease_price'] = $lease;
            $rental = round($allocatedUnit[0]['unitRental'], 2);
            $transactionCharge = round((((float) $allocatedUnit[0]['rentDeposit']) + ((float) $allocatedUnit[0]['unitRental']) + $lease + ((float) $totalFixedCosts) + ((float) $totalAdditionalCosts)) * (((float) $paymentsettings[0]['transactionCharge'])/100), 2);
            $total = ((float) $allocatedUnit[0]['rentDeposit']) + ((float) $allocatedUnit[0]['unitRental']) + $lease + ((float) $totalFixedCosts) + ((float) $totalAdditionalCosts) + ((float) $transactionCharge);
            $attributes['amount_total'] = $total;
            $attributes['additional_charges'] = $fixed_charges.$additional_charges;
            $attributes['issue_date'] = $invoice[0]['issue_date'];
            $invoice_number = $invoice[0]['invoice_number'];
            $invoice_title = "Deposit & Lease Invoice";
            $subject = $invoice_title;

            $replacements = array(
                '({firstName})'             => $attributes['tenant_name'],
                "({company_logo})"          => "http://tms.remax-townandcountry.co.za/static/img/user/invoicelogo.jpg",
                "({company_name})"          => "Remax Real Estate",
                "({company_address})"       => "",
                "({company_city_zip_state})"=> " ● ",
                "({company_phone_fax})"     => "",
                "({company_email_web})"     => "https://www.remax-townandcountry.co.za ● https://www.remax-townandcountry.co.za",
                "({issue_date_label})"      => "Issue Date: ",
                "({issue_date})"            => $attributes['issue_date'],
                "({bill_to_label})"         => "Bill to",
                "({client_name})"           => $attributes['tenant_name'],
                "({client_address})"        => $attributes['tenant_address'],
                "({client_phone_fax})"      => $attributes['tenant_phone'],
                "({client_email})"          => $attributes['email'],
                "({invoice_title})"         => $invoice_title,
                "({invoice_number})"        => "#".$invoice_number,
                "({item_row_number_label})" => "",
                "({item_description_label})"=> "Description",
                "({item_price_label})"      => "Cost",
                "({item_line_total_label})" => "Linetotal",
                "({item_row_number})"       => 1,
                "({item_description})"      => $attributes['invoice_description'],
                "({item_price})"            => 'Deposit R'.$attributes['invoice_deposit_price'] . 'Rental R'.$rental.' + Lease R' . $attributes['invoice_lease_price']. $attributes['additional_charges']. " + Online Payment Fee R ".$transactionCharge,
                "({item_line_total})"       => "R ".$attributes['amount_total'],
                "({amount_total_label})"    => "Total",
                "({amount_total})"          => "R ".$attributes['amount_total'],
                "({amount_paid_label})"     => "Paid",
                "({amount_paid})"           => "R ".$attributes['amount_total'],
                "({terms_label})"           => "Terms & Notes",
                "({terms})"                 => $attributes['tenant_name'].", thank you very much. We really appreciate your business."
            );

            $message = preg_replace(array_keys($replacements), array_values($replacements), $message);

            // Add recipients and other message details
            // $mail->addTo($attributes['email'])->
            //     setFrom("support@remax.talentindx.co.za")->
            //     setSubject($subject)->
            //     setHtml($message);

            // $sendgrid->send($mail);

            send_email($attributes, $subject, $message);
            
        } else if(!$deposit) {

            $sth = $db->prepare('INSERT INTO billingInfo (userID,transactionAmount,info, errorLog,dateTime) VALUES (:userID,:transactionAmount,:info,:errorLog, :dateTime);');
            $sth->execute(array(':userID' => $userID, ':transactionAmount' => $_POST['custom_str2'], ':info' => "success", ':errorLog' => "", ':dateTime' => date("Y-m-d H:i:s")));
            
            $propertyAgentID = $rental_property[0]['agentID'];
            $sth = $db->prepare('SELECT * FROM rentalproperties WHERE propertyReference = :propertyReference AND deleted = :deleted');
            $sth->execute(array(
                ':propertyReference' => $propertyReference,
                ':deleted' => 'n'
            ));
            $rental_property = $sth->fetchAll();

            $sth = $db->prepare('INSERT INTO applications (userID, propertyReference, agentID, applicationFee, dateCreated, dateUpdated) 
                                VALUES(:userID, :propertyReference, agentID, :paid, :dateCreated, :dateUpdated)');
            $sth->execute(array(
                ':userID' =>$userID,
                ':propertyReference' =>$propertyReference,
                ':agentID' => $propertyAgentID,
                ':paid' => "unpaid",
                ':dateCreated' => date("Y-m-d H:i:s"),
                ':dateUpdated' => date("Y-m-d H:i:s")
            ));
            $applicationID = $db->lastInsertId();

            $sth = $db->prepare('UPDATE applicantdetails SET applicationID = :applicationID, dateUpdated = :dateUpdated 
            WHERE userID = :userID');
            $sth->execute(array(
                ':applicationID' => (int) $applicationID,
                ':dateUpdated' => date("Y-m-d H:i:s"),
                ':userID' => $userID,
            ));

            $sth = $db->prepare('SELECT * FROM applicantdetails WHERE userID = :userID');
            $sth->execute(array(
                ':userID' => $userID
            ));
            $user_details = $sth->fetchAll();

            $sth = $db->prepare('SELECT * FROM propertyagents WHERE id = :agentID AND isDeleted = :isDeleted');
            $sth->execute(array(
                ':agentID' => $propertyAgentID,
                ':isDeleted' => 'n'
            ));
            $property_agent = $sth->fetchAll();

            $sth = $db->prepare('UPDATE rentalproperties SET applicantID = :applicantID, agentFullName = :agentFullName, applicationID = :applicationID, dateUpdated = :dateUpdated 
            WHERE propertyReference = :propertyReference');
            $sth->execute(array(
                ':applicantID' => (int) $user_details[0]['id'],
                ':agentFullName' => $property_agent[0]['firstName']. " ".$property_agent[0]['lastName'],
                ':applicationID' => (int) $applicationID,
                ':dateUpdated' => date("Y-m-d H:i:s"),
                ':propertyReference' => $propertyReference,
            ));

            $sth = $db->prepare('INSERT INTO applicantdata (userID, dataType, dataLabel, data, dateCreated, dateUpdated) 
                                VALUES (:userID, :dataType, :dataLabel, :data, :dateCreated, :dateUpdated)');
            $sth->execute(array(
                ':userID' =>$userID,
                ':dataType' =>  "onlinePayment",
                ':dataLabel' => "proofOfPayment",
                ':data' =>  $m_payment_id,
                ':applicationID' => (int) $applicationID,
                ':dateCreated' => date("Y-m-d H:i:s"),
                ':dateUpdated' => date("Y-m-d H:i:s")
            ));

            $sth = $db->prepare('INSERT INTO userChecklist (userID, approvedBy, title, isVerified, dateCreated, dateUpdated)
                                VALUES (:userID, :approvedBy, :title, :isVerified, :dateCreated, :dateUpdated)');
            $sth->execute(array(':userID' =>$userID,
                ':approvedBy' => "onlinePayment",
                ':title' => "proofOfPayment",
                ':isVerified' => "y",
                ':applicationID' => (int) $applicationID,
                ':dateCreated' => date("Y-m-d H:i:s"),
                ':dateUpdated' => date("Y-m-d H:i:s")
            ));

            $sth = $db->prepare('INSERT INTO userChecklist (userID, approvedBy, title, isVerified, dateCreated, dateUpdated)
                                VALUES (:userID, :approvedBy, :title, :isVerified, :dateCreated, :dateUpdated)');
            $sth->execute(array(':userID' =>$userID,
                ':approvedBy' => "onlinePortal",
                ':title' => "email",
                ':isVerified' => "y",
                ':applicationID' => (int) $applicationID,
                ':dateCreated' => date("Y-m-d H:i:s"),
                ':dateUpdated' => date("Y-m-d H:i:s")
            ));

            $sth = $db->prepare('INSERT INTO userChecklist (userID, approvedBy, title, isVerified, dateCreated, dateUpdated)
                                VALUES (:userID, :approvedBy, :title, :isVerified, :dateCreated, :dateUpdated)');
            $sth->execute(array(':userID' =>$userID,
                ':approvedBy' => "onlinePortal",
                ':title' => "phone",
                ':isVerified' => "y",
                ':applicationID' => (int) $applicationID,
                ':dateCreated' => date("Y-m-d H:i:s"),
                ':dateUpdated' => date("Y-m-d H:i:s")
            ));

            //T&Cs
            $sth = $db->prepare('INSERT INTO applicantdata (userID, dataType, dataLabel, data, dateCreated, dateUpdated) 
            VALUES (:userID, :dataType, :dataLabel, :data, :dateCreated, :dateUpdated)');
            $sth->execute(array(
                ':userID' =>$userID,
                ':dataType' =>  "checkbox",
                ':dataLabel' => "creditCheckConsent",
                ':data' =>  "Yes",
                ':applicationID' => (int) $applicationID,
                ':dateCreated' => date("Y-m-d H:i:s"),
                ':dateUpdated' => date("Y-m-d H:i:s")
            ));

            $sth = $db->prepare('INSERT INTO applicantdata (userID, dataType, dataLabel, data, dateCreated, dateUpdated) 
            VALUES (:userID, :dataType, :dataLabel, :data, :dateCreated, :dateUpdated)');
            $sth->execute(array(
                ':userID' =>$userID,
                ':dataType' =>  "checkbox",
                ':dataLabel' => "referenceCheck",
                ':data' =>  "Yes",
                ':applicationID' => (int) $applicationID,
                ':dateCreated' => date("Y-m-d H:i:s"),
                ':dateUpdated' => date("Y-m-d H:i:s")
            ));

            //Notify Administration
            $fetch_results = $db->prepare("SELECT ap.id AS applicationID, cp.propertyReference as complexName, us.email, concat(ad.firstName, ' ', ad.lastName) AS firstName 
            FROM applications ap 
            LEFT JOIN rentalproperties cp ON ap.id = cp.applicationID 
            LEFT JOIN users us ON ap.userID = us.id 
            LEFT JOIN applicantdetails ad ON ap.userID = ad.userID
            WHERE ap.userID = :id AND ap.id = :applicationID;");
            $fetch_results->execute(array(':id' =>$userID, ':applicationID' => (int) $applicationID));
            $user_details = $fetch_results->fetchAll();
            
            $attributes = [];
            $attributes = $user_details[0];
            $message = file_get_contents('ui/emailTemplates/application.php');
            //Replace the codetags with the message contents
            $host_url = 'tms.remax-townandcountry.co.za';
            if($_SERVER['HTTP_HOST'] == "itnremaxstage.remax-townandcountry.co.za"){
                $host_url = 'tmsstage.remax-townandcountry.co.za';
            } else {
                $host_url = 'tms.remax-townandcountry.co.za';
            }

            $replacements = array(
                '({email})' => $attributes['email'],
                '({firstName})' => $attributes['firstName'],
                '({complexName})' => $attributes['complexName'],
                '({support_email})' => 'support@remax-townsandcountry.co.za',
                '({admin_url})' => 'https://'.$host_url.'/login'
            );
            $message = preg_replace( array_keys( $replacements ), array_values( $replacements ), $message );
           
            if(count($property_agent)>0){
                if($property_agent[0]['email']){
                    $send_to['firstName'] = $property_agent[0]['firstName']. " ".$property_agent[0]['lastName'];
                    $send_to['email'] = $property_agent[0]['email'];
                    $send_mail = send_email($send_to, "New Application Submission", $message);
                }
            }

            $sth = $db->prepare('SELECT * FROM applicationnotificationemails WHERE isDeleted = :isDeleted');
            $sth->execute(array(
                ':isDeleted' => 'n'
            ));
            $applicationnotificationemails = $sth->fetchAll();

            foreach ($applicationnotificationemails as $send_mail) {
                $send_to['firstName'] = $send_mail['firstName']. " " .$send_mail['lastName'];
                $send_to['email'] = $send_mail['email'];
                $send_mail = send_email($send_to, $subject, $message); 
            }
        }

    }
    // If the transaction was NOT valid
    else
    {
        // Log for investigation
        $response = 'fail';
        $pfError = true;
        $pfErrMsg = PF_ERR_INVALID_DATA;
        $dateTime = date('Y-m-d H:i:s', time());
        //UNSUCCESSFULL PAYMENT

        $error_log = "PF_ERR_INVALID_DATA";
        //ANY OTHER ERRO OCCURRED
        $id = $_POST['custom_str4'];
        // Write output to file // DEBUG
        $invoiceLog = 'id_' . $id . '_invoiceLog' . '_' . $dateTime.".txt";
        $sth = $db->prepare('INSERT INTO billingInfo (userID,transactionAmount,info, errorLog,dateTime) VALUES (:userID,:transactionAmount,:info,:errorLog, :dateTime);');
        $sth->execute(array(':userID' => $id, ':transactionAmount' => $_POST['custom_str3'], ':info' => $response, ':errorLog' =>$error_log, ':dateTime' => $dateTime));
    }
}

// If an error occurred
if ( $pfError )
{
    $response = 'fail';
    $output .= "\n\nAn error occurred!";
    $output .= "\nError = " . $pfErrMsg;

    $error_log = $pfErrMsg;
    //ANY OTHER ERRO OCCURRED
    $id = $_POST['custom_str4'];
    $dateTime = date('Y-m-d H:i:s', time());
    // Write output to file // DEBUG
    $invoiceLog = 'id_' . $id . '_invoiceLog' . '_' . $dateTime.".txt";
    $sth = $db->prepare('INSERT INTO billingInfo (userID,transactionAmount,info, errorLog,dateTime) VALUES (:userID,:transactionAmount,:info,:errorLog, :dateTime);');
    $sth->execute(array(':userID' => $id, ':transactionAmount' => $_POST['custom_str3'], ':info' => $response, ':errorLog' =>$error_log, ':dateTime' => $dateTime));
}