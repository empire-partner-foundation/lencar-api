<?php
date_default_timezone_set('Africa/Johannesburg'); //or change to whatever timezone you want
error_reporting(0);

// Kickstart the framework
$f3 = require 'lib/base.php';

$f3->set('DEBUG', 1);
if ((float) PCRE_VERSION < 8.0) {
    trigger_error('PCRE version is out of date');
}

// Load configuration
$f3->config('config.ini');

$access = Access::instance();

//$env = getenv('ENV');
$env = "local";

$env = $_SERVER['HTTP_HOST'];


$f3->set("SESSION.SERVER", $env);

$ip = gethostbyname($env);
$system_enviroment = 'prod';
//Live
$xdsUrl = "https://www.web.xds.co.za/xdsconnect/XDSConnectWS.asmx?WSDL";

if(strpos($env, 'lencar-api') > -1){
    //Local
    $db = new DB\SQL(
        'mysql:host=localhost;port=3306;dbname=remaxstagetenantmanagement',
        'root',
        '');
        // var_dump($db);
        // exit;
    
    $xdsUrl = "https://www.uat.xds.co.za/xdsconnect/XDSConnectWS.asmx?WSDL";
    $system_enviroment = 'local';
} else if(strpos($ip, '127.0.0.1') > -1){

    //PRODUCTION
    $xdsUrl = "https://www.web.xds.co.za/xdsconnect/XDSConnectWS.asmx?WSDL";
    $options = array(
                       PDO::MYSQL_ATTR_SSL_VERIFY_SERVER_CERT => false,
                       PDO::MYSQL_ATTR_SSL_KEY    =>'/etc/letsencrypt/live/api.tms-lencar.co.za/privkey.pem',
                       PDO::MYSQL_ATTR_SSL_CERT=>'/etc/letsencrypt/live/api.tms-lencar.co.za/fullchain.pem');
                       
    //    $db = new DB\SQL(
    //        'mysql:host=127.0.0.1;port=3306;dbname=remaxtenantmanagement',
    //        'admin',
    //        'NJnorxms99@VZIlr2ukrG',$options
    //        );

    $db = new DB\SQL(
        'mysql:host=127.0.0.1;port=3306;dbname=remaxstagetenantmanagement',
        'root',
        'pVerFHZjZaQ2YE',$options
        );
        $system_enviroment = 'prod';
} else if(strpos($env, 'remaxstage') > -1){

    //STAGE
    $xdsUrl = "https://www.uat.xds.co.za/xdsconnect/XDSConnectWS.asmx?WSDL";
    
    $options = array(
                       PDO::MYSQL_ATTR_SSL_VERIFY_SERVER_CERT => false,
                       PDO::MYSQL_ATTR_SSL_KEY    =>'/var/www/certs/client-key.pem',
                       PDO::MYSQL_ATTR_SSL_CERT=>'/var/www/certs/client-cert.pem',
                       PDO::MYSQL_ATTR_SSL_CA    =>'/var/www/certs/ca.pem');
                       
       $db = new DB\SQL(
           'mysql:host=192.168.20.188;port=3306;dbname=remaxstagetenantmanagement',
           'root',
           'NJnorxms99@VZIlr2ukrG',$options
           );
           $system_enviroment = 'stage';
} else if(strpos($env, 'tms.remax-townandcountry.co.za') > -1){

    //Test
    $xdsUrl = "https://www.web.xds.co.za/xdsconnect/XDSConnectWS.asmx?WSDL";
    
    $options = array(
                       PDO::MYSQL_ATTR_SSL_VERIFY_SERVER_CERT => false,
                       PDO::MYSQL_ATTR_SSL_KEY    =>'/var/www/certs/client-key.pem',
                       PDO::MYSQL_ATTR_SSL_CERT=>'/var/www/certs/client-cert.pem',
                       PDO::MYSQL_ATTR_SSL_CA    =>'/var/www/certs/ca.pem');
                       
    //    $db = new DB\SQL(
    //        'mysql:host=192.168.20.188;port=3306;dbname=remaxtenantmanagement',
    //        'root',
    //        'NJnorxms99@VZIlr2ukrG',$options
    //        );

    $db = new DB\SQL(
        'mysql:host=192.168.20.188;port=3306;dbname=remaxtenantmanagement',
        'root',
        'NJnorxms99@VZIlr2ukrG',$options
        );
        $system_enviroment = 'prod';
} else {
    $xdsUrl = "https://www.web.xds.co.za/xdsconnect/XDSConnectWS.asmx?WSDL";
    $options = array(
                    PDO::MYSQL_ATTR_SSL_VERIFY_SERVER_CERT => true,
                    PDO::MYSQL_ATTR_SSL_KEY    =>'/etc/letsencrypt/live/api.tms-lencar.co.za/privkey.pem',
                    PDO::MYSQL_ATTR_SSL_CERT=>'/etc/letsencrypt/live/api.tms-lencar.co.za/fullchain.pem');
                    
    // $db = new DB\SQL(
    //     'mysql:host=192.168.20.188;port=3306;dbname=remaxtenantmanagement',
    //     'root',
    //     'NJnorxms99@VZIlr2ukrG',$options
    //     );

    $db = new DB\SQL(
        'mysql:host=api.tms-lencar.co.za;port=3306;dbname=remaxstagetenantmanagement',
        'root',
        'pVerFHZjZaQ2YE',$options
        );
        $system_enviroment = 'prod';
} 

$f3->set('AUTOLOAD', 'controller/');

include './routes.php';

$f3->run();
