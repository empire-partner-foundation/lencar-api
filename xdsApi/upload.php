<?php

$folder = "user_" . $_POST["userID"];
$target_dir = "uploads/$folder/";
if (!is_dir("$target_dir")) {
    mkdir("$target_dir", 0777, true);
    chmod("$target_dir", 0777);
}

foreach ($_FILES as $formFieldName => $value) {

    $target_file = $target_dir . basename($_FILES["$formFieldName"]["name"]);
    $uploadOk = true;
    $fileType = strtolower(pathinfo($target_file, PATHINFO_EXTENSION));
    // Check if image file is a actual image or fake image
    // if (isset($_POST["submit"])) {
    //     $check = getimagesize($_FILES[$formFieldName]["tmp_name"]);
    //     if ($check !== false) {
    //         echo "File is an image - " . $check["mime"] . ".";
    //         $uploadOk = 1;
    //     } else {
    //         echo "File is not an image.";
    //         $uploadOk = 0;
    //     }
    // }
    // Check if file already exists
    if (file_exists($target_file)) {
        //echo "Error, file already exists.";
        //$uploadOk = 0;
    }
    // Check file size
    if ($_FILES["$formFieldName"]["size"] > 500000) {
        echo "Error, your file is too large.";
        $uploadOk = false;
        exit;
    }
    // Allow certain file formats
    if ($fileType != "jpg" && $fileType != "png" && $fileType != "jpeg"
        && $fileType != "pdf") {
            echo "Error, only JPG, JPEG, PNG & PDF files are allowed.";
        $uploadOk = false;
        exit;
    }
    // Check if $uploadOk is set to 0 by an error
    if (!$uploadOk) {
        echo "Error, your file was not uploaded.";
        exit;
        // if everything is ok, try to upload file
        header("Location: http://zelriprop.talentindx.co.za/user/profile/#profile-documents");

    } else {
        if (move_uploaded_file($_FILES["$formFieldName"]["tmp_name"], $target_file)) {
            //$target_file;
            $uploadOk = file_get_contents("http://zelriprop.talentindx.co.za/user/profile/upload/$target_file/$formFieldName");
            echo $uploadOk;
        } else {
            echo "Error, there was an error uploading your file.";
        }
    }
}
if (!$uploadOk) 
    echo "Error, there was an error uploading your file.";

header("Location: http://zelriprop.talentindx.co.za/user/profile/#profile-documents");

