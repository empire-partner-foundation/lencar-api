<?php

class Emailer {

    private $smtp;
    ## Mail Server :
    private $host = 'smtp.gmail.com';                   // mail.server
    private $port = '465';                              // 465
    private $scheme = 'ssl';                            // ssl
    private $user = '@email';                           // test@email.net
    private $password = 'pass';                    // password

    public function __construct($recipient, $subject) {

        $this->smtp = new SMTP($this->host, $this->port, $this->scheme, $this->user, $this->password);

        echo $this->smtp->set('Errors-to', '<info@smartviewtechnology.co.za>');
        echo $this->smtp->set('To', '"User" <'.$recipient.'>');
        echo $this->smtp->set('From', '"Smart Housing" <info@zelriprop.talentindx.co.za>');
        echo $this->smtp->set('Subject', $subject);
    }

    public function send($message)
    {
        try{
            $email = $this->smtp->send($message);

            return $email;
        }catch(Exception $exception){
            throw new Error('Error sending email');
        }
    }
}
