<?php
header('Access-Control-Allow-Origin: *');

header('Access-Control-Allow-Methods: GET, POST');

header("Access-Control-Allow-Headers: X-Requested-With");
header('Content-Type: application/json');
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
use SoapClient;


class XdsController extends AppController
{
    public function getConnectTicket($f3, $params){
        header('Content-Type: application/json');

        //echo file_get_contents('php://input');
        $data = json_decode(file_get_contents('php://input'), true);

        global $db, $xdsUrl;

        if($f3->get("POST") || isset($data['email'])){
            if(isset($data['email'])) $attributes = $data;
            elseif($f3->get("POST.email")) $attributes = $f3->get("POST"); 
            $user = $this->authenticate($attributes);
            if (!$user) {
                echo json_encode(array('status' => 'error', 'message' => 'Email Acount Not Found. Please Contact Your Admin'));
            }
            else{
                global $db;
                $date = date("Y-m-d");
                $query = "SELECT * FROM xdsTickets WHERE userID=:userID AND dateCreated = :dateCreated";
                $vars = array(
                    ':userID' => $user[0]['id'],
                    ':dateCreated' => $date,
                );
                $xdsTickets = $db->exec($query, $vars);

                if(sizeof($xdsTickets) > 0){
                    if($xdsTickets[0]['usagesRemaining'] > 0){
                        $connectTicket = $xdsTickets[0]['connectTicket'];
                        echo json_encode(array('status' => 'success', 'message' => $connectTicket));

                    }
                    else{
                        echo json_encode(array('status' => 'error', 'message' => 'You have reached daily limit for token usage!'));
                    }
                }else{
                    $strUser = "Smitk_uat";
                    $strPwd = "Sm!t0805";
                    $client = new \SoapClient ($xdsUrl); 
                    $parameters = array ("strUser"=>$strUser, "strPwd"=>$strPwd); 
                    $configQry = $client->Login ($parameters);
    
                    $connectTicket = $configQry->LoginResult;

                    $query = "INSERT INTO xdsTickets (userID, connectTicket, usagesRemaining, dateCreated)
                                VALUES (:userID, :connectTicket, :usagesRemaining, NOW())";
                    $vars = array(
                        ':userID' =>  $user[0]['id'],
                        ':connectTicket' => $connectTicket,
                        ':usagesRemaining' => 50,
                    );
                    $xdsTickets = $db->exec($query, $vars);

                    echo json_encode(array('status' => 'success', 'message' => $connectTicket));

                }

            }

        }

        
    }

    private function authenticate($attributes) {

        $user = $this->getRepository('users')->getByAttribute('email', $attributes['email']);
        //$password = hash('sha256', $attributes['password']);
        
        if ($user) {

            return $user;
        }

        return false;
    }

    public function getCreditReportPdf($f3, $params){
        header('Content-Type: application/json');

        global $db, $xdsUrl;

        $data = json_decode(file_get_contents('php://input'), true);


        if($f3->get("POST") || isset($data['connectTicket'])){

            

            if(isset($data['connectTicket'])){
                $IdNumber = $data['IdNumber'];
                $ConnectTicket = $data['connectTicket'];
            }
            else{
                $IdNumber = $f3->get("POST.IdNumber");
                $ConnectTicket = $f3->get("POST.connectTicket");
            }

            $date = date("Y-m-d");
            $query = "SELECT * FROM xdsTickets WHERE connectTicket=:connectTicket AND dateCreated = :dateCreated";
            $vars = array(
                ':connectTicket' => $ConnectTicket,
                ':dateCreated' => $date,
            );
            $xdsTickets = $db->exec($query, $vars);

            if(sizeof($xdsTickets) > 0){
                if($xdsTickets[0]['usagesRemaining'] > 0){

                    $client = new \SoapClient ($xdsUrl);
                    $parameters = array (
                        "ConnectTicket"=>$ConnectTicket,"IdNumber"=>$IdNumber, "PassportNo"=>"");
                    $ConnectGetConsumerCreditReportBinary = $client->ConnectGetConsumerCreditReportBinary($parameters);
                    $pdfString = $ConnectGetConsumerCreditReportBinary->ConnectGetConsumerCreditReportBinaryResult;
                    //echo" ConnectGetConsumerCreditReportBinary";
                    $xdsResponse = base64_encode($pdfString);

                    $query = "INSERT INTO creditReports (userID, ticketID, idNumber, xdsResponse, dateCreated)
                                VALUES (:userID, :ticketID, :idNumber, :xdsResponse, NOW())";
                    $vars = array(
                        ':userID' =>  $xdsTickets[0]['userID'],
                        ':ticketID' => $xdsTickets[0]['id'],
                        ':idNumber' => $IdNumber,
                        ':xdsResponse' => $xdsResponse,
                    );
                    $creditReports = $db->exec($query, $vars);

                    
                    $query = "UPDATE xdsTickets SET usagesRemaining = usagesRemaining - 1 WHERE id = :id";
                    $vars = array(
                        ':id' =>  $xdsTickets[0]['id'],
                    );
                    $xdsTickets = $db->exec($query, $vars);

                    echo json_encode(array('status' => 'success', 'message' => $xdsResponse));

                }
                else{
                    echo json_encode(array('status' => 'error', 'message' => 'You have reached daily limit for token usage!'));
                }
            }else{

                echo json_encode(array('status' => 'error', 'message' => 'Invalid token used!'));
                
            }
        }

         
    }

}
