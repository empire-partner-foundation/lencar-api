<?php

require_once 'AppController.php';
require_once 'passwordController.php';

class AuthController extends AppController
{
    public function loginPage($f3)
    {
        if ($f3->get('SESSION.USER.id')) {
            $f3->reroute('/user/dashboard');
        } else {
            $f3->set('content', 'auth/login.htm');
            echo View::instance()->render('auth_template.htm');
        }
    }

    public function loginPost($f3)
    {
        try {
            $attributes = $f3->get('POST');
            $user = $this->authenticate($attributes);
            $f3->set('SESSION.count', 1);

            if (!$user) {
                $f3->set('SESSION.message', array('msg' => 'Incorrect password/email.', 'alert' => 'danger'));
                $f3->reroute('/login');
            } elseif ($user[0]['isVerified'] == '0') {
                $f3->set('SESSION.message', array('msg' => 'Your email has not been verified! Please check your email and activate.', 'alert' => 'danger'));
                $f3->reroute('/login');
            } elseif ($user[0]['userState'] == '0') {
                $f3->set('SESSION.message', array('msg' => 'Your account is currently disabled.', 'alert' => 'danger'));
                $f3->reroute('/login');
            }

            Auth::createSession($f3, $user[0]);
            $f3->set('SESSION.login', 'logged');
            if($user[0]['pmLevel'] == 2) $f3->reroute('/user/dashboard');
            //else if($user[0]['pmLevel'] == 2 && $user[0]['isNew'] == 1) $f3->reroute('/user/dashboard');
            else if($user[0]['pmLevel'] == 1) $f3->reroute('/admin');
        } catch (Exception $exception) {
            return $exception->getMessage();
        }
    }

    private function authenticate($attributes) {

        $user = $this->getRepository('users')->getByAttribute('email', $attributes['email']);
        $password = hash('sha256', $attributes['password']);
        
        if ($user[0]['password'] === $password) {

            return $user;
        }

        return false;
    }

    public function registerPage($f3)
    {
        if ($f3->get('SESSION.USER.id')) {
            $f3->reroute('/user/dashboard');
        } else {
            $f3->set('content', 'auth/register.htm');
            echo View::instance()->render('auth_template.htm');
        }

    }

    public function registerPost($f3)
    {

        try {
            $attributes = $f3->get('POST');

            //Clear Past Error Messages
            $f3->set('SESSION.emailError', null);
            $f3->set('SESSION.agreementError', null);
            $f3->set('SESSION.phoneError', null);
            $f3->set('SESSION.firstnameError', null);
            $f3->set('SESSION.lastnameError', null);
            $f3->set('SESSION.passwordError', null);
            $f3->set('SESSION.message', array('error' => null, 'alert' => 'danger'));

            $validate = true;
            $f3->set('SESSION.count', 1);
            $f3->set('SESSION.email', $attributes['email']);
            $f3->set('SESSION.firstName', $attributes['firstName']);
            $f3->set('SESSION.lastName', $attributes['lastName']);
            $f3->set('SESSION.phone', $attributes['phone']);

            if ($attributes['agreement'] != "1") {
                $f3->set('SESSION.agreementError', 'Agree to our terms to continue');
                $validate = false;
            }

            if ($attributes['email'] == "") {
                $f3->set('SESSION.emailError', 'Email is required');
                $validate = false;
            }
            $user = $this->getRepository('users')->getByAttribute('email', $attributes['email']);
            if ($user) {
                $f3->set('SESSION.emailError', 'An account with that email already exists.');
                $validate = false;
            }

            if ($attributes['phone'] == "") {
                $f3->set('SESSION.phoneError', 'Phone number is required.');
                $validate = false;
            }

            if ($attributes['firstName'] == "") {
                $f3->set('SESSION.firstnameError', 'First Name is required.');
                $validate = false;
            }

            if ($attributes['lastName'] == "") {
                $f3->set('SESSION.lastnameError', 'Last Name is required.');
                $validate = false;
            }

            if ($attributes['password'] != $attributes['password_confirm']) {
                $f3->set('SESSION.passwordError', 'Passwords do not match');
                $validate = false;

            }

            if ($validate) {
                $verificationCode = rand();
                $attributes['verificationCode'] = $verificationCode;

                $this->createAccount($f3, $attributes);

                $subject = "Welcome to Tenant Onboarding";

                $this->dispatchEmail($attributes, $subject, "welcome");
                $f3->set('SESSION.message', array('msg' => 'Please check your email to verify your account.', 'alert' => 'success'));
                $f3->reroute('/login');

            } else {
                $f3->set('SESSION.message', array('error' => 'Please fix the following errors.', 'alert' => 'danger'));
                $f3->reroute('/register');
            }

        } catch (Exception $exception) {
            return $exception->getMessage();
        }

    }

    public function createAccount($f3, $attributes)
    {
        global $db;

        $password = $password = hash('sha256', $f3->get('POST.password')); //create_hash(trim($attributes['password']));
        

        $query = "INSERT INTO `users` (`email`, `pmLevel`, `userType`,`userState`,`isVerified`,`password`,`verificationCode`,`firstName`,`lastName`,`phone`,`dateCreated`,`dateUpdated`)
        VALUES (:email, :pmLevel, :userType, :userState, :isVerified, :password, :verificationCode, :firstName, :lastName, :phone, :dateCreated, :dateUpdated)";
        $vars = array(
            ':email' => $attributes['email'],
            ':pmLevel'=> 2,
            ':userType' => 2,
            ':userState' => 1,
            ':isVerified' => 0,
            ':password' => $password,
            ':verificationCode' => $attributes['verificationCode'],
            ':firstName' => $attributes['firstName'],
            ':lastName' => $attributes['lastName'],
            ':phone' => $attributes['phone'],
            ':dateCreated' => date('Y-m-d H:i:s'),
            ':dateUpdated' => date('Y-m-d H:i:s'),
        );
        $results = $db->exec($query, $vars);
        $insertedID = $db->lastInsertId();

        $user = $this->getRepository('applicantdetails')->getByAttribute('userID', $insertedID);
        if ($user) {
            //$f3->set('SESSION.emailError', 'An account with that email already exists.');
            //$validate = false;
        }
        else{
            $sql = "INSERT INTO `applicantdetails` (`email`,`phone`,`altPhone`,`firstName`,`lastName`,`userID`,`dateCreated`,`dateUpdated`)
            VALUES (:email, :phone, :altPhone, :firstName, :lastName, :userID, :dateCreated, :dateUpdated)";
            $vars1 = array(
                ':email' => $attributes['email'],
                ':phone' => $attributes['phone'],
                ':altPhone' => $attributes['phone'],
                ':userID' => $insertedID,
                ':firstName' => $attributes['firstName'],
                ':lastName' => $attributes['lastName'],
                ':dateCreated' => date('Y-m-d H:i:s'),
                ':dateUpdated' => date('Y-m-d H:i:s'),
            );
            $results = $db->exec($sql, $vars1);
        }

        

    }

    public function verifyAccount($f3, $params, $attributes)
    {
        global $db;

        $sql = "SELECT email FROM users where email = :email AND verificationCode = :verificationCode";
        $vars = array(
            ':email' => $params['email'],
            ':verificationCode' => $attributes['verificationCode'],
        );
        $results = $db->exec($sql, $vars);

        if (count($results) > 0) {
            $sql = "UPDATE users SET isVerified = :isVerified where email = :email AND verificationCode = :verificationCode";
            $vars = array(
                ':email' => $params['email'],
                ':isVerified' => 1,
                ':verificationCode' => $attributes['verificationCode'],
            );
            $results = $db->exec($sql, $vars);
            $f3->set('SESSION.count', 1);
            $f3->set('SESSION.message', array('msg' => 'Account Verified, Please Log In.', 'alert' => 'success'));
            $f3->reroute("/login");

        } else {
            $f3->set('SESSION.count', 1);
            $f3->set('SESSION.message', array('msg' => 'Incorrect Verification Code', 'alert' => 'danger'));
            $f3->reroute("/verify/" . $params['email']);
        }

    }


    public function activate($f3, $params) {
        try {

            $code = $params['code'];
            $f3->set('POST.isVerified', 1);
            $attributes = $f3->get('POST');
            $definition = $this->getDefinition($attributes);

            $user = $this->getRepository('users')->updateRecord(array('verificationCode =?', $code), $definition);

            if ($user) {
                $f3->set('SESSION.count', 1);
                $f3->set('SESSION.message', array('msg' => 'Your profile has been activated successfully! Please log in to your account', 'alert' => 'info'));
            } else {
                $f3->set('SESSION.count', 1);
                $f3->set('SESSION.message', array('msg' => 'Your profile has been not been activated successfully! Please contact support', 'alert' => 'danger'));
            }

            $f3->reroute("/login");
        } catch (Exception $exception) {
            echo json_encode($exception->getMessage());
        }
    }

    public function verifyPage($f3, $params)
    {
        try {
            if ($f3->get('SESSION.USER.id')) {
                $f3->reroute('/user/dashboard');
            }
            else{
                $attributes = $f3->get('POST');
                $this->verifyAccount($f3, $params, $attributes);
                $f3->set('email', $params['email']);
                $f3->set('content', 'auth/verify.htm');
                echo View::instance()->render('auth_template.htm');
            }
            
        } catch (Exception $exception) {
            return $exception->getMessage();
        }
    }

    public function verifyPost($f3, $params)
    {
        try {
            $attributes = $f3->get('POST');
            $this->verifyAccount($f3, $params, $attributes);
        } catch (Exception $exception) {
            return $exception->getMessage();
        }
    }

    public function passwordResetPage($f3)
    {
        if ($f3->get('SESSION.USER.id')) {
            $f3->reroute('/user/dashboard');
        } else {
            $f3->set('email', $params['email']);
            $f3->set('content', 'auth/resetPassword.htm');
            echo View::instance()->render('auth_template.htm');
        }
    }

    public function passwordResetPost($f3)
    {
        global $db;
        try {
        
            $attributes = $f3->get('POST');
            $f3->set('SESSION.count', 1);

            $user = $this->getRepository('users')->getByAttribute('email', $attributes['email']);
            if (!$user) {
                $f3->set('SESSION.emailError', 'An account with that email already exists.');
                $f3->reroute('/login');
            }

            $newPassword = PasswordGenerator::generate();

            $attributes['firstName'] = $user[0]['firstName'];
            $attributes['password'] =  $newPassword;

            $password = hash('sha256', $newPassword); //create_hash(trim($attributes['password']));

            $sql = "UPDATE users SET password = :password where email = :email";
            $vars = array(
                ':email' => $attributes['email'],
                ':password' => $password,
            );
            $results = $db->exec($sql, $vars);

            

            $subject = "Tenant Onboarding";

            $this->dispatchEmail($attributes, $subject, "passwordReset");

            $f3->set('SESSION.message', array('msg' => 'Please check your email for your new password.', 'alert' => 'success'));
            $f3->reroute('/login');
        } catch (Exception $exception) {
            return $exception->getMessage();
        }
        $f3->reroute('/login');
    }

    public function passwordChangePage($f3, $params)
    {
        if ($f3->get('SESSION.USER.id')) {
            $f3->reroute('/user/dashboard');
        } else {
            $f3->set('email', $params['email']);
            $f3->set('content', 'auth/changePassword.htm');
            echo View::instance()->render('auth_template.htm');
        }
    }

    public function passwordChangePost($f3, $params)
    {
        try {
            $attributes = $f3->get('POST');
            $this->verifyAccount($f3, $params, $attributes);
        } catch (Exception $exception) {
            return $exception->getMessage();
        }
    }

    public function logout($f3)
    {
        Auth::destroySession($f3);
        $f3->reroute("/login");
    }

}
