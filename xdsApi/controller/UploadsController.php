<?php

class UploadsController {

    
    /**
     * Cron Job upload method fail-safe update
     */
    function cron_job() {
        //Create server upload environment
        ini_set('memory_limit','512M');
        ini_set('max_execution_time', 300); //300 seconds = 5 minutes
        header("Content-Type: text/plain");
        echo "BEGIN SELF-UPLOAD: ".date("d-m-Y h:i a")."\n";
        
        //Read files in the folder
          $db= new PDO(
        'mysql:host=localhost;port=3306;dbname=unifiedb_ackermans-tembisa',
        'unifiedb_pcount',
        'golive3000');
    
        $dataColumns = [];
        $dataRows = [];
        $file = '';
        if ($handle = opendir("../.ump_platform/raw_data/ackermans-tembisa/People_counting_raw")) {
        //echo "Directory handle: $handle\n";
        //echo "Entries:\n";
    
        // This is the correct way to loop over the directory. 
            while (false !== ($entry = readdir($handle))) {
                $temp = explode(".",$entry);
        		if(isset($temp[1]))$ext = $temp[1];
        		if($ext == "csv") {
        			//echo "$entry<br>";
        				
        		$file = $entry;
        
        		$row = 1;
        		$flag = true;
        		$assetId=0;
        		$server = 1;
        		$csvDate = '';
        		if (($csvhandle = fopen("../.ump_platform/raw_data/ackermans-tembisa/People_counting_raw/$file", "r")) !== FALSE) {
        			while (($tempColumns = fgetcsv($csvhandle, 1000, ",")) !== FALSE) {
        				if($flag) { $flag = false; continue; }
        			
        			    if ($tempColumns[0] != ''){
                            //Assign row for IN Counter
                            $dataColumns = array();
                            if (count($tempColumns) == 7) {
                                $csvDate = date('Y').'-'.$tempDate[0].'-'.$tempDate[1];
                                //Get date
                                $tempDate = explode('.', $tempColumns[0]);
                                $dataColumns['cam_date'] = date('Y').'-'.$tempDate[0].'-'.$tempDate[1];
                                $dataColumns['cam_number'] = $tempColumns[3];
                                $dataColumns['cam_counter'] = 'IN';
                                $dataColumns['cam_result'] = $tempColumns[4];
                                //Get time
                                $tempTime = explode("-", $tempColumns[1]);
                                $dataColumns['cam_start_time'] = $tempTime[0];
                                $dataColumns['cam_end_time'] = $tempTime[1];
                                $dataColumns['cam_server'] = $server;
                                $dataColumns['ast_id'] = $assetId;
                                array_push($dataRows, $dataColumns);
                            }
                            //Add column to row
                            
                            //Assign row for OUT Counter
                            $dataColumns = array();
                            if (count($tempColumns) == 7) {
                                //Get date
                                $tempDate = explode('.', $tempColumns[0]);
                                $dataColumns['cam_date'] = date('Y').'-'.$tempDate[0].'-'.$tempDate[1];
                                $dataColumns['cam_number'] = $tempColumns[3];
                                $dataColumns['cam_counter'] = 'OUT';
                                $dataColumns['cam_result'] = $tempColumns[5];
                                //Get time
                                $tempTime = explode("-", $tempColumns[1]);
                                $dataColumns['cam_start_time'] = $tempTime[0];
                                $dataColumns['cam_end_time'] = $tempTime[1];
                                $dataColumns['cam_server'] = $server;
                                $dataColumns['ast_id'] = $assetId;
                                array_push($dataRows, $dataColumns);
                            }
                        
    			        }
            
        			}
        			fclose($csvhandle);
        		}
        		
        		if(!is_dir("../.ump_platform/raw_data/ackermans-tembisa/People_counting_raw/csv_archive")) {
        			mkdir("../.ump_platform/raw_data/ackermans-tembisa/People_counting_raw/csv_archive", 0777, true);
        			chmod("../.ump_platform/raw_data/ackermans-tembisa/People_counting_raw/csv_archive", 0777); 
        		}
        		//unlink("csv_uploads/".$file);
        		rename("../.ump_platform/raw_data/ackermans-tembisa/People_counting_raw/$file","../.ump_platform/raw_data/ackermans-tembisa/People_counting_raw/csv_archive/$file");
        		
        		$date = date("Y-m-d");
        		$sql = $db->prepare("DELETE FROM `pcg_cameras` WHERE `cam_date` = :date");
        		$sql->bindValue(":date",$csvDate, PDO::PARAM_STR);
        		$sql->execute();
        	
        		$sql = $db->prepare("INSERT INTO `pcg_cameras`(`cam_date`, `cam_number`, `cam_counter`, `cam_result`, `cam_start_time`, `cam_end_time`, `cam_server`, `ast_id`) 
        		                    VALUES (:cam_date,:cam_number,:cam_counter,:cam_result,:start_time,:end_time,:cam_server,:ast_id)");
        
        		foreach ($dataRows as $key => $value) {
        			
        			$sql->bindValue(":cam_date",$value['cam_date'], PDO::PARAM_STR);
        			$sql->bindValue(":cam_number",$value["cam_number"], PDO::PARAM_STR);
                    $sql->bindValue(":cam_counter",$value["cam_counter"], PDO::PARAM_STR);
        			$sql->bindValue(":cam_result",$value["cam_result"], PDO::PARAM_STR);
        			$sql->bindValue(":start_time", $value["cam_start_time"], PDO::PARAM_STR);
        			$sql->bindValue(":end_time", $value["cam_end_time"], PDO::PARAM_STR);
        			$sql->bindValue(":cam_server", $value["cam_server"], PDO::PARAM_STR);
        			$sql->bindValue(":ast_id", $value["ast_id"], PDO::PARAM_STR);
        			$sql->execute();
        		}
        		
        		//echo "success";
        		}
            }
        
        
            closedir($handle);
        }
        echo "success";
        exit;
    }
    
    /*
    *SAB cron
    */
    
    function sab_cron() {
        //Create server upload environment
        ini_set('memory_limit','512M');
        ini_set('max_execution_time', 300); //300 seconds = 5 minutes
        header("Content-Type: text/plain");
        echo "BEGIN SELF-UPLOAD: ".date("d-m-Y h:i a")."\n";
        
        //Read files in the folder
          $db= new PDO(
            'mysql:host=localhost;port=3306;dbname=unifiedb_sab',
            'unifiedb_sab',
            'golive2018');
    
        $dataColumns = [];
        $dataRows = [];
        $file = '';
        if ($handle = opendir("../.ump_platform/raw_data/sab/People_counting_raw")) {
        //echo "Directory handle: $handle\n";
        //echo "Entries:\n";
    
        // This is the correct way to loop over the directory. 
            while (false !== ($entry = readdir($handle))) {
                $temp = explode(".",$entry);
        		if(isset($temp[1]))$ext = $temp[1];
        		if($ext == "csv") {
        			//echo "$entry<br>";
        				
        		$file = $entry;
        
        		$row = 1;
        		$flag = true;
        		$assetId=0;
        		$server = 1;
        		$csvDate = '';
        		if (($csvhandle = fopen("../.ump_platform/raw_data/sab/People_counting_raw/$file", "r")) !== FALSE) {
        			while (($tempColumns = fgetcsv($csvhandle, 1000, ",")) !== FALSE) {
        				if($flag) { $flag = false; continue; }
        			
        			    if ($tempColumns[0] != ''){
                            //Assign row for IN Counter
                            $dataColumns = array();
                            if (count($tempColumns) == 7) {
                                
                                //Get date
                                $tempDate = explode('.', $tempColumns[0]);
                                $csvDate = $tempColumns[0];
                                
                                $dataColumns['cam_date'] = $tempColumns[0];
                                $dataColumns['cam_number'] = $tempColumns[1];
                                $dataColumns['cam_counter'] = $tempColumns[2];
                                $dataColumns['cam_result'] = $tempColumns[3];
                                //Get time
                                $dataColumns['cam_start_time'] = $tempColumns[4];
                                $dataColumns['cam_end_time'] = $tempColumns[5];
                                $dataColumns['cam_server'] =$tempColumns[6];
                                $dataColumns['ast_id'] = $assetId;
                                array_push($dataRows, $dataColumns);
                            }
                            //Add column to row
                            
                            //Assign row for OUT Counter
                            /*$dataColumns = array();
                            if (count($tempColumns) == 7) {
                                //Get date
                                $tempDate = explode('.', $tempColumns[0]);
                                $dataColumns['cam_date'] = date('Y').'-'.$tempDate[0].'-'.$tempDate[1];
                                $dataColumns['cam_number'] = $tempColumns[3];
                                $dataColumns['cam_counter'] = 'OUT';
                                $dataColumns['cam_result'] = $tempColumns[5];
                                //Get time
                                $tempTime = explode("-", $tempColumns[1]);
                                $dataColumns['cam_start_time'] = $tempTime[0];
                                $dataColumns['cam_end_time'] = $tempTime[1];
                                $dataColumns['cam_server'] = $server;
                                $dataColumns['ast_id'] = $assetId;
                                array_push($dataRows, $dataColumns);
                            }*/
                        
    			        }
            
        			}
        			fclose($csvhandle);
        		}
        		echo $csvDate;
        		//exit;
        		
        		if(!is_dir("../.ump_platform/raw_data/sab/People_counting_raw/csv_archive")) {
        			mkdir("../.ump_platform/raw_data/sab/People_counting_raw/csv_archive", 0777, true);
        			chmod("../.ump_platform/raw_data/sab/People_counting_raw/csv_archive", 0777); 
        		}
        		//unlink("csv_uploads/".$file);
        		rename("../.ump_platform/raw_data/sab/People_counting_raw/$file","../.ump_platform/raw_data/sab/People_counting_raw/csv_archive/$file");
        		
        		$date = date("Y-m-d");
        		$sql = $db->prepare("DELETE FROM `pcg_cameras` WHERE `cam_date` = :date");
        		$sql->bindValue(":date",$csvDate, PDO::PARAM_STR);
        		$sql->execute();
        	
        		$sql = $db->prepare("INSERT INTO `pcg_cameras`(`cam_date`, `cam_number`, `cam_counter`, `cam_result`, `cam_start_time`, `cam_end_time`, `cam_server`, `ast_id`) 
        		                    VALUES (:cam_date,:cam_number,:cam_counter,:cam_result,:start_time,:end_time,:cam_server,:ast_id)");
        
        		foreach ($dataRows as $key => $value) {
        			
        			$sql->bindValue(":cam_date",$value['cam_date'], PDO::PARAM_STR);
        			$sql->bindValue(":cam_number",$value["cam_number"], PDO::PARAM_STR);
                    $sql->bindValue(":cam_counter",$value["cam_counter"], PDO::PARAM_STR);
        			$sql->bindValue(":cam_result",$value["cam_result"], PDO::PARAM_STR);
        			$sql->bindValue(":start_time", $value["cam_start_time"], PDO::PARAM_STR);
        			$sql->bindValue(":end_time", $value["cam_end_time"], PDO::PARAM_STR);
        			$sql->bindValue(":cam_server", $value["cam_server"], PDO::PARAM_STR);
        			$sql->bindValue(":ast_id", $value["ast_id"], PDO::PARAM_STR);
        			$sql->execute();
        		}
        		
        		//echo "success";
        		}
            }
        
        
            closedir($handle);
        }
        echo "success";
        exit;
    }
    
    /**
     * Process Raw Data File from NUUIO System
     * @return Mixed
     */
    private function process_file($filepath, $filename, $assetId=0, $server=1) {
        //Open file
        $file = new File($filepath->pwd() . DS . $filename);
        //Lock file
        $file->lock = true;
        //Read file content
        $contents = $file->read();
        //Break Down the rows
        $dataRows = array();
        $rows = explode("\n", $contents);
        if (count($rows) < 24) {
            $rows = explode("\r", $contents);
        }
        if (count($rows) < 24) {
            $rows = explode("\r\n", $contents);
        }
        if (count($rows) < 24) {
            return "PROCESS FILE 01: Unable to retrieve rows for file '$filename'\n";
        }

        //Read each row's column data
        for ($i=1; $i<count($rows); $i++) {
            $tempColumns = explode(",",$rows[$i]);
            //Clean rows
            for ($z=0; $z<count($tempColumns); $z++) {
                $tempColumns[$z] = trim($tempColumns[$z]);
            }
            //Assign row for IN Counter
            $dataColumns = array();
            if (count($tempColumns) == 7) {
                //Get date
                $tempDate = explode('.', $tempColumns[0]);
                $dataColumns['cam_date'] = date('Y').'-'.$tempDate[0].'-'.$tempDate[1];
                $dataColumns['cam_number'] = $tempColumns[3];
                $dataColumns['cam_counter'] = 'IN';
                $dataColumns['cam_result'] = $tempColumns[4];
                //Get time
                $tempTime = explode("-", $tempColumns[1]);
                $dataColumns['cam_start_time'] = $tempTime[0];
                $dataColumns['cam_end_time'] = $tempTime[1];
                $dataColumns['cam_server'] = $server;
                $dataColumns['ast_id'] = $assetId;
            }
            //Add column to row
            array_push($dataRows, $dataColumns);
            //Assign row for OUT Counter
            $dataColumns = array();
            if (count($tempColumns) == 7) {
                //Get date
                $tempDate = explode('.', $tempColumns[0]);
                $dataColumns['cam_date'] = date('Y').'-'.$tempDate[0].'-'.$tempDate[1];
                $dataColumns['cam_number'] = $tempColumns[3];
                $dataColumns['cam_counter'] = 'OUT';
                $dataColumns['cam_result'] = $tempColumns[5];
                //Get time
                $tempTime = explode("-", $tempColumns[1]);
                $dataColumns['cam_start_time'] = $tempTime[0];
                $dataColumns['cam_end_time'] = $tempTime[1];
                $dataColumns['cam_server'] = $server;
                $dataColumns['ast_id'] = $assetId;
            }
            //Add column to row
            array_push($dataRows, $dataColumns);
        }
        $file->close();
        return $dataRows;
    }
    
}
