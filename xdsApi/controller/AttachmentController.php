<?php

class AttachmentController extends AppController {

    public function create($f3) {
        try {
            $attributes = $f3->get('POST');
            $attachment = $this->createAttachment($f3, $attributes['client_id'], $attributes['scd_portfolio_id'], $attributes['scd_policy_id'], $attributes['scd_type']);

            if (!$attachment) {
                $this->ajaxError("Error uploading file");
            }

            $uploader = new FileUploader($f3, './uploads/');
            $files = $uploader->uploadImage('attachment', $attachment);

            $this->ajaxSuccess("Attachment uploaded successfully!", "/user/view/".Authenticate::user($f3)['id']."#client".$attributes['client_id'], false);
        } catch (Exception $exception) {
            $this->ajaxError($exception->getCode() . ' : ' . $exception->getMessage());
        }
    }

    public function acceptAttachment($f3, $params) {
        try {
            $updateDefinition = array(
                'table' => 'fna_scanned_documents',
                'idField' => 'scd_scanned_document_id',
                'idValue' => $params['id'],
                'attribute' => 'scd_status',
                'value' => 'accepted',
            );
            
            $this->getRepository('fna_scanned_documents')->updateByAttribute($updateDefinition);

            $this->ajaxSuccess("Attachment document approved!", false, true);
        } catch (Exception $exception) {
            $this->ajaxError($exception->getCode() . ' : ' . $exception->getMessage());
        }
    }
    
    public function pendingAttachment($f3, $params) {
        try {
            $updateDefinition = array(
                'table' => 'fna_scanned_documents',
                'idField' => 'scd_scanned_document_id',
                'idValue' => $params['id'],
                'attribute' => 'scd_status',
                'value' => 'pending',
            );
            
            $this->getRepository('fna_scanned_documents')->updateByAttribute($updateDefinition);
            
            $this->ajaxSuccess("Attachment document pending!", false, true);
        } catch (Exception $exception) {
            $this->ajaxError($exception->getCode() . ' : ' . $exception->getMessage());
        }
    }

}
