<?php

require_once 'AppController.php';

class AjaxController extends AppController
{

    public function validate_email($f3)
    {
        try {
            $email = $f3->get("POST.usr_email");
            $users = $this->getRepository('users')->getByAttribute('usr_email', $email);

            if (sizeof($users) > 0) {
                echo "n";
            } else {
                echo 'y';
            }

        } catch (Exception $exception) {
            return $exception->getMessage();
        }
    }

    public function convert($hours, $minutes)
    {
        return $hours + round($minutes / 60, 2);
    }

    public function form_fields($f3, $params)
    {
        $this->authCheck($f3);
        global $db;

        try {

            $query = "SELECT * FROM fields WHERE formID = :formID ORDER BY id DESC";
            $vars = array(
                ':formID' => $f3->get('POST.form_id'),
            );
            $fields = $db->exec($query, $vars);

            $f3->set('SESSION.message', array('msg' => 'Fields retrieved successfully!', 'alert' => 'success'));

            echo json_encode($fields);
        } catch (Exception $exception) {
            echo json_encode($exception->getMessage());
        }
    }

    public function field_state($f3, $params)
    {
        $this->authCheck($f3);
        global $db;
        $formID = $f3->get('POST.form_id');
        $state = $f3->get('POST.state');

        try {

            $query = "UPDATE forms SET state = :state WHERE id = :formID";
            $vars = array(
                ':state' => $state,
                ':formID' => $formID,
            );
            $update = $db->exec($query, $vars);

            echo $state;
        } catch (Exception $exception) {
            echo json_encode($exception->getMessage());
        }
    }

    public function field_options($f3, $params)
    {
        $this->authCheck($f3);
        global $db;

        try {

            $query = "SELECT * FROM fieldOptions WHERE fieldID = :fieldID ORDER BY id DESC";
            $vars = array(
                ':fieldID' => $f3->get('POST.field_id'),
            );
            $options = $db->exec($query, $vars);

            $f3->set('SESSION.message', array('msg' => 'Field Options retrieved successfully!', 'alert' => 'success'));

            echo json_encode($options);
        } catch (Exception $exception) {
            echo json_encode($exception->getMessage());
        }
    }

    public function update_form($f3, $params)
    {
        $this->authCheck($f3);
        global $db;

        try {
            $userID = $f3->get('SESSION.USER.id');
            $form = $f3->get('POST.form');
            $data = json_decode($form, true);

            $deletedFields = $f3->get('POST.deleted');
            $delete = json_decode($deletedFields, true);

            if (count($delete['fields']) > 0) {
                for ($a = 0; $a < count($delete['fields']); $a++) {
                    $delet_query = "DELETE FROM fieldOptions WHERE fieldID = :fieldID";
                    $vars_delete = array(
                        ':fieldID' => $delete['fields'][$a]['id'],
                    );
                    $result = $db->exec($delet_query, $vars_delete);

                    $delet_query2 = "DELETE FROM fields WHERE id = :fieldID";
                    $vars_delete2 = array(
                        ':fieldID' => $delete['fields'][$a]['id'],
                    );
                    $result2 = $db->exec($delet_query2, $vars_delete2);
                }
            }

            if (count($data['formData']) > 0) {
                for ($i = 0; $i < count($data['formData']); $i++) {
                    $query2 = "INSERT INTO `fields` (`formID`, `title`, `type`, `dateCreated`)
                    VALUES (:formID, :title, :type, :dateCreated)";
                    $vars2 = array(
                        ':formID' => $data['formID'],
                        ':title' => $data['formData'][$i]['label'],
                        ':type' => $data['formData'][$i]['type'],
                        ':dateCreated' => date('Y-m-d H:i:s'),
                    );
                    $results2 = $db->exec($query2, $vars2);
                    $fieldID = $db->lastInsertId();

                    if ($data['formData'][$i]['type'] == "select") {
                        # create field options
                        for ($x = 0; $x < count($data['formData'][$i]['options']); $x++) {
                            $query3 = "INSERT INTO `fieldOptions` (`fieldID`, `title`, `value`, `dateCreated`)
                            VALUES (:fieldID, :title, :value, :dateCreated)";
                            $vars3 = array(
                                ':fieldID' => $fieldID,
                                ':title' => $data['formData'][$i]['options'][$x]['label'],
                                ':value' => $data['formData'][$i]['options'][$x]['value'],
                                ':dateCreated' => date('Y-m-d H:i:s'),
                            );
                            $results3 = $db->exec($query3, $vars3);
                        }
                    }
                }
            }

            echo 'Form updated successfully!';
        } catch (Exception $exception) {
            echo json_encode($exception->getMessage());
        }
    }

    public function createApplication($f3, $params)
    {
        $this->authCheck($f3);
        global $db;

        try {
            $query = "INSERT INTO applications (userID, complexID, dateCreated, dateUpdated) VALUES(:userID, :complexID, NOW(), NOW()) ";
            $vars = array(
                ':userID' => $f3->get("SESSION.USER.id"),
                ':complexID' => $f3->get('POST.complexID'),
            );
            $userdata = $db->exec($query, $vars);

            //******************* Validate Applicant Details *************************//
            $query = "SELECT *  from applicantdetails WHERE userID = :id";
            $vars = array(
                ':id' => $f3->get("SESSION.USER.id"),
            );
            $results = $db->exec($query, $vars);

            $params["IDNumber"] = $results[0]["idPassportNo"];

            $idVerificationResponse = $this->validateID($f3, $params);

            $idVerificationResponse = strtolower($idVerificationResponse);

            $verifiedData = [];

            $firstName = strtolower($results[0]["firstName"]);
            $lastName = strtolower($results[0]["lastName"]);
            $idPassportNo = strtolower($results[0]["idPassportNo"]);

            if (strpos($idVerificationResponse, $firstName) > -1) {
                array_push($verifiedData, "firstName");
            }

            if (strpos($idVerificationResponse, $lastName) > -1) {
                array_push($verifiedData, "lastName");
            }

            if (strpos($idVerificationResponse, $idPassportNo) > -1) {
                array_push($verifiedData, "idPassportNo");
                array_push($verifiedData, "dob");
            }

            $arrCheckList = [];

            $arrCheckList["personalInfo"] = ["firstName", "lastName", "idPassportNo", "dob", "address", "maritalStatus"];
            $arrCheckList["contactInfo"] = ["email", "phone", "altPhone"];
            $arrCheckList["employerInfo"] = ["occupation", "employer", "employerContact", "totalIncome", "totalExpenses"];
            $arrCheckList["emergencyInfo"] = ["nextOfKin", "nextOfKinContact"];
            $arrCheckList["documentInfo"] = ["idDocument", "payslip", "bankStatement"];

            $update = false;

            foreach ($verifiedData as $column) {

                $query = "SELECT * FROM userChecklist WHERE userID = :userID AND title = :title";
                $vars = array(
                    ':userID' => $f3->get("SESSION.USER.id"),
                    ':title' => $column,
                );
                $userdata = $db->exec($query, $vars);

                if (sizeof($userdata) > 0) {
                    $update = true;
                }

                //$dataArr = explode("_", $f3->get("POST.$column"));
                if ($update) {
                    $query = "UPDATE userChecklist SET approvedBy=:adminID, isVerified=:isVerified WHERE id=:id";
                    $vars = array(
                        ':adminID' => $userID,
                        ':isVerified' => "y",
                        ':id' => $userdata[0]['id'],
                    );
                    $userChecklist = $db->exec($query, $vars);
                } else {
                    $query = "INSERT INTO userChecklist (userID, approvedBy, title, isVerified, dateCreated, dateUpdated)
                        VALUES (:userID, :adminID, :title, :isVerified, NOW(), NOW())";
                    $vars = array(
                        ':userID' => $f3->get("POST.userID"),
                        ':adminID' => $userID,
                        ':title' => $column,
                        ':isVerified' => "y",
                    );
                    $userChecklist = $db->exec($query, $vars);
                }
            }

            $f3->set('SESSION.message', array('msg' => 'Application created successfully!', 'alert' => 'info'));
            echo 'Application created successfully!';
        } catch (Exception $exception) {
            echo json_encode($exception->getMessage());
        }

    }

    public function create_form($f3, $params)
    {
        $this->authCheck($f3);
        global $db;

        try {
            //$f3->set('SESSION.count', 1);
            $userID = $f3->get('SESSION.USER.id');
            $update = false;

            $attributes = $f3->get('POST');
            $definition = $this->getDefinition($attributes);
            $idArray = ['userID =?', $userID];
            if ($f3->get('POST')) {

                $form = $f3->get('POST.form');
                $data = json_decode($form, true);

                //create form
                $query = "INSERT INTO `forms` (`title`, `description`, `state`, `dateCreated`)
                VALUES (:title, :description, :state, :dateCreated)";
                $vars = array(
                    ':title' => $data['name'],
                    ':description' => $data['description'],
                    ':state' => 1,
                    ':dateCreated' => date('Y-m-d H:i:s'),
                );
                $results = $db->exec($query, $vars);
                $formID = $db->lastInsertId();

                //create fields
                for ($i = 0; $i < count($data['formData']); $i++) {
                    $query2 = "INSERT INTO `fields` (`formID`, `title`, `type`, `dateCreated`)
                    VALUES (:formID, :title, :type, :dateCreated)";
                    $vars2 = array(
                        ':formID' => $formID,
                        ':title' => $data['formData'][$i]['label'],
                        ':type' => $data['formData'][$i]['type'],
                        ':dateCreated' => date('Y-m-d H:i:s'),
                    );
                    $results2 = $db->exec($query2, $vars2);
                    $fieldID = $db->lastInsertId();

                    if ($data['formData'][$i]['type'] == "select") {
                        # create field options
                        for ($x = 0; $x < count($data['formData'][$i]['options']); $x++) {
                            $query3 = "INSERT INTO `fieldOptions` (`fieldID`, `title`, `value`, `dateCreated`)
                            VALUES (:fieldID, :title, :value, :dateCreated)";
                            $vars3 = array(
                                ':fieldID' => $fieldID,
                                ':title' => $data['formData'][$i]['options'][$x]['label'],
                                ':value' => $data['formData'][$i]['options'][$x]['value'],
                                ':dateCreated' => date('Y-m-d H:i:s'),
                            );
                            $results3 = $db->exec($query3, $vars3);
                        }
                    }
                }

                echo 'Form created successfully!';

                $f3->set('SESSION.message', array('msg' => 'Form created successfully!', 'alert' => 'info'));
            } else {
                echo "No Form was created!";
            }

            // $f3->reroute("/users");
        } catch (Exception $exception) {
            echo json_encode($exception->getMessage());
        }
    }

    public function formsave($f3, $params)
    {
        $this->authCheck($f3);
        global $db;

        try {
            //$f3->set('SESSION.count', 1);
            $formtype = $params['formtype'];
            $userID = $f3->get('SESSION.USER.id');

            $query = "SELECT * FROM `applicantdetails` where userID = :userID";
            $vars = array(
                ':userID' => $userID,
            );
            $user_profiles = $db->exec($query, $vars);

            $attributes = $f3->get('POST');
            if ($params['formtype'] == "employmentInfo") {
                $attributes['rentTrench'] = 1;
                $query = "SELECT * FROM `rentTrench` where minIncome <= :minIncome AND maxIncome >= :maxIncome AND deleted= :isDeleted";
                $vars = array(
                    ':minIncome' => $attributes['totalIncome'],
                    ':maxIncome' => $attributes['totalIncome'],
                    ':isDeleted' => "n",
                );
                $results = $db->exec($query, $vars);
                $attributes['rentTrench'] = $results[0]['id'];
            }

            if (count($user_profiles) > 0) {
                $update = true;
            } else {
                $attributes['userID'] = $userID;
                $update = false;
            }

            $definition = $this->getDefinition($attributes);
            $idArray = ['userID =?', $userID];
            if ($f3->get('POST')) {

                if ($params['formtype'] == "personalInfo") {
                    $userArray = ['id =?', $userID];

                    $this->getRepository("users")->updateRecord($userArray, $definition);
                }

                if ($update) {
                    $info = $this->getRepository("applicantdetails")->updateRecord($idArray, $definition);
                } else {
                    $info = $this->getRepository("applicantdetails")->createRecord($definition);
                }

                // $users = $this->getRepository('users')->getByAttribute('usr_id', $user_id);
                // $username = $users[0]['usr_firstname']." ".$users[0]['usr_surname'];

                // $recipient = "info@g3performance.co.za";
                // $subject = "$table info";

                // $message = "User : $username <br> logged data: $subject <br>".implode(',',$attributes);

                // $this->dispatchEmail($recipient, $subject, $message, $user_name);

                echo "Info saved successfully!";
                $f3->set('SESSION.message', array('msg' => 'Data saved successfully!', 'alert' => 'info'));
            } else {
                echo "No information was saved!";
            }

            // $f3->reroute("/users");
        } catch (Exception $exception) {
            echo json_encode($exception->getMessage());
        }
    }

    public function adminFormSave($f3, $params)
    {
        $this->authCheck($f3);

        try {
            //$f3->set('SESSION.count', 1);
            $formtype = $params['formtype'];
            $userID = $f3->get('POST.userID');
            $update = true;

            $attributes = $f3->get('POST');
            $definition = $this->getDefinition($attributes);
            $idArray = ['userID =?', $userID];
            if ($f3->get('POST')) {

                if ($update) {
                    $info = $this->getRepository("applicantdetails")->updateRecord($idArray, $definition);
                } else {
                    $info = $this->getRepository("applicantdetails")->createRecord($definition);
                }

                // $users = $this->getRepository('users')->getByAttribute('usr_id', $user_id);
                // $username = $users[0]['usr_firstname']." ".$users[0]['usr_surname'];

                // $recipient = "info@g3performance.co.za";
                // $subject = "$table info";

                // $message = "User : $username <br> logged data: $subject <br>".implode(',',$attributes);

                // $this->dispatchEmail($recipient, $subject, $message, $user_name);

                echo "Info saved successfully!";
                $f3->set('SESSION.message', array('msg' => 'Data saved successfully!', 'alert' => 'info'));
            } else {
                echo "No information was saved!";
            }

            // $f3->reroute("/users");
        } catch (Exception $exception) {
            echo json_encode($exception->getMessage());
        }
    }

    public function verifyForm($f3, $params)
    {
        global $db;
        $this->authCheck($f3);

        try {
            //$f3->set('SESSION.count', 1);
            $formType = $params['formtype'];
            $adminID = $f3->get('SESSION.USER.id');
            $update = true;

            //if post lets save
            if ($f3->get('POST')) {
                $arrCheckList = [];

                $arrCheckList["personalInfo"] = ["firstName", "lastName", "idPassportNo", "dob", "address", "maritalStatus", "bankName", "branchCode", "accountNumber", "accountType"];
                $arrCheckList["contactInfo"] = ["email", "phone", "altPhone"];
                $arrCheckList["employerInfo"] = ["occupation", "employer", "employerContact", "totalIncome", "totalExpenses"];
                $arrCheckList["emergencyInfo"] = ["nextOfKin", "nextOfKinContact"];
                $arrCheckList["documentInfo"] = ["idDocument", "payslip", "bankStatement", "proofOfPayment"];

                //loop through array for the submitted form type
                foreach ($arrCheckList[$formType] as $column) {
                    $isVerified = 'n';
                    $update = false;

                    //if the field is in post its a verification
                    foreach ($f3->get('POST') as $feild => $value) {
                        if ($feild == $column) {
                            $isVerified = 'y';
                        }

                    }

                    //update or insert.
                    $query = "SELECT * FROM userChecklist WHERE userID = :userID AND title = :title";
                    $vars = array(
                        ':userID' => $f3->get("POST.userID"),
                        ':title' => $column,
                    );
                    $userdata = $db->exec($query, $vars);
                    if (isset($userdata[0]['title'])) {
                        $update = true;
                    }

                    if ($update) {
                        $query = "UPDATE userChecklist SET approvedBy=:adminID, isVerified=:isVerified, dateUpdated=NOW() WHERE id=:id";
                        $vars = array(
                            ':adminID' => $adminID,
                            ':isVerified' => $isVerified,
                            ':id' => $userdata[0]['id'],
                        );
                        $userChecklist = $db->exec($query, $vars);
                    } else {
                        $query = "INSERT INTO userChecklist (userID, approvedBy, title, isVerified, dateCreated, dateUpdated)
                                          VALUES (:userID, :adminID, :title, :isVerified, NOW(), NOW())";
                        $vars = array(
                            ':userID' => $f3->get("POST.userID"),
                            ':adminID' => $adminID,
                            ':title' => $column,
                            ':isVerified' => $isVerified,
                        );
                        $userChecklist = $db->exec($query, $vars);
                    }

                    if ($column == "proofOfPayment") {
                        $params['userID'] = $f3->get("POST.userID");
                        $this->xdsVerification($f3, $params);
                    }

                }

                echo "Info saved successfully!";
                $f3->set('SESSION.message', array('msg' => 'Data saved successfully!', 'alert' => 'info'));
            } else {
                echo "No information was saved!";
            }

        } catch (Exception $exception) {
            echo json_encode($exception->getMessage());
        }
    }

    

    public function delete($f3, $params)
    {
        try {
            $this->getRepository('cps_users')->deleteRecord($params['id']);
            $f3->set('SESSION.message', array('msg' => 'User deleted successfully!', 'alert' => 'success'));

            $f3->reroute("/users");
        } catch (Exception $exception) {
            echo json_encode($exception->getMessage());
        }
    }

    public function update($f3, $params)
    {
        try {
            $attributes = $f3->get('POST');
            $definition = $this->getDefinition($attributes);

            $this->getRepository('cps_users')->updateRecord(array('id' => $params['id']), $definition);
            $f3->set('SESSION.message', array('msg' => 'User updated successfully!', 'alert' => 'success'));

            $f3->reroute("/users");
        } catch (Exception $exception) {
            echo json_encode($exception->getMessage());
        }
    }

    public function getApplicationVerification($f3, $params)
    {
        global $db;
        $this->authCheck($f3);

        $applicationID = $params['applicationID'];

        $query = "SELECT userID  FROM applications WHERE id = :id";
        $vars = array(
            ':id' => $applicationID,
        );
        $applications = $db->exec($query, $vars);

        $query = "SELECT ad.*, uc.id as checkID, uc.title, uc.isVerified from applicantdetails ad LEFT JOIN userChecklist uc ON uc.userID = ad.userID WHERE ad.userID = :userID";
        $vars = array(
            ':userID' => $applications[0]['userID'],
        );
        $results = $db->exec($query, $vars);

        $f3->set('userID', $results[0]['userID']);

        $query = "SELECT dt.*  FROM applicantdetails ad LEFT JOIN applicantdata dt ON ad.userID = dt.userID WHERE ad.userID = :userID";
        $vars = array(
            ':userID' => $applications[0]['userID'],
        );
        $userdata = $db->exec($query, $vars);

        $arrCheckList = [];

        $getApplicationVerification = [];

        $arrCheckList["personalInfo"] = ["firstName", "lastName", "idPassportNo", "dob", "address", "maritalStatus", "bankName", "branchCode", "accountNumber", "accountType"];
        $arrCheckList["contactInfo"] = ["email", "phone", "altPhone"];
        $arrCheckList["employerInfo"] = ["occupation", "employer", "employerContact", "totalIncome", "totalExpenses"];
        $arrCheckList["emergencyInfo"] = ["nextOfKin", "nextOfKinContact"];
        $arrCheckList["documentInfo"] = ["idDocument", "payslip", "bankStatement", "proofOfPayment"];

        $isColumn = [];

        foreach ($arrCheckList as $key => $arrlist) {

            foreach ($arrlist as $column) {
                $isVerified = 'p';
                $val = '';
                $title = $column;

                foreach ($results as $list) {
                    array_push($isColumn, $column);
                    if ($list["title"] == $column) {
                        $isVerified = $list["isVerified"];
                    }
                    if (isset($list[$column])) {
                        $val = $list[$column];
                    }
                    
                }
                if ($key == "personalInfo") {
                    $getApplicationVerification[] = ["title" => $title, "value" => $val, "isVerified" => $isVerified, "applicantData" => $results[0]["applicantData"]];
                }
                if ($key == "contactInfo") {
                    array_push($isColumn, $column);

                    $getApplicationVerification[] = ["title" => $title, "value" => $results[0][$column], "isVerified" => $isVerified, "applicantData" => $results[0]["applicantData"]];
                }
                if ($key == "employerInfo") {
                    array_push($isColumn, $column);

                    $getApplicationVerification[] = ["title" => $title, "value" => $results[0][$column], "isVerified" => $isVerified, "applicantData" => $results[0]["applicantData"]];
                }
                if ($key == "emergencyInfo") {
                    array_push($isColumn, $column);

                    $getApplicationVerification[] = ["title" => $title, "value" => $results[0][$column], "isVerified" => $isVerified, "applicantData" => $results[0]["applicantData"]];
                }
                if ($key == "documentInfo") {
                    array_push($isColumn, $column);

                    $getApplicationVerification[] = ["title" => $title, "value" => $results[0][$column], "isVerified" => $isVerified, "applicantData" => $results[0]["applicantData"]];
               
                    // array_push($isColumn, $column);

                    // foreach ($userdata as $row) {
                    //     $docType = "";

                    //     if ($row['dataLabel'] == $column) {
                    //         $dataArr = explode(",", $row['data']);
                    //         if (!in_array('Error', $dataArr) && $row['data'] != "") {
                    //             if ($list["title"] == $column) {
                    //                 $isVerified = $list["isVerified"];
                    //             }

                    //             $ext = strtolower(substr($row['data'], -3));
                    //             $imgArr = ['jpg', 'jpeg', 'png'];
                    //             $docArr = ['pdf'];
                    //             if (in_array($ext, $imgArr)) {
                    //                 $docType = "image";
                    //             }

                    //             if (in_array($ext, $docArr)) {
                    //                 $docType = "pdf";
                    //             }

                    //             $getApplicationVerification[] = ["docType" => $docType, "title" => $title, "value" => $row['data'], "isVerified" => $isVerified, "applicantData" => $results[0]["applicantData"]];

                    //         }

                    //     } else {
                    //         $getApplicationVerification[] = ["docType" => $docType, "title" => $title, "value" => "", "isVerified" => "y", "applicantData" => ""];
                    //     }

                    // }
                }
            }
        }

        echo json_encode($getApplicationVerification);
    }

    public function approveApplication($f3, $params)
    {
        global $db;
        $this->authCheck($f3);

        $applicationID = $params['applicationID'];

        $approvalData = [];

        $query = "SELECT ap.userID, cp.complexName  FROM applications ap
                    LEFT JOIN complex cp ON cp.id = ap.complexID
                    WHERE ap.id = :id";
        $vars = array(
            ':id' => $applicationID,
        );
        $applications = $db->exec($query, $vars);

        $approvalData['complexName'] = $applications[0]['complexName'];

        //check application percentage
        $query = "SELECT dt.*  FROM applicantdetails ad LEFT JOIN applicantdata dt ON ad.userID = dt.userID WHERE ad.userID = :userID";
        $vars = array(
            ':userID' => $applications[0]['userID'],
        );
        $userdata = $db->exec($query, $vars);

        $query = "SELECT ad.*, uc.id as checkID, uc.title, uc.isVerified from applicantdetails ad LEFT JOIN userChecklist uc ON uc.userID = ad.userID WHERE ad.userID = :userID";
        $vars = array(
            ':userID' => $applications[0]['userID'],
        );
        $results = $db->exec($query, $vars);
        $arrCheckList = [];

        $profilePerc = 0;

        $arrCheckList["personalInfo"] = ["firstName", "lastName", "idPassportNo", "dob", "address", "maritalStatus", "bankName", "branchCode", "accountNumber", "accountType"];
        $arrCheckList["contactInfo"] = ["email", "phone", "altPhone"];
        $arrCheckList["employerInfo"] = ["occupation", "employer", "employerContact", "totalIncome", "totalExpenses"];
        $arrCheckList["emergencyInfo"] = ["nextOfKin", "nextOfKinContact"];
        $arrCheckList["documentInfo"] = ["idDocument", "payslip", "bankStatement", "proofOfPayment"];

        $isColumn = [];

        foreach ($arrCheckList as $key => $arrlist) {

            foreach ($arrlist as $column) {
                $isVerified = 'n';
                $val = '';

                foreach ($results as $newKey => $list) {
                    array_push($isColumn, $column);
                    if ($list["title"] == $column) {
                        $isVerified = $list["isVerified"];
                    }
                    if (isset($list[$column])) {
                        $val = $list[$column];
                    }

                    $title = preg_replace("([A-Z])", " $0", $column);
                }
                if ($key == "personalInfo") {
                    if ($isVerified == 'y') {
                        $profilePerc = $profilePerc + 2;
                    }
                    $personalDetails[] = ["title" => $title, "value" => $val, "isVerified" => $isVerified, "applicantData" => $results[0]["applicantData"]];
                }
                if ($key == "contactInfo") {
                    array_push($isColumn, $column);
                    if ($isVerified == 'y') {
                        $profilePerc = $profilePerc + 6.66;
                    }

                    $contactDetails[] = ["title" => $title, "value" => $results[0][$column], "isVerified" => $isVerified, "applicantData" => $results[0]["applicantData"]];
                }
                if ($key == "employerInfo") {
                    array_push($isColumn, $column);
                    if ($isVerified == 'y') {
                        $profilePerc = $profilePerc + 4;
                    }

                    $employerDetails[] = ["title" => $title, "value" => $results[0][$column], "isVerified" => $isVerified, "applicantData" => $results[0]["applicantData"]];
                }
                if ($key == "emergencyInfo") {
                    array_push($isColumn, $column);
                    if ($isVerified == 'y') {
                        $profilePerc = $profilePerc + 10;
                    }

                    $emergencyDetails[] = ["title" => $title, "value" => $results[0][$column], "isVerified" => $isVerified, "applicantData" => $results[0]["applicantData"]];
                }
                if ($key == "documentInfo") {
                    array_push($isColumn, $column);

                    foreach ($userdata as $row) {

                        if ($row['dataLabel'] == $column) {
                            $dataArr = explode(",", $row['data']);
                            if (!in_array('Error', $dataArr) && $row['data'] != "") {
                                if ($isVerified == 'y') {
                                    $profilePerc = $profilePerc + 5;
                                }

                                $ext = strtolower(substr($row['data'], -3));
                                $imgArr = ['jpg', 'jpeg', 'png'];
                                $docArr = ['pdf'];
                                if (in_array($ext, $imgArr)) {
                                    $docType = "image";
                                }

                                if (in_array($ext, $docArr)) {
                                    $docType = "pdf";
                                }

                                $documentDetails[] = ["docType" => $docType, "title" => $title, "value" => $row['data'], "isVerified" => $isVerified, "applicantData" => $results[0]["applicantData"]];

                            }

                        }
                    }
                }
            }
        }

        $approvalData['profilePerc'] = round($profilePerc);

        $query = "SELECT rentTrench from applicantdetails WHERE userID = :userID";
        $vars = array(
            ':userID' => $applications[0]['userID'],
        );
        $rentTrench = $db->exec($query, $vars);

        $query = "SELECT * FROM units WHERE trenchID = :trenchID AND occupied = :occupied";
        $vars = array(
            ':trenchID' => $rentTrench[0]['rentTrench'],
            ':occupied' => "n",

        );
        $results = $db->exec($query, $vars);

        $approvalData['units'] = $results;

        echo json_encode($approvalData);

    }

    public function getProofOfPayment($f3, $params)
    {
        global $db;
        $this->authCheck($f3);

        $applicationID = $params['applicationID'];


        $query = "SELECT userID FROM applications
                    WHERE id = :id";
        $vars = array(
            ':id' => $applicationID,
        );
        $applications = $db->exec($query, $vars);


        //check application percentage
        $query = "SELECT *  FROM applicantdata WHERE userID = :userID AND dataType = :dataType";
        $vars = array(
            ':userID' => $applications[0]['userID'],
            ':dataType' => "proofOfPayment",

        );
        $applicantdata = $db->exec($query, $vars);

        echo json_encode($applicantdata);

    }

}
