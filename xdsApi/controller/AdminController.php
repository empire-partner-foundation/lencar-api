<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class AdminController extends AppController
{

    public function dashboard($f3, $params)
    {
        $this->authCheck($f3);
        global $db;
        $query = "SELECT COUNT(*) as totalUnits FROM units WHERE deleted=:deleted";
        $vars = array(
            ':deleted' => 'n',
        );
        $totalUnits = $db->exec($query, $vars);

        $f3->set("totalUnits", $totalUnits[0]['totalUnits']);

        $query = "SELECT COUNT(*) as occupiedUnits FROM units WHERE deleted=:deleted AND occupied=:occupied";
        $vars = array(
            ':deleted' => 'n',
            ':occupied' => 'y',
        );
        $occupiedUnits = $db->exec($query, $vars);

        $f3->set("occupiedUnits", $occupiedUnits[0]['occupiedUnits']);

        $f3->set("availableUnits", $totalUnits[0]['totalUnits'] - $occupiedUnits[0]['occupiedUnits']);

        global $db;
        $this->authCheck($f3);
        $query = "SELECT COUNT(*) as totalApplications  FROM applications";
        $applicantdata = $db->exec($query);
        $f3->set('totalApplications', $applicantdata[0]['totalApplications']);

        $query = "SELECT COUNT(*) as approvedApplications  FROM applications WHERE applicationStatus = :status";
        $vars = array(
            ':status' => "a",
        );
        $applicantdata = $db->exec($query, $vars);
        $f3->set('approvedApplications', $applicantdata[0]['approvedApplications']);

        $query = "SELECT COUNT(*) as declinedApplications  FROM applications WHERE applicationStatus = :status";
        $vars = array(
            ':status' => "d",
        );
        $applicantdata = $db->exec($query, $vars);
        $f3->set('declinedApplications', $applicantdata[0]['declinedApplications']);

        $query = "SELECT COUNT(*) as pendingApplications  FROM applications WHERE applicationStatus = :status";
        $vars = array(
            ':status' => "p",
        );
        $applicantdata = $db->exec($query, $vars);
        $f3->set('pendingApplications', $applicantdata[0]['pendingApplications']);

        $query = "SELECT COUNT(*) as onholdUnits FROM units WHERE deleted=:deleted AND occupied=:occupied";
        $vars = array(
            ':deleted' => 'n',
            ':occupied' => 'h',
        );
        $units = $db->exec($query, $vars);

        $f3->set("onholdUnits", $units[0]['onholdUnits']);

        $query = "SELECT *  FROM users WHERE id = :userID";
        $vars = array(
            ':userID' => $f3->get("SESSION.USER.id"),
        );
        $userData = $db->exec($query, $vars);

        $f3->set("isNew", $userData[0]['isNew']);
        $this->proceed($f3);

        $f3->set('content', 'admin/index.html');
        echo \Template::instance()->render('admin_template.htm');
    }

    public function proceed($f3)
    {
        $this->authCheck($f3);
        $f3->set('POST.isNew', 1);
        $attributes = $f3->get('POST');
        $definition = $this->getDefinition($attributes);
        $user = $this->getRepository('users')->updateRecord(array('id =?', $f3->get('SESSION.USER.id')), $definition);

        //$f3->reroute('/user/dashboard');
    }

    /*
     * viewTrench
     */
    public function viewUsers($f3)
    {
        $this->authCheck($f3);
        global $db;
        $query = "SELECT * FROM users WHERE userType= :userType AND isDeleted= :isDeleted";
        $vars = array(
            ':userType' => 1,
            ':isDeleted' => "n",
        );
        $results = $db->exec($query, $vars);
        $f3->set('data', $results);

        $f3->set('content', '../ui/admin/users/index.html');

        echo \Template::instance()->render('admin_template.htm');
    }

    public function createUser($f3)
    {
        $this->authCheck($f3);
        try {
            $f3->set('POST.dateCreated', date("Y-m-d"));
            $f3->set('POST.dateUpdated', date("Y-m-d"));
            $attributes = $f3->get('POST');
            $user = $this->getRepository('users')->getByAttribute('email', $attributes['email']);
            if ($user) {
                $f3->set('SESSION.emailError', 'An account with that email already exists.');
                $f3->reroute("/admin/users");
            }

            $definition = $this->getDefinition($attributes);
            //$this->getRepository('users')->createRecord($definition);
            $this->createAccount($f3, $attributes);
            $subject = "Welcome to Tenant Onboarding";

            $this->dispatchEmail($attributes, $subject, "welcome");
            $f3->set('SESSION.message', array('msg' => 'User created successfully!', 'alert' => 'success'));

            $f3->reroute("/admin/users");
        } catch (Exception $exception) {
            echo json_encode($exception->getMessage());
        }
    }

    public function createAccount($f3, $attributes)
    {
        global $db;

        $password = hash('sha256', $attributes['password']); //create_hash(trim($attributes['password']));
        //$verificationCode = rand();

        $query = "INSERT INTO `users` (`email`, `pmLevel`, `userType`,`userState`,`isVerified`,`password`,`verificationCode`,`firstName`,`lastName`,`phone`,`dateCreated`,`dateUpdated`)
        VALUES (:email, :pmLevel, :userType, :userState, :isVerified, :password, :verificationCode, :firstName, :lastName, :phone, :dateCreated, :dateUpdated)";
        $vars = array(
            ':email' => $attributes['email'],
            ':pmLevel' => 1,
            ':userType' => 1,
            ':userState' => 1,
            ':isVerified' => 1,
            ':password' => $password,
            ':verificationCode' => "12345", //$verificationCode, nimrod
            ':firstName' => $attributes['firstName'],
            ':lastName' => $attributes['lastName'],
            ':phone' => $attributes['phone'],
            ':dateCreated' => date('Y-m-d H:i:s'),
            ':dateUpdated' => date('Y-m-d H:i:s'),
        );
        $results = $db->exec($query, $vars);
        // $insertedID = $db->lastInsertId();

        // $user = $this->getRepository('applicantdetails')->getByAttribute('userID', $insertedID);
        // if ($user) {
        //     //$f3->set('SESSION.emailError', 'An account with that email already exists.');
        //     //$validate = false;
        // }
        // else{
        //     $sql = "INSERT INTO `applicantdetails` (`email`,`phone`,`altPhone`,`firstName`,`lastName`,`userID`,`dateCreated`,`dateUpdated`)
        //     VALUES (:email, :phone, :altPhone, :firstName, :lastName, :userID, :dateCreated, :dateUpdated)";
        //     $vars1 = array(
        //         ':email' => $attributes['email'],
        //         ':phone' => $attributes['phone'],
        //         ':altPhone' => $attributes['phone'],
        //         ':userID' => $insertedID,
        //         ':firstName' => $attributes['firstName'],
        //         ':lastName' => $attributes['lastName'],
        //         ':dateCreated' => date('Y-m-d H:i:s'),
        //         ':dateUpdated' => date('Y-m-d H:i:s'),
        //     );
        //     $results = $db->exec($sql, $vars1);
        // }

    }

    public function deleteUser($f3, $params)
    {
        $this->authCheck($f3);
        $attributes = ["isDeleted" => 'y', "dateUpdate" => date("Y-m-d")];
        $definition = $this->getDefinition($attributes);
        try {
            $idArray = ['id = ?', $params['id']];

            $user = $this->getRepository('users')->updateRecord($idArray, $definition);
            $f3->set('SESSION.message', array('msg' => 'User deleted successfully!', 'alert' => 'success'));

            $f3->reroute("/admin/users");
        } catch (Exception $exception) {
            echo json_encode($exception->getMessage());
        }
    }

    public function updateUser($f3, $params)
    {
        $this->authCheck($f3);
        try {
            if ($f3->get('POST')) {
                $f3->set('POST.dateUpdated', date("Y-m-d"));
                $users = $this->getRepository('users')->getByAttribute('id', $params['id']);

                if (!empty($f3->get('POST.password'))) {
                    $password = hash('sha256', $f3->get('POST.password'));
                    $f3->set('POST.password', $password);
                } else {
                    $f3->set('POST.password', $users[0]['password']);
                }

                $attributes = $f3->get('POST');
                $definition = $this->getDefinition($attributes);

                $idArray = ['id = ?', $params['id']];

                $user = $this->getRepository('users')->updateRecord($idArray, $definition);

                $f3->reroute("/admin/users");
            }

            $users = $this->getRepository('users')->getByAttribute('id', $params['id']);
            $f3->set('POST', $users[0]);

            $f3->set('content', '../ui/admin/users/updateUser.html');
            echo \Template::instance()->render('admin_template.htm');

        } catch (Exception $exception) {
            echo json_encode($exception->getMessage());
        }
    }

    public function viewApplications($f3)
    {
        global $db;
        $this->authCheck($f3);
        $query = "SELECT COUNT(*) as totalApplications  FROM applications";
        $applicantdata = $db->exec($query);
        $f3->set('totalApplications', $applicantdata[0]['totalApplications']);

        $query = "SELECT COUNT(*) as approvedApplications  FROM applications WHERE applicationStatus = :status";
        $vars = array(
            ':status' => "a",
        );
        $applicantdata = $db->exec($query, $vars);
        $f3->set('approvedApplications', $applicantdata[0]['approvedApplications']);

        $query = "SELECT COUNT(*) as declinedApplications  FROM applications WHERE applicationStatus = :status";
        $vars = array(
            ':status' => "d",
        );
        $applicantdata = $db->exec($query, $vars);
        $f3->set('declinedApplications', $applicantdata[0]['declinedApplications']);

        $query = "SELECT COUNT(*) as pendingApplications  FROM applications WHERE applicationStatus = :status";
        $vars = array(
            ':status' => "p",
        );
        $applicantdata = $db->exec($query, $vars);
        $f3->set('pendingApplications', $applicantdata[0]['pendingApplications']);

        $query = "SELECT ap.*, ad.firstName, ad.lastName, rt.title as rentTrench  FROM applications ap 
                    LEFT JOIN applicantdetails ad ON ap.userID = ad.userID
                    LEFT JOIN rentTrench rt ON rt.id = ad.rentTrench
                    WHERE ap.isDeleted = :del";
        $vars = array(
            ':del' => "n",
        );
        $applicantdata = $db->exec($query, $vars);

        $f3->set('data', $applicantdata);

        $f3->set('content', '../ui/admin/applications/index.html');

        echo \Template::instance()->render('admin_template.htm');
    }

    public function updateApplicationStatus($f3, $params)
    {
        $this->authCheck($f3);
        $status = $params['status'];
        $attributes = ["applicationStatus" => $status, "dateUpdated" => date("Y-m-d")];
        $definition = $this->getDefinition($attributes);
        try {
            $idArray = ['id = ?', $params['id']];

            $user = $this->getRepository('applications')->updateRecord($idArray, $definition);
            $f3->set('SESSION.message', array('msg' => 'Application updated successfully!', 'alert' => 'success'));

            $f3->reroute("/admin/applications");
        } catch (Exception $exception) {
            echo json_encode($exception->getMessage());
        }

    }

    public function deleteApplication($f3, $params)
    {
        $this->authCheck($f3);
        $attributes = ["isDeleted" => 'y', "dateUpdate" => date("Y-m-d")];
        $definition = $this->getDefinition($attributes);
        try {
            $idArray = ['id = ?', $params['id']];

            $user = $this->getRepository('applications')->updateRecord($idArray, $definition);
            $f3->set("SESSION.count",1);
            $f3->set('SESSION.message', array('msg' => 'Application deleted successfully!', 'alert' => 'success'));

            $f3->reroute("/admin/applications");
        } catch (Exception $exception) {
            echo json_encode($exception->getMessage());
        }

    }

    public function editApplication($f3, $params)
    {
        global $db;
        $this->authCheck($f3);
        $query = "SELECT dt.*  FROM applicantdetails ad LEFT JOIN applicantdata dt ON ad.userID = dt.userID WHERE ad.userID = :userID";
        $vars = array(
            ':userID' => $params['id'],
        );
        $userdata = $db->exec($query, $vars);

        $f3->set('userID', $params['id']);

        if ($f3->get('POST')) {
            $folder = "user_" . $params['id'];

            $docsArr = [];
            //complex Documents
            foreach ($_FILES as $formFieldName => $value) {
                //$resultUrl = $this->basicUpload($f3,$formFieldName);
                if($value['name'] != '') {
                    $resultUrl = $this->fileUpload($f3, $formFieldName, $folder);
                    $docsArr[] = $formFieldName;
                }
            }

            $count = 0;
            //var_dump($docsArr);


            foreach ($resultUrl as $fileUrl => $bool) {
                //var_dump($resultUrl);

                //$f3->set("POST.$formFieldName", $fileUrl);
                $update = false;
                $formFieldName = $docsArr[$count];

                foreach ($userdata as $row) {
                    if ($row['dataLabel'] == $formFieldName) {
                        $update = true;
                    }
                }
                if ($update) {
                    $query = "UPDATE applicantdata SET data=:data WHERE userID = :userID AND dataLabel=:dataLabel";
                    $vars = array(
                        ':data' => $fileUrl,
                        ':userID' =>  $params['id'],
                        ':dataLabel' => $formFieldName,
                    );
                    $applicantdata = $db->exec($query, $vars);
                } else {
                    $query = "INSERT INTO applicantdata (userID, data, dataType, dataLabel, dateCreated, dateUpdated)
                                VALUES (:userID, :data, :dataType, :dataLabel, NOW(), NOW())";
                    $vars = array(
                        ':userID' =>  $params['id'],
                        ':data' => $fileUrl,
                        ':dataType' => "document",
                        ':dataLabel' => $formFieldName,
                    );
                    $applicantdata = $db->exec($query, $vars);
                }
                $count++;
            }

        }

        $query = "SELECT dt.*  FROM applicantdetails ad LEFT JOIN applicantdata dt ON ad.userID = dt.userID WHERE ad.userID = :userID";
        $vars = array(
            ':userID' => $params['id'],
        );
        $userdata = $db->exec($query, $vars);

        $newDocsArr = [];

        foreach($userdata as $row){
            $title = preg_replace("([A-Z])", " $0", $row['dataLabel']);

            $ext = substr($row['data'], -5);
            $extArr = explode(".", $ext);

            $imgArr = ['jpg', 'jpeg', 'png'];
            $docArr = ['pdf', 'docx', 'doc'];

            if (in_array(strtolower($extArr[1]), $imgArr)) {
                $docType = "img";
            }elseif (in_array(strtolower($extArr[1]), $docArr)) {
                $docType = "doc";
            }
            else {
                $docType = "null";
            }


            $newDocsArr[] = ["docType" => $docType, "id"=>$row['id'],"dataType"=>$row['dataType'],
                            "dataLabel"=>$row['dataLabel'],"data"=>$row['data'],"title"=>$title];

        }
        
        $f3->set('documentDetails', $newDocsArr);



        $f3->set('profilePercentage', round($profilePerc));

        $query = "SELECT *  from applicantdetails WHERE userID = :id";
        $vars = array(
            ':id' => $params['id'],
        );
        $results = $db->exec($query, $vars);

        $arrCheckList = [];

        $profilePerc = 0;

        $arrCheckList["personalInfo"] = ["firstName", "lastName", "idPassportNo", "dob", "address", "maritalStatus","bankName","branchCode","accountNumber","accountType"];
        $arrCheckList["contactInfo"] = ["email", "phone", "altPhone"];
        $arrCheckList["employerInfo"] = ["occupation", "employer", "employerContact", "totalIncome", "totalExpenses"];
        $arrCheckList["emergencyInfo"] = ["nextOfKin", "nextOfKinContact"];
        $arrCheckList["documentInfo"] = ["idDocument", "payslip", "bankStatement","proofOfPayment"];

        foreach ($arrCheckList as $key => $list) {
            foreach ($list as $column) {
                if ($key == "personalInfo" && $results[0][$column] != "") {
                    $profilePerc = $profilePerc + 2;
                    $personalPerc = $personalPerc + 2;
                }
                if ($key == "contactInfo" && $results[0][$column] != "") {
                    $profilePerc = $profilePerc + 6.66;
                    $contactPerc = $contactPerc + 6.66;
                }
                if ($key == "employerInfo" && $results[0][$column] != "") {
                    $profilePerc = $profilePerc + 4;
                    $employerPerc = $employerPerc + 4;
                }
                if ($key == "emergencyInfo" && $results[0][$column] != "") {
                    $profilePerc = $profilePerc + 10;
                    $emergencyPerc = $emergencyPerc + 10;
                }
                if ($key == "documentInfo") {
                    foreach ($userdata as $row) {

                        if ($row['dataLabel'] == $column) {
                            $dataArr = explode(",", $row['data']);
                            if (!in_array('Error', $dataArr) && $row['data'] != "") {
                                $profilePerc = $profilePerc + 5;
                                $documentPerc = $documentPerc + 5;
                            }

                        }
                    }
                }
            }
        }

        $f3->set('profilePercentage', round($profilePerc));

        $f3->set('personalPerc', round($personalPerc));
        $f3->set('contactPerc', round($contactPerc));
        $f3->set('employerPerc', round($employerPerc));
        $f3->set('emergencyPerc', round($emergencyPerc));
        $f3->set('documentPerc', round($documentPerc));

        $f3->set('POST', $results[0]);
        $f3->set('content', '../ui/admin/users/profile.html');

        echo \Template::instance()->render('admin_template.htm');
    }


    public function viewApplication($f3, $params)
    {
        global $db;
        $this->authCheck($f3);

        $applicationID = $params['id'];

        $f3->set('applicationID', $applicationID);

        $query = "SELECT userID  FROM applications WHERE id = :id";
        $vars = array(
            ':id' => $applicationID,
        );
        $applications = $db->exec($query, $vars);

        if($f3->get("POST")){
            $userID = (integer) $applications[0]['userID'];
            $this->verifyApplication($f3, $userID);
        }

        $query = "SELECT ad.*, uc.id as checkID, uc.title, uc.isVerified from applicantdetails ad LEFT JOIN userChecklist uc ON uc.userID = ad.userID WHERE ad.userID = :userID";
        $vars = array(
            ':userID' => $applications[0]['userID'],
        );
        $results = $db->exec($query, $vars);

        $f3->set('userID', $results[0]['userID']);

        $query = "SELECT dt.*  FROM applicantdetails ad LEFT JOIN applicantdata dt ON ad.userID = dt.userID WHERE ad.userID = :userID";
        $vars = array(
            ':userID' => $applications[0]['userID'],
        );
        $userdata = $db->exec($query, $vars);

        $arrCheckList = [];

        $personalDetails = [];
        $contactDetails = [];
        $employerDetails = [];
        $emergencyDetails = [];
        $documentDetails = [];

        $arrCheckList["personalInfo"] = ["firstName", "lastName", "idPassportNo", "dob", "address", "maritalStatus","bankName","branchCode","accountNumber","accountType"];
        $arrCheckList["contactInfo"] = ["email", "phone", "altPhone"];
        $arrCheckList["employerInfo"] = ["occupation", "employer", "employerContact", "totalIncome", "totalExpenses"];
        $arrCheckList["emergencyInfo"] = ["nextOfKin", "nextOfKinContact"];
        $arrCheckList["documentInfo"] = ["idDocument", "payslip", "bankStatement", "proofOfPayment","creditReport","rentDeposit"];

        $isColumn = [];

        foreach ($arrCheckList as $key => $arrlist) {
           
            foreach ($arrlist as $column) {
                $isVerified = 'n';
                $val = '';
                 
                foreach ($results as $newKey => $list) {
                        array_push($isColumn, $column);
                        if ($list["title"] == $column) {
                            $isVerified = $list["isVerified"];
                        }
                        if(isset($list[$column])) $val = $list[$column];
                        $title = preg_replace("([A-Z])", " $0", $column);
                    }
                    if ($key == "personalInfo") {
                        $personalDetails[] = ["title" => $title, "key" => $column, "value" => $val, "isVerified" => $isVerified, "applicantData" => $results[0]["applicantData"]];
                    }
                    if ($key == "contactInfo") {
                        array_push($isColumn, $column);
                        if ($list["title"] == $column) {
                            $isVerified = $list["isVerified"];
                        }

                        $contactDetails[] = ["title" => $title, "key" => $column, "value" => $results[0][$column], "isVerified" => $isVerified, "applicantData" => $results[0]["applicantData"]];
                    }
                    if ($key == "employerInfo") {
                        array_push($isColumn, $column);
                        if ($list["title"] == $column) {
                            $isVerified = $list["isVerified"];
                        }

                        $employerDetails[] = ["title" => $title, "key" => $column, "value" => $results[0][$column], "isVerified" => $isVerified, "applicantData" => $results[0]["applicantData"]];
                    }
                    if ($key == "emergencyInfo") {
                        array_push($isColumn, $column);
                        if ($list["title"] == $column) {
                            $isVerified = $list["isVerified"];
                        }

                        $emergencyDetails[] = ["title" => $title, "key" => $column, "value" => $results[0][$column], "isVerified" => $isVerified, "applicantData" => $results[0]["applicantData"]];
                    }
                    if ($key == "documentInfo") {
                        array_push($isColumn, $column);

                        foreach ($userdata as $row) {

                            if ($row['dataLabel'] == $column) {
                                $dataArr = explode(",", $row['data']);
                                if (!in_array('Error', $dataArr) && $row['data'] != "") {
                                    if ($list["title"] == $column) {
                                        $isVerified = $list["isVerified"];
                                    }

                                    $ext = substr($row['data'],-5);
                                    $extArr = explode(".",$ext);


                                    $imgArr = ['jpg', 'jpeg', 'png'];
                                    $docArr = ['pdf','docx','doc'];

                                    if (in_array(strtolower($extArr[1]), $imgArr)) {
                                        $docType = "img";
                                    }
                                    elseif (in_array(strtolower($extArr[1]), $docArr)) {
                                        $docType = "doc";
                                    }
                                    else{

                                       $docType = "null";
                                    }

                                    $documentDetails[] = ["docType" => $docType, "title" => $title, "key" => $column, "value" => $row['data'], "isVerified" => $isVerified, "applicantData" => $results[0]["applicantData"]];

                                }
                                else if(in_array('Error', $dataArr)){
                                    $documentDetails[] = ["docType" => "null", "title" => $title, "key" => $column, "value" => "Error on Document Upload", "isVerified" => $isVerified, "applicantData" => $results[0]["applicantData"]];
                                }
    

                            }
                            
                    }
                }
            }
        }

        $f3->set('personalDetails', $personalDetails);
        $f3->set('contactDetails', $contactDetails);
        $f3->set('employerDetails', $employerDetails);
        $f3->set('emergencyDetails', $emergencyDetails);
        $f3->set('documentDetails', $documentDetails);

        $f3->set('content', '../ui/admin/applications/viewApplication.html');

        echo \Template::instance()->render('admin_template.htm');
    }

    public function verifyApplication($f3, $userID)
    {
        global $db;

        try {
            //$f3->set('SESSION.count', 1);
            $adminID = $f3->get('SESSION.USER.id');
            $update = true;

            //if post lets save
                $arrCheckList = [];

                $arrCheckList["personalInfo"] = ["firstName", "lastName", "idPassportNo", "dob", "address", "maritalStatus", "bankName", "branchCode", "accountNumber", "accountType"];
                $arrCheckList["contactInfo"] = ["email", "phone", "altPhone"];
                $arrCheckList["employerInfo"] = ["occupation", "employer", "employerContact", "totalIncome", "totalExpenses"];
                $arrCheckList["emergencyInfo"] = ["nextOfKin", "nextOfKinContact"];
                $arrCheckList["documentInfo"] = ["idDocument", "payslip", "bankStatement", "proofOfPayment","creditReport"];

                //loop through array for the submitted form type
                foreach ($arrCheckList[$f3->get('POST.formType')] as $key => $column) {

                    $isVerified = 'n';
                    $update = false;

                    //if the field is in post its a verification
                    foreach ($f3->get('POST') as $feild => $value) {
                        if ($feild == $column) {
                            $isVerified = 'y';
                        }

                    }
                    //update or insert.
                    $query = "SELECT * FROM userChecklist WHERE userID = :userID AND title = :title";
                    $varsArr = array(
                        ':userID' => $userID,
                        ':title' => $column,
                    );
                    $userdata = $db->exec($query, $varsArr);
                    if (isset($userdata[0]['title'])) {
                        $update = true;
                    }

                    if ($update) {
                        $query = "UPDATE userChecklist SET approvedBy=:adminID, isVerified=:isVerified, dateUpdated=NOW() WHERE id=:id";
                        $varsArr = array(
                            ':adminID' => $adminID,
                            ':isVerified' => $isVerified,
                            ':id' => $userdata[0]['id'],
                        );
                        $userChecklist = $db->exec($query, $varsArr);
                    } else {
                        $query = "INSERT INTO userChecklist (userID, approvedBy, title, isVerified, dateCreated, dateUpdated)
                                          VALUES (:userID, :adminID, :title, :isVerified, NOW(), NOW())";
                        $varsArr = array(
                            ':userID' => $userID,
                            ':adminID' => $adminID,
                            ':title' => $column,
                            ':isVerified' => $isVerified,
                        );
                        $userChecklist = $db->exec($query, $varsArr);
                    }
                   

                }

                $f3->set('SESSION.message', array('msg' => 'Data saved successfully!', 'alert' => 'info'));

        } catch (Exception $exception) {
            echo json_encode($exception->getMessage());
        }
    }

    public function approveApplication($f3, $params)
    {
        global $db;
        $this->authCheck($f3);

        $f3->set("SESSION.count",1);

        if($f3->get("POST")){

            $applicationID = $f3->get("POST.applicationID");

            $query = "SELECT md.*, ad.firstName, ad.email, ap.userID  FROM applications ap
                        LEFT JOIN metaData md ON md.mainID = ap.complexID
                        LEFT JOIN applicantdetails ad ON ad.userID = ap.userID
                        WHERE ap.id = :id";
            $vars = array(
                ':id' => $applicationID,
            );
            $applicationsData = $db->exec($query, $vars);

            $userID = $applicationsData[0]['userID'];


            $query = "UPDATE applications SET applicationStatus = :applicationStatus, unitID = :unitID  WHERE id = :id";
            $vars = array(
                ':unitID' => $f3->get("POST.unit"),
                ':applicationStatus' => "a",
                ':id' => $applicationID,
            );
            $applicationsUpdate = $db->exec($query, $vars);

            $query = "UPDATE units SET occupied= :occupied, applicantID = :applicantID WHERE id = :id";
            $vars = array(
                ':occupied' => "y",
                ':applicantID' => $userID,
                ':id' => $f3->get("POST.unit"),
            );
            $unitUpdate = $db->exec($query, $vars);

            
            $subject = "Application Approved";

            $userdata = [];

            foreach($applicationsData as $row){

                $userdata[$row["dataLabel"]] = $row["data"];
                $userdata["email"] = $row["email"];
                $userdata["firstName"] = $row["firstName"];
            }

            $this->dispatchEmail($userdata, $subject, "approval");

            $f3->set('SESSION.message', array('msg' => 'Application approved successfully! Email has been sent to applicant', 'alert' => 'success'));


            $f3->reroute("/admin/applications");

        }

    }

    public function verifyProofOfPayment($f3, $params)
    {
        global $db;
        $this->authCheck($f3);

        $f3->set("SESSION.count",1);

        if($f3->get("POST")){
            

            $applicationID = $f3->get("POST.applicationID");

            $verification = $f3->get("POST.verification");

            $query = "SELECT md.*, ad.firstName, ad.email, ap.userID  FROM applications ap
            LEFT JOIN metaData md ON md.mainID = ap.complexID
            LEFT JOIN applicantdetails ad ON ad.userID = ap.userID
            WHERE ap.id = :id";
            $vars = array(
                ':id' => $applicationID,
            );
            $applicationsData = $db->exec($query, $vars);

            $userID = $applicationsData[0]['userID'];

            $query = "INSERT INTO userChecklist (userID, approvedBy, title, isVerified, dateCreated, dateUpdated)
                    VALUES (:userID, :approvedBy, :title, :isVerified, NOW(), NOW())";
            $vars = array(':userID' =>$userID,
                        ':approvedBy' => $f3->get('SESSION.USER.id'),
                        ':title' => "proofOfPayment",
                        ':isVerified' => $verification,
                    );
            $userChecklist = $db->exec($query, $vars);

            if($verification == "y"){
                $query = "UPDATE applications SET applicationFee = :applicationFee WHERE id = :id";
                $vars = array(
                    ':applicationFee' => "paid",
                    ':id' => $applicationID,
                );
                $applicationsUpdate = $db->exec($query, $vars);


                //xds verification
                $params['userID'] =  $userID;
                $this->xdsVerification($f3, $params);
                $f3->reroute("/admin/application/view/$applicationID");

            }
            else 
                $f3->reroute("/admin/applications");


        }

    }

    public function uploadDocuments($f3, $params)
    {
        global $db;
        $this->authCheck($f3);

        $folder = "csv_uploads";

        if (!is_dir("uploads/csv_uploads")) {
            mkdir("uploads/csv_uploads", 0777, true);
            chmod("uploads/csv_uploads", 0777);
        }

        $formFieldName = $params['fileName'];

        $resultUrl = $this->fileUpload($f3, $formFieldName, $folder);

        $dataArr = [];

        foreach ($resultUrl as $fileUrl => $bool) {
            if (!$bool) {
                $f3->set('SESSION.message', array('msg' => 'Invalid file imported! Only CSV files can be uploaded', 'alert' => 'error'));
            } else {
                if ($handle = opendir("uploads/csv_uploads")) {
                    //echo "Directory handle: $handle\n";
                    $row = 0;

                    // This is the correct way to loop over the directory.
                    while (false !== ($entry = readdir($handle))) {
                        $temp = explode(".", $entry);
                        if (isset($temp[1])) {
                            $ext = $temp[1];
                        }

                        if ($ext == "csv") {
                            echo "$entry<br>";
                            $file = $entry;

                            $flag = true;
                            //
                            if (($csvhandle = fopen("uploads/csv_uploads/$file", "r")) !== false) {
                                while (($data = fgetcsv($csvhandle, 1000, ";")) !== false) {
                                    if ($row > 0) {
                                        if (!empty($data[0])) {
                                            $dataArr[] = array("unitName" => $data[0], "complexID" => $data[1], "size" => $data[2], "trenchID" => $data[3], "rental" => $data[4], "description" => $data[5],
                                            );
                                        }
                                    }
                                    $row++;
                                }
                                fclose($csvhandle);
                            }

                            $query = "INSERT INTO `units` (`unitName`, `complexID`, `trenchID`, `description`, `size`, `rental`, `dateCreated`, `dateUpdated`)
                                        VALUES (:unitName, :complexID,:trenchID,:description,:size,:rental, NOW(), NOW())";

                            if (sizeof($dataArr) > 0) {
                                foreach ($dataArr as $key => $value) {

                                    //new DB update
                                    $vars = array(
                                        ':unitName' => $value["unitName"],
                                        ':complexID' => $value["complexID"],
                                        ':trenchID' => $value["trenchID"],
                                        ':description' => $value["description"],
                                        ':size' => $value["size"],
                                        ':rental' => $value["rental"],

                                    );
                                    $unitData = $db->exec($query, $vars);
                                }
                            }

                            if (!is_dir("uploads/csv_archive")) {
                                mkdir("uploads/csv_archive", 0777, true);
                                chmod("uploads/csv_archive", 0777);
                            }
                            //unlink("csv_uploads/".$file);
                            $date = date("Y-m-d");
                            rename("uploads/csv_uploads/$file", "uploads/csv_archive/$date.$file");

                        }

                    }
                    closedir($handle);
                    $f3->set('SESSION.message', array('msg' => 'Units imported successfully!', 'alert' => 'success'));

                } else {
                    $f3->set('SESSION.message', array('msg' => 'Unable to import csv file', 'alert' => 'error'));

                }
            }
        }
        $f3->reroute("/admin/units");

    }


    public function verifyForm($f3, $params) {
        global $db;
        try {
            //$f3->set('SESSION.count', 1);
            $formtype = $params['formtype'];
            $userID = $f3->get('SESSION.USER.id');
            $update = true;

            if ($f3->get('POST')) {
                $arrCheckList = [];

                $arrCheckList["personalInfo"] = ["firstName", "lastName", "idPassportNo", "dob", "address", "maritalStatus","bankName","branchCode","accountNumber","accountType"];
                $arrCheckList["contactInfo"] = ["email", "phone", "altPhone"];
                $arrCheckList["employerInfo"] = ["occupation", "employer", "employerContact", "totalIncome", "totalExpenses"];
                $arrCheckList["emergencyInfo"] = ["nextOfKin", "nextOfKinContact"];
                $arrCheckList["documentInfo"] = ["idDocument", "payslip", "bankStatement","proofOfPayment"];

                $update = false;

                
                foreach ($f3->get('POST') as $column => $value) {
                   
                    $query = "SELECT * FROM userChecklist WHERE userID = :userID AND title = :title";
                    $vars = array(
                        ':userID' => $f3->get("POST.userID"),
                        ':title' => $column,
                    );
                    $userdata = $db->exec($query, $vars);
    
                    if(sizeof($userdata) > 0) $update = true;
                        //$dataArr = explode("_", $f3->get("POST.$column"));
                        if ($column != "userID") {
                            if ($update) {
                                $query = "UPDATE userChecklist SET approvedBy=:adminID, isVerified=:isVerified WHERE id=:id";
                                $vars = array(
                                    ':adminID' =>$userID,
                                    ':isVerified' =>"y",
                                    ':id' => $userdata[0]['id'],
                                );
                                $userChecklist = $db->exec($query, $vars);                            
                            }
                            else {
                                $query = "INSERT INTO userChecklist (userID, approvedBy, title, isVerified, dateCreated, dateUpdated)
                                          VALUES (:userID, :adminID, :title, :isVerified, NOW(), NOW())";
                                $vars = array(
                                    ':userID' => $f3->get("POST.userID"),
                                    ':adminID' =>$userID,
                                    ':title' => $column,
                                    ':isVerified' => "y",
                                );
                                $userChecklist = $db->exec($query, $vars);
                            }
                        }
                        // if ($key == "contactInfo" && $f3->get("POST.$column") != "") {
                        //     if ($update) {
                        //         $query = "UPDATE userChecklist SET isVerified=:isVerified WHERE id=:id";
                        //         $vars = array(
                        //             ':isVerified' =>"y",
                        //             ':id' => $userdata[0]['id'],
                        //         );
                        //         $userChecklist = $db->exec($query, $vars);                            
                        //     }
                        //     else {
                        //         $query = "INSERT INTO userChecklist (userID, title, isVerified, dateCreated, dateUpdated)
                        //                   VALUES (:userID, :title, :isVerified, NOW(), NOW())";
                        //         $vars = array(
                        //             ':userID' => $f3->get("POST.userID"),
                        //             ':title' => $column,
                        //             ':isVerified' => "y",
                        //         );
                        //         $userChecklist = $db->exec($query, $vars);
                        //     }
                        // }
                        // if ($key == "employerInfo" && $f3->get("POST.$column") != "") {
                        //     if ($update) {
                        //         $query = "UPDATE userChecklist SET isVerified=:isVerified WHERE id=:id";
                        //         $vars = array(
                        //             ':isVerified' =>"y",
                        //             ':id' => $userdata[0]['id'],
                        //         );
                        //         $userChecklist = $db->exec($query, $vars);                            
                        //     }
                        //     else {
                        //         $query = "INSERT INTO userChecklist (userID, title, isVerified, dateCreated, dateUpdated)
                        //                   VALUES (:userID, :title, :isVerified, NOW(), NOW())";
                        //         $vars = array(
                        //             ':userID' => $f3->get("POST.userID"),
                        //             ':title' => $column,
                        //             ':isVerified' => "y",
                        //         );
                        //         $userChecklist = $db->exec($query, $vars);
                        //     }
                        // }
                        // if ($key == "emergencyInfo" && $f3->get("POST.$column") != "") {
                        //     if ($update) {
                        //         $query = "UPDATE userChecklist SET isVerified=:isVerified WHERE id=:id";
                        //         $vars = array(
                        //             ':isVerified' =>"y",
                        //             ':id' => $userdata[0]['id'],
                        //         );
                        //         $userChecklist = $db->exec($query, $vars);                            
                        //     }
                        //     else {
                        //         $query = "INSERT INTO userChecklist (userID, title, isVerified, dateCreated, dateUpdated)
                        //                   VALUES (:userID, :title, :isVerified, NOW(), NOW())";
                        //         $vars = array(
                        //             ':userID' => $f3->get("POST.userID"),
                        //             ':title' => $column,
                        //             ':isVerified' => "y",
                        //         );
                        //         $userChecklist = $db->exec($query, $vars);
                        //     }
                        // }
                        // if ($key == "documentInfo") {
                        //     foreach ($userdata as $row) {
        
                        //         if ($row['dataLabel'] == $column) {
                        //             $dataArr = explode(",", $row['data']);
                        //             if (!in_array('Error', $dataArr) && $row['data'] != "") {
                        //                 if ($update) {
                        //                     $query = "UPDATE userChecklist SET isVerified=:isVerified WHERE id=:id";
                        //                     $vars = array(
                        //                         ':isVerified' =>"y",
                        //                         ':id' => $userdata[0]['id'],
                        //                     );
                        //                     $userChecklist = $db->exec($query, $vars);                            
                        //                 }
                        //                 else {
                        //                     $query = "INSERT INTO userChecklist (userID, title, isVerified, dateCreated, dateUpdated)
                        //                               VALUES (:userID, :title, :isVerified, NOW(), NOW())";
                        //                     $vars = array(
                        //                         ':userID' => $f3->get("POST.userID"),
                        //                         ':title' => $column,
                        //                         ':isVerified' => "y",
                        //                     );
                        //                     $userChecklist = $db->exec($query, $vars);
                        //                 }
                        //             }
        
                        //         }
                        //     }
                        // }
                    //}
                }
                
                

                $f3->set('SESSION.message', array('msg' => 'Application updated successfully', 'alert' => 'success'));
            } else {
                $f3->set('SESSION.message', array('msg' => 'No Information was saved', 'alert' => 'error'));

            }

            $f3->reroute("/admin/aplication/view/".$params['id']);


        } catch (Exception $exception) {
            echo json_encode($exception->getMessage());
        }
    }

    /*
     * Documents
     */
    public function viewDocuments($f3)
    {
        $complex = $this->getRepository('documents')->getByAttribute("deleted","n");
        $f3->set('data', $complex);

        $f3->set('content', '../ui/admin/documents/index.html');

        echo \Template::instance()->render('admin_template.htm');
    }

    public function createDocuments($f3)
    {
        $this->authCheck($f3);
        global $db;
        try {
            $f3->set('POST.dateCreated', date("Y-m-d"));
            $f3->set('POST.dateUpdated', date("Y-m-d"));
            $attributes = $f3->get('POST');

            $formFieldName = "document";

            //foreach ($_FILES as $formFieldName => $value) {

                //$resultUrl = $this->basicUpload($f3,$formFieldName);
            $resultUrl = $this->fileUpload($f3, $formFieldName, $folder);

            //}

            $CheckList = ["idDocument", "bankStatement", "payslip"];
            $count = 0;

            foreach ($resultUrl as $fileUrl => $bool) {

                $query = "INSERT INTO documents (title, documentUrl, dateCreated, dateUpdated)
                                VALUES (:title, :documentUrl, NOW(), NOW())";
                $vars = array(
                    ':title' =>  $f3->get('POST.title'),
                    ':documentUrl' => $fileUrl
                );
                $documents = $db->exec($query, $vars);

            }
            

            $f3->set('SESSION.message', array('msg' => 'documents created successfully!', 'alert' => 'success'));

            $f3->reroute("/admin/documents");
        } catch (Exception $exception) {
            echo json_encode($exception->getMessage());
        }
    }

    public function deleteDocuments($f3, $params)
    {
        $this->authCheck($f3);
        $attributes = ["deleted"=>'y', "dateUpdated"=>date("Y-m-d")];
        $definition = $this->getDefinition($attributes);
        try { 
            $idArray = ['id = ?',$params['id']];

            $user = $this->getRepository('documents')->updateRecord($idArray, $definition);
            $f3->set('SESSION.message', array('msg' => 'Document deleted successfully!', 'alert' => 'success'));

            $f3->reroute("/admin/documents");
        } catch (Exception $exception) {
            echo json_encode($exception->getMessage());
        }
        
    }

    public function updateComplex($f3, $params)
    {
        $this->authCheck($f3);
        try {
            if($f3->get('POST')){
                $f3->set('POST.dateUpdated', date("Y-m-d"));
                $attributes = $f3->get('POST');
                $definition = $this->getDefinition($attributes);

                $idArray = ['id = ?',$params['id']];

                $user = $this->getRepository('documents')->updateRecord($idArray, $definition);

                $f3->reroute("/admin/documents");
            }

            $users = $this->getRepository('documents')->getByAttribute('id', $params['id']);
            $f3->set('POST', $users[0]);

            $f3->set('content', '../ui/admin/documents/updateDocuments.html');
            echo \Template::instance()->render('admin_template.htm');
            
        } catch (Exception $exception) {
            echo json_encode($exception->getMessage());
        }
    }
}
