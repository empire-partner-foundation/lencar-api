<?php

class TenantPortalController extends AppController
{

    //
    //  API Methods
    //

    /*
     * Tenants
     */
    public function index_tenant($f3)
    {
        try {
            $shopUnits = $this->getRepository('cps_shop_units')->getAllRecords();
            $unitCategories = $this->getRepository('cps_unit_categories')->getAllRecords();
            $tenants = $this->getRepository('cps_tenants')->getAllRecords();

            $f3->set('categories', $unitCategories);
            $f3->set('units', $shopUnits);
            $f3->set('tenants', $tenants);

            $f3->set('content', '../ui/tenant_portal/pages/tenant/index.html');

            echo \Template::instance()->render('admin_template.htm');
            echo Template::instance()->render('../ui/tenant_portal/template.htm');
        } catch (Exception $exception) {
            $f3->set('SESSION.message', array('msg' => $exception->getMessage(), 'alert' => 'error'));
            $f3->reroute("/tenant_portal/tenants");
        }
    }

    public function create_tenant($f3)
    {
        try {
            $attributes = $f3->get('POST');
            unset($attributes['shop_unit_id']);
            $newPassword = PasswordGenerator::generate();

            $userAttributes = array(
                'type' => 'user',
                'name' => $attributes['company_represenatative_name'],
                'surname' => $attributes['company_represenatative_surname'],
                'contact' => $attributes['company_represenatative_contact'],
                'email' => $attributes['company_represenatative_email'],
                'password' => hash('sha256', $newPassword),
                'surname' => $attributes['company_represenatative_surname'],
                'contact' => $attributes['company_represenatative_contact'],
                'email' => $attributes['company_represenatative_email'],
                'password' => hash('sha256', $newPassword),
            );

            // create/save default tenant user (login profile)
            $user = $this->getRepository('cps_users')->createRecord($userDefinition);

            // send new user login credentials
            $message = "Hi " . $userAttributes['name'] . ' ' . $userAttributes['password'] . '.
            Your new login credentials are : ' . $userAttributes['email'] . ' and the password is ' . $newPassword;
            $message = "Hi " . $userAttributes['name'] . ' ' . $userAttributes['password'] . '.
            Your new login credentials are : ' . $userAttributes['email'] . ' and the password is ' . $newPassword;

            $this->dispatchEmail('kmolotsi.developer@gmail.com', 'New User Signup', $message);

            $this->getRepository('cps_tenants')->createRecord($tenantDefinition);
            $f3->set('SESSION.message', array('msg' => 'Tenant created successfully!', 'alert' => 'success'));

            $f3->reroute("/tenant_portal/tenants");
        } catch (Exception $exception) {
            $f3->set('SESSION.message', array('msg' => $exception->getMessage(), 'alert' => 'error'));
        }
    }

    public function deleteTenant($f3, $params)
    {
        try {
            $this->getRepository('cps_tenants')->deleteRecord($params['id']);
            $f3->set('SESSION.message', array('msg' => 'Tenant deleted successfully!', 'alert' => 'success'));

            $f3->reroute("/tenant_portal/tenants");
        } catch (Exception $exception) {
            echo json_encode($exception->getMessage());
        }
    }

    public function updateTenant($f3, $params)
    {
        try {
            $attributes = $f3->get('POST');
            $definition = $this->getDefinition($attributes);

            $this->getRepository('cps_tenants')->updateRecord($params['id'], $definition);

            $f3->reroute("/tenant_portal/tenants");
        } catch (Exception $exception) {
            echo json_encode($exception->getMessage());
        }
    }

    private function setKpiVariablesTenant($f3)
    {
        try {
            $kpi = array();

            $kpi['totalTenants'] = count($this->getRepository('cps_tenants')->getAllRecords());
            $kpi['inactiveTenants'] = count($this->getRepository('cps_tenants')->getByAttribute('active', 0));
            $kpi['tenantUnits'] = count($this->getRepository('cps_tenant_accounts')->getAllRecords());

            setlocale(LC_MONETARY, "");

            $f3->set('totalTenants', $kpi['totalTenants']);
            $f3->set('activeTenants', $kpi['activeTenants']);
            $f3->set('inactiveTenants', $kpi['inactiveTenants']);
            $f3->set('tenantUnits', $kpi['tenantUnits']);
            $f3->set('tenantUnits', $kpi['tenantUnits']);

        } catch (Exception $exception) {
            return $exception->getMessage();
        }
    }

    public function viewTenants($f3, $params)
    {
        //$this->authCheck($f3);
        //global $db;

        $f3->set('content', '../ui/admin/tenants/index.html');
        echo \Template::instance()->render('admin_template.htm');
    }

    /*
     * Fincance
     */
    public function viewFinance($f3)
    {
        //$shopUnits = $this->getRepository('cps_shop_units')->getAllRecords();
        //$unitCategories = $this->getRepository('cps_unit_categories')->getAllRecords();

        // set kpi variables
        //$this->setKpiVariablesUnit($f3);

        //$f3->set('categories', $unitCategories);
        //$f3->set('units', $shopUnits);
        $f3->set('content', '../ui/admin/finance/finance.html');

        echo \Template::instance()->render('admin_template.htm');
    }

    public function viewInvoices($f3)
    {
        //$shopUnits = $this->getRepository('cps_shop_units')->getAllRecords();
        //$unitCategories = $this->getRepository('cps_unit_categories')->getAllRecords();

        // set kpi variables
        //$this->setKpiVariablesUnit($f3);

        //$f3->set('categories', $unitCategories);
        //$f3->set('units', $shopUnits);
        $f3->set('content', '../ui/admin/finance/invoices.html');

        echo \Template::instance()->render('admin_template.htm');
    }

    public function updateForm($f3, $params)
    {
        $this->authCheck($f3);
        try {

            $f3->set('form_id', $params['id']);

            $f3->set('content', '../ui/admin/forms/updateForm.html');
            echo \Template::instance()->render('admin_template.htm');

        } catch (Exception $exception) {
            echo json_encode($exception->getMessage());
        }
    }

    public function deleteForm($f3, $params)
    {
        try {
            $this->getRepository('forms')->deleteRecord($params['id']);
            $f3->set('SESSION.message', array('msg' => 'Form deleted successfully!', 'alert' => 'success'));

            $f3->reroute("/admin/forms");
        } catch (Exception $exception) {
            echo json_encode($exception->getMessage());
        }
    }

    /*
     * viewForms
     */
    public function viewForms($f3)
    {
        global $db;
        $query = "SELECT *, IF(state=1, 'checked', '') AS status, (SELECT COUNT(id) FROM formsubmissions WHERE forms.id = formID) AS results FROM forms WHERE id > :value";
        $vars = array(
            ':value' => 0,
        );
        $forms = $db->exec($query, $vars);

        $query = "SELECT * FROM formsubmissions";
        $submissions = $db->exec($query);

        $query = "SELECT * FROM forms WHERE state = 1";
        $active_forms = $db->exec($query);

        $query = "SELECT * FROM forms WHERE state = 0";
        $inactive_forms = $db->exec($query);

        $f3->set('forms', $forms);
        $f3->set('total_forms', count($forms));
        $f3->set('total_submissions', count($submissions));
        $f3->set('active_forms', count($active_forms));
        $f3->set('inactive_forms', count($inactive_forms));

        $f3->set('content', '../ui/admin/forms/index.html');

        echo \Template::instance()->render('admin_template.htm');
    }

    /*
     * viewSubmissions
     */
    public function viewResults($f3, $params)
    {
        //get form submissions
        global $db;
        $query = "SELECT * FROM fields WHERE formID = :formID ORDER BY id ASC";
        $vars = array(
            ':formID' => $params['id'],
        );
        $fields = $db->exec($query, $vars);

        $query = "SELECT * FROM formsubmissions WHERE formID = :formID ORDER BY id ASC";
        $vars = array(
            ':formID' => $params['id'],
        );
        $submissions = $db->exec($query, $vars);

        $rows = array();
        $user_rows = array();
        for ($i = 0; $i < count($submissions); $i++) {

            for ($x = 0; $x < count($fields); $x++) {
                # code...
                $query = "SELECT * FROM applicantdata WHERE fieldID = :fieldID AND submissionID = :submissionID ORDER BY id ASC";
                $vars = array(
                    ':fieldID' => $fields[$x]['id'],
                    ':submissionID' => $submissions[$i]['id'],
                );
                $user_data = $db->exec($query, $vars);
                $rows[$x] = $user_data;
            }
            $user_rows[$i] = $rows;
        }

        $f3->set('fields', $fields);
        $f3->set('submissions', $submissions);
        $f3->set('data', $user_rows);
        $f3->set('rowCount', count($user_rows));
        $f3->set('fieldsCount', count($fields));
        $f3->set('content', '../ui/admin/forms/results.html');

        echo \Template::instance()->render('admin_template.htm');
    }

    /*
     * viewChecklist
     */
    public function viewChecklist($f3)
    {
        //$shopUnits = $this->getRepository('cps_shop_units')->getAllRecords();
        //$unitCategories = $this->getRepository('cps_unit_categories')->getAllRecords();

        // set kpi variables
        //$this->setKpiVariablesUnit($f3);

        //$f3->set('categories', $unitCategories);
        //$f3->set('units', $shopUnits);
        $f3->set('content', '../ui/admin/checklist/checklist.html');

        echo \Template::instance()->render('admin_template.htm');
    }

    /*
     * Units Category/Complex
     */
    public function viewComplex($f3)
    {
        $complex = $this->getRepository('complex')->getByAttribute("deleted", "n");
        $f3->set('data', $complex);

        $f3->set('content', '../ui/admin/housing/complex.html');

        echo \Template::instance()->render('admin_template.htm');
    }

    public function createComplex($f3)
    {
        $this->authCheck($f3);
        try {
            $f3->set('POST.dateCreated', date("Y-m-d"));
            $f3->set('POST.dateUpdated', date("Y-m-d"));
            $attributes = $f3->get('POST');

            $definition = $this->getDefinition($attributes);
            $this->getRepository('complex')->createRecord($definition);

            $query = "INSERT INTO complex (complexName, orgID, description, address, dateCreated, dateUpdated)
                                VALUES (:complexName, :orgID, :description, :address, NOW(), NOW())";
            $vars = array(
                ':complexName' => $f3->get("POST.complexName"),
                ':orgID' => 1,
                ':description' => $f3->get("POST.description"),
                ':description' => $f3->get("POST.address"),
            );
            $complex = $db->exec($query, $vars);

            $mainID = $db->lastInsertId();

            $docsArr = [];
            $folder = "complex_".$mainID;


            //complex Documents
            foreach ($_FILES as $formFieldName => $value) {
                //$resultUrl = $this->basicUpload($f3,$formFieldName);
                $resultUrl = $this->fileUpload($f3, $formFieldName, $folder);
                $docsArr[] = $formFieldName;
            }

            $count = 0;

            foreach ($resultUrl as $fileUrl => $bool) {
                //$f3->set("POST.$formFieldName", $fileUrl);
                $update = false;
                $formFieldName = $docsArr[$count];

                $query = "INSERT INTO metaData (mainID, dataType, data, dataLabel, dateCreated, dateUpdated)
                        VALUES (:mainID, :dataType, :data, :dataLabel, NOW(), NOW())";
                $vars = array(
                    ':mainID' => $mainID,
                    ':dataType' => "document",
                    ':data' => $fileUrl,
                    ':dataLabel' => $formFieldName,
                );
                $docsData = $db->exec($query, $vars);
                $count++;
            }

            $f3->set('SESSION.message', array('msg' => 'complex created successfully!', 'alert' => 'success'));

            $f3->reroute("/admin/complex");
        } catch (Exception $exception) {
            echo json_encode($exception->getMessage());
        }
    }

    public function deleteComplex($f3, $params)
    {
        $this->authCheck($f3);
        $attributes = ["deleted" => 'y', "dateUpdated" => date("Y-m-d")];
        $definition = $this->getDefinition($attributes);
        try {
            $idArray = ['id = ?', $params['id']];

            $user = $this->getRepository('complex')->updateRecord($idArray, $definition);
            $f3->set('SESSION.message', array('msg' => 'Complex deleted successfully!', 'alert' => 'success'));

            $f3->reroute("/admin/complex");
        } catch (Exception $exception) {
            echo json_encode($exception->getMessage());
        }

    }

    public function updateComplex($f3, $params)
    {
        $this->authCheck($f3);
        global $db;
        try {
            if ($f3->get('POST')) {
                $f3->set('POST.dateUpdated', date("Y-m-d"));
                $attributes = $f3->get('POST');
                $definition = $this->getDefinition($attributes);

                $idArray = ['id = ?', $params['id']];

                $user = $this->getRepository('complex')->updateRecord($idArray, $definition);

                //complex Documents
                if (isset($_FILES)) {

                    $docsArr = [];
                    $folder = "complex_".$params['id'];

                    foreach ($_FILES as $formFieldName => $value) {
                        //$resultUrl = $this->basicUpload($f3,$formFieldName);
                        if($value['name'] != '') {
                            $resultUrl = $this->fileUpload($f3, $formFieldName, $folder);
                            $docsArr[] = $formFieldName;
                        }

                    }

                    $count = 0;
                    if (sizeof($resultUrl) > 0) {
                        foreach ($resultUrl as $fileUrl => $bool) {
                            //$f3->set("POST.$formFieldName", $fileUrl);
                            $update = false;
                            $formFieldName = $docsArr[$count];

                            $query = "SELECT * FROM metaData WHERE mainID = :mainID";
                            $vars = array(
                                ':mainID' => $params['id'],
                            );
                            $complexDocuments = $db->exec($query, $vars);

                            foreach ($complexDocuments as $row) {
                                if ($row['dataLabel'] == $formFieldName) {
                                    $update = true;
                                }
                            }

                            if ($update) {
                                $query = "UPDATE metaData SET data=:data, dateUpdated = NOW() WHERE mainID = :mainID AND dataLabel=:dataLabel";
                                $vars = array(
                                    ':data' => $fileUrl,
                                    ':mainID' => $params['id'],
                                    ':dataLabel' => $formFieldName,
                                );
                                $docsData = $db->exec($query, $vars);
                            } else {
                                $query = "INSERT INTO metaData (mainID, datatype, data, dataLabel, dateCreated, dateUpdated)
                                        VALUES (:mainID, :datatype, :data, :dataLabel, NOW(), NOW())";
                                $vars = array(
                                    ':mainID' => $params['id'],
                                    ':datatype' => "document",
                                    ':data' => $fileUrl,
                                    ':dataLabel' => $formFieldName,
                                );
                                $docsData = $db->exec($query, $vars);
                            }
                            $count++;
                        }
                    }
                }

                $f3->reroute("/admin/complex");
            }

            $query = "SELECT * FROM metaData WHERE mainID = :mainID";
            $vars = array(
                ':mainID' => $params['id'],
            );
            $Documents = $db->exec($query, $vars);

            $f3->set('complexDocuments', $Documents);

            $users = $this->getRepository('complex')->getByAttribute('id', $params['id']);
            $f3->set('POST', $users[0]);

            $f3->set('content', '../ui/admin/housing/updateComplex.html');
            echo \Template::instance()->render('admin_template.htm');

        } catch (Exception $exception) {
            echo json_encode($exception->getMessage());
        }
    }

    /*
     * viewTrench
     */
    public function viewTrench($f3)
    {
        $rentTrench = $this->getRepository('rentTrench')->getByAttribute("deleted", "n");
        $f3->set('data', $rentTrench);

        $f3->set('content', '../ui/admin/housing/trench.html');

        echo \Template::instance()->render('admin_template.htm');
    }

    public function createTrench($f3)
    {
        $this->authCheck($f3);
        try {
            $f3->set('POST.dateCreated', date("Y-m-d"));
            $f3->set('POST.dateUpdated', date("Y-m-d"));
            $attributes = $f3->get('POST');

            $definition = $this->getDefinition($attributes);
            $this->getRepository('rentTrench')->createRecord($definition);
            $f3->set('SESSION.message', array('msg' => 'rentTrench created successfully!', 'alert' => 'success'));

            $f3->reroute("/admin/trench");
        } catch (Exception $exception) {
            echo json_encode($exception->getMessage());
        }
    }

    public function deleteTrench($f3, $params)
    {
        $this->authCheck($f3);
        $attributes = ["deleted" => 'y', "dateUpdated" => date("Y-m-d")];
        $definition = $this->getDefinition($attributes);
        try {
            $idArray = ['id = ?', $params['id']];

            $user = $this->getRepository('rentTrench')->updateRecord($idArray, $definition);
            $f3->set('SESSION.message', array('msg' => 'rentTrench deleted successfully!', 'alert' => 'success'));

            $f3->reroute("/admin/trench");
        } catch (Exception $exception) {
            echo json_encode($exception->getMessage());
        }
    }

    public function updateTrench($f3, $params)
    {
        $this->authCheck($f3);
        try {
            if ($f3->get('POST')) {
                $f3->set('POST.dateUpdated', date("Y-m-d"));
                $attributes = $f3->get('POST');
                $definition = $this->getDefinition($attributes);

                $idArray = ['id = ?', $params['id']];

                $user = $this->getRepository('rentTrench')->updateRecord($idArray, $definition);

                $f3->reroute("/admin/trench");
            }

            $users = $this->getRepository('rentTrench')->getByAttribute('id', $params['id']);
            $f3->set('POST', $users[0]);

            $f3->set('content', '../ui/admin/housing/updateTrench.html');
            echo \Template::instance()->render('admin_template.htm');

        } catch (Exception $exception) {
            echo json_encode($exception->getMessage());
        }
    }

    /*
     * Housing Units
     * Shop Units
     */
    public function viewUnits($f3)
    {
        global $db;
        $complex = $this->getRepository('complex')->getByAttribute("deleted", "n");
        $f3->set('complex', $complex);

        $rentTrench = $this->getRepository('rentTrench')->getByAttribute("deleted", "n");
        $f3->set('rentTrench', $rentTrench);

        $query = "SELECT u.*, c.complexName as complex, tr.title as trench FROM units u LEFT JOIN complex c ON u.complexID = c.id LEFT JOIN rentTrench tr ON u.trenchID = tr.id WHERE u.deleted = :deleted";
        $vars = array(
            ':deleted' => 'n',
        );
        $units = $db->exec($query, $vars);
        $f3->set('data', $units);

        $f3->set('content', '../ui/admin/housing/units.html');

        echo \Template::instance()->render('admin_template.htm');
    }

    public function createUnit($f3)
    {
        $this->authCheck($f3);
        try {
            $f3->set('POST.dateCreated', date("Y-m-d"));
            $f3->set('POST.dateUpdated', date("Y-m-d"));
            $attributes = $f3->get('POST');
            $attributes['occupied'] = "n";

            $definition = $this->getDefinition($attributes);
            $this->getRepository('units')->createRecord($definition);
            $f3->set('SESSION.message', array('msg' => 'Unit created successfully!', 'alert' => 'success'));

            $f3->reroute("/admin/units");
        } catch (Exception $exception) {
            echo json_encode($exception->getMessage());
        }
    }

    public function deleteUnit($f3, $params)
    {
        $this->authCheck($f3);
        $attributes = ["deleted" => 'y', "dateUpdated" => date("Y-m-d")];
        $definition = $this->getDefinition($attributes);
        try {
            $idArray = ['id = ?', $params['id']];

            $user = $this->getRepository('units')->updateRecord($idArray, $definition);
            $f3->set('SESSION.message', array('msg' => 'rentTrench deleted successfully!', 'alert' => 'success'));

            $f3->reroute("/admin/units");
        } catch (Exception $exception) {
            echo json_encode($exception->getMessage());
        }
    }

    public function updateUnit($f3, $params)
    {
        $this->authCheck($f3);
        global $db;
        try {
            if ($f3->get('POST')) {
                $f3->set('POST.dateUpdated', date("Y-m-d"));
                $attributes = $f3->get('POST');
                $definition = $this->getDefinition($attributes);

                $idArray = ['id = ?', $params['id']];

                $user = $this->getRepository('units')->updateRecord($idArray, $definition);

                $f3->reroute("/admin/units");
            }

            $complex = $this->getRepository('complex')->getByAttribute("deleted", "n");
            $f3->set('complex', $complex);

            $rentTrench = $this->getRepository('rentTrench')->getByAttribute("deleted", "n");
            $f3->set('rentTrench', $rentTrench);

            $query = "SELECT u.*, c.complexName as complex, tr.title as trench FROM units u LEFT JOIN complex c ON u.complexID = c.id LEFT JOIN rentTrench tr ON u.trenchID = tr.id WHERE u.id = :unitID";
            $vars = array(
                ':unitID' => $params['id'],
            );
            $units = $db->exec($query, $vars);

            $f3->set('POST', $units[0]);

            $f3->set('content', '../ui/admin/housing/updateUnit.html');
            echo \Template::instance()->render('admin_template.htm');

        } catch (Exception $exception) {
            echo json_encode($exception->getMessage());
        }
    }

    public function getAll_unit()
    {
        try {
            $units = $this->getRepository('cps_shop_units')->getAllRecords();

            foreach ($units as $unit) {
                $response[] = array(
                    'id' => $unit['id'],
                    'unit_code' => $unit['unit_code'],
                    'section' => $unit['section'],
                    'description' => $unit['description'],
                    'size' => $unit['size'],
                    'occupied' => $unit['occupied'],
                    'rental_rate' => $unit['rental_rate'],
                );
            }

            echo json_encode($response);
            exit();
        } catch (Exception $exception) {
            echo json_encode($exception->getMessage());
        }
    }

    private function setKpiVariablesUnit($f3)
    {
        $kpi = [];

        // get kpi calculated kpi data
        $kpi['temporarilyUnavailable'] = count($this->getRepository('cps_shop_units')->getByAttribute('occupied', 2));

        // set currency locale to local (ZAR)
        setlocale(LC_MONETARY, "");

        // set calculated kpi variables
        $f3->set('areaTotal', $kpi['areaTotal']);
        $f3->set('unoccupiedAvailable', $kpi['unoccupiedAvailable']);
        $f3->set('temporarilyUnavailable', $kpi['temporarilyUnavailable']);
    }

}
