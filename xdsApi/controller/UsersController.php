<?php

require_once 'AppController.php';

class UsersController extends AppController
{

    public function dashboard($f3, $params)
    {
        global $db;
        $this->authCheck($f3);

        $query = "SELECT *  FROM applications WHERE userID = :userID";
        $vars = array(
            ':userID' => $f3->get("SESSION.USER.id"),
        );
        $applicantdata = $db->exec($query, $vars);

        $f3->set('applicationFeeStatus', $applicantdata[0]['applicationFee']);
        $f3->set('hasApplied', 'n');

        if (sizeof($applicantdata) > 0) {

            $query = "SELECT *  FROM userChecklist WHERE userID = :userID AND title = :idPassportNo";
            $vars = array(
                ':userID' => $f3->get("SESSION.USER.id"),
                ':idPassportNo' => "idPassportNo",
            );
            $userChecklist = $db->exec($query, $vars);
            //xdsVerification only run if there is no validation
            if (sizeof($userChecklist) < 1 && $applicantdata[0]['applicationFee'] == "paid") {
                $params['userID'] = $f3->get("SESSION.USER.id");

                $this->xdsVerification($f3, $params);
            }

            $f3->set('hasApplied', 'y');
            if ($applicantdata[0]['applicationStatus'] == 'a') {
                $f3->set('applicationStatus', 'Approved');
            } else if ($applicantdata[0]['applicationStatus'] == 'p') {
                $f3->set('applicationStatus', 'Pending');
            } else if ($applicantdata[0]['applicationStatus'] == 'd') {
                $f3->set('applicationStatus', 'Denied');
            }
        } else {
            $f3->set('hasApplied', 'n');
        }

        //$units = $this->getRepository('units')->getByAttribute("id", $applicantdata[0]['unitID']);
        $query = "SELECT *  FROM units WHERE id = :unitID";
        $vars = array(
            ':unitID' => $applicantdata[0]['unitID'],
        );
        $allocatedUnit = $db->exec($query, $vars);

        $f3->set('allocatedUnit', $allocatedUnit[0]['unitName']);

        $f3->set('rentDeposit', $allocatedUnit[0]['rental']);


        $complexes = $this->getRepository('complex')->getByAttribute("deleted", "n");
        $f3->set('complexes', $complexes);
        $applicationComplex = $this->getRepository('complex')->getByAttribute("id", $applicantdata[0]['complexID']);
        if (count($applicationComplex) > 0) {
            $f3->set('appliedComplex', $applicationComplex[0]['complexName']);
        }

        $f3->set('applicationFee', "200");

        $query = "SELECT dt.*  FROM applicantdetails ad LEFT JOIN applicantdata dt ON ad.userID = dt.userID WHERE ad.userID = :userID";
        $vars = array(
            ':userID' => $f3->get("SESSION.USER.id"),
        );
        $userdata = $db->exec($query, $vars);

        $f3->set('depositFeePaymentProcessed', "n");

        for ($i=0; $i < count($userdata); $i++) { 
            if ($userdata[$i]['dataLabel']=='rentDeposit') {
                $f3->set('depositFeePaymentProcessed', "y");
            }
        }

        $query = "SELECT *  from applicantdetails WHERE userID = :id";
        $vars = array(
            ':id' => $f3->get("SESSION.USER.id"),
        );
        $results = $db->exec($query, $vars);

        if($results[0]['maritalStatus'] == "Married"){
            $f3->set('applicationFee', "400");
        }

        if ($f3->get('POST')) {
            $folder = "user_" . $f3->get("SESSION.USER.id");

            if ($f3->get('POST.complexID')) {
                $application = true;
            } else {
                $application = false;
            }

            if ($f3->get('POST.rentDeposit')) {
                $deposit = true;
            } else {
                $deposit = false;
            }

            foreach ($_FILES as $formFieldName => $value) {

                //$resultUrl = $this->basicUpload($f3,$formFieldName);
                $resultUrl = $this->fileUpload($f3, $formFieldName, $folder);

            }

            $CheckList = ["leaseAgreement", "keySlip", "proofOfPayment"];
            $count = 0;

            foreach ($resultUrl as $fileUrl => $bool) {
                //$f3->set("POST.$formFieldName", $fileUrl);
                $update = false;
                $formFieldName = $CheckList[$count];

                foreach ($userdata as $row) {
                    if ($row['dataType'] == $formFieldName) {
                        $update = true;
                    }
                }
                if ($update) {
                    if ($application) {
                        $query = "INSERT INTO applicantdata (userID, data, dataType, dataLabel, dateCreated, dateUpdated)
                                    VALUES (:userID, :data, :dataType, :dataLabel, NOW(), NOW())";
                        $vars = array(
                            ':userID' => $f3->get("SESSION.USER.id"),
                            ':data' => $fileUrl,
                            ':dataType' => 'proofOfPayment',
                            ':dataLabel' => 'proofOfPayment',
                        );
                        $applicantdata = $db->exec($query, $vars);

                        $apply = $this->saveApplication($f3, $params, $f3->get('POST.complexID'), 'pending');
                        $f3->set('SESSION.message', array('msg' => 'Application saved successfully!', 'alert' => 'info'));
                    } else if($deposit) {
                        $query = "INSERT INTO applicantdata (userID, data, dataType, dataLabel, dateCreated, dateUpdated)
                                    VALUES (:userID, :data, :dataType, :dataLabel, NOW(), NOW())";
                        $vars = array(
                            ':userID' => $f3->get("SESSION.USER.id"),
                            ':data' => $fileUrl,
                            ':dataType' => 'onlinePayment',
                            ':dataLabel' => 'rentDeposit',
                        );
                        $applicantdata = $db->exec($query, $vars);

                        $query = "INSERT INTO userChecklist (userID, approvedBy, title, isVerified, dateCreated, dateUpdated)
                            VALUES (:userID, :approvedBy, :title, :isVerified, NOW(), NOW())";
                        $vars = array(
                            ':userID' => $f3->get("SESSION.USER.id"),
                            ':approvedBy' => "onlinePortal",
                            ':title' => "rentDeposit",
                            ':isVerified' => "n",
                        );
                        $userChecklist = $db->exec($query, $vars);
                        $f3->set('SESSION.message', array('msg' => 'Deposit Proof of Payment Submitted Successfully!', 'alert' => 'info'));
                    } else {
                        $query = "UPDATE applicantdata SET data=:data WHERE userID = :userID AND dataType=:dataType";
                        $vars = array(
                            ':data' => $fileUrl,
                            ':userID' => $f3->get("SESSION.USER.id"),
                            ':dataType' => $formFieldName,
                        );
                        $applicantdata = $db->exec($query, $vars);

                        $f3->set('SESSION.message', array('msg' => 'Signed Documents saved successfully!', 'alert' => 'info'));
                    }
                } else {
                    if ($application) {
                        $query = "INSERT INTO applicantdata (userID, data, dataType, dataLabel, dateCreated, dateUpdated)
                                    VALUES (:userID, :data, :dataType, :dataLabel, NOW(), NOW())";
                        $vars = array(
                            ':userID' => $f3->get("SESSION.USER.id"),
                            ':data' => $fileUrl,
                            ':dataType' => 'proofOfPayment',
                            ':dataLabel' => 'proofOfPayment',
                        );
                        $applicantdata = $db->exec($query, $vars);

                        $apply = $this->saveApplication($f3, $params, $f3->get('POST.complexID'), 'pending');
                        $f3->set('SESSION.message', array('msg' => 'Application saved successfully!', 'alert' => 'info'));
                    } else if($deposit) {
                        $query = "INSERT INTO applicantdata (userID, data, dataType, dataLabel, dateCreated, dateUpdated)
                                    VALUES (:userID, :data, :dataType, :dataLabel, NOW(), NOW())";
                        $vars = array(
                            ':userID' => $f3->get("SESSION.USER.id"),
                            ':data' => $fileUrl,
                            ':dataType' => 'onlinePayment',
                            ':dataLabel' => 'rentDeposit',
                        );
                        $applicantdata = $db->exec($query, $vars);

                        $query = "INSERT INTO userChecklist (userID, approvedBy, title, isVerified, dateCreated, dateUpdated)
                            VALUES (:userID, :approvedBy, :title, :isVerified, NOW(), NOW())";
                        $vars = array(
                            ':userID' => $f3->get("SESSION.USER.id"),
                            ':approvedBy' => "onlinePortal",
                            ':title' => "rentDeposit",
                            ':isVerified' => "n",
                        );
                        $userChecklist = $db->exec($query, $vars);
                        $f3->set('SESSION.message', array('msg' => 'Deposit Proof of Payment Submitted Successfully!', 'alert' => 'info'));
                    } else {
                        $query = "INSERT INTO applicantdata (userID, data, dataType, dataLabel, dateCreated, dateUpdated)
                                    VALUES (:userID, :data, :dataType, :dataLabel, NOW(), NOW())";
                        $vars = array(
                            ':userID' => $f3->get("SESSION.USER.id"),
                            ':data' => $fileUrl,
                            ':dataType' => $formFieldName,
                            ':dataLabel' => $formFieldName,
                        );
                        $applicantdata = $db->exec($query, $vars);

                        $f3->set('SESSION.message', array('msg' => 'Signed Documents saved successfully!', 'alert' => 'info'));
                    }
                }
                $count++;
            }

        }

        $query = "SELECT *  from userChecklist WHERE userID = :id";
        $vars = array(
            ':id' => $f3->get("SESSION.USER.id"),
        );
        $checkList = $db->exec($query, $vars);

        $arrCheckList = [];

        $profilePerc = 0;
        $personalPerc = 0;
        $contactPerc = 0;
        $employerPerc = 0;
        $emergencyPerc = 0;
        $documentPerc = 0;

        $arrCheckList["personalInfo"] = ["firstName", "lastName", "idPassportNo", "dob", "address", "maritalStatus", "bankName", "branchCode", "accountNumber", "accountType"];
        $arrCheckList["contactInfo"] = ["email", "phone", "altPhone"];
        $arrCheckList["employerInfo"] = ["occupation", "employer", "employerContact", "totalIncome", "totalExpenses"];
        $arrCheckList["emergencyInfo"] = ["nextOfKin", "nextOfKinContact"];
        $arrCheckList["documentInfo"] = ["idDocument", "payslip", "bankStatement"];

        $documentVerification = 0;

        foreach ($arrCheckList["documentInfo"] as $column) {
            foreach ($checkList as $verify) {
                if ($verify['title'] == 'idDocument') {
                    if ($verify['isVerified'] == 'y') {
                        $f3->set('idDocVerified', 'y');
                        $documentVerification++;
                    } else {
                        $f3->set('idDocVerified', 'n');
                    }
                } else if ($verify['title'] == 'bankStatement') {
                    if ($verify['isVerified'] == 'y') {
                        $f3->set('bankStatementVerified', 'y');
                        $documentVerification++;
                    } else {
                        $f3->set('bankStatementVerified', 'n');
                    }
                } else if ($verify['title'] == 'payslip') {
                    if ($verify['isVerified'] == 'y') {
                        $f3->set('payslipVerified', 'y');
                        $documentVerification++;
                    } else {
                        $f3->set('payslipVerified', 'n');
                    }
                } else if ($verify['title'] == 'proofOfPayment') {
                    if ($verify['isVerified'] == 'y') {
                        $f3->set('proofOfPaymentVerified', 'y');
                    } else {
                        $f3->set('proofOfPaymentVerified', 'n');
                    }
                } else if ($verify['title'] == 'rentDeposit') {
                    if ($verify['isVerified'] == 'y') {
                        $f3->set('rentDepositVerified', 'Paid');
                    } else {
                        $f3->set('rentDepositVerified', 'Unpaid');
                    }
                }
            }
        }

        $f3->set('documentVerification', $documentVerification);

        $signedDocuments = 0;

        foreach ($arrCheckList as $key => $list) {
            foreach ($list as $column) {
                if ($key == "personalInfo" && $results[0][$column] != "") {
                    $profilePerc = $profilePerc + 2;
                    $personalPerc = $personalPerc + 2;
                }
                if ($key == "contactInfo" && $results[0][$column] != "") {
                    $profilePerc = $profilePerc + 6.66;
                    $contactPerc = $contactPerc + 6.66;
                }
                if ($key == "employerInfo" && $results[0][$column] != "") {
                    $profilePerc = $profilePerc + 4;
                    $employerPerc = $employerPerc + 4;
                }
                if ($key == "emergencyInfo" && $results[0][$column] != "") {
                    $profilePerc = $profilePerc + 10;
                    $emergencyPerc = $emergencyPerc + 10;
                }
                if ($key == "documentInfo") {
                    foreach ($userdata as $row) {

                        if ($row['dataType'] == $column) {
                            $dataArr = explode(",", $row['data']);
                            if (!in_array('Error', $dataArr) && $row['data'] != "") {
                                $profilePerc = $profilePerc + 6.66;
                                $documentPerc = $documentPerc + 6.66;
                            }

                        }

                        if ($row['dataLabel'] == 'leaseAgreement') {
                            $signedDocuments++;
                        } else if ($row['dataLabel'] == 'keySlip') {
                            $signedDocuments++;
                        }
                    }
                }
            }
        }

        if ($signedDocuments == 0) {
            $signedDocuments = 'no files';
        } else {
            $signedDocuments = 'uploaded';
        }

        $f3->set('signedDocuments', $signedDocuments);
        $f3->set('profilePercentage', round($profilePerc));

        $f3->set('personalPerc', round($personalPerc));
        $f3->set('contactPerc', round($contactPerc));
        $f3->set('employerPerc', round($employerPerc));
        $f3->set('emergencyPerc', round($emergencyPerc));
        $f3->set('documentPerc', round($documentPerc));

        if ($personalPerc < 20) {
            $f3->set("personalInfo", 'n');
        } else {
            $f3->set("personalInfo", 'completed');
        }

        if (round($contactPerc) < 20) {
            $f3->set("contactInfo", 'n');
        } else {
            $f3->set("contactInfo", 'completed');
        }

        if (round($employerPerc) < 20) {
            $f3->set("employerInfo", 'n');
        } else {
            $f3->set("employerInfo", 'completed');
        }

        if ($emergencyPerc < 20) {
            $f3->set("emergencyInfo", 'n');
        } else {
            $f3->set("emergencyInfo", 'completed');
        }

        if ($documentPerc < 20) {
            $f3->set("documentInfo", 'n');
        } else {
            $f3->set("documentInfo", 'completed');
        }

        if (round($profilePerc) > 99) {
            $f3->set("canSubmit", 'y');
        } else {
            $f3->set("canSubmit", 'n');
        }

        $query = "SELECT *  FROM users WHERE id = :userID";
        $vars = array(
            ':userID' => $f3->get("SESSION.USER.id"),
        );
        $userData = $db->exec($query, $vars);

        // ////////////////////////////- PayFast -/////////////////////////////
        // //Check neccessary variables to check if device is eligible for paymets
        // //PayFast Variables
        $merchant_id = '10000100';
        $merchant_key = '46f0cd694581a';
        $return_url = 'http://zelriprop.talentindx.co.za/user/dashboard';
        $cancel_url = 'http://zelriprop.talentindx.co.za/user/dashboard#cancel';
        $notify_url = 'http://zelriprop.talentindx.co.za/itn.php';
        $testingMode = true;
        // $passPhrase = '6qM9lTEATRRNHVIpFcbB';

        // Construct variables
        $pfData = array(
            // Merchant details
            // 'merchant_id' => $merchant_id,
            // 'merchant_key' => $merchant_key,

            //SandBox
            'merchant_id' => $merchant_id,
            'merchant_key' => $merchant_key,

            'return_url' => $return_url,
            'cancel_url' => $cancel_url,
            'notify_url' => $notify_url,
            // Buyer details
            'email_address' => $userData[0]['email'],
            // Transaction details
            'm_payment_id' => 'id_' . $f3->get("SESSION.USER.id") . mt_rand(150000, 5000000) . $f3->get("SESSION.USER.id"), //Unique payment ID to pass through to notify_url
            // Amount needs to be in ZAR

            // If multicurrency system its conversion has to be done before building this array

            'item_name' => 'Application Fee',
            'item_description' => 'Application for Review Process to initiate',
            'custom_str1' => '',
            'custom_str2' => '',
            'custom_str3' => '',
        );

        // Construct variables
        $pfDataDeposit = array(
            // Merchant details
            // 'merchant_id' => $merchant_id,
            // 'merchant_key' => $merchant_key,

            //SandBox
            'merchant_id' => $merchant_id,
            'merchant_key' => $merchant_key,

            'return_url' => $return_url,
            'cancel_url' => $cancel_url,
            'notify_url' => $notify_url,
            // Buyer details
            'email_address' => $userData[0]['email'],
            // Transaction details
            'm_payment_id' => 'id_' . $f3->get("SESSION.USER.id") . mt_rand(150000, 5000000) . $f3->get("SESSION.USER.id"), //Unique payment ID to pass through to notify_url
            // Amount needs to be in ZAR

            // If multicurrency system its conversion has to be done before building this array

            'item_name' => 'Deposit Fee',
            'item_description' => 'Application for Review Process to initiate',
            'custom_str1' => '',
            'custom_str2' => '',
            'custom_str3' => '',
        );

        // Create parameter string
        $pfOutput = '';
        foreach ($pfData as $key => $val) {
            if (!empty($val)) {
                $pfOutput .= $key . '=' . urlencode(trim($val)) . '&';
            }
        }

        // Create parameter string
        $pfOutputDeposit = '';
        foreach ($pfDataDeposit as $key => $val) {
            if (!empty($val)) {
                $pfOutputDeposit .= $key . '=' . urlencode(trim($val)) . '&';
            }
        }

        $pfHost = $testingMode ? 'sandbox.payfast.co.za' : 'www.payfast.co.za';
        $f3->set("pfHost", $pfHost);
        $f3->set("pfData", $pfData);
        $f3->set("pfDataDeposit", $pfDataDeposit);

        //////////////////////////////////////////////////////////////

        $f3->set("isNew", $userData[0]['isNew']);
        $this->proceed($f3);

        $f3->set('content', '../ui/user/index.html');

        echo \Template::instance()->render('user_template.htm');
    }

    public function onboarding($f3, $params)
    {
        $this->authCheck($f3);
        $f3->set('content', '../ui/user/onboarding/index.html');

        echo \Template::instance()->render('user_template.htm');
    }

    public function proceed($f3)
    {
        $this->authCheck($f3);
        $f3->set('POST.isNew', 1);
        $attributes = $f3->get('POST');
        $definition = $this->getDefinition($attributes);
        $user = $this->getRepository('users')->updateRecord(array('id =?', $f3->get('SESSION.USER.id')), $definition);

        //$f3->reroute('/user/dashboard');
    }

    public function payfastNotify($f3)
    {
        /**
         * Notes:
         * - All lines with the suffix "// DEBUG" are for debugging purposes and
         *   can safely be removed from live code.
         * - Remember to set PAYFAST_SERVER to LIVE for production/live site
         * - If there is a passphrase set on the sandbox account, include it on line 68
         */
        // General defines
        define('PAYFAST_SERVER', 'TEST'); // Whether to use "sandbox" test server or live server
        define('USER_AGENT', 'Mozilla/4.0 (compatible; MSIE 5.01; Windows NT 5.0)'); // User Agent for cURL

        // Error Messages
        define('PF_ERR_AMOUNT_MISMATCH', 'Amount mismatch');
        define('PF_ERR_BAD_SOURCE_IP', 'Bad source IP address');
        define('PF_ERR_CONNECT_FAILED', 'Failed to connect to PayFast');
        define('PF_ERR_BAD_ACCESS', 'Bad access of page');
        define('PF_ERR_INVALID_SIGNATURE', 'Security signature mismatch');
        define('PF_ERR_CURL_ERROR', 'An error occurred executing cURL');
        define('PF_ERR_INVALID_DATA', 'The data received is invalid');
        define('PF_ERR_UNKNOWN', 'Unknown error occurred');

        // General Messages
        define('PF_MSG_OK', 'Payment was successful');
        define('PF_MSG_FAILED', 'Payment has failed');

        // Notify PayFast that information has been received
        header('HTTP/1.0 200 OK');
        flush();

        // Variable initialization
        $pfError = false;
        $pfErrMsg = '';
        $dateTime = date('Y-m-d H:i:s', time());
        $output = ''; // DEBUG
        $pfParamString = '';
        $pfHost = (PAYFAST_SERVER == 'LIVE') ? 'www.payfast.co.za' : 'sandbox.payfast.co.za';
        // Sandbox
        //$pfHost = 'sandbox.payfast.co.za';
        // Production
        // $pfHost = 'www.payfast.co.za';
        $pfData = array();
        $output = "ITN Response Received\n\n";

        //// Dump the submitted variables and calculate security signature
        if (!$pfError) {
            $output .= "Posted Variables:\n"; // DEBUG

            // Strip any slashes in data
            foreach ($_POST as $key => $val) {
                $pfData[$key] = stripslashes($val);
                $output .= "$key = $val\n";
            }

            // Dump the submitted variables and calculate security signature
            foreach ($pfData as $key => $val) {
                if ($key != 'signature') {
                    $pfParamString .= $key . '=' . urlencode($val) . '&';
                }

            }

            // Remove the last '&' from the parameter string
            $pfParamString = substr($pfParamString, 0, -1);
            $pfTempParamString = $pfParamString;

            // If a passphrase has been set in the PayFast Settings, include it in the signature string.
            //$passPhrase = '6qM9lTEATRRNHVIpFcbB'; //You need to get this from a constant or stored in your website/database
            if (!empty($passPhrase)) {
                $pfTempParamString .= '&passphrase=' . urlencode($passPhrase);
            }
            $signature = md5($pfTempParamString);

            $result = ($_POST['signature'] == $signature);

            $output .= "\nSecurity Signature:\n"; // DEBUG
            $output .= "- posted     = " . $_POST['signature'] . "\n"; // DEBUG
            $output .= "- calculated = " . $signature . "\n"; // DEBUG
            $output .= "- result     = " . ($result ? 'SUCCESS' : 'FAILURE') . "\n"; // DEBUG

            if (!$result) {
                $pfError = true;
                $pfErrMsg = PF_ERR_INVALID_SIGNATURE;
            }
        }

        //// Verify source IP
        if (!$pfError) {
            $validHosts = array(
                'www.payfast.co.za',
                'sandbox.payfast.co.za',
                'w1w.payfast.co.za',
                'w2w.payfast.co.za',
            );

            $validIps = array();

            foreach ($validHosts as $pfHostname) {
                $ips = gethostbynamel($pfHostname);

                if ($ips !== false) {
                    $validIps = array_merge($validIps, $ips);
                }
            }

            // Remove duplicates
            $validIps = array_unique($validIps);

            if (!in_array($_SERVER['REMOTE_ADDR'], $validIps)) {
                $pfError = true;
                $pfErrMsg = PF_ERR_BAD_SOURCE_IP;
            }
        }

        //// Connect to server to validate data received
        if (!$pfError) {
            // Use cURL (If it's available)
            if (function_exists('curl_init')) {
                $output .= "\nUsing cURL\n"; // DEBUG

                // Create default cURL object
                $ch = curl_init();

                // Base settings
                $curlOpts = array(
                    // Base options
                    CURLOPT_USERAGENT => USER_AGENT, // Set user agent
                    CURLOPT_RETURNTRANSFER => true, // Return output as string rather than outputting it
                    CURLOPT_HEADER => false, // Don't include header in output
                    CURLOPT_SSL_VERIFYHOST => 2,
                    CURLOPT_SSL_VERIFYPEER => 1,

                    // Standard settings
                    CURLOPT_URL => 'https://' . $pfHost . '/eng/query/validate',
                    CURLOPT_POST => true,
                    CURLOPT_POSTFIELDS => $pfParamString,
                );
                curl_setopt_array($ch, $curlOpts);

                // Execute CURL
                $res = curl_exec($ch);
                curl_close($ch);

                if ($res === false) {
                    $pfError = true;
                    $pfErrMsg = PF_ERR_CURL_ERROR;
                }
            }
            // Use fsockopen
            else {
                $output .= "\nUsing fsockopen\n"; // DEBUG

                // Construct Header
                $header = "POST /eng/query/validate HTTP/1.0\r\n";
                $header .= "Host: " . $pfHost . "\r\n";
                $header .= "Content-Type: application/x-www-form-urlencoded\r\n";
                $header .= "Content-Length: " . strlen($pfParamString) . "\r\n\r\n";

                // Connect to server
                $socket = fsockopen('ssl://' . $pfHost, 443, $errno, $errstr, 10);

                // Send command to server
                fputs($socket, $header . $pfParamString);

                // Read the response from the server
                $res = '';
                $headerDone = false;

                while (!feof($socket)) {
                    $line = fgets($socket, 1024);

                    // Check if we are finished reading the header yet
                    if (strcmp($line, "\r\n") == 0) {
                        // read the header
                        $headerDone = true;
                    }
                    // If header has been processed
                    else if ($headerDone) {
                        // Read the main response
                        $res .= $line;
                    }
                }
            }
        }

        //// Get data from server
        if (!$pfError) {
            // Parse the returned data
            $lines = explode("\n", $res);

            $output .= "\nValidate response from server:\n"; // DEBUG

            foreach ($lines as $line) // DEBUG
            {
                $output .= $line . "\n";
            }
            // DEBUG
        }

        //// Interpret the response from server
        if (!$pfError) {
            // Get the response from PayFast (VALID or INVALID)
            $result = trim($lines[0]);

            $output .= "\nResult = " . $result; // DEBUG

            // If the transaction was valid
            if (strcmp($result, 'VALID') == 0) {

                //SUCCESSFUL PAYMENT
                $userID = $f3->get('POST.custom_str4');
                $complexID = $f3->get('POST.custom_str5');

                $application = $this->submitApplication($f3, $userID, $complexID, 'paid');
            }
            // If the transaction was NOT valid
            else {
                // Log for investigation
                //UNSUCCESSFULL PAYMENT

            }
        }

        // If an error occurred
        if ($pfError) {
            $response = 'fail';
            $output .= "\n\nAn error occurred!";
            $output .= "\nError = " . $pfErrMsg;
            //ANY OTHER ERRO OCCURRED
        }
    }

    public function submitApplication($f3, $userID, $complexID, $applicationFeeStatus)
    {
        global $db;
        $this->authCheck($f3);
        $query = "INSERT INTO applications (userID, complexID, applicationFee, dateCreated, dateUpdated) VALUES(:userID, :complexID, :applicationFeeStatus, NOW(), NOW()) ";
        $vars = array(
            ':userID' => $userID,
            ':complexID' => $complexID,
            ':applicationFeeStatus' => $applicationFeeStatus,
        );
        $userdata = $db->exec($query, $vars);

        $f3->set('SESSION.message', array('msg' => 'Application saved successfully!', 'alert' => 'info'));
    }

    /*
     * viewApplications
     */
    public function viewApplications($f3)
    {
        global $db;
        $this->authCheck($f3);
        $query = "SELECT ap.*  FROM applications ap LEFT JOIN applicantdetails ad ON ap.userID = ad.userID WHERE ap.userID = :userID";
        $vars = array(
            ':userID' => $f3->get("SESSION.USER.id"),
        );
        $applicantdata = $db->exec($query, $vars);

        $f3->set('data', $applicantdata);

        $f3->set('content', '../ui/user/applications/index.html');

        echo \Template::instance()->render('user_template.htm');
    }

    public function viewDocuments($f3)
    {
        global $db;
        $this->authCheck($f3);
        $query = "SELECT * FROM applicantdata WHERE userID = :userID AND dataLabel IN('leaseAgreement','keySlip','idDocument','payslip','bankStatement','proofOfPayment') ORDER BY id DESC";
        $vars = array(
            ':userID' => $f3->get("SESSION.USER.id"),
        );
        $documents = $db->exec($query, $vars);

        $newDocsArr = [];
        foreach ($documents as $document) {
            $title = preg_replace("([A-Z])", " $0", $document['dataLabel']);

            $newDocsArr[] = ["id" => $document['id'], "dataType" => $document['dataType'],
                "dataLabel" => $document['dataLabel'], "data" => $document['data'], "title" => $title];
        }

        $f3->set('documents', $newDocsArr);

        $f3->set('content', '../ui/user/documents/index.html');

        echo \Template::instance()->render('user_template.htm');
    }

    public function saveApplication($f3, $params, $complexID)
    {
        global $db;
        $this->authCheck($f3);
        $query = "INSERT INTO applications (userID, complexID, dateCreated, dateUpdated) VALUES(:userID, :complexID, NOW(), NOW()) ";
        $vars = array(
            ':userID' => $f3->get("SESSION.USER.id"),
            ':complexID' => $complexID,
        );
        $userdata = $db->exec($query, $vars);

        $query = "INSERT INTO userChecklist (userID, approvedBy, title, isVerified, dateCreated, dateUpdated)
                            VALUES (:userID, :approvedBy, :title, :isVerified, NOW(), NOW())";
        $vars = array(
            ':userID' => $f3->get("SESSION.USER.id"),
            ':approvedBy' => "onlinePortal",
            ':title' => "email",
            ':isVerified' => "y",
        );
        $userChecklist = $db->exec($query, $vars);

    }

    public function createApplication($f3, $params)
    {
        global $db;
        $this->authCheck($f3);
        $query = "INSERT INTO applications (userID, dateCreated, dateUpdated) VALUES(:userID, NOW(), NOW()) ";
        $vars = array(
            ':userID' => $f3->get("SESSION.USER.id"),
        );
        $userdata = $db->exec($query, $vars);

        $query = "SELECT *  from applicantdetails WHERE userID = :id";
        $vars = array(
            ':id' => $f3->get("SESSION.USER.id"),
        );
        $results = $db->exec($query, $vars);

        $params["IDNumber"] = $results[0]["idPassportNo"];

        $params['strUser'] = "Smitk_uat";
        $params['strPwd'] = "Sm!t0805";

        $f3->reroute('/user/dashboard');
    }

    public function updateApplication($f3, $params)
    {
        $this->authCheck($f3);
        $f3->set('content', '../ui/user/onboarding.html');

        echo \Template::instance()->render('user_template.htm');
    }

    public function uploadDocuments($f3, $params)
    {
        global $db;
        $this->authCheck($f3);
        $query = "SELECT dt.*  FROM applicantdetails ad LEFT JOIN applicantdata dt ON ad.userID = dt.userID WHERE ad.userID = :userID";
        $vars = array(
            ':userID' => $f3->get("SESSION.USER.id"),
        );
        $userdata = $db->exec($query, $vars);

        //$userID = $params['id'];
        $folder = "user_" . $f3->get("SESSION.USER.id");

        $docsArr = [];
        //complex Documents
        foreach ($_FILES as $formFieldName => $value) {
            //$resultUrl = $this->basicUpload($f3,$formFieldName);
            if ($value['name'] != '') {
                $resultUrl = $this->fileUpload($f3, $formFieldName, $folder);
                $docsArr[] = $formFieldName;
            }
        }

        $count = 0;

        foreach ($resultUrl as $fileUrl => $bool) {
            //var_dump($resultUrl);

            //$f3->set("POST.$formFieldName", $fileUrl);
            $update = false;
            $formFieldName = $docsArr[$count];

            foreach ($userdata as $row) {
                if ($row['dataLabel'] == $formFieldName) {
                    $update = true;
                }
            }
            if ($update) {
                $query = "UPDATE applicantdata SET data=:data WHERE userID = :userID AND dataLabel=:dataLabel";
                $vars = array(
                    ':data' => $fileUrl,
                    ':userID' => $f3->get("SESSION.USER.id"),
                    ':dataLabel' => $formFieldName,
                );
                $applicantdata = $db->exec($query, $vars);
            } else {
                $query = "INSERT INTO applicantdata (userID, data, dataType, dataLabel, dateCreated, dateUpdated)
                            VALUES (:userID, :data, :dataType, :dataLabel, NOW(), NOW())";
                $vars = array(
                    ':userID' => $f3->get("SESSION.USER.id"),
                    ':data' => $fileUrl,
                    ':dataType' => "document",
                    ':dataLabel' => $formFieldName,
                );
                $applicantdata = $db->exec($query, $vars);
            }
            $count++;
        }

        $f3->reroute('/user/profile#profile-documents');

    }

    public function profile($f3, $params)
    {
        global $db;
        $this->authCheck($f3);
        $query = "SELECT dt.*  FROM applicantdetails ad LEFT JOIN applicantdata dt ON ad.userID = dt.userID WHERE ad.userID = :userID";
        $vars = array(
            ':userID' => $f3->get("SESSION.USER.id"),
        );
        $userdata = $db->exec($query, $vars);

        $complexes = $this->getRepository('complex')->getByAttribute("deleted", "n");
        $f3->set('complexes', $complexes);

        if ($f3->get('POST')) {
            $folder = "user_" . $f3->get("SESSION.USER.id");

            if ($_FILES) {

                $docsArr = [];
                //complex Documents
                foreach ($_FILES as $formFieldName => $value) {
                    //$resultUrl = $this->basicUpload($f3,$formFieldName);
                    if ($value['name'] != '') {
                        $resultUrl = $this->fileUpload($f3, $formFieldName, $folder);
                        $docsArr[] = $formFieldName;
                    }
                }

                $count = 0;

                if (sizeof($resultUrl) > 0) {
                    foreach ($resultUrl as $fileUrl => $bool) {
                        //$f3->set("POST.$formFieldName", $fileUrl);
                        $update = false;
                        $formFieldName = $docsArr[$count];

                        foreach ($userdata as $row) {
                            if ($row['dataType'] == $formFieldName) {
                                $update = true;
                            }
                        }
                        if ($update) {
                            $query = "UPDATE applicantdata SET data=:data WHERE userID = :userID AND dataType=:dataType";
                            $vars = array(
                                ':data' => $fileUrl,
                                ':userID' => $f3->get("SESSION.USER.id"),
                                ':dataType' => $formFieldName,
                            );
                            $applicantdata = $db->exec($query, $vars);
                            $f3->set('SESSION.message', array('msg' => 'Documents saved successfully!', 'alert' => 'info'));
                        } else {
                            $query = "INSERT INTO applicantdata (userID, data, dataType, dataLabel, dateCreated, dateUpdated)
                                VALUES (:userID, :data, :dataType, :dataLabel, NOW(), NOW())";
                            $vars = array(
                                ':userID' => $f3->get("SESSION.USER.id"),
                                ':data' => $fileUrl,
                                ':dataType' => $formFieldName,
                                ':dataLabel' => $formFieldName,
                            );
                            $applicantdata = $db->exec($query, $vars);
                            $f3->set('SESSION.message', array('msg' => 'Documents saved successfully!', 'alert' => 'info'));
                        }
                        $count++;
                    }
                }
                $f3->reroute("/user/dashboard");

            }

            //normal form save

            $userID = $f3->get('SESSION.USER.id');

            $query = "SELECT * FROM `applicantdetails` where userID = :userID";
            $vars = array(
                ':userID' => $userID,
            );
            $user_profiles = $db->exec($query, $vars);

            $attributes = $f3->get('POST');
            if ($f3->get('POST.totalIncome')) {
                $attributes['rentTrench'] = 1;
                $query = "SELECT * FROM `rentTrench` where minIncome <= :minIncome AND maxIncome >= :maxIncome AND deleted= :isDeleted";
                $vars = array(
                    ':minIncome' => $attributes['totalIncome'],
                    ':maxIncome' => $attributes['totalIncome'],
                    ':isDeleted' => "n",
                );
                $results = $db->exec($query, $vars);
                $attributes['rentTrench'] = $results[0]['id'];
            }

            if (count($user_profiles) > 0) {
                $update = true;
            } else {
                $attributes['userID'] = $userID;
                $update = false;
            }

            $definition = $this->getDefinition($attributes);
            $idArray = ['userID =?', $userID];

            if ($f3->get('POST.firstName')) {
                $userArray = ['id =?', $userID];

                $this->getRepository("users")->updateRecord($userArray, $definition);
            }

            if ($update) {
                $info = $this->getRepository("applicantdetails")->updateRecord($idArray, $definition);
            } else {
                $info = $this->getRepository("applicantdetails")->createRecord($definition);
            }

            $f3->set('SESSION.message', array('msg' => 'Data saved successfully!', 'alert' => 'info'));

        }

        $query = "SELECT *  from applicantdetails WHERE userID = :id";
        $vars = array(
            ':id' => $f3->get("SESSION.USER.id"),
        );
        $results = $db->exec($query, $vars);

        $f3->set('applicationFee', "200");
        if ($results[0]['marriageDetails']) {
            $f3->set('applicationFee', "400");
        }

        //get new list of documents
        $query = "SELECT dt.*  FROM applicantdetails ad LEFT JOIN applicantdata dt ON ad.userID = dt.userID WHERE ad.userID = :userID";
        $vars = array(
            ':userID' => $f3->get("SESSION.USER.id"),
        );
        $userdata = $db->exec($query, $vars);

        $query = "SELECT *  from applicantdetails WHERE userID = :id";
        $vars = array(
            ':id' => $f3->get("SESSION.USER.id"),
        );
        $results = $db->exec($query, $vars);

        $arrCheckList = [];

        $profilePerc = 0;

        $arrCheckList["personalInfo"] = ["firstName", "lastName", "idPassportNo", "dob", "address", "maritalStatus", "bankName", "branchCode", "accountNumber", "accountType"];
        $arrCheckList["contactInfo"] = ["email", "phone", "altPhone"];
        $arrCheckList["employerInfo"] = ["occupation", "employer", "employerContact", "totalIncome", "totalExpenses"];
        $arrCheckList["emergencyInfo"] = ["nextOfKin", "nextOfKinContact"];
        $arrCheckList["documentInfo"] = ["idDocument", "payslip", "bankStatement"];

        foreach ($arrCheckList as $key => $list) {
            foreach ($list as $column) {
                if ($key == "personalInfo" && $results[0][$column] != "") {
                    $profilePerc = $profilePerc + 2;
                    $personalPerc = $personalPerc + 2;
                }
                if ($key == "contactInfo" && $results[0][$column] != "") {
                    $profilePerc = $profilePerc + 6.66;
                    $contactPerc = $contactPerc + 6.66;
                }
                if ($key == "employerInfo" && $results[0][$column] != "") {
                    $profilePerc = $profilePerc + 4;
                    $employerPerc = $employerPerc + 4;
                }
                if ($key == "emergencyInfo" && $results[0][$column] != "") {
                    $profilePerc = $profilePerc + 10;
                    $emergencyPerc = $emergencyPerc + 10;
                }
                if ($key == "documentInfo") {
                    foreach ($userdata as $row) {

                        if ($row['dataType'] == $column) {
                            $dataArr = explode(",", $row['data']);
                            if (!in_array('Error', $dataArr) && $row['data'] != "") {
                                $profilePerc = $profilePerc + 6.66;
                                $documentPerc = $documentPerc + 6.66;
                            }

                        }
                    }
                }
            }
        }

        $f3->set('profilePercentage', round($profilePerc));

        // ////////////////////////////- PayFast -/////////////////////////////
        // //Check neccessary variables to check if device is eligible for paymets
        // //PayFast Variables
        $merchant_id = '10000100';
        $merchant_key = '46f0cd694581a';
        $return_url = 'http://zelriprop.talentindx.co.za/user/dashboard';
        $cancel_url = 'http://zelriprop.talentindx.co.za/user/dashboard#cancel';
        $notify_url = 'http://zelriprop.talentindx.co.za/itn.php';
        $testingMode = true;
        // $passPhrase = '6qM9lTEATRRNHVIpFcbB';

        // Construct variables
        $pfData = array(
            // Merchant details
            // 'merchant_id' => $merchant_id,
            // 'merchant_key' => $merchant_key,

            //SandBox
            'merchant_id' => $merchant_id,
            'merchant_key' => $merchant_key,

            'return_url' => $return_url,
            'cancel_url' => $cancel_url,
            'notify_url' => $notify_url,
            // Buyer details
            'email_address' => $userdata[0]['email'],
            // Transaction details
            'm_payment_id' => 'id_'.$f3->get("SESSION.USER.id").mt_rand(150000, 5000000).$f3->get("SESSION.USER.id"), //Unique payment ID to pass through to notify_url
            // Amount needs to be in ZAR

            // If multicurrency system its conversion has to be done before building this array

            'item_name' => 'Application Fee',
            'item_description' => 'Application for Review Process to initiate',
            'custom_str1' => '',
            'custom_str2' => '',
            'custom_str3' => '',
        );

        // Create parameter string
        $pfOutput = '';
        foreach ($pfData as $key => $val) {
            if (!empty($val)) {
                $pfOutput .= $key . '=' . urlencode(trim($val)) . '&';
            }
        }

        $pfHost = $testingMode ? 'sandbox.payfast.co.za' : 'www.payfast.co.za';
        $f3->set("pfHost", $pfHost);
        $f3->set("pfData", $pfData);

        $query = "SELECT *  FROM applications WHERE userID = :userID";
        $vars = array(
            ':userID' => $f3->get("SESSION.USER.id"),
        );
        $applicantdata = $db->exec($query, $vars);

        if (count($applicantdata) > 0) {
            $f3->set('hasApplied', 'y');
        } else {
            $f3->set('hasApplied', 'n');
        }

        $f3->set('profilePercentage', round($profilePerc));
        $f3->set('personalPerc', round($personalPerc));
        $f3->set('contactPerc', round($contactPerc));
        $f3->set('employerPerc', round($employerPerc));
        $f3->set('emergencyPerc', round($emergencyPerc));
        $f3->set('documentPerc', round($documentPerc));

        $f3->set('POST', $results[0]);
        $f3->set('content', '../ui/user/profile/index.html');

        echo \Template::instance()->render('user_template.htm');
    }

    public function invoices($f3, $params)
    {
        $f3->set('content', '../ui/user/finance/invoices.html');

        echo \Template::instance()->render('user_template.htm');
    }

    public function basicUpload($f3, $formFieldName, $Files)
    {
        $folder = "user_" . $f3->get("SESSION.USER.id");
        $target_dir = "uploads/$folder/";
        if (!is_dir("$target_dir")) {
            mkdir("$target_dir", 0777, true);
            chmod("$target_dir", 0777);
        }

        $target_file = $target_dir . basename($_FILES[$formFieldName]["name"]);
        $uploadOk = 1;
        $fileType = strtolower(pathinfo($target_file, PATHINFO_EXTENSION));
        // Check if image file is a actual image or fake image
        // if (isset($_POST["submit"])) {
        //     $check = getimagesize($_FILES[$formFieldName]["tmp_name"]);
        //     if ($check !== false) {
        //         echo "File is an image - " . $check["mime"] . ".";
        //         $uploadOk = 1;
        //     } else {
        //         echo "File is not an image.";
        //         $uploadOk = 0;
        //     }
        // }
        // Check if file already exists
        if (file_exists($target_file)) {
            //echo "Error, file already exists.";
            //$uploadOk = 0;
        }
        // Check file size
        if ($_FILES[$formFieldName]["size"] > 500000) {
            return "Error, your file is too large.";
            $uploadOk = 0;
        }
        // Allow certain file formats
        if ($fileType != "jpg" && $fileType != "png" && $fileType != "jpeg"
            && $fileType != "pdf") {
            return "Error, only JPG, JPEG, PNG & PDF files are allowed.";
            $uploadOk = 0;
        }
        // Check if $uploadOk is set to 0 by an error
        if ($uploadOk == 0) {
            return "Error, your file was not uploaded.";
            // if everything is ok, try to upload file
        } else {
            if (move_uploaded_file($_FILES[$formFieldName]["tmp_name"], $target_file)) {
                return $target_file;
            } else {
                return "Error, there was an error uploading your file.";
            }
        }
    }

    public function admin_users($f3)
    {
        try {
            $this->authCheck($f3);

            $users = $this->getRepository('users')->getByAttribute('usr_role', 2);

            $f3->set('users', $users);
            $f3->set('content', '../ui/pages/admin/users.html');

            echo \Template::instance()->render('template.html');
        } catch (Exception $exception) {
            return $exception->getMessage();
        }
    }

    public function user_details($f3, $params)
    {
        try {
            $this->authCheck($f3);
            if ($f3->get('POST')) {
                $f3->set('SESSION.count', 1);

                $user_id = $params['id'];

                $idArray = ['usr_id = ?', $user_id];

                $email = $f3->get("POST.usr_email");
                $users = $this->getRepository('users')->getByAttribute('usr_id', $user_id);

                if (!empty($f3->get('POST.usr_phone'))) {
                    $formFieldName = 'usr_img';
                    $folder = "user_" . $f3->get('POST.usr_phone');
                    $f3->set('POST.usr_img', "uploads/$folder/avatar.jpg");
                    $this->fileUpload($f3, $formFieldName, $folder);
                }
                if (!empty($f3->get('POST.usr_password'))) {
                    $password = hash('sha256', $f3->get('POST.usr_password'));
                    $f3->set('POST.usr_password', $password);
                } else {
                    $f3->set('POST.usr_password', $users[0]['usr_password']);
                }

                if (!$f3->get('POST.usr_active')) {
                    $f3->set('POST.usr_active', 'n');
                }

                $attributes = $f3->get('POST');
                $definition = $this->getDefinition($attributes);

                $user = $this->getRepository('users')->updateRecord($idArray, $definition);

                $f3->set('SESSION.message', array('msg' => 'Profile updated successfully!', 'alert' => 'info'));

                $f3->reroute("/admin/users");
            }
            $user_id = $params['id'];
            $users = $this->getRepository('users')->getByAttribute('usr_id', $user_id);

            $groups = $this->getRepository('groups')->getByAttribute('grp_deleted', 'n');

            $f3->set('groups', $groups);

            $f3->set('POST', $users[0]);
            $f3->set('content', '../ui/pages/admin/user.html');

            echo \Template::instance()->render('template.html');
        } catch (Exception $exception) {
            return $exception->getMessage();
        }
    }

    public function register($f3)
    {
        try {
            if ($f3->get('POST')) {
                $f3->set('SESSION.count', 1);

                $email = $f3->get("POST.usr_email");
                $users = $this->getRepository('users')->getByAttribute('usr_email', $email);

                if (sizeof($users) > 0) {
                    $f3->set('SESSION.message', array('msg' => 'A profile with the entered email address already exists', 'alert' => 'warning'));
                    $f3->reroute("/register");
                }

                $formFieldName = 'usr_img';
                $code = md5("golf" . time());

                $folder = "user_" . $f3->get('POST.usr_phone');
                $f3->set('POST.usr_img', "uploads/$folder/avatar.jpg");
                $f3->set('POST.usr_created', date("Y-m-d"));
                $f3->set('POST.usr_vercode', $code);
                $f3->set('POST.usr_role', 2);
                $password = hash('sha256', $f3->get('POST.usr_password'));
                $f3->set('POST.usr_password', $password);

                $attributes = $f3->get('POST');
                $definition = $this->getDefinition($attributes);

                $user = $this->getRepository('users')->createRecord($definition);
                $user_id = $user->usr_id;
                $recipient = $f3->get('POST.usr_email');
                $user_name = $f3->get('POST.usr_firstname');

                $this->fileUpload($f3, $formFieldName, $folder);

                $message = '<p>Hi ' . $user_name . ',</p><p>To activate your Golf Practice Tracker profile, click on this link: </p>
                                <table role="presentation" border="0" cellpadding="0" cellspacing="0" class="btn btn-primary">
                                  <tbody>
                                    <tr>
                                      <td align="left">
                                        <table role="presentation" border="0" cellpadding="0" cellspacing="0">
                                          <tbody>
                                            <tr>
                                              <td> <a href="http://' . $_SERVER['HTTP_HOST'] . '/activate/' . $code . '" target="_blank" class="btn btn-primary">Activate</a> </td>
                                            </tr>
                                            </tbody>
                                            </table>
                                            </td>
                                            </tr>
                                            </tbody>
                                            </table>';

                $subject = "Golf Practice Tracker Activation";

                $this->dispatchEmail($recipient, $subject, $message, $user_name);

                $f3->set('SESSION.message', array('msg' => 'User created successfully! Please check your email to activate your profile', 'alert' => 'info'));

                $f3->reroute("/login");
            }
            $f3->set('content', '../ui/pages/auth/register.html');

            echo \Template::instance()->render('login_template.htm');
        } catch (Exception $exception) {
            echo json_encode($exception->getMessage());
        }
    }

    public function activate($f3, $params)
    {
        try {

            $code = $params['code'];
            $f3->set('POST.usr_verified', 'y');
            $attributes = $f3->get('POST');
            $definition = $this->getDefinition($attributes);

            $user = $this->getRepository('users')->updateRecord(array('usr_vercode =?', $code), $definition);

            if ($user) {
                $f3->set('SESSION.count', 1);
                $f3->set('SESSION.message', array('msg' => 'Your profile has been activated successfully! Please log in to your account', 'alert' => 'info'));
            } else {
                $f3->set('SESSION.count', 1);
                $f3->set('SESSION.message', array('msg' => 'Your profile has been not been activated successfully! Please contact support', 'alert' => 'danger'));
            }

            $f3->reroute("/login");
        } catch (Exception $exception) {
            echo json_encode($exception->getMessage());
        }
    }

    public function delete($f3, $params)
    {
        try {
            $this->getRepository('cps_users')->deleteRecord($params['id']);
            $f3->set('SESSION.message', array('msg' => 'User deleted successfully!', 'alert' => 'success'));

            $f3->reroute("/users");
        } catch (Exception $exception) {
            echo json_encode($exception->getMessage());
        }
    }

    public function update($f3, $params)
    {
        try {
            $attributes = $f3->get('POST');
            $definition = $this->getDefinition($attributes);

            $this->getRepository('cps_users')->updateRecord(array('id' => $params['id']), $definition);
            $f3->set('SESSION.message', array('msg' => 'User updated successfully!', 'alert' => 'success'));

            $f3->reroute("/users");
        } catch (Exception $exception) {
            echo json_encode($exception->getMessage());
        }
    }

    public function viewRequests($f3)
    {
        $userID = $f3->get('SESSION.USER.id');
        global $db;
        $query = "SELECT *, (SELECT COUNT(id) FROM formsubmissions WHERE forms.id = formID AND userID = :userID) AS submissions FROM forms WHERE state = :value";
        $vars = array(
            ':userID' => $userID,
            ':value' => 1,
        );
        $forms = $db->exec($query, $vars);
        $f3->set('forms', $forms);

        $f3->set('content', '../ui/user/requests/index.html');
        echo \Template::instance()->render('user_template.htm');
    }

    public function formPost($f3)
    {
        try {
            $userID = $f3->get('SESSION.USER.id');
            $attributes = $f3->get('POST');
            global $db;

            //get form fields
            $query = "SELECT * FROM fields WHERE formID = :formID ORDER BY id DESC";
            $vars = array(
                ':formID' => $attributes['formID'],
            );
            $fields = $db->exec($query, $vars);

            //save submission
            $query = "INSERT INTO `formsubmissions` (`formID`, `userID`, `dateCreated`)
            VALUES (:formID, :userID, :dateCreated)";
            $vars = array(
                ':formID' => $attributes['formID'],
                ':userID' => $userID,
                ':dateCreated' => date('Y-m-d H:i:s'),
            );
            $submission = $db->exec($query, $vars);
            $submissionID = $db->lastInsertId();

            for ($i = 0; $i < count($fields); $i++) {

                if ($fields[$i]['type'] == 'file') {

                    $folder = "user_" . $userID;
                    $formFieldName = 'file_upload';
                    $name = $attributes['field-' . $fields[$i]['id']];
                    $f3->set('POST.file_upload', "uploads/$folder/$name");
                    $resultUrl = $this->fileUpload($f3, $formFieldName, $folder);

                    //insert applicant-data
                    $query = "INSERT INTO `applicantdata` (`userID`, `dataType`, `data`, `fieldID`, `submissionID`, `dateCreated`)
                    VALUES (:userID, :dataType, :data, :fieldID, :submissionID, :dateCreated)";
                    $vars = array(
                        ':userID' => $userID,
                        ':dataType' => $fields[$i]['type'],
                        ':data' => $resultUrl,
                        ':fieldID' => $fields[$i]['id'],
                        ':submissionID' => $submissionID,
                        ':dateCreated' => date('Y-m-d H:i:s'),
                    );
                    $results = $db->exec($query, $vars);
                } else {
                    //insert applicant-data
                    $query = "INSERT INTO `applicantdata` (`userID`, `dataType`, `data`, `fieldID`, `submissionID`, `dateCreated`)
                    VALUES (:userID, :dataType, :data, :fieldID, :submissionID, :dateCreated)";
                    $vars = array(
                        ':userID' => $userID,
                        ':dataType' => $fields[$i]['type'],
                        ':data' => $attributes['field-' . $fields[$i]['id']],
                        ':fieldID' => $fields[$i]['id'],
                        ':submissionID' => $submissionID,
                        ':dateCreated' => date('Y-m-d H:i:s'),
                    );
                    $results = $db->exec($query, $vars);
                }
            }

            $f3->set('SESSION.message', array('msg' => 'Form Submitted successfully!', 'alert' => 'success'));
            $f3->reroute("/user/additional-info");
        } catch (Exception $exception) {
            return $exception->getMessage();
        }
    }
}
