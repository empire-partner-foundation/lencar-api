<?php

require_once 'AppDefinition.php';

require_once 'AppRepository.php';

require_once 'utilities/Auth.php';

require_once 'utilities/Emailer.php';

require_once 'utilities/Response.php';

require_once 'passwordController.php';

require_once 'utilities/PasswordGenerator.php';

require_once 'SendGrid/SendGrid.php';
require_once 'SendGrid/Mail.php';

use SoapClient;


abstract class AppController
{

    protected $f3;

    public function __construct()
    {
        $f3 = Base::instance();
        $this->f3 = $f3;

        if (Auth::user($f3)) {
            $f3->set('auth', array('user' => Auth::user($f3)));
        }
    }

    protected function authCheck($f3)
    {
        $f3->set('SESSION.count', 1);
        if (!Auth::user($f3)) {
            $f3->set('SESSION.message', array('msg' => 'You must login to continue', 'alert' => 'danger'));
            $f3->reroute('/login');
        }
        if ($f3->get('SESSION.LAST_ACTIVITY') + 3600 < time() && $f3->get('SESSION.login') != 'login') {
            $f3->set('SESSION.login', 'login');
            $f3->set('SESSION.message', array('msg' => 'You must login to continue', 'alert' => 'danger'));
            $f3->reroute('/login');
        }
    }



    public function fileUpload($f3, $formFieldName, $folder)
    {

        if (!file_exists("uploads/$folder")) {
            mkdir("uploads/$folder", 0777, true);
            chmod("uploads/$folder", 0777);
        }
        $f3->set('UPLOADS', "uploads/$folder/"); // don't forget to set an Upload directory, and make it writable!
        $web = \Web::instance();
        //$mime = $web->mime($file); // returns 'image/jpeg'
        $overwrite = true; // set to true, to overwrite an existing file; Default: false
        $slug = true; // rename file to filesystem-friendly version

        $files = $web->receive(function ($file, $formFieldName) {

            //var_dump($file);
            /* looks like:
            array(5) {
            ["name"] =>     string(19) "csshat_quittung.png"
            ["type"] =>     string(9) "image/png"
            ["tmp_name"] => string(14) "/tmp/php2YS85Q"
            ["error"] =>    int(0)
            ["size"] =>     int(172245)
            } */

            //$extArr = ['jpg', 'jpeg', 'png', 'pdf'];
            $ext = explode(".", $file['name']);

            // // $file['name'] already contains the slugged name now
            // if(!in_array($ext, $fileExt)) // if ext not support
            //     return "This uploaded file is not valid"; //, return false will skip moving it

            // maybe you want to check the file size
            if ($file['size'] > (5 * 1024 * 1024)) // if bigger than 5 MB
            {
                return "Error, your file is too large.";
            }
            // this file is not valid, return false will skip moving it
            // everything went fine, hurray!
            return true; // allows the file to be moved from php tmp dir to your defined upload dir
        }, true, function ($fileBaseName, $formFieldName) {
            // build new file name from base name or input field name
            $ext = substr($fileBaseName,-5);
            $extArr = explode(".",$ext);

            return $formFieldName.".".$extArr[1];
        }, $overwrite, $slug
        );

        return $files;
    }

    protected function dispatchEmail($attributes, $subject, $template)
    {
        $message = file_get_contents("ui/emailTemplates/$template.php");

        //Replace the codetags with the message contents
        $replacements = array(
            '({email})' => $attributes['email'],
            '({password})' => $attributes['password'],
            '({support_email})' => 'support@zelriprop.talentindx.co.za',
            '({login_url})' => 'http://' . $_SERVER['HTTP_HOST']."/login",
            '({help_url})' => 'http://' . $_SERVER['HTTP_HOST'],
            '({welcomeLetter_url})' => 'http://' . $_SERVER['HTTP_HOST']."/".$attributes['welcomeLetter'],
            '({keyslip_url})' => 'http://' . $_SERVER['HTTP_HOST']."/".$attributes['keyslip'],
            '({leaseAgreement_url})' => 'http://' . $_SERVER['HTTP_HOST']."/".$attributes['leaseAgreement'],
            '({approvalLetter_url})' => 'http://' . $_SERVER['HTTP_HOST']."/".$attributes['approvalLetter'],
            '({action_url})' => 'http://' . $_SERVER['HTTP_HOST'] . "/activate"."/".$attributes['verificationCode'],
        );

        $message = preg_replace(array_keys($replacements), array_values($replacements), $message);

        $sendgrid = new SendGrid\SendGrid('keenangeorge', 'Charnte#1');

        // Make a message object
        $email = new SendGrid\Mail();

        $email->setFrom("support@zelriprop.talentindx.co.za", "Zelriprop Team");
        $email->setSubject($subject);
        $email->addTo($attributes['email'], $attributes['firstName']);
        $email->setCcs(array());
        $email->setBccs(array());
        //$email->setText("and easy to do anywhere, even with PHP");
        $email->setHtml($message
        );
        
        try {
            $response = $sendgrid->send($email);
            //print $response->statusCode() . "\n";
            //print_r($response->headers());
            //print $response->body() . "\n";
        } catch (Exception $e) {
            echo 'Caught exception: ' . $e->getMessage() . "\n";
        }
    }
    

    public function xdsLogin($f3, $params){
        global $db, $xdsUrl;

        $strUser = $params['strUser'];//"Smitk_uat";
        $strPwd = $params['strPwd'];//"Sm!t0805";
        $client = new \SoapClient ($xdsUrl); 
        $parameters = array ("strUser"=>$strUser, "strPwd"=>$strPwd); 
        $configQry = $client->Login ($parameters);

        return $configQry->LoginResult;
    }

    public function validateID($f3, $params){
        global $db, $xdsUrl;
        $ConnectTicket =  $params['ConnectTicket'];

        $IdNumber = $params['IDNumber'];
        $FirstName = "";
        $Surname = "";
        $BirthDate = "";
        $YourReference = "";
        $VoucherCode = "";
        $client = new \SoapClient($xdsUrl);
        $parameters = array (
            "ConnectTicket"=>$ConnectTicket,"IdNumber"=>$IdNumber,"PassportNo"=>"","FirstName"=>$FirstName,
            "Surname"=>$Surname,"BirthDate"=>$BirthDate,"YourReference"=>$YourReference,
            "VoucherCode"=>$VoucherCode);
        $ConnectIDVerification = $client->ConnectIDVerification($parameters);
        $ConnectIDVerification = new SimpleXMLElement($ConnectIDVerification->ConnectIDVerificationResult);

        
        return $ConnectIDVerification;
    }

    public function validateMaritalStatus($f3, $params){
        global $db, $xdsUrl;
        $ConnectTicket =  $params['ConnectTicket'];

        $IdNumber = $params['IDNumber'];
        $ProductId = 39;
        $EnquiryReason = "";
        $FirstName = "";
        $Surname = "";
        $BirthDate = "";
        $YourReference = "";
        $VoucherCode = "";
        $client = new \SoapClient($xdsUrl);
        $parameters = array ("ConnectTicket"=>$ConnectTicket,"EnquiryReason"=>$EnquiryReason,"ProductId"=>$ProductId,"IdNumber"=>$IdNumber,"PassportNo"=>"","FirstName"=>$FirstName,"Surname"=>$Surname,"BirthDate"=>$BirthDate,"YourReference"=>$YourReference,"VoucherCode"=>$VoucherCode);

        $ConnectConsumerMatch = $client->ConnectConsumerMatch($parameters);
        $ConnectConsumerMatch = new SimpleXMLElement($ConnectConsumerMatch->ConnectConsumerMatchResult);
        //echo $ConnectConsumerMatch->ConsumerDetails->EnquiryID;
       

        //ConnectGetResult
        $EnquiryID = $ConnectConsumerMatch->ConsumerDetails->EnquiryID;
        $EnquiryResultID = $ConnectConsumerMatch->ConsumerDetails->EnquiryResultID;
        $client = new \SoapClient ($xdsUrl);
        $parameters = array (
            "ConnectTicket"=>$ConnectTicket,"EnquiryID"=>$EnquiryID,"EnquiryResultID"=>$EnquiryResultID,
            "ProductID"=>$ProductId);
        $ConnectGetResult = $client->ConnectGetResult($parameters);
        $ConnectGetResult = new SimpleXMLElement($ConnectGetResult->ConnectGetResultResult);

        return $ConnectGetResult;
    }

    public function getCreditReportPdf($f3, $params){
        global $db, $xdsUrl;

        $IdNumber = $params['creditIDNo'];
        $userID = $params['userID'];
        $ConnectTicket = $params['ConnectTicket'];

        $client = new \SoapClient ($xdsUrl);
        $parameters = array (
            "ConnectTicket"=>$ConnectTicket,"IdNumber"=>$IdNumber, "PassportNo"=>"");
        $ConnectGetConsumerCreditReportBinary = $client->ConnectGetConsumerCreditReportBinary($parameters);
        $pdfString = $ConnectGetConsumerCreditReportBinary->ConnectGetConsumerCreditReportBinaryResult;
        //echo" ConnectGetConsumerCreditReportBinary";
        $data = base64_encode($pdfString);
        $uploadFolder ="reports/user_$userID";

        if (!is_dir($uploadFolder)) {
            mkdir($uploadFolder, 0777, true);
        }
        $reportFile = $uploadFolder."/creditReport_$IdNumber.pdf";


        if(file_put_contents($reportFile, base64_decode($data))){

            return $reportFile;
        }
        else{
            return $pdfString;
        }

         
    }

    public function getCreditReport($f3, $params){
        global $db, $xdsUrl;
        $ConnectTicket =  $params['ConnectTicket'];

        $IdNumber = $params['IDNumber'];
        $ProductId = 15;
        $EnquiryReason = "";
        $FirstName = "";
        $Surname = "";
        $BirthDate = "";
        $YourReference = "";
        $VoucherCode = "";
        $client = new \SoapClient($xdsUrl);
        $parameters = array ("ConnectTicket"=>$ConnectTicket,"EnquiryReason"=>$EnquiryReason,"ProductId"=>$ProductId,"IdNumber"=>$IdNumber,"PassportNo"=>"","FirstName"=>$FirstName,"Surname"=>$Surname,"BirthDate"=>$BirthDate,"YourReference"=>$YourReference,"VoucherCode"=>$VoucherCode);

        $ConnectConsumerMatch = $client->ConnectConsumerMatch($parameters);
        $ConnectConsumerMatch = new SimpleXMLElement($ConnectConsumerMatch->ConnectConsumerMatchResult);
        //echo $ConnectConsumerMatch->ConsumerDetails->EnquiryID;
       

        //ConnectGetResult
        $EnquiryID = $ConnectConsumerMatch->ConsumerDetails->EnquiryID;
        $EnquiryResultID = $ConnectConsumerMatch->ConsumerDetails->EnquiryResultID;
        $client = new \SoapClient ($xdsUrl);
        $parameters = array (
            "ConnectTicket"=>$ConnectTicket,"EnquiryID"=>$EnquiryID,"EnquiryResultID"=>$EnquiryResultID,
            "ProductID"=>$ProductId);
        $ConnectGetResult = $client->ConnectGetResult($parameters);
        $ConnectGetResult = new SimpleXMLElement($ConnectGetResult->ConnectGetResultResult);

        return $ConnectGetResult;
    }

    public function getAccountVerification($f3, $params){
        global $db, $xdsUrl;

        $ConnectTicket =  $params['ConnectTicket'];

        $VerificationType = 'Individual';        
        $Entity ="None";
        $Initials = $params['initials'];
        $SurName = $params['lastName'];
        $IDNo = $params['IDNumber'];
        $IDType = "SID";
        $CompanyName = "";
        $Reg1 = "";
        $Reg2 = "";
        $Reg3 = "";
        $TrustNo ="";
        $AccNo = $params['accountNumber'];
        $BranchCode = $params['branchCode'];
        $Acctype = $params['accountType'];
        $BankName = $params['bankName'];
        $client = new \SoapClient ($xdsUrl);
        $parameters = array (
            "ConnectTicket"=>$ConnectTicket,"VerificationType"=>$VerificationType,"Entity"=>$Entity,"Initials"=>$Initials,"SurName"=>$SurName,
            "IDNo"=>$IDNo,"IDType"=>"SID",  "CompanyName"=>"","Reg1"=>"","Reg2"=>"","Reg3"=>"","TrustNo"=>"","AccNo"=>$AccNo,
            "BranchCode"=>$BranchCode,"Acctype"=>$Acctype,"BankName"=>$BankName,
            "VoucherCode"=>"","YourReference"=>"");
        $ConnectAccountVerificationRealTime = $client->ConnectAccountVerificationRealTime($parameters);
        $ConnectAccountVerificationRealTime = new SimpleXMLElement($ConnectAccountVerificationRealTime->ConnectAccountVerificationRealTimeResult);

        $EnquiryLogID = "70395789";//$ConnectAccountVerificationRealTime->ReferenceNo;
        $client = new \SoapClient ($xdsUrl);
        $parameters = array (
            "ConnectTicket"=>$ConnectTicket,"EnquiryLogID"=>$EnquiryLogID);
        $ConnectGetAccountVerificationResult = $client->ConnectGetAccountVerificationResult($parameters);
        $ConnectGetAccountVerificationResult = new SimpleXMLElement($ConnectGetAccountVerificationResult->ConnectGetAccountVerificationResultResult);

        return $ConnectGetAccountVerificationResult;
    }

    public function xdsVerification($f3, $params)
    {
        global $db;
        $this->authCheck($f3);

        $query = "SELECT *  from applicantdetails WHERE userID = :id";
        $vars = array(
            ':id' => $params['userID'],
        );
        $results = $db->exec($query, $vars);

        $params["IDNumber"] = $results[0]["idPassportNo"];
        $params["initials"] = substr($results[0]["lastName"], 0,1);
        $params["lastName"] = $results[0]["lastName"];

        $params["accountNumber"] = $results[0]["accountNumber"];
        $params["branchCode"] = $results[0]["branchCode"];
        $params["bankName"] = $results[0]["bankName"];
        $params["accountType"] = $results[0]["accountType"];

        $params["maritalStatus"] = $results[0]["maritalStatus"];
        $params["spouseIdPassportNo"] = $results[0]["spouseIdPassportNo"];

        $params['strUser'] = "Smitk_uat";
        $params['strPwd'] = "Sm!t0805";

        $verifiedData = [];

        $ConnectTicket = $this->xdsLogin($f3, $params);

        $params['ConnectTicket'] = $ConnectTicket;


        ///////////////////////////////////////////////////////////////////////////////////////////////////////
        //verify ID
        $idVerificationResponse = $this->validateID($f3, $params);
        // echo"<br><br>";

        // var_dump($idVerificationResponse);
        
        // echo"<br><br>";
        

        //ID of person is Deceased Status
        if ($idVerificationResponse->ConsumerDetails->DeceasedStatus == "Active") {

            $firstName = strtolower($results[0]["firstName"]);
            $lastName = strtolower($results[0]["lastName"]);
            $idPassportNo = strtolower($results[0]["idPassportNo"]);

            //array_push($verifiedData, "email");
            array_push($verifiedData, "idPassportNo");
            array_push($verifiedData, "dob");

            if (strtolower($idVerificationResponse->ConsumerDetails->FirstName) == $firstName) {
                array_push($verifiedData, "firstName");
            }

            if (strtolower($idVerificationResponse->ConsumerDetails->Surname) == $lastName) {
                array_push($verifiedData, "lastName");
            }
            //////////////////////////////////////////////////////////////////////////////////////////////////////
            //getCredit Report for Main User
            $params['creditIDNo'] = $results[0]["idPassportNo"];
            $getCreditReportPdf = $this->getCreditReportPdf($f3, $params);

            $query = "INSERT INTO applicantdata (userID, data, dataType, dataLabel, dateCreated, dateUpdated)
                                VALUES (:userID, :data, :dataType, :dataLabel, NOW(), NOW())";
            $vars = array(
                ':userID' => $params['userID'],
                ':data' => $getCreditReportPdf,
                ':dataType' => 'document',
                ':dataLabel' => 'creditReport',
            );
            $applicantdata = $db->exec($query, $vars);


            //////////////////////////////////////////////////////////////////////////////////////////////////////
            //verify Marital Status
            $validateMaritalStatus = $this->validateMaritalStatus($f3, $params);
            if (strtolower($validateMaritalStatus->ConsumerDetail->MaritalStatusDesc) == strtolower($results[0]["maritalStatus"])) {
                array_push($verifiedData, "maritalStatus");
            }


            //credit Report for Spouse
            if($results[0]["maritalStatus"] == "Married" ||  $validateMaritalStatus->ConsumerDetail->MaritalStatusDesc == "Married"){
                //normal query
                if($results[0]["maritalStatus"] == "Married") {
                    $params['creditIDNo'] = $results[0]["spouseIdPassportNo"];

                    $getCreditReportPdf = $this->getCreditReportPdf($f3, $params);

                    $vars = array(
                        ':userID' => $params['userID'],
                        ':data' => $getCreditReportPdf,
                        ':dataType' => 'document',
                        ':dataLabel' => 'creditReport',
                    );
                    $applicantdata = $db->exec($query, $vars);
                }
                //if status still says married from DHA
                if($results[0]["maritalStatus"] != "Married" && $validateMaritalStatus->ConsumerDetail->MaritalStatusDesc == "Married") {
                    $getCreditReportPdf = $this->getCreditReportPdf($f3, $params);

                    $vars = array(
                        ':userID' => $params['userID'],
                        ':data' => $getCreditReportPdf,
                        ':dataType' => 'document',
                        ':dataLabel' => 'creditReport',
                    );
                    $applicantdata = $db->exec($query, $vars);
                }
                
                //if return spouse ID is different from one on db
                if($results[0]["maritalStatus"] == "Married" && $validateMaritalStatus->ConsumerMaritalStatusEnquiry->SpouseIDno !=  $params['creditIDNo']) {
                    $getCreditReportPdf = $this->getCreditReportPdf($f3, $params);

                    $vars = array(
                        ':userID' => $params['userID'],
                        ':data' => $getCreditReportPdf,
                        ':dataType' => 'document',
                        ':dataLabel' => 'creditReport',
                    );
                    $applicantdata = $db->exec($query, $vars);
                }


            }

            ///////////////////////////////////////////////////////////////////////////////////////////////////////
            //get Verify Bank Details
            // $getAccountVerification = $this->getAccountVerification($f3, $params);
            // if (isset($getAccountVerification->ResultFile->ACCOUNTNUMBER)) {
            //     array_push($verifiedData, "accountNumber");
            //     array_push($verifiedData, "branchCode");
            //     array_push($verifiedData, "accountType");
            //     array_push($verifiedData, "bankName");
            // }

            $update = false;

            $adminID = "xdsApi";

            foreach ($verifiedData as $column) {

                $query = "SELECT * FROM userChecklist WHERE userID = :userID AND title = :title";
                $vars = array(
                    ':userID' => $params['userID'],
                    ':title' => $column,
                );
                $userdata = $db->exec($query, $vars);

                if (sizeof($userdata) > 0) {
                    $update = true;
                }

                //$dataArr = explode("_", $f3->get("POST.$column"));
                if ($update) {
                    $query = "UPDATE userChecklist SET approvedBy=:adminID, isVerified=:isVerified WHERE id=:id";
                    $vars = array(
                        ':adminID' => $adminID,
                        ':isVerified' => "y",
                        ':id' => $userdata[0]['id'],
                    );
                    $userChecklist = $db->exec($query, $vars);
                } else {
                    $query = "INSERT INTO userChecklist (userID, approvedBy, title, isVerified, dateCreated, dateUpdated)
                            VALUES (:userID, :approvedBy, :title, :isVerified, NOW(), NOW())";
                    $vars = array(
                        ':userID' => $params['userID'],
                        ':approvedBy' => $adminID,
                        ':title' => $column,
                        ':isVerified' => "y",
                    );
                    $userChecklist = $db->exec($query, $vars);
                }

            }
            //$f3->reroute('/user/dashboard');
        }
    }



    protected function getRepository($table)
    {
        return new AppRepository($table);
    }

    protected function getDefinition($attributes)
    {
        return new AppDefinition($attributes);
    }

}
