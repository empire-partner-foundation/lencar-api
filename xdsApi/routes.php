<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * Start Routing
 */

 //filedownloads
$f3->route('GET /downloads/@filename',
function($f3,$args) {
    // send() method returns FALSE if file doesn't exist
    if (!Web::instance()->send($args['filename']))
        // Generate an HTTP 404
    $f3->error(404);
}
);

$f3->route('GET|POST /xdsApi/getConnectTicket', 'XdsController->getConnectTicket');
$f3->route('GET|POST /getCreditReport', 'XdsController->getCreditReportPdf');

 //$f3->route('GET /login', 'AuthController->loginPage');
// $f3->route('POST /login', 'AuthController->loginPost');

// $f3->route('GET|POST /verify/@email/@verificationCode', 'AuthController->verifyPage');
// $f3->route('GET|POST /verify/@email', 'AuthController->verifyPost');

// $f3->route('GET|POST /activate/@code', 'AuthController->activate');


// $f3->route('GET /password/reset', 'AuthController->passwordResetPage');
// $f3->route('POST /password/reset', 'AuthController->passwordResetPost');
// $f3->route('GET /password/reset/@email/@verificationCode/@resetStatus', 'AuthController->passwordResetChange');

// $f3->route('GET /register', 'AuthController->registerPage');
// $f3->route('POST /register', 'AuthController->registerPost');

/*
 *Landing Page
 */
$f3->route('GET|POST /',
    function ($f3) {
        phpinfo();
    }
);

// $f3->route('GET /index',
//     function ($f3) {
//         $f3->set('content', 'welcome.htm');
//         echo View::instance()->render('welcome_template.htm');
//     }
// );

// /*
//  * Contact page
//  */
// $f3->route('GET|POST /contact',
//     function ($f3) {
//         $f3->set('content', 'contact.htm');
//         echo View::instance()->render('welcome_template.htm');
//     }
// );

// $f3->route('GET|POST /forgotPassword', 'AuthController->forgot');

// $f3->route('GET|POST /activate/@code', 'AuthController->activate');

// $f3->route('GET|POST /logout', 'AuthController->logout');


// $access->deny('/admin*');
// $access->allow('/admin*', 1); // allow "admin" to access /secured.htm

// $user = $f3->get('SESSION.USER.role');

// $access->authorize($user, function ($route, $subject) {
//     global $f3;
//     $f3->set('SESSION.count', 4);
//     $f3->set('SESSION.message', array('msg' => "You are not authorized to access $route", 'alert' => 'warning'));
//     $f3->reroute("/");
// });

// /*
//  * Admin
//  */
// $f3->route('GET|POST /admin', 'AdminController->dashboard');

// /*
//  * Pages
//  */
// $f3->route('GET|POST /admin/units', 'TenantPortalController->viewUnits');
// $f3->route('GET|POST /admin/units/create', 'TenantPortalController->createUnit');
// $f3->route('GET|POST /admin/units/update/@id', 'TenantPortalController->updateUnit');
// $f3->route('GET|POST /admin/units/delete/@id', 'TenantPortalController->deleteUnit');
// $f3->route('GET|POST /admin/units/upload/@fileName', 'AdminController->uploadDocuments');




// $f3->route('GET|POST /admin/complex', 'TenantPortalController->viewComplex');
// $f3->route('GET|POST /admin/complex/create', 'TenantPortalController->createComplex');
// $f3->route('GET|POST /admin/complex/update/@id', 'TenantPortalController->updateComplex');
// $f3->route('GET|POST /admin/complex/delete/@id', 'TenantPortalController->deleteComplex');

// $f3->route('GET|POST /admin/trench', 'TenantPortalController->viewTrench');
// $f3->route('GET|POST /admin/trench/create', 'TenantPortalController->createTrench');
// $f3->route('GET|POST /admin/trench/update/@id', 'TenantPortalController->updateTrench');
// $f3->route('GET|POST /admin/trench/delete/@id', 'TenantPortalController->deleteTrench');

// $f3->route('GET|POST /admin/documents', 'AdminController->viewDocuments');
// $f3->route('GET|POST /admin/documents/create', 'AdminController->createDocuments');
// $f3->route('GET|POST /admin/documents/update/@id', 'AdminController->updateDocuments');
// $f3->route('GET|POST /admin/documents/delete/@id', 'AdminController->deleteDocuments');


// $f3->route('GET|POST /admin/checklist', 'TenantPortalController->viewChecklist');

// $f3->route('GET|POST /admin/applications', 'AdminController->viewApplications');
// $f3->route('GET|POST /admin/application/status/@status/@id', 'AdminController->updateApplicationStatus');
// $f3->route('GET|POST /admin/application/delete/@id', 'AdminController->deleteApplication');
// $f3->route('GET|POST /admin/application/view/@id', 'AdminController->viewApplication');
// $f3->route('GET|POST /admin/application/edit/@id', 'AdminController->editApplication');
// $f3->route('GET|POST /admin/application/approve', 'AdminController->approveApplication');
// $f3->route('GET|POST /admin/application/verifyProofOfPayment', 'AdminController->verifyProofOfPayment');





// $f3->route('GET|POST /admin/tenants', 'TenantPortalController->viewTenants');


// $f3->route('GET|POST /admin/finance', 'TenantPortalController->viewFinance');
// $f3->route('GET|POST /admin/invoices', 'TenantPortalController->viewInvoices');
// $f3->route('GET|POST /admin/forms', 'TenantPortalController->viewForms');
// $f3->route('GET|POST /admin/form/@id/results', 'TenantPortalController->viewResults');
// $f3->route('GET|POST /admin/form/update/@id', 'TenantPortalController->updateForm');
// $f3->route('GET|POST /admin/form/delete/@id', 'TenantPortalController->deleteForm');

// $f3->route('GET|POST /admin/users', 'AdminController->viewUsers');
// $f3->route('GET|POST /admin/users/create', 'AdminController->createUser');
// $f3->route('GET|POST /admin/users/update/@id', 'AdminController->updateUser');
// $f3->route('GET|POST /admin/users/delete/@id', 'AdminController->deleteUser');

// $f3->route('GET|POST /admin/groups', 'GroupsController->index');
// $f3->route('GET|POST /admin/groups/@action', 'GroupsController->@action');
// $f3->route('GET|POST /admin/groups/@action/@id', 'GroupsController->@action');

// ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// /*
//  * User
//  */
// $f3->route('GET|POST /user/dashboard', 'UsersController->dashboard');

// $f3->route('GET|POST /user/onboarding', 'UsersController->onboarding');

// $f3->route('GET|POST /user/proceed', 'UsersController->proceed');

// /*
//  * Pages
//  */
// $f3->route('GET|POST /user/application', 'UsersController->viewApplications');
// $f3->route('GET|POST /user/application/create', 'UsersController->createApplication');
// $f3->route('GET|POST /user/application/update', 'UsersController->updateApplication');
// $f3->route('GET|POST /user/documents', 'UsersController->viewDocuments');

// $f3->route('GET|POST /user/profile', 'UsersController->profile');
// $f3->route('GET|POST /user/profile/update', 'UsersController->updateProfile');
// $f3->route('GET|POST /user/profile/upload', 'UsersController->uploadDocuments');


// $f3->route('GET|POST /user/invoices', 'UsersController->invoices');


// $f3->route('GET|POST /user/additional-info', 'UsersController->viewRequests');
// $f3->route('POST /additional-info', 'UsersController->formPost');

// //######################################################################################################################

// //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// /*
//  * Ajax Calls
//  */
// $f3->route('GET|POST /formsave/@formtype [ajax]', 'AjaxController->formsave');
// $f3->route('GET|POST /adminformsave/@formtype [ajax]', 'AjaxController->adminFormSave');
// $f3->route('GET|POST /verifyForm/@formtype [ajax]', 'AjaxController->verifyForm');
// $f3->route('GET|POST /getApplicationVerification/@applicationID [ajax]', 'AjaxController->getApplicationVerification');
// $f3->route('GET|POST /approveApplication/@applicationID [ajax]', 'AjaxController->approveApplication');
// $f3->route('GET|POST /getProofOfPayment/@applicationID [ajax]', 'AjaxController->getProofOfPayment');




// $f3->route('GET|POST /@action [ajax]', 'AjaxController->@action');

// $f3->route('GET|POST /contact_form [ajax]', 'AjaxController->contactForm');
// $f3->route('GET|POST /ajaxdata/@formkey/@id [ajax]', 'AjaxController->ajax_data');

// $f3->route('GET|POST /form/create/ [ajax]', 'AjaxController->create_form');
// $f3->route('GET|POST /form/update/ [ajax]', 'AjaxController->update_form');


$f3->route('GET|POST /user/applications/create/ [ajax]', 'AjaxController->createApplication');
$f3->route('GET|POST /user/form/fields [ajax]', 'AjaxController->form_fields');
$f3->route('GET|POST /user/field/options [ajax]', 'AjaxController->field_options');
$f3->route('GET|POST /admin/form/state [ajax]', 'AjaxController->field_state');




/*
 * Reports
 */
$f3->route('GET|POST /reports', 'ReportsController->index');
$f3->route('GET|POST /reports/monthly/@section/@year/@month', 'ReportsController->monthly_report');
$f3->route('GET|POST /reports/yearly/@section/@year', 'ReportsController->yearly_report');

//message set to empty
if ($f3->get('SESSION.count') == 0) {
    $f3->set('SESSION.count', 0);
    $f3->set('SESSION.message', array('msg' => '', 'alert' => ''));
}

if ($f3->get('SESSION.count') > 0) {
    $curr_sess = 0;
    $curr_sess = (integer) $f3->get('SESSION.count');
    $curr_sess = $curr_sess - 1;

    $f3->set('SESSION.count', $curr_sess);
}
$f3->set('SESSION.count', 0);

$f3->set('SESSION.LAST_ACTIVITY', time());
