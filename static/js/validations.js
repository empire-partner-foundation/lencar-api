/*
 *  validations.js
 *  property of Twigg Technology (Pty) Ltd
 *  this version released 18 Feb 2019
 *
 * REQUIRED: jQuery library
 * VALID YEARS:
 *    1950 - 2049 for functions: validDateDMY(), validDateYMD()
 *    1945 - 2044 for functions: validIDnum()
*/

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// defaults
if (typeof errMsgHover === 'undefined')
   var errMsgHover = true;

if (typeof HasChanged === 'undefined')
   var HasChanged = false;

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// when you add class has-error, use this function to add an error message to an input / textarea / etc
// when you remove class has-error, call the function without a message and any existing error message will be removed
// USAGE: if errMsgHover = true then the message will display on mouseover,
//        otherwise it will be displayed on the screen underneath the input/select with the error
// NOTE: if the error message is lost underneath the border of the parent element, give the input a permanent class of "err-go-up"
function addErrMsg(thisid, thismsg, thisgroup)
{
   // remove error message
   $("#errmsg_"+thisid).remove();
   if (!errMsgHover)
      $("#"+thisid).removeClass("NoMarginBtm");

   // if a message has been passed, display it
   if (typeof thismsg !== 'undefined')
   {
      var thisclass = "errmsg";
      if (typeof thisgroup === 'boolean' && thisgroup)
      {
         thisclass = "grouperrmsg";
         $("[id="+thisid+"]").hover(function() {$("#errmsg_"+thisid).show();}, function() {$("#errmsg_"+thisid).hide();});
      }

      if (errMsgHover)
         $("[id="+thisid+"]:last").after("<span class='"+thisclass+"' id='errmsg_"+thisid+"' >"+thismsg+"</span>");
      else
         $("[id="+thisid+"]:last").addClass("NoMarginBtm").after("<div class='"+thisclass+"' id='errmsg_"+thisid+"'>** "+thismsg+"</div>");
   }
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// polyfiller for IE
Number.isInteger = Number.isInteger || function(value)
{
   return typeof value === 'number' &&
          isFinite(value) &&
          Math.floor(value) === value;
};


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// clean input by removing non-printing chars
function CleanInput(str)
{
   return str.replace(/[^\x20-\x7E]+/g, '');
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// *** these functions used charcode and keycode - both deprecated!! ***
function TextOnly(e) {}
function NumbersOnly(e,tel) {}
function TextAreaOnly(e){}
// how to handle old code using textonly/textareaonly on onkeypress - deprecated
// so add kdtextonly/kdtextareaonly to onkeydown wherever the first is found
$(document).ready(function(){
   $(":text, input[type=text]").each(function(){
      var test = $(this).attr("onkeypress");
      if (test && test.indexOf("return TextOnly") >= 0)
         $(this).on("keydown", kdTextOnly);
      else if (test && test.indexOf("return NumbersOnly") >= 0)
         $(this).on("keydown", kdNumbersOnly);
   });
   $("textarea").each(function(){
      var test = $(this).attr("onkeypress");
      if (test && test.indexOf("return TextAreaOnly") >= 0)
         $(this).on("keydown", kdTextAreaOnly);
   });
})

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// *** Use - MUST BE: onkeydown="kdTextOnly(event)"
// prevention - limit chars that can be used
// allow backspace 8, tab 9, space 32, ! 33, end 35, home 36, left arrow 37, & 38, ' 39, right arrow 39, ( 40, ) 41,
//   * 42, + 43, comma 44, - 45, . 46, del 46, / 47, : 58, ; 59, ? 63, _ 95
function kdTextOnly(e)
{
   var myKey = e.key ? e.key : "Unidentified";
   if (myKey != "Unidentified")
   {
      // https://developer.mozilla.org/en-US/docs/Web/API/KeyboardEvent/key/Key_Values ... last 4 are IE specific
      var keyAOK = ["Backspace","Delete","Home","End","ArrowLeft","ArrowRight","Tab","Copy","Cut","Paste","Undo","Redo","Clear","Spacebar","Left","Right","Del"];
      var aok = $.inArray(myKey, keyAOK);
      var keySOK = "!&*()-_+:;,.?/' 0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
      var sok = keySOK.indexOf(myKey);
      if (aok < 0 && sok < 0)
         e.preventDefault();
   }
   else
   {
      // Safari
      // https://www.w3schools.com/charsets/ref_utf_basic_latin.asp
      var unicode = e.charCode ? e.charCode : e.keyCode;
      var charOK = [8,9,32,33,35,36,37,38,39,40,41,42,43,44,45,46,47,58,59,63,95];
      var aok = $.inArray(unicode, charOK);
      if (aok < 0)
         if (unicode<48 || (unicode>57 && unicode<65) || (unicode>90 && unicode<97) || unicode>122)
            e.preventDefault();
   }
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// prevention - limit chars that can be used
// *** Use - MUST BE: onkeydown="kdTextAreaOnly(event)"
// allow backspace 8, tab 9, enter 13, space 32, ! 33, end 35, home 36, left arrow 37, & 38, ' 39, right arrow 39, ( 40, ) 41,
//   * 42, + 43, comma 44, - 45, . 46, del 46, / 47, : 58, ; 59, ? 63, _ 95
function kdTextAreaOnly(e)
{
   var myKey = e.key ? e.key : "Unidentified";
   if (myKey != "Unidentified")
   {
      // https://developer.mozilla.org/en-US/docs/Web/API/KeyboardEvent/key/Key_Values ... last 4 are IE specific
      var keyAOK = ["Backspace","Delete","Home","End","ArrowLeft","ArrowRight","ArrowDown","ArrowUp","Tab","Enter","Copy","Cut","Paste","Undo","Redo","Clear","Spacebar","Left","Right","Del"];
      var aok = $.inArray(myKey, keyAOK);
      var keySOK = "!&*()-_+:;,.?/' 0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
      var sok = keySOK.indexOf(myKey);
      if (aok < 0 && sok < 0)
         e.preventDefault();
   }
   else
   {
      // Safari
      // https://www.w3schools.com/charsets/ref_utf_basic_latin.asp
      var unicode = e.charCode ? e.charCode : e.keyCode;
      var charOK = [8,9,13,32,33,35,36,37,38,39,40,41,42,43,44,45,46,47,58,59,63,95];
      var aok = $.inArray(unicode, charOK);
      if (aok < 0)
         if (unicode<48 || (unicode>57 && unicode<65) || (unicode>90 && unicode<97) || unicode>122)
            e.preventDefault();
   }
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// validate a text input - min length and check for unlawful chars
// allows: - _ & : ; , . ? ' / ( ) 0-9 a-z A-Z and space
function validText(fieldID, minlen)
{
   if (typeof minlen === 'undefined') minlen = 2;
   var alphaExp = /^[!&*()_+:;,.?/ 0-9a-zA-Z'-]+$/;
   var field = $.trim($("#"+fieldID).val());
   field = CleanInput(field);
   var fieldLow = field.toLowerCase();

   if (minlen == 0 && field.length == 0 || field.length >= minlen && field.match(alphaExp) && fieldLow.indexOf("http") < 0 && fieldLow.indexOf("www") < 0 && fieldLow.indexOf("ftp") < 0)
   {
      $("#"+fieldID).removeClass("has-error");
      addErrMsg(fieldID);
      HasChanged = true;
      return true;
   }
   else
   {
      $("#"+fieldID).addClass("has-error");
      var msg = "You can only use these characters: a&nbsp;-&nbsp;z&nbsp; A&nbsp;-&nbsp;Z&nbsp; 0&nbsp;-&nbsp;9&nbsp; ! & * ( ) - _ + : ; , . ? / ' &nbsp;as well as spaces";
      if (minlen > 0)
         msg += "\n& you need to enter "+minlen+" or more characters";
      addErrMsg(fieldID, msg);
      return false;
   }
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// validate a text area input - as above but also allows newline, carriage return and tab
// 3rd parameter can either be max lines if the number is less than 13, or max words
function validTextArea(fieldID, minlen, maxLinesWords)
{
   if (typeof minlen === 'undefined' || !Number.isInteger(minlen) || minlen < 0)
      minlen = 2;
   if (typeof maxLinesWords === 'undefined' || !Number.isInteger(maxLinesWords) || maxLinesWords < 1)
   {
      maxlines = -1;
      maxwords = -1;
   }
   else if (maxLinesWords < 13)
   {
      // 1 to 12 must be lines
      maxlines = maxLinesWords;
      maxwords = -1;
   }
   else
   {
      // must be words
      maxlines = -1;
      maxwords = maxLinesWords;
   }

   var alphaExp = /^[!&*()_+:;,.?/ 0-9a-zA-Z\s'-]+$/;    // the final \s allows space, newline, carriage return and tab
   var field = $.trim($("#"+fieldID).val());
   // it's slightly faster to put these functions in the if... but negligable difference and this is clearer
   var countWords = field.trim().split(/\s+\b/).length;  // exclude punctuation, empty == 1 but not important
   var countLines = field.split("\n").length;
   if ((minlen == 0 && field.length == 0) || field.length >= minlen && field.match(alphaExp) && (maxwords < 0 || countWords <= maxwords) && (maxlines < 0 || countLines <= maxlines))
   {
      $("#"+fieldID).removeClass("has-error");
      addErrMsg(fieldID);
      HasChanged = true;
      return true;
   }
   else
   {
      $("#"+fieldID).addClass("has-error");
      var msg = "You can only use these characters: a - z&nbsp; A - Z&nbsp; 0 - 9&nbsp; ! & * ( ) - _ + : ; , . ? / ' "+
                "&nbsp;as well as spaces, and you can press Enter for new lines";
      if (minlen != -1)
         msg += "<br>You need to enter more than "+minlen+" characters";
      if (maxwords != -1)
         msg += "<br>You may not enter more than "+maxwords+" words";
      else if (maxlines != -1)
         msg += "<br>You may not enter more than "+maxlines+" line(s)";
      addErrMsg(fieldID, msg);
      return false;
   }
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// validate an email address input
function validEmail(fieldID, require)
{
   if (typeof require !== 'boolean') require = true;
   var email_pattern = /^[a-zA-Z0-9._%+-]+@(?:[a-zA-Z0-9-]+\.)+[a-zA-Z]{2,4}$/i;
   var field = $.trim($("#"+fieldID).val());
   var atpos = field.indexOf("@");
   var dot1pos = field.indexOf(".", atpos);
   var dot2pos = field.lastIndexOf(".");
   if ((field.length > 0 || require) && (!field.match(email_pattern) || atpos < 1 || dot1pos - atpos < 3 || field.length - dot2pos < 3))
   {
      $("#"+fieldID).addClass("has-error");
      addErrMsg(fieldID, "Please enter a valid email address");
      return false;
   }
   else
   {
      $("#"+fieldID).removeClass("has-error");
      addErrMsg(fieldID);
      HasChanged = true;
      return true;
   }
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// validate a url input
// regex from  https://gist.github.com/dperini/729294, Author: Diego Perini
function validUrl(fieldID, require)
{
   if (typeof require !== 'boolean') require = true;
   var field = $.trim($("#"+fieldID).val());
   if (field.length > 3)
   {
      var url3 = field.substr(0,3);
      if (url3 == "www" || url3 != "htt")
         field = "http://"+field;
   }
   // first decode to get rid of existing codes, then encode - otherwise it encodes the encoding!
   field = decodeURI(field);
   field = encodeURI(field);
   $("#"+fieldID).val(field);
   var pattern = /^(?:(?:https?:)?\/\/)?(?:(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+\.)+(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+(?::\d{2,5})?(?:\/\S*)?$/i;

   if ((!require && field.length == 0) || pattern.test(field))
   {
      $("#"+fieldID).removeClass("has-error");
      addErrMsg(fieldID);
      HasChanged = true;
      return true;
   }
   else
   {
      $("#"+fieldID).addClass("has-error");
      addErrMsg(fieldID, "Please enter a valid URL");
      return false;
   }
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// validate dates of type d monthname yyyy
function validDateDMY(fieldID, require)
{
   if (typeof require !== 'boolean') require = true;
   var isOK = true;
   var m31 = ["Jan","January","Mar","March","May","Jul","July","Aug","August","Oct","October","Dec","December"];
   var m30 = ["Apr","April","Jun","June","Sep","September","Nov","November"];
   var m29 = ["Feb","February"];
   var minYear = 1950;
   var maxYear = 2049;
   var v = $.trim($("#"+fieldID).val());

   if (!require && v.length == 0)
      isOK = true;
   else if (typeof v === "string" && v.length > 9 && v.match(/^\d{1,2} [a-zA-Z]{3,9} \d{4}$/) )
   {
      var ymd = v.split(' ');
      if (ymd[2] < minYear || ymd[2] > maxYear || ymd[0] < 1) isOK = false;
      else if (m31.indexOf(ymd[1]) > -1)
      {
         if (ymd[0] > 31) isOK = false;
      }
      else if (m30.indexOf(ymd[1]) > -1)
      {
         if (ymd[0] > 30) isOK = false;
      }
      else if (m29.indexOf(ymd[1]) > -1)
      {
         // leap years: not all centuries! only those divisable by 400; all non-centuries divisable by 4
         if (ymd[2] % 400 == 0 || ymd[2] % 4 == 0 && ymd[2] % 100 != 0)
         {
            if (ymd[0] > 29) isOK = false;
         }
         else if (ymd[0] > 28) isOK = false;
      }
      else isOK = false;
   }
   else isOK = false;

   if (isOK)
   {
      $("#"+fieldID).removeClass("has-error");
      addErrMsg(fieldID);
   }
   else
   {
      $("#"+fieldID).addClass("has-error");
      if ($("#"+fieldID).hasClass("hasDatepicker"))
         addErrMsg(fieldID, "Please select a date");
      else
         addErrMsg(fieldID, "This date must have the format: day month year<br>where the month is the full month name or the first 3 letters of it, and the year must be the full 4 digits");
   }
   return isOK;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// validate dates of type yyyy-mm-dd
function validDateYMD(fieldID, require, altFieldID)
{
   if (typeof require === 'string' && require == "valu")
   {
      require = true;
      var v = $.trim(fieldID);
      fieldID = "";
   }
   else
   {
      var v = $.trim($("#"+fieldID).val());
      if (typeof require !== 'boolean') require = true;
   }
   var isOK = true;
   var minYear = 1950;
   var maxYear = 2049;
   var m31 = [1,3,5,7,8,10,12];
   var m30 = [4,6,9,11];

   if (!require && v.length == 0)
      isOK = true;
   else if (typeof v === "string" && v.length == 10 && v.match(/^\d{4}-\d{2}-\d{2}$/) )
   {
      var ymd = v.split('-');
      ymd[0] = parseInt(ymd[0], 10);
      ymd[1] = parseInt(ymd[1], 10);
      ymd[2] = parseInt(ymd[2], 10);
      if(ymd[0] < minYear || ymd[0] > maxYear || ymd[1] < 1 || ymd[1] > 12 || ymd[2] < 1)
         isOK = false;
      else if (m31.indexOf(ymd[1]) > -1)
      {
         if (ymd[2] > 31) isOK = false;
      }
      else if (m30.indexOf(ymd[1]) > -1)
      {
         if (ymd[2] > 30) isOK = false;
      }
      else if (ymd[1] == 2)
      {
         // leap years: not all centuries! only those divisable by 400; all non-centuries divisable by 4
         if (ymd[0] % 400 == 0 || (ymd[0] % 4 == 0 && ymd[0] % 100 != 0))
         {
            if (ymd[2] > 29) isOK = false;
         }
         else if (ymd[2] > 28) isOK = false;
      }
   }
   else isOK = false;

   if (fieldID.length > 0)
   {
      // validate the hidden field but put the error on the visible (probably datepicker) field
      var errID = fieldID;
      if (typeof altFieldID === 'string' && altFieldID.length > 2)
         errID = altFieldID;

      if (isOK)
      {
         $("#"+errID).removeClass("has-error");
         addErrMsg(errID);
      }
      else
      {
         $("#"+errID).addClass("has-error");
         if ($("#"+errID).hasClass("hasDatepicker"))
            addErrMsg(errID, "Please select a date");
         else
            addErrMsg(errID, "This date must have the format: yyyy-mm-dd<br>where yyyy is a 4-digit year number, mm is a 2-digit month number, dd is a 2-digit day number, and the hyphens must be there too");
      }
   }
   return isOK;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// *** Use - MUST BE: onkeydown="kdNumbersOnly(event)"
// prevention - limit chars that can be used
// allow backspace 8, tab 9, space 32, end 35, home 36, left arrow 37, ( 40, ) 41, + 43, - 45, . 46, del 46,
//   and numbers 0 - 9
function kdNumbersOnly(e,telORspec)
{
   var tel = false;     // telephone number ?
   var spec = "";       // decimal and/or negative number ?
   if (typeof telORspec === 'boolean' && telORspec)
   {
      tel = true;       // a telephone number
      spec = "+() ";    // telephone number chars
   }
   else if (typeof telORspec === 'string')
   {
      if (telORspec.indexOf('.') !== -1)
         spec += ".";   // . = decimal
      if (telORspec.indexOf('-') !== -1)
         spec += "-";   // - = negative number
   }

   var myKey = e.key ? e.key : "Unidentified";
   if (myKey != "Unidentified")
   {
      // https://developer.mozilla.org/en-US/docs/Web/API/KeyboardEvent/key/Key_Values ... 4 duplicates are IE specific
      var keyAOK = ["Home","End","ArrowLeft","ArrowRight","Tab","Copy","Cut","Paste","Undo","Redo","Clear","Spacebar","Left","Right","Backspace","Delete","Del"];
      var aok = $.inArray(myKey, keyAOK);
      var keySOK = "0123456789" + spec;
      var sok = keySOK.indexOf(myKey);
      if (aok < 0 && sok < 0)
         e.preventDefault();
   }
   else
   {
      // Safari
      // https://www.w3schools.com/charsets/ref_utf_basic_latin.asp
      var unicode = e.charCode ? e.charCode : e.keyCode;
      if (tel)                   // tel number
         var charOK = [8,9,32,35,36,37,40,41,43,48,49,50,51,52,53,54,55,56,57];
      else if (spec.strlen > 0)  // neg and decimal
         var charOK = [8,9,35,36,37,45,46,48,49,50,51,52,53,54,55,56,57];
      else                       // integer
         var charOK = [8,9,35,36,37,48,49,50,51,52,53,54,55,56,57];
      var aok = $.inArray(unicode, charOK);
      if (aok < 0)
         e.preventDefault();
   }
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// work-around: otherwise rounding -2.5 gives -2 not -3 as expected!!
function roundNumber(num, dec)
{
   if (typeof dec == "undefined")
      dec = 0;
   var x = Math.pow(10, dec);
   var y = Math.pow(10, dec + 1);
   var neg = false;
   if (num < 0)
   {
      neg = true;
      num *= -1;
   }
   var result = Math.round(Math.round(num * y) / 10) / x;
   if (neg) result *= -1;
   return result;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// replacement for parseInt - returns the integer portion
function toInteger(x)
{
   x = x.toString().trim().replace(/[^0-9](.*)$/, "");
   if (x.length == 0) x = "0";
   else if (x.length > 15) x = x.substr(0,15);  // php has a limit of 15 digits
   return x;
}
// validate an INTEGER number input
// you can specify any one or none of minval, maxval, or exactlen
// you can also specify minval & maxval (exactlen will not be checked) or minval & exactlen
function validIntNum(fieldID, minval, maxval, exactlen, require)
{
   if (typeof minval !== 'number') minval = -1;
   if (typeof maxval !== 'number') maxval = -1;
   if (typeof exactlen !== 'number') exactlen = -1;
   if (typeof require !== 'boolean') require = false;
   var allOK = false;
   var field = $("#"+fieldID).val();
   if ($.isNumeric(field))
      field = toInteger(field);
   else
      field = 0;

   if (minval > -1 && maxval > -1)
   {
      // min and max values
      if (field < minval)
         field = minval;
      else if (field > maxval)
         field = maxval;
      allOK = true;
   }
   else if (minval > -1 && exactlen > -1)
   {
      // min value and exactlen
      if (field < minval)
         field = minval;
      if (field.length == exactlen)
         allOK = true;
   }
   else if (minval > -1)
   {
      // min value, no max
      if (field < minval)
         field = minval;
      allOK = true;
   }
   else if (maxval > -1)
   {
      // max value, no min
      if (field > maxval)
         field = maxval;
      allOK = true;
   }
   else if (exactlen > -1)
   {
      // exact length
      if (field.length == exactlen)
         allOK = true;
   }
   else if (require)
   {
      if ($("#"+fieldID).val().trim().length > 0)
         allOK = true;
   }
   else
      allOK = true;
   $("#"+fieldID).val(field);

   if (allOK)
   {
      $("#"+fieldID).removeClass("has-error");
      addErrMsg(fieldID);
      HasChanged = true;
      return true;
   }
   else
   {
      $("#"+fieldID).addClass("has-error");
      var msg = "This must be an integer";
      if (minval > -1 && maxval > -1) msg += " between "+minval+" and "+maxval;
      else if (minval > -1) msg += " with a minimum value of "+minval;
      else if (maxval > -1) msg += " with a maximum value of "+maxval;
      if (exactlen > -1) msg += ", and it must be exactly "+exactlen+" digit(s) in length";
      addErrMsg(fieldID, msg);
      return false;
   }
}
// legacy function name ...
function validNum(fieldID, minval, maxval, exactlen)
{
   return validIntNum(fieldID, minval, maxval, exactlen);
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// validate decimal number
function validDecNum(fieldID, dp, allowZero, maxval)
{
   if (typeof dp !== 'number') dp = -1;
   if (typeof allowZero !== 'boolean') allowZero = false;
   if (typeof maxval !== 'number') maxval = -1;

   var field = $("#"+fieldID).val();
   if (!$.isNumeric(field))
      field = 0;
   if (dp > -1)
      field = roundNumber(field, dp);     // force fixed number of dp - also puts in leading 0
   else
      field = Number(field);
   if (maxval > -1 && field > maxval)
      field = maxval;
   $("#"+fieldID).val(field);

   if (!allowZero && field == 0)
   {
      $("#"+fieldID).addClass("has-error");
      var msg = "This must be a decimal number";
      if (dp > -1)
      {
         msg += " with "+dp+" decimal place";
         if (dp > 1)
            msg += "s";
      }
      if (allowZero)
         msg += ", or it can be zero";
      else
         msg += "; zero is not allowed";
      addErrMsg(fieldID, msg);
      return false;
   }
   else
   {
      $("#"+fieldID).removeClass("has-error");
      addErrMsg(fieldID);
      HasChanged = true;
      return true;
   }
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// validate a telephone number input
// eg. 033 330 7333, (033) 330 7333, +27 (0)33 330 7333, ...
function validTelno(fieldID, require)
{
   if (typeof require !== 'boolean') require = true;
   var tel_pattern = /^[0-9() ]+$/;
   var field = $.trim($("#"+fieldID).val());
   var checkit = false;
   if (require || field.length > 0)
      checkit = true;
   var prefix = field.substr(0,1);
   if (prefix === "(")
   {
      prefix = field.substr(0,2);
      field = field.substr(2);
   }
   else
      field = field.substr(1);
   if (checkit && (!field.match(tel_pattern) || (prefix != "+" && prefix != "0" && prefix != "(0") || field.length<9))
   {
      $("#"+fieldID).addClass("has-error");
      var msg = "This telephone number can include spaces and ( ), and it must start with +, 0, or '(0'";
      addErrMsg(fieldID, msg);
      return false;
   }
   else
   {
      $("#"+fieldID).removeClass("has-error");
      addErrMsg(fieldID);
      HasChanged = true;
      return true;
   }
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// validate an SA ID number input
function validIDnum(fieldID, allowEmpty)
{
   if (typeof allowEmpty !== 'boolean') allowEmpty = false;
   var allOK = true;
   var field = $.trim($("#"+fieldID).val());
   var pattern = /^\d+$/;
   if (field == null || field.length == 0)
   {
      if (!allowEmpty)
         allOK = false;
   }
   else if (field.length != 13 || !field.match(pattern))
      allOK = false;
   if (allOK && field.length > 0)
   {
      var yy = parseInt(field.substr(0,2),10);
      // 1945 - 2044
      if (yy < 45) yy = "20"+yy;
      else yy = "19"+yy;
      allOK = validDateYMD(yy+"-"+field.substr(2,2)+"-"+field.substr(4,2), "valu");
   }

   if (!allOK)
   {
      $("#"+fieldID).addClass("has-error");
      addErrMsg(fieldID, "Please enter a valid South african ID number");
      return false;
   }
   else
   {
      $("#"+fieldID).removeClass("has-error");
      addErrMsg(fieldID);
      HasChanged = true;
      return true;
   }
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// validate a set of checkboxes - at least 1 checked
// fieldname = the name of the "set" of checkboxes
// ErrorIdORjQuery = true if "fieldname" is a jQuery identifier,
//                OR it's the id that the error message must follow
// ONE error message is attached to/displayed after the last checkbox, or to/after the element with the specified id -
// it's better to put all the checkboxes in a div so that the error message is nicely displayed at the end and doesn't disrupt any labels, nor is it hidden by labels
function validCheckbox(fieldname, ErrorIdORjQuery)
{
   var jQueryid = false;
   var ErrorMsgID = "";
   if (typeof ErrorIdORjQuery == 'boolean')
      jQueryid = ErrorIdORjQuery;
   else if (typeof ErrorIdORjQuery == 'string')
      ErrorMsgID = ErrorIdORjQuery;

   if (jQueryid)
   {
      var checkb = $(fieldname);
      var checkg = $(fieldname);
   }
   else
   {
      var checkb = $("input[name='"+fieldname+"']");
      var checkg = $("input[name='"+fieldname+"']");
//      var checkg = $("input[name='"+fieldname+"'] + label + br, input[name='"+fieldname+"'] + label, input[name='"+fieldname+"']");
   }
   if (checkb.length == 0)
      return false;
   if (ErrorMsgID.length > 0)
      var eid = "err_"+ErrorMsgID;
   else
      var eid = "err_"+checkb[ checkb.length - 1 ].id;

   if (checkb.filter(':checked').length == 0)
   {
      if (!checkg.parent().is("span.has-error"))
         checkg.wrap("<span class='has-error' id='"+eid+"' />");
      addErrMsg(eid, "Please select one or more of these options", true);
      return false;
   }
   else
   {
      if (checkg.parent().is("span.has-error"))
         checkg.unwrap();
      addErrMsg(eid);
      HasChanged = true;
      return true;
   }
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// check that one radio button within a group (rName) has been checked
// ONE error message is displayed after the last radio button, or after the element with the specified id
function validRadio(fieldname, ErrorMsgID, clearErrors)
{
   var radiob = $("input[name='"+fieldname+"']");
   if (radiob.length == 0)
      return false;
//   var radiog = $("input[name='"+fieldname+"'], input[name='"+fieldname+"'] + label, input[name='"+fieldname+"'] + label + br");
   var radiog = $("input[name='"+fieldname+"']");
   if (typeof ErrorMsgID === "string" && ErrorMsgID.length > 0)
      var eid = "err_"+ErrorMsgID;
   else
      var eid = "err_"+radiog[ radiog.length - 1 ].id;
   if (typeof clearErrors !== "boolean")
      clearErrors = false;

   if (!clearErrors && radiob.filter(':checked').length == 0)
   {
      if (!radiog.parent().is("span.has-error"))
         radiog.wrap("<span class='has-error' id='"+eid+"' />");
      addErrMsg(eid, "Please select one of these options", true);
      return false;
   }
   else
   {
      if (radiog.parent().is("span.has-error"))
         radiog.unwrap();
      addErrMsg(eid);
      HasChanged = true;
      return true;
   }
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// check that a dropdown / select value has been selected
function validSelect(fieldID, allowZero)
{
   if (typeof allowZero !== "boolean") allowZero = false;
   var dd = $("#"+fieldID).prop("selectedIndex");
   if (dd < 0 || !allowZero && dd < 1)
   {
      $("#"+fieldID).addClass("has-error");
      addErrMsg(fieldID, "Please select an option from the drop-down list");
      return false;
   }
   else
   {
      $("#"+fieldID).removeClass("has-error");
      addErrMsg(fieldID);
      HasChanged = true;
      return true;
   }
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
